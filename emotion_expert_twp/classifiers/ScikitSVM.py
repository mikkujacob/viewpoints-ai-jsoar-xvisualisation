from sklearn.svm.classes import SVC
import numpy as np
from classifiers.ScikitClassifier import ScikitClassifier


class ScikitSVM(ScikitClassifier):
    def __init__(self, relevantParameters, kernel, c, cwmode = None):
        self._c = c
        self._cwmode = cwmode
        self._kernel = kernel
        if cwmode:
            self._classifier = SVC(kernel = kernel, C = c, class_weight = cwmode)
        else:
            self._classifier = SVC(kernel = kernel, C = c)
        self._relevantParameters = relevantParameters
        
    def clone(self):
        return ScikitSVM(self._relevantParameters, self._kernel, self._c, self._cwmode)
    
    def fit(self, X, y):
        y = np.array([1 if yi else -1 for yi in y])
        self._classifier.fit(X, y)
        
    def predict(self, X):
        prediction = self._classifier.predict(X)
        return prediction == 1  
    
    def get_params(self):
        return {"relevantParameters" : self._relevantParameters, "kernel" : self._kernel, "c" : self._c, "cwmode" : self._cwmode}
        