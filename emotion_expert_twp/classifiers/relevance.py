from scipy.stats import pearsonr
from math import sqrt

def selectRelevantParameters(labeledCollection):
    if not labeledCollection:
        return []
    sample0, label0 = labeledCollection[0]
    parameters = sample0.keys()
    relevantParameters = []
    n = len(labeledCollection)
    for parameter in parameters:
        paramVals = []
        labels = []
        for sample, label in labeledCollection:
            paramVals.append(sample[parameter])
            labels.append(label)
        correlation, pvalue = pearsonr(paramVals, labels)
        if pvalue < 0.15:
            relevantParameters.append(parameter)
    return relevantParameters
