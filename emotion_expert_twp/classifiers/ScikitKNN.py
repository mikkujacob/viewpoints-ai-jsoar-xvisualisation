from classifiers.ScikitClassifier import ScikitClassifier
from sklearn.neighbors.classification import KNeighborsClassifier
import numpy as np

class ScikitKNN(ScikitClassifier):
    def __init__(self, relevantParameters, p, k):
        self._relevantParameters = relevantParameters
        self._k = k
        self._p = p
        self._classifier = KNeighborsClassifier(n_neighbors = k, metric = 'mahalanobis', algorithm = 'brute', p = p)
        
    def clone(self):
        return ScikitKNN(self._relevantParameters, self._p, self._k)
    
    def fit(self, X, y):
        self._classifier.fit(X, y)
        
    def predict(self, X):
        return self._classifier.predict(X)  
    
    def get_params(self):
        return {"relevantParameters" : self._relevantParameters, "p" : self._p, "k" : self._k}
