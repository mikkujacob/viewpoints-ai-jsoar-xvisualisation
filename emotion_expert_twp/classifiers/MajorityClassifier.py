from classifiers.ScikitClassifier import ScikitClassifier
import numpy as np

class MajorityClassifier(ScikitClassifier):
    def __init__(self, relevantParameters, classifiers):
        self._relevantParameters = relevantParameters
        self._classifiers = classifiers
        
    def clone(self):
        clones = [classifier.clone() for classifier in self._classifiers]
        return MajorityClassifier(self._relevantParameters, clones)
    
    def fit(self, X, y):
        for classifier in self._classifiers:
            classifier.fit(X, y)
            
    def predict(self, X):
        opinionMatrix = np.zeros((len(self._classifiers), len(X)), dtype = np.bool)
        for i, classifier in enumerate(self._classifiers):
            opinionMatrix[i] = classifier.predict(X)
        y = np.zeros(len(X), dtype = np.bool)
        votes = np.sum(opinionMatrix, axis = 0)
        y[votes > len(self._classifiers)/2] = True
        y[votes < len(self._classifiers)/2] = False
        y[votes == len(self._classifiers)/2] = opinionMatrix[0][votes == len(self._classifiers)/2]
        return y
    