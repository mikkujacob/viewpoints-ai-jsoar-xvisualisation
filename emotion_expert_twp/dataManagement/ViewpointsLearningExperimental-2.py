from dataManagement.getTrainingData import getTrainingData
from classifiers.relevance import selectRelevantParameters
from classifiers.scorer import precisionRecallFMeasureOf, errorRateOf
from classifiers.ScikitLogisticRegression import ScikitLogisticRegressionClassifier
from classifiers.ScikitMixtureOfGaussiansClassifier import ScikitMixtureOfGaussiansClassifier
from classifiers.ScikitSVM import ScikitSVM
from classifiers.ScikitKNN import ScikitKNN
from classifiers.ScikitAdaBoost import ScikitAdaBoost, ScikitAdaSVMBoost
import numpy as np
from classifiers.MajorityClassifier import MajorityClassifier
from classifiers.ExpertiseClassifier import ExpertiseClassifier
from classifiers.ScikitRandomForest import ScikitRandomForest
from classifiers.BaselineClassifier import BaselineClassifier
from classifiers.ScikitDecisionTree import ScikitDecisionTree

trainingCollection = getTrainingData(within = "data", category = "angry")
#testCollection = getTrainingData(within = "data", category = "angry")
relevantParameters = selectRelevantParameters(trainingCollection)
#for relevantParameter in relevantParameters:
#    print(relevantParameter)
#               ScikitKNN(relevantParameters, k = 1, p = 2),
#               ScikitKNN(relevantParameters, k = 1, p = np.inf),
#               ScikitKNN(relevantParameters, k = 2, p = 1),
#               ScikitKNN(relevantParameters, k = 2, p = 2),
#               ScikitKNN(relevantParameters, k = 2, p = np.inf),
#               ScikitKNN(relevantParameters, k = 3, p = 1),
#               ScikitKNN(relevantParameters, k = 3, p = 2),
#               ScikitKNN(relevantParameters, k = 3, p = np.inf)]
#               #ScikitRandomForest(relevantParameters, 500),
#               #ScikitAdaBoost(relevantParameters, n = 14),
#               #ScikitSVM(relevantParameters, kernel = 'rbf', c = 12)]
#classifier = ExpertiseClassifier(relevantParameters, classifiers)
#classifier.weightOnLibrary(trainingCollection)
#classifier = ScikitKNN(relevantParameters, k = 1, p = 1)
#classifier = ScikitAdaBoost(relevantParameters, n = 18)
#print(classifier.autotuneIntOnLibrary(trainingCollection, tuneRange = [("n", (1, 20))]))
classifier = ScikitRandomForest(relevantParameters, k = 50)
#classifier = ScikitDecisionTree(relevantParameters, split_k = 12, leaf_k = 2, depth = 11)
#classifier = ScikitSVM(relevantParameters, kernel = 'linear', c = 40)
#classifier = ScikitAdaSVMBoost(relevantParameters, n = 1, c = 40)
#classifier = ScikitSVM(relevantParameters, kernel = 'rbf', c = 1000)
#print(classifier.cvOnLibrary(trainingCollection))
#print(precisionRecallFMeasureOf(classifier, testCollection))
print(classifier.autotuneIntOnLibrary(trainingCollection, [("k", (50, 50))], loud = True, nTries = 10))
#print(precisionRecallFMeasureOf(classifier, on = trainingCollection))
#print(classifier.cvOnLibrary(trainingCollection))
#classifier = ScikitLogisticRegressionClassifier(relevantParameters, c = 1.0)
#classifier = ScikitSVM(relevantParameters, kernel = 'linear', c = 12)
#classifier = ScikitAdaBoost(relevantParameters, n = 18)
#print(classifier.autotuneIntOnLibrary(trainingCollection, [("k", (1, 10))], loud = True))
#print(classifier.autotuneFloatOnLibrary(trainingCollection, [("c", (0.1, 50))], 0.001, loud = True))
#classifiers = [ScikitAdaBoost(relevantParameters, n = 18),
#               ScikitSVM(relevantParameters, kernel = 'rbf', c = 12),
#               ScikitSVM(relevantParameters, kernel = 'linear', c = 40)]
#for classifier in classifiers:
#    print(classifier.cvOnLibrary(trainingCollection))
#knn = ScikitKNN(relevantParameters, p = np.inf, k = 1)
#print(knn.cvOnLibrary(trainingCollection))
#expertiseClassifier = ExpertiseClassifier(relevantParameters, classifiers)
#expertiseClassifier.weightOnLibrary(trainingCollection)
#print(expertiseClassifier.cvOnLibrary(trainingCollection))


#for k in [1, 2, 3, 4, 5, 6, 7]:
#    print(k)
#    knn = ScikitKNN(relevantParameters, k)
#    print(knn.cvOnLibrary(trainingCollection))
#results = []
#for wi in xrange(1, 10):
#    w = 0.1 * wi
#    print(w)
#    for n in xrange(1, 20):
#        print(n)
#        adaboost = ScikitAdaBoost(relevantParameters, n, 10000, w)
#        result = adaboost.cvOnLibrary(trainingCollection)
#        print(result)
#        results.append((result['fMeasure'], c, n))
#    print()
#fm, c, n = max(results, key = lambda (fm, c, n): fm)
#print("Best c: %s. Best n: %s. Fmeasure: %s" % (c, n, fm))
#for d in [2, 3, 4]:
#    print(d)
#    for c in [1, 10, 100, 1000]:
#        print(c)
#        svm = ScikitSVM(relevantParameters, c, d)
#        print(svm.cvOnLibrary(trainingCollection))
#    print(" ")
#for c in xrange(1, 100):
#    print(c)
#    svm = ScikitSVM(relevantParameters, 'rbf', c)
#    print(svm.cvOnLibrary(trainingCollection))
#svm.trainOnLibrary(trainingCollection)
#print(precisionRecallFMeasureOf(svm, on = trainingCollection))

#for k in xrange(1, 5):
#    gaussmix = ScikitMixtureOfGaussiansClassifier(relevantParameters, k)
#    print(gaussmix.cvOnLibrar(trainingCollection))
#gaussmix.trainOnLibrary(trainingCollection)
#print(gaussmix.scoreOnLibrary(trainingCollection))
#print(precisionRecallFMeasureOf(gaussmix, on = trainingCollection))
#gaussmixscores = gaussmix.cvOnLibrary(trainingCollection)
#print("Accuracy: %0.2f (+/- %0.2f)" % (gaussmixscores.mean(), gaussmixscores.std() * 2))

#relevantParameters = selectRelevantParameters(trainingCollection)
#for relevantParameter in relevantParameters:
#    print(relevantParameter)
#logit = ScikitLogisticRegressionClassifier(relevantParameters)
#print(logit.cvOnLibrary(trainingCollection))
#logit.trainOnLibrary(trainingCollection)
#print(1.0 - errorRateOf(logit, on = trainingCollection))
#logitcvscores = logit.cvOnLibrary(trainingCollection)
#print("Accuracy: %0.2f (+/- %0.2f)" % (logitcvscores.mean(), logitcvscores.std() * 2))
#logit.trainOnLibrary(trainingCollection)
#print(precisionRecallFMeasureOf(logit, on = trainingCollection))
