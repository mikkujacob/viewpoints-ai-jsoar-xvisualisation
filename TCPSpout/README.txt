===========
 TCPSpout
===========

TCPSpout is a utility which can share images between different hosts by using Spout2 running on Microsoft Windows with TCP/IP. As TCPSpout is compatible with the communication protocol TCPSyphon running on OSX, it allows output image from VJ software on both of OSX and Microsoft Windows to exchange alternatively.

[Download]
http://techlife.sg/TCPSpout/

[Special Thanks]
http://spout.zeal.co/

YOU MUST;
	Install Bonjour. 
		https://developer.apple.com/softwarelicensing/agreements/bonjour.php#bonjourwin
	or
		http://www.softpedia.com/get/Office-tools/Other-Office-Tools/Bonjour.shtml

Version 1.30
	updated by Spout version 2.004
	linked by Spout2.dll

Version 1.20
	support alpha information on PNG encode.

Version 1.10
	support Turbojpeg.

Version 1.01
	updated by Spout version 2.001
	improve receive function on TCPSpoutClient.

Version 1.00
	Quick-build release


Why we use this.

Generally for streaming images RTMP[Real Time Messaging Protocol] etc. is usually used. However, to meet requests such as latency minimization, stable quality of image and so on, you cannot say these are perfect methods in view of image direction.

At this moment, we are adapting a simple solution; Codec based on Motion JPEG, Animation PNG etc. though in the future, we assume that more suitable Codec would be released or we can make it by ourselves. Sometimes DXTC (called DXTn or S3 Texture Compression) is suggested to our products, but compression rate itself is definitely inferior to JPEG etc. Absolutely we can utilize its advantage of transaction only on GPU, but it is too large to communicate its data on the network. Recently since new technologies (encoding H.264 on GPU, or making JPEG encoder by CUDA) are coming up, we have a plan to make version up applying optimal technology to our products.


Simplified BSD license,

     Copyright 2014-2015 z37soft (Nozomu Miura). All rights reserved.
     
     Redistribution and use in binary forms, with or without
     modification, are permitted provided that the following conditions are met:
          
     * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
     
     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
     ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
     WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY
     DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
     ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
     (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.