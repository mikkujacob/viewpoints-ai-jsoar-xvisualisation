from classifiers.ScikitClassifier import ScikitClassifier
from sklearn.tree import DecisionTreeClassifier

class ScikitDecisionTree(ScikitClassifier):
    def __init__(self, relevantParameters, split_k, leaf_k, depth, nclasses = 2):
        ScikitClassifier.__init__(self, nclasses)
        self._relevantParameters = relevantParameters
        self._split_k = split_k
        self._leaf_k = leaf_k
        self._depth = depth
        self._classifier = DecisionTreeClassifier(criterion = 'entropy', min_samples_split = split_k, min_samples_leaf = leaf_k, max_depth = depth)
        
    def clone(self):
        return ScikitDecisionTree(self._relevantParameters, self._split_k, self._leaf_k, self._depth, self.nlabels())
    
    def fit(self, X, y):
        self._classifier.fit(X, y)
        
    def predict(self, X):
        return self._classifier.predict(X)  
    
    def get_params(self, deep = False):
        return {"relevantParameters" : self._relevantParameters, "split_k" : self._split_k, "leaf_k" : self._leaf_k, "depth" : self._depth, "nclasses" : self.nlabels()}
