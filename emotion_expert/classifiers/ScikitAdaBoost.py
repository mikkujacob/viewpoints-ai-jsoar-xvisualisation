from classifiers.ScikitClassifier import ScikitClassifier
from sklearn.ensemble.weight_boosting import AdaBoostClassifier
from sklearn.svm.classes import SVC
import numpy as np

class ScikitAdaBoost(ScikitClassifier):
    def __init__(self, relevantParameters, n, nclasses = 2):
        ScikitClassifier.__init__(self, nclasses)
        self._relevantParameters = relevantParameters
        self._n = n
        self._classifier = AdaBoostClassifier(n_estimators = n)
        
    def clone(self):
        return ScikitAdaBoost(self._relevantParameters, self._n, self.nlabels())
    
    def fit(self, X, y):
        self._classifier.fit(X, y)
        
    def predict(self, X):
        return self._classifier.predict(X)
    
    def get_params(self):
        return {"relevantParameters" : self._relevantParameters, "n" : self._n, "nclasses" : self.nlabels()}

class ScikitAdaSVMBoost(ScikitClassifier):
    def __init__(self, relevantParameters, n, c):
        self._relevantParameters = relevantParameters
        self._n = n
        self._c = c
        self._classifier = AdaBoostClassifier(base_estimator = SVC(kernel = 'linear', C = c), n_estimators = n, algorithm = 'SAMME')
        
    def clone(self):
        return ScikitAdaSVMBoost(self._relevantParameters, self._n, self._c)
    
    def fit(self, X, y):
        self._classifier.fit(X, y)
        
    def predict(self, X):
        return self._classifier.predict(X)
    
    def get_params(self):
        return {"relevantParameters" : self._relevantParameters, "n" : self._n, "c" : self._c}
        
        