from cPickle import load
from dataManagement.getTrainingData import getGestureParametersFromLines
import os
import sys

classifiers = dict()
rootdir = os.path.dirname(os.path.realpath(__file__))
for emotion in ["angry", "fear", "sad", "joy"]:
    with open("%s/pickle/%s.pickle" % (rootdir, emotion)) as piclkefile:
        classifiers[emotion] = load(piclkefile)
while True:
    parametersLines = []
    while True:
        line = raw_input("")
        if "" == line:
            break
        else:
            parametersLines.append(line)
    if not parametersLines:
        continue
    parameters = getGestureParametersFromLines(parametersLines)
    emotions = []
    for emotion, classifier in classifiers.items():
        if "pos" == classifier.classify(parameters):
            emotions.append(emotion)
    if emotions:
        print(" ".join(emotions))
    else:
        print("none")
    sys.stdout.flush()
    