from os import listdir
from dataManagement.getTrainingData import getTrainingData
from support.relevance import selectRelevantParameters
from classifiers.ScikitRandomForest import ScikitRandomForest
from classifiers.ScikitDecisionTree import ScikitDecisionTree
from pickle import dump

def createClassifierSeed(emotion, relevantParameters):
    if "angry"  == emotion:
        return ScikitDecisionTree(relevantParameters, split_k = 13, leaf_k = 5, depth = None)
    elif "fear" == emotion:
        return ScikitDecisionTree(relevantParameters, split_k = 13, leaf_k = 2, depth = None)
    elif "sad"  == emotion:
        return ScikitRandomForest(relevantParameters, k = 500)
    elif "joy"  == emotion:
        return ScikitRandomForest(relevantParameters, k = 500)
    else:
        return None

for emotion in ["angry", "fear", "sad", "joy"]:
    print("learning %s" % emotion)
    trainingCollection = getTrainingData(within = "data", category = emotion)
    relevantParameters = selectRelevantParameters(trainingCollection)
    classifier = createClassifierSeed(emotion, relevantParameters)
    classifier.trainOnLibrary(trainingCollection)
    with open("pickle/%s.pickle" % emotion, "w+") as picklefile:
        dump(classifier, picklefile)
