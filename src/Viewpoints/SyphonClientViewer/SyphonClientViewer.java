package Viewpoints.SyphonClientViewer;

import Viewpoints.FrontEnd.Communication.Video.Syphon.SyphonClient;
import processing.core.PApplet;

public class SyphonClientViewer extends PApplet
{
	/**
	 * Generated Serial Number
	 */
	private static final long serialVersionUID = -6775578239550487675L;
	SyphonClient syphonClient;
	
	public void setup()
	{
		size(displayWidth, displayHeight, P3D);
		
//		syphonClient = new SyphonClient(this, "VAIFrontEnd");
//		syphonClient = new SyphonClient(this, "TCPSyphonClient");
		syphonClient = new SyphonClient(this, "StencilStarSketch");
		
		println("Available Syphon servers:");
		println(SyphonClient.listServers());
		
		background(0);
	}
	
	public void draw()
	{
		if(syphonClient.available())
		{
			background(0);
			g = syphonClient.getGraphics(g);
		    image(g, 0, 0, displayWidth, displayHeight);
		}
	}
	
	@Override
	public boolean sketchFullScreen() {
		  return true;
	}
	
	public static void main(String[] args)
	{
		PApplet.main("Viewpoints.SyphonClientViewer.SyphonClientViewer", new String[]{"--full-screen", "--display=1"});
	}

}
