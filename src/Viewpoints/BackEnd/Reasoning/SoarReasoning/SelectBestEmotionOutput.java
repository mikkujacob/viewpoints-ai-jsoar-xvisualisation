package Viewpoints.BackEnd.Reasoning.SoarReasoning;

import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.LinkedList;

import org.jsoar.kernel.memory.Wme;
import org.jsoar.kernel.rhs.functions.AbstractRhsFunctionHandler;
import org.jsoar.kernel.rhs.functions.RhsFunctionContext;
import org.jsoar.kernel.rhs.functions.RhsFunctionException;
import org.jsoar.kernel.rhs.functions.RhsFunctions;
import org.jsoar.kernel.symbols.Identifier;
import org.jsoar.kernel.symbols.StringSymbol;
import org.jsoar.kernel.symbols.Symbol;

public class SelectBestEmotionOutput extends AbstractRhsFunctionHandler {

	public SelectBestEmotionOutput()
	{
		super("select-best-emotion", 1, 3);
	}
	
	@Override
	public Symbol execute(RhsFunctionContext context, List<Symbol> arguments)
			throws RhsFunctionException {
		
		//System.out.println("SelectBestEmotion got called...!!");
		RhsFunctions.checkArgumentCount(this, arguments);
        
        Identifier argId1 = arguments.get(0).asIdentifier();
        Identifier argId2 = null;
        StringSymbol argId3 = null;
        
        if(argId1 == null)
        {
            throw new RhsFunctionException("First argument to '" + getName() + "' RHS function must be an identifier.");
        }
        if(arguments.size() > 1)
        {
	        argId2 = arguments.get(1).asIdentifier();
	        if(argId2 == null)
	        {
	            throw new RhsFunctionException("Second argument to '" + getName() + "' RHS function must be an identifier.");
	        }
	        argId3 = arguments.get(2).asString();
	        if(argId3 == null)
	        {
	            throw new RhsFunctionException("Second argu	ment to '" + getName() + "' RHS function must be an String.");
	        }
        }
		
		return selectBestEmotion(context, argId1, argId2, argId3.getValue());
	}
	
	private Identifier selectBestEmotion(RhsFunctionContext context, Identifier id, Identifier id2, String id3)
	{		 
		List<Identifier> outputCandidates = new LinkedList<Identifier>();
		Identifier result = id;
		Random random = new Random();
		
		//Go through the history.
		Iterator<Wme> historyIterator = id2.getWmes();
		while(historyIterator.hasNext())
		{
			//Going through all the history attributes..
			Wme historyChildWme = historyIterator.next();
			//System.out.println(historyChildWme.toString());
			
			if (historyChildWme.getAttribute().toString().equalsIgnoreCase("node")) {
				//System.out.println("Found node..!!");
				Iterator<Wme> nodeChildren = historyChildWme.getChildren();
				while (nodeChildren.hasNext()) {
					//Going through all of the node to get to input. 
					Wme nodeChild = nodeChildren.next();
					if (nodeChild.getAttribute().toString().equalsIgnoreCase("input")) {
						Iterator<Wme> inputChildren = nodeChild.getChildren();
						//Going through input to find gesture. 
						//System.out.println("Going through the inputs to find gesture.");
						while (inputChildren.hasNext()) {
							Wme inputChild = inputChildren.next();
							if (inputChild.getAttribute().toString().equalsIgnoreCase("gesture")) {
								Iterator<Wme> gestureChildren = inputChild.getChildren();
								//Going through gesture to check the emotion. 
								while (gestureChildren.hasNext()) {
									Wme gestureChild = gestureChildren.next();
									if (gestureChild.getAttribute().toString().equalsIgnoreCase("emotion")) {
										//System.out.println("Checking for the emotion value.");
										//System.out.println("EMOTION VALUE :" + gestureChild.getValue().toString());
										if (gestureChild.getValue().toString().equals(id3)) {
											outputCandidates.add(inputChild.getValue().asIdentifier());
											break;
										}
									}
								}
								break;
							}
						}
						break;
					}
				}
			}
		}

		if (!outputCandidates.isEmpty()) {
			result = outputCandidates.get(random.nextInt(outputCandidates.size()));
		}
		return result;
	}
}
