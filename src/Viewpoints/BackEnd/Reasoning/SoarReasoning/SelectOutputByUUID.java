package Viewpoints.BackEnd.Reasoning.SoarReasoning;

import java.util.Iterator;
import java.util.List;

import org.jsoar.kernel.memory.Wme;
import org.jsoar.kernel.rhs.functions.AbstractRhsFunctionHandler;
import org.jsoar.kernel.rhs.functions.RhsFunctionContext;
import org.jsoar.kernel.rhs.functions.RhsFunctionException;
import org.jsoar.kernel.rhs.functions.RhsFunctions;
import org.jsoar.kernel.symbols.Identifier;
import org.jsoar.kernel.symbols.StringSymbol;
import org.jsoar.kernel.symbols.Symbol;

public class SelectOutputByUUID extends AbstractRhsFunctionHandler {

	public SelectOutputByUUID()
	{
		super("select-output-by-uuid", 1, 3);
	}
	
	@Override
	public Symbol execute(RhsFunctionContext context, List<Symbol> arguments)
			throws RhsFunctionException {
		
		RhsFunctions.checkArgumentCount(this, arguments);
        
        Identifier argId1 = arguments.get(0).asIdentifier();
        Identifier argId2 = null;
        StringSymbol argId3 = null;
        
        if(argId1 == null)
        {
            throw new RhsFunctionException("First argument to '" + getName() + "' RHS function must be an identifier for the current input gesture.");
        }
        if(arguments.size() > 1)
        {
	        argId2 = arguments.get(1).asIdentifier();
	        if(argId2 == null)
	        {
	            throw new RhsFunctionException("Second argument to '" + getName() + "' RHS function must be an identifier for the entire history.");
	        }
	        argId3 = arguments.get(2).asString();
	        if(argId3 == null)
	        {
	            throw new RhsFunctionException("Second argument to '" + getName() + "' RHS function must be a String with the desired UUID.");
	        }
        }
		
		return selectBestEmotion(context, argId1, argId2, argId3.getValue());
	}
	
	private Identifier selectBestEmotion(RhsFunctionContext context, Identifier id, Identifier id2, String id3)
	{		 
		Identifier result = id;
		
		//Go through the history.
		Iterator<Wme> historyIterator = id2.getWmes();
		while(historyIterator.hasNext())
		{
			//Going through all the history attributes..
			Wme historyChildWme = historyIterator.next();
			
			if (historyChildWme.getAttribute().toString().equalsIgnoreCase("node")) {
				Iterator<Wme> nodeChildren = historyChildWme.getChildren();
				while (nodeChildren.hasNext()) { 
					Wme nodeChild = nodeChildren.next();
					if (nodeChild.getAttribute().toString().equalsIgnoreCase("input")) {
						Iterator<Wme> inputChildren = nodeChild.getChildren();
						while (inputChildren.hasNext()) {
							Wme inputChild = inputChildren.next();
							if (inputChild.getAttribute().toString().equalsIgnoreCase("gesture")) {
								Iterator<Wme> gestureChildren = inputChild.getChildren();
								while (gestureChildren.hasNext()) {
									Wme gestureChild = gestureChildren.next();
									if (gestureChild.getAttribute().toString().equalsIgnoreCase("uuid")) {
										if (gestureChild.getValue().toString().equals(id3)) {
											return inputChild.getValue().asIdentifier();
										}
									}
								}
								break;
							}
						}
						break;
					}
				}
			}
		}
		return result;
	}
}
