package Viewpoints.BackEnd.Reasoning.SoarReasoning;

import java.util.Iterator;
import java.util.List;

import org.jsoar.kernel.memory.Wme;
import org.jsoar.kernel.rhs.functions.AbstractRhsFunctionHandler;
import org.jsoar.kernel.rhs.functions.RhsFunctionContext;
import org.jsoar.kernel.rhs.functions.RhsFunctionException;
import org.jsoar.kernel.rhs.functions.RhsFunctions;
import org.jsoar.kernel.symbols.Identifier;
import org.jsoar.kernel.symbols.Symbol;

public class SelectBestOutputRhsFunction extends AbstractRhsFunctionHandler {

	public SelectBestOutputRhsFunction()
	{
		super("select-best-output", 1, 2);
	}
	
	@Override
	public Symbol execute(RhsFunctionContext context, List<Symbol> arguments)
			throws RhsFunctionException {
		
		RhsFunctions.checkArgumentCount(this, arguments);
        
        Identifier argId1 = arguments.get(0).asIdentifier();
        Identifier argId2 = null;
        
        if(argId1 == null)
        {
            throw new RhsFunctionException("First argument to '" + getName() + "' RHS function must be an identifier.");
        }
        if(arguments.size() > 1)
        {
	        argId2 = arguments.get(1).asIdentifier();
	        if(argId2 == null)
	        {
	            throw new RhsFunctionException("Second argument to '" + getName() + "' RHS function must be an identifier.");
	        }
        }
		
		return selectBestOutput(context, argId1, argId2);
	}
	
	private Identifier selectBestOutput(RhsFunctionContext context, Identifier id, Identifier id2)
	{
		if(id2 != null)
		{
			return id2;
		}
		
		Iterator<Wme> outputsIterator = id.getWmes();

		Wme tempRootWme = outputsIterator.next();
		Iterator<Wme> outputChildrenIterator = tempRootWme.getValue().asIdentifier().getWmes();
		Wme tempOutputWme = null;
		Wme storageCountWme = null;
		Wme matchCountWme = null;
		
		while(outputChildrenIterator.hasNext())
		{
			Wme tempWme = outputChildrenIterator.next();
			if(tempWme.getAttribute().toString().equalsIgnoreCase("gesture"))
			{
				tempOutputWme = tempWme;
			}
			else if(tempWme.getAttribute().toString().equalsIgnoreCase("storage-count"))
			{
				storageCountWme = tempWme;
			}
			else if(tempWme.getAttribute().toString().equalsIgnoreCase("match-count"))
			{
				matchCountWme = tempWme;
			}
		}

		Wme bestOutputWme = tempOutputWme;

		double tempScore = calculateScore(storageCountWme.getValue().asInteger().getValue(), matchCountWme.getValue().asInteger().getValue());
		double bestScore = tempScore;
		
		while(outputsIterator.hasNext())
		{
			outputChildrenIterator = outputsIterator.next().getValue().asIdentifier().getWmes();
			
			while(outputChildrenIterator.hasNext())
			{
				Wme tempWme = outputChildrenIterator.next();
				if(tempWme.getAttribute().toString().equalsIgnoreCase("gesture"))
				{
					tempOutputWme = tempWme;
				}
				else if(tempWme.getAttribute().toString().equalsIgnoreCase("storage-count"))
				{
					storageCountWme = tempWme;
				}
				else if(tempWme.getAttribute().toString().equalsIgnoreCase("match-count"))
				{
					matchCountWme = tempWme;
				}
			}

			tempScore = calculateScore(storageCountWme.getValue().asInteger().getValue(), matchCountWme.getValue().asInteger().getValue());
			
			if(bestScore < tempScore)
			{
				bestScore = tempScore;
				bestOutputWme = tempOutputWme;
			}
		}
		
		return bestOutputWme.getValue().asIdentifier();
	}
	
	private double calculateScore(long storageCount, long matchCount)
	{
		return (1.5 * storageCount - matchCount);
	}
}
