
package Viewpoints.BackEnd.GestureStorage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.JIDX;

import java.util.UUID;

/**
 * =====================
 * Gesture Library Class
 * =====================
 * 
 * The Gesture Library uses an N-ARY TREE or a TRIE data structure with multiple root nodes.
 * - The GestureLibrary class is the main wrapper/handler
 * - Each branch is a complete and unique gesture
 * - The Integer of the HashMap in GestureLibrary and GestureNode is the unique ID from the Gesture Table
 * - Duplicate gestures are not added to the tree
 * - A threshold "sphere of tolerance" dictates whether a new gesture is unique or a duplicate
 * - Example (GL = Gesture Library, GN = GestureNode):
 *       
 *       ====GL====
 *      /    |    \
 *     GN    GN   GN
 *    /  \  /    /  \
 *   GN GN GN  GN   GN
 *                    \
 *                     GN
 * 
 * - // More notes go here
 * 
 */
public class GestureLibrary {

	/**
	 * Main tree data structure, an ArrayList of class HashMaps of HashMaps...
	 * 
	 */
	private ArrayList<GestureNode> tree;
	
	
	/**
	 * Count of gestures added to library
	 * 
	 */
	private int gestureCount;
	
	
	/**
	 * Used for converting original gesture frames into stripped poses
	 * Change this to tweak threshold for difference in poses
	 * 
	 */
	private static final double threshold = 20.0;
	
	/**
	 * Used for keeping track of unique gestures
	 */
	private UUID currentUniqueID;
	
	
	/**
	 * Used to identify type of match in order to perform different operations
	 * 
	 */
	private enum MATCH_TYPE {
		NO_MATCH, PARTIAL_MATCH, COMPLETE_MATCH;
	}
	
	
	/**
	 * Constructor
	 * 
	 */
	public GestureLibrary() {
		this.tree = new ArrayList<GestureNode>();
		this.gestureCount = 0;
	}
	
	
	/**
	 * Main function for adding a gesture to the library
	 * Tree main steps in algorithm:
	 * 	- 1. Strip original gesture to poses
	 *  - 2. Check stripped gesture for match type
	 *  - 3. Perform operation based on match type
	 * @param originalGesture
	 * 
	 */
	public UUID addGesture(LinkedHashMap<Integer, Body> originalGesture) {
		
		//STEP 0: Generate UUID
		this.currentUniqueID = UUID.randomUUID();
		
		// STEP 1: Strip Original Gesture to POSES
		LinkedHashMap<Integer, Body> strippedGesture = stripGesture(originalGesture);
				
		// STEP 2: Check stripped gesture for type of match against existing library
		MATCH_TYPE matchResult = matchGesture(strippedGesture);
		
		// STEP 3: Perform operation based on result of matchGesture()
		processGesture(strippedGesture, matchResult);
		
		// Debug text
		System.out.println("> [Gesture Library]: Gesture Count = " + this.gestureCount);
		System.out.println("> [Gesture Library]: Size of Original Gesture: " + originalGesture.size());
		System.out.println("> [Gesture Library]: Size of Stripped Gesture: " + strippedGesture.size()); 
		System.out.println("> [Gesture Library]: Matching Resulted in: " + matchResult.name());
		
		return currentUniqueID;
	}
	
	
	/**
	 * Strips original gesture to poses based on delta between successive frames
	 * @param originalGesture
	 * @return strippedGesture
	 * 
	 */
	private LinkedHashMap<Integer, Body> stripGesture(LinkedHashMap<Integer, Body> originalGesture) {
		
		// New Data structure to hold poses
		LinkedHashMap<Integer, Body> strippedGesture = new LinkedHashMap<Integer, Body>();
		
		/* Sub-Algorithm:
		 *  - Add first frame
		 *  - Iterate through origianlGesture
		 *  - Compare i with i-1
		 *  - If i is significantly different, add i
		 */
				
		//Iterator<Entry<Integer, Body>> it = originalGesture.entrySet().iterator();
		//while(it.hasNext()) {
		//}
		
		Integer prevIndex = null;
		Body prevBody = null;
		
		// Always add first frame to strippedGesture
		if(originalGesture.size() > 0) {
			prevIndex = 0;
			prevBody = originalGesture.get(prevIndex);
			strippedGesture.put(prevIndex, prevBody);
		}

		// Iterate through gesture
		for(Entry<Integer, Body> entry : originalGesture.entrySet()) {
			
			// if current and previous are NOT the same, add current to strippedGesture
			if(compareJSData(entry.getValue(), prevBody) == false) {
				strippedGesture.put(entry.getKey(), entry.getValue());
			}
			
			prevIndex = entry.getKey();
			prevBody = entry.getValue();
		}
		
		// Return stripped gesture
		return strippedGesture;
	}
	
	
	/**
	 * Compares gesture against existing library to ascertain type of match
	 * @param strippedGesture
	 * @return MATCH_TYPE
	 * 
	 */
	private MATCH_TYPE matchGesture(LinkedHashMap<Integer, Body> strippedGesture) {
		
		// Graceful failure
		if(strippedGesture == null || strippedGesture.size() == 0) {
			System.out.println("*** ERROR ***: matchGesture() was passed null or empty gesture!");
			return null;
		}
		
		// Variables used in sub-algorithm below
		MATCH_TYPE matchResult = null;
		Integer partialMatchKey = null;
		Body firstFrame = strippedGesture.entrySet().iterator().next().getValue();
		UUID matchedUUID = null;
		
		// Set match type to no match if tree is empty
		if(tree.size() == 0) {
			matchResult = MATCH_TYPE.NO_MATCH;
			return matchResult;
		}
		
		Boolean isFound = false;
		// Iterate to find matching first frame
		for(int i = 0; i < tree.size(); i++) {
			// Compare the first frame with the top level node for partial match
			if(compareJSData(firstFrame, tree.get(i).getGestureData().body)) {
				matchResult = MATCH_TYPE.PARTIAL_MATCH;
				partialMatchKey = i;
//				System.out.println("> FIRST MATCHING FRAME FOUND!");
				matchedUUID = tree.get(i).getID();
				isFound = true;
				break;
			}
		}
		
		if(!isFound) {
			System.out.println("> NO MATCH POINT REACHED!");
			matchResult = MATCH_TYPE.NO_MATCH;
			return matchResult;
		}

		// --- If we reached here, that means there is at least a partial match ---
		// --- Let's check to see where the match ends ---
		
		// Graceful failure
		if(matchResult != MATCH_TYPE.PARTIAL_MATCH) {
			System.out.println("*** ERROR ***: matchGesture() tried to proceed with partial match with a null key!");
			return null;
		}
		
		GestureNode temp = tree.get(partialMatchKey);
		Iterator<Entry<Integer, Body>> it = strippedGesture.entrySet().iterator();
		Entry<Integer, Body> element = it.next(); // we've already checked first frame
//		System.out.println("Element - " + element.toString());
		// Iterate through the stripped gesture
		outerloop:
		while(it.hasNext()) {
			// Check children of current pointer temp
			isFound = false;
			for(GestureNode gestureChild : temp.getChildren().values()) {
				element = it.next();
//				System.out.println("Element - " + element.toString());
				if(compareJSData(element.getValue(), gestureChild.getGestureData().body)) {
					// if a match is found
					temp = gestureChild;
					isFound = true;
				}
			}
			
			if(!isFound) {
				break outerloop;
			}
		}
		
		// Did we search till the end of Stripped Gesture or did we break prematurely?
		if(it.hasNext() == false) {
			matchResult = MATCH_TYPE.COMPLETE_MATCH;							
			this.currentUniqueID = matchedUUID;
		} else {
			matchResult = MATCH_TYPE.PARTIAL_MATCH;
			/**
			 * Only a partial match found, so we need to build a fork
			 */
			do {
				// Generate gesture data
				GestureData data = new GestureData(temp.getGestureData().index+1, element.getValue());

				// Create gesture node
				UUID tempID = this.currentUniqueID;
				Boolean isEndOfGesture = (it.hasNext()) ? true : false; 
				GestureNode node = new GestureNode(tempID, temp, isEndOfGesture, data);

				// Add child
				temp.addChild(node);

				// Increment both temp and iterator
				temp = temp.getChildren().get(tempID);
				element = it.next();
//				System.out.println("Element - " + element.toString());
			} while(it.hasNext());
			
			System.out.println("> [Gesture Library]: Partial Match Found, added fork to existing branch.");
			this.gestureCount++;
		}
		
		return matchResult;
	}

	
	/**
	 * Perform operation based on result of matchResult()
	 * @param strippedGesture 
	 * @param matchResult
	 * 
	 */
	private void processGesture(LinkedHashMap<Integer, Body> strippedGesture, MATCH_TYPE matchResult) {
		
		switch (matchResult) {
		/**
		 *  NO MATCH: Add entirely new branch to library
		 */
		case NO_MATCH:
			
			GestureNode temp = null;
			int i = 0;
			
			for(Entry<Integer, Body> entry : strippedGesture.entrySet()) {
				
				// Generate gesture data
				GestureData data = new GestureData(entry.getKey(), entry.getValue());
				
				// Create gesture node
				UUID tempID = this.currentUniqueID;
				Boolean isEndOfGesture = (i == strippedGesture.size()-1) ? true : false; 
				GestureNode node = new GestureNode(tempID, temp, isEndOfGesture, data);
				
				if(i == 0) {
					// First node, add to top level tree (ArrayList)
					tree.add(node);
					temp = tree.get(tree.size()-1);
				} else {
					// Not first node, add child to temp
					temp.addChild(node);
					temp = temp.getChildren().get(tempID);
				}
				
				i++;
			}
			
			System.out.println("> [Gesture Library]: No match found, added new branch to library.");
			this.gestureCount++;
			break;
			
		/**
		 *  PARTIAL MATCH: Add fork to existing branch in library
		 */
		case PARTIAL_MATCH:
			// Do nothing, code for adding fork is in matchGesture() to reduce redundancy
			break;
			
		/**
		 *  COMPLETE MATCH: Add nothing to library
		 */
		case COMPLETE_MATCH:
			// Do nothing, found a complete match
			break;
			
		default:
			System.out.println("*** ERROR ***: matchGesture() did not return a valid type!");
			break;
		}
		
	}

	
	/**
	 * Compare Joint Space data for two given Body class objects 
	 * @param body1
	 * @param body2
	 * @return Boolean
	 */
	private Boolean compareJSData(Body body1, Body body2) {
		// Gav TODO: Add egocentric transformation
		
//		// New Origin for body 1
//		double originX1 = Math.abs(body1.get(JIDX.NECK).x - body1.get(JIDX.WAIST).x);
//		double originY1 = Math.abs(body1.get(JIDX.NECK).y - body1.get(JIDX.WAIST).y);
//		double originZ1 = Math.abs(body1.get(JIDX.NECK).z - body1.get(JIDX.WAIST).z);
//		
//		// New Origin for body 2
//		double originX2 = Math.abs(body2.get(JIDX.NECK).x - body2.get(JIDX.WAIST).x);
//		double originY2 = Math.abs(body2.get(JIDX.NECK).y - body2.get(JIDX.WAIST).y);
//		double originZ2 = Math.abs(body2.get(JIDX.NECK).z - body2.get(JIDX.WAIST).z);
		
		for(JIDX index : body1.keySet()) {
			double deltaX = Math.abs(body1.get(index).x - body2.get(index).x);
			double deltaY = Math.abs(body1.get(index).y - body2.get(index).y);
			double deltaZ = Math.abs(body1.get(index).z - body2.get(index).z);
			if(deltaX >= GestureLibrary.threshold || 
			   deltaY >= GestureLibrary.threshold || 
			   deltaZ >= GestureLibrary.threshold) {
				return false;
			}
		}
		
		return true;
	}

	
	/**
	 * Returns current gesture count
	 * @return int
	 * 
	 */
	public int getGestureCount() {
		return this.gestureCount;
	}
	
	
	/**
	 * OLD version of addGesture(), scrapped and no longer used
	 * @param ID
	 * @param gesture
	 * 
	 */
//	public void addGestureOLD(Integer ID, LinkedHashMap<Integer, Body> gesture) {
//
//	Boolean matchingGesture = false;
//	
//	// Iterate through tree
//	for(GestureNode gestureNode : tree) {
//		// Find matching gesture
//		if(gestureNode.getID() == ID) {
//			matchingGesture = true;
//			/*
//			 * Sub-Algorithm:
//			 * 		- Iterate through nodes
//			 * 		- Compare body date with gesture that is passed in, index by index
//			 * 		- If stuff doesn't match, start new branch till you reach end
//			 */
//			
//			int iterator = 1; 
//			GestureNode temp = tree.get(iterator);
//			// For each of the indices (frames) in the complete gesture
//			for(Integer index : gesture.keySet()) {
//				if(compareJSData(temp.getGestureData().body, gesture.get(index))/* && temp.getGestureData().index == index*/) {
//					// Gesture frame matches, go a level deeper
//					temp = temp.getChildren().get(ID);
//				}
//				else {
//					// Reached point where gesture doesn't match, fork new branch
//					// -- START BRANCH FORK CODE --
//					// Generate gesture data
//					GestureData data = new GestureData(index, gesture.get(index));
//					// Create gesture node
//					Boolean isEndOfGesture = (index == gesture.size()+1) ? true : false; 
//					GestureNode node = new GestureNode(ID, null, isEndOfGesture, data);
//					// Create temporary iterator
//					if(index == 0) {
//						// First index, add directly to tree
//						tree.add(node);
//						temp = tree.get(index).getSelf();
//					}
//					else {
//						// Not the first index, need to add to end of branch
//						temp = tree.get(index);
//						int i = 1;
//						// Loop till you reach end, using temp as target pointer
//						while(i<index && temp.getChildren().get(ID) != null) {
//							temp = temp.getChildren().get(ID);
//							i++;
//						}
//						// End found, add node 
//						temp.addChild(node);
//					}
//					// -- END BRANCH FORK CODE --
//				}
//			}
//			
//	
//			// For each of the indices (frames) in the complete gesture
//			for(Integer index : gesture.keySet()) {
//				
//			}
//		
//		}
//		// ------ END OF MATCHING GESTURE ALGORITHM ------ 
//		// ------- START OF NEW GESTURE ALGORITHM --------
//		// Matching gesture not found, let's create a new branch
//		else {
//			// For each of the indices (frames) in the complete gesture
//			for(Integer index : gesture.keySet()) {
//				// Generate gesture data
//				GestureData data = new GestureData(index, gesture.get(index));
//				// Create gesture node
//				Boolean isEndOfGesture = (index == gesture.size()+1) ? true : false; 
//				GestureNode node = new GestureNode(ID, null, isEndOfGesture, data);
//				// Create temporary iterator
//				GestureNode temp;
//				if(index == 0) {
//					// First index, add directly to tree
//					tree.add(node);
//					temp = tree.get(index).getSelf();
//				}
//				else {
//					// Not the first index, need to add to end of branch
//					temp = tree.get(index);
//					int i = 1;
//					// Loop till you reach end, using temp as target pointer
//					while(i<index && temp.getChildren().get(ID) != null) {
//						temp = temp.getChildren().get(ID);
//						i++;
//					}
//					// End found, add node 
//					temp.addChild(node);
//				}
//			}
//		}
//	}
	
	
} // --- END OF GestureLibrary Class ---

// ------------------------------------------------------------------------------------

/**
 * Gesture Node
 * - Non-Public Support Class used by GestureLibrary
 * 
 */
class GestureNode {
	
	/**
	 * Private Node Data
	 */
	private UUID ID; // ID from Gesture Table
	private GestureNode parent; // Parent Node
	private HashMap<UUID, GestureNode> children; // Tree Children
	private Boolean isEndOfGesture; // Is this node the end of a gesture?
	private GestureData gestureData; // Actual data stored in node
	
	/**
	 * Default Constructor, should never get evoked
	 */
	public GestureNode() {
		this.children = new HashMap<UUID, GestureNode>();
		this.ID = UUID.randomUUID();
		this.isEndOfGesture = false;
	}
	
	/**
	 * Main Constructor
	 * @param ID
	 * @param parent
	 * @param isEndOfGesture
	 * @param gestureData
	 */
	public GestureNode(UUID ID, GestureNode parent, Boolean isEndOfGesture, GestureData gestureData) {
		this.ID = ID;
		this.parent = parent;
		this.children = new HashMap<UUID, GestureNode>();
		this.isEndOfGesture = isEndOfGesture;
		this.gestureData = gestureData;
	}
	

	/**
	 * Add Child
	 * @param node
	 */
	public void addChild(GestureNode node) {
		this.children.put(node.ID, node);
	}
	
	/**
	 * Get Functions
	 * 
	 */
	public GestureNode getSelf() {
		return this;
	}
	public UUID getID() {
		return this.ID;
	}
	public GestureNode getParent() {
		return this.parent;
	}
	public HashMap<UUID, GestureNode> getChildren() {
		return this.children;
	}
	public Boolean getIsEndOfGesture() {
		return this.isEndOfGesture;
	}
	public GestureData getGestureData() {
		return this.gestureData;
	}

}

//------------------------------------------------------------------------------------

/**
 * Gesture Data
 * - Non-Public Support Class used by GestureNode
 * - All data is publicly accessible, no need for get/set functions
 *
 */
class GestureData {
	
	public int index; // original index from JointSpaceGesture
	public Body body;
	
	public GestureData() {
		this.index = 0;
		this.body = null;
	}
	
	public GestureData(Integer index, Body body) {
		this.index = index;
		this.body = body;
	}
	
}