
package Viewpoints.BackEnd.GestureStorage;

import java.io.File;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.UUID;

import Viewpoints.FrontEnd.Communication.File.FileUtilities;
import Viewpoints.FrontEnd.Gesture.JointSpaceGesture.JointSpaceGesture;

/**
 * ===================
 * Gesture File Table Class
 * ===================
 * 
 * Stores gestures based on JointSpaceGesture.java
 *
 */
public class GestureFileTable implements Serializable {
	
	/**
	 * Generated UID. Change at own peril to destroy backwards compatibility.
	 */
	private static final long serialVersionUID = -7256433248366722266L;
	/**
	 * Gesture Table Data
	 * UUID -> ArrayList<FileName>
	 */
	private String gestureDir = "runtime-gesture-segments" + File.separator;
	private String gestureFileTableSaveDir = "file_communications" + File.separator;
	private String gestureFileTableSaveFileName = "gesture-file-table.db";
	private LinkedHashMap<UUID, ArrayList<String>> gestureFileTable;
	private Integer IDCountSoFar;
	private Boolean initialized = false;
	
	/**
	 * Constructor
	 * Called automatically upon creation of class object
	 */
	public GestureFileTable() {
		if(!this.initialized) {
			initializeGestureTable(this.gestureDir);
			this.initialized = true;
		}
	}
	
	/**
	 * Constructor
	 * Called automatically upon creation of class object
	 */
	public GestureFileTable(String gestureDir) {
		if(!this.initialized) {
			initializeGestureTable(gestureDir);
			this.initialized = true;
		}
	}
	
	/**
	 * Initialize Gesture Table
	 * This should only get called once upon class object creation
	 * Private: called by constructor
	 */
	private void initializeGestureTable(String gestureDir) {
		this.gestureFileTable = new LinkedHashMap<UUID, ArrayList<String>>();
		this.IDCountSoFar = 0;
		this.gestureDir = gestureDir;
	}
	
	public void loadGestureTable() {
		GestureFileTable gestureFileTable = FileUtilities.deserializeGestureFileTable(gestureFileTableSaveDir + File.separator + gestureFileTableSaveFileName);
		if(gestureFileTable != null) {
			this.gestureFileTable = gestureFileTable.gestureFileTable;
			this.IDCountSoFar = gestureFileTable.IDCountSoFar;
			System.out.println("Number of gestures in Gesture Table: " + this.IDCountSoFar);
		}
	}
	
	public void storeGestureTable() {
		FileUtilities.serializeGestureFileTable(gestureFileTableSaveDir + File.separator + gestureFileTableSaveFileName, this);
	}
	
	/**
	 * Add new gesture to the gesture table
	 * Called each time a new gesture is to be added to the table
	 */
	public void addGesture(UUID ID, String fileName) {
		if(this.initialized) {
			ArrayList<String> temp = this.gestureFileTable.get(ID);
			if(temp == null) {
				temp = new ArrayList<String>();
			}
			temp.add(fileName);
			this.gestureFileTable.put(ID, temp);
			this.IDCountSoFar++;
			storeGestureTable();
		}
	}
	
	/**
	 * Get/Set Functions
	 * Add all future get/set functions to this group below to maintain consistency 
	 */
	public Integer getGestureCount() {
		return this.IDCountSoFar;
	}
	
	public LinkedHashMap<UUID, ArrayList<String>> getGestureTable() {
		return this.gestureFileTable;
	}
	
	public String getGestureFileName(UUID uniqueID) {
		//Returns the latest gesture file name with that UUID
		try {
			return this.gestureFileTable.get(uniqueID).get(this.gestureFileTable.get(uniqueID).size() - 1);
		} catch (NullPointerException e) {
			System.out.println("ERROR: null pointer exception thrown in epmem.db. Restart system after deleting epmem.db, gesture-file-table.db, and the 'jsg' and 'param' files in runtime-gesture-segments.");
			return null;
		}
	}
	
	public JointSpaceGesture getJointSpaceGesture(UUID uniqueID) {
		//Returns the latest gesture with that UUID
		return FileUtilities.deserializeJointsGesture(getGestureFileName(uniqueID));
	}
}
