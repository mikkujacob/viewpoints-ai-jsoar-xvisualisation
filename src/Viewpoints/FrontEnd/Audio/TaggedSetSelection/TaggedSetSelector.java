package Viewpoints.FrontEnd.Audio.TaggedSetSelection;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map.Entry;
import java.util.Random;

/**
 * Class that defines a mechanism to choose a target object with 
 * some set of tags, given a source object with some set of tags, 
 * rules defining what tags can be used to select what other tags, 
 * and a set of objects and their tags.
 * @author mikhail.jacob
 *
 * @param <T> - The Type of object that is to be selected.
 */
public class TaggedSetSelector<T>
{
	/**
	 * An enumerated type defining the mechanisms usable to 
	 * select from a candidate set of objects.
	 * @author mikhail.jacob
	 *
	 */
	public enum SelectionMechanism
	{
		RANDOM,
		WEIGHTED_RANDOM
	}
	
	/**
	 * The selection mechanism to use to choose the final target 
	 * object.
	 */
	private SelectionMechanism mechanism;
	
	/**
	 * The ArrayList of rules defining what tags can be used to 
	 * select what other tags.
	 */
	private ArrayList<SelectionRule> selectionRules;
	
	/**
	 * A table containing references to objects of the desired 
	 * type and the tags associated with it.
	 */
	private Hashtable<T, TaggedSet> objectTaggedSetTable;
	
	/**
	 * A bonus weight given to objects that match multiple times 
	 * due to different overlapping rules. This is from the 
	 * rationale that multiple matches imply greater match 
	 * strength for selection.
	 */
	private final float recurringMatchBonus = 5f;
	
	/**
	 * A Random object used to generate random values for 
	 * variables.
	 */
	private Random random;
	
	/**
	 * Public parameterized constructor for this class given a 
	 * selection mechanism to use.
	 * @param mechanism - The selection mechanism to use.
	 */
	public TaggedSetSelector(SelectionMechanism mechanism)
	{
		this.mechanism = mechanism;
		random = new Random();
		selectionRules = new ArrayList<SelectionRule>();
		objectTaggedSetTable = new Hashtable<T, TaggedSet>();
	}
	
	/**
	 * Add a selection rule to the list in order to use it for 
	 * matching.
	 * @param selectionRule - The selection rule to add.
	 */
	public void addSelectionRule(SelectionRule selectionRule)
	{
		selectionRules.add(selectionRule);
	}
	
	/**
	 * Add an object and its set of tags to the table that tracks 
	 * the objects and their sets of tags
	 * @param object - The object added to the table
	 * @param taggedSet - The set of tags for that object.
	 */
	public void addObjectTaggedSetToTable(T object, TaggedSet taggedSet)
	{
		objectTaggedSetTable.put(object, taggedSet);
	}
	
	/**
	 * Method that gets a source object and uses selection rules 
	 * to find all possible candidate target objects to return. 
	 * Then, according to which selection mechanism is being used, 
	 * one final target object is selected from them.
	 * @param source - The source object used.
	 * @return - The selected target object.
	 */
	public T selectNextFromTags(T source)
	{
		//TODO: REMOVE
		System.out.println("Selection Rules: " + selectionRules.toString());
		System.out.println();
		
		Hashtable<T, Float> candidates = createCandidateMatchList(objectTaggedSetTable.get(source));
		
		switch(mechanism)
		{
			case RANDOM:
			{
				return selectRandom(candidates);
			}
			case WEIGHTED_RANDOM:
			{
				return selectWeightedRandom(candidates);
			}
			default:
			{
				return selectRandom(candidates);
			}
		}
	}
	
	/**
	 * Method that given a set of tags, uses all selection rules 
	 * to create a set of all possible candidates for the target 
	 * object.
	 * @param sourceTaggedSet - The source set of tags used for 
	 * selection.
	 * @return - The set of candidates for the target object.
	 */
	private Hashtable<T, Float> createCandidateMatchList(TaggedSet sourceTaggedSet)
	{
		Hashtable<T, Float> candidateMatchList = new Hashtable<T, Float>();
		
		for(SelectionRule selectionRule : selectionRules)
		{
			if(sourceTaggedSet.getTagSet().containsAll(selectionRule.getFirstSet().getTagSet()))
			{
				Hashtable<T, Float> candidateMatchListForRule = createCandidateMatchListForRule(selectionRule);
				for(Entry<T, Float> entry : candidateMatchListForRule.entrySet())
				{
					if(candidateMatchList.containsKey(entry.getKey()))
					{
						float higherWeight = (candidateMatchList.get(entry.getKey()) >= entry.getValue()) ? candidateMatchList.get(entry.getKey()) : entry.getValue();
						candidateMatchList.put(entry.getKey(), higherWeight + recurringMatchBonus);
					}
					else
					{
						candidateMatchList.put(entry.getKey(), entry.getValue());
					}
				}
			}
		}
		
		return candidateMatchList;
	}
	
	/**
	 * Method that creates a candidate list of matching objects 
	 * given a single selection rule to use.
	 * @param selectionRule - The selection rule to use.
	 * @return - The candidate set of target objects selected 
	 * given the selection rule.
	 */
	private Hashtable<T, Float> createCandidateMatchListForRule(SelectionRule selectionRule)
	{
		//TODO: REMOVE
		System.out.println("Selection Rule Matched: " + selectionRule.toString());
		
		Hashtable<T, Float> candidateMatchListForRule = new Hashtable<T, Float>();
		
		for(Entry<T, TaggedSet> entry : objectTaggedSetTable.entrySet())
		{
			if(entry.getValue().getTagSet().containsAll(selectionRule.getSecondSet().getTagSet()))
			{
				candidateMatchListForRule.put(entry.getKey(), selectionRule.getWeight());
			}
		}
		
		return candidateMatchListForRule;
	}
	
	/**
	 * Method to select a final target candidate object at random 
	 * from a set of candidates.
	 * @param candidates - The set of candidates.
	 * @return - The final target object.
	 */
	private T selectRandom(Hashtable<T, Float> candidates)
	{
		if(candidates.isEmpty())
		{
			return null;
		}
		
		ArrayList<T> candidateList = new ArrayList<>(candidates.keySet());
		int index = random.nextInt(candidateList.size());
		
		//TODO: REMOVE
		//System.out.println("Candidate List: " + candidateList.toString());
		//System.out.println();
		return candidateList.get(index);
	}
	
	/**
	 * Method to select a final target candidate object using a 
	 * weighted random selection from a set of candidates.
	 * @param candidates - The set of candidates.
	 * @return - The final target object.
	 */
	private T selectWeightedRandom(Hashtable<T, Float> candidates)
	{
		if(candidates.isEmpty())
		{
			return null;
		}
		else if(candidates.size() == 1)
		{
			for(T key : candidates.keySet())
			{
				return key;
			}
		}
		
		ArrayList<T> candidateList = new ArrayList<>();
		ArrayList<Float> weightList = new ArrayList<>();
		ArrayList<Float> cdfList = new ArrayList<>();
		
		for(Entry<T, Float> entry : candidates.entrySet())
		{
			candidateList.add(entry.getKey());
			weightList.add(entry.getValue());
			
			if(cdfList.isEmpty())
			{
				cdfList.add(entry.getValue());
			}
			else
			{
				cdfList.add(entry.getValue() + cdfList.get(cdfList.size() - 1));
			}
		}
		
		//TODO: REMOVE
		//System.out.println("Candidate List: " + candidateList.toString());
		
		float cumulativeWeight = cdfList.get(cdfList.size() - 1);
		
		float randomChoice = random.nextFloat() * cumulativeWeight;
		
		for(int lowerIndex = 0, upperIndex = 1; lowerIndex < cdfList.size(); lowerIndex++, upperIndex++)
		{
			if(cdfList.get(upperIndex) < randomChoice)
			{
				continue;
			}
			
			if(randomChoice - cdfList.get(lowerIndex) < cdfList.get(upperIndex) - randomChoice)
			{
				return candidateList.get(lowerIndex);
			}
			else if(randomChoice - cdfList.get(lowerIndex) > cdfList.get(upperIndex) - randomChoice)
			{
				return candidateList.get(upperIndex);
			}
			else
			{
				if(random.nextBoolean())
				{
					return candidateList.get(lowerIndex);
				}
				else
				{
					return candidateList.get(upperIndex);
				}
			}
		}
		
		return null;
	}

	/**
	 * Getter for the selection mechanism used in selection.
	 * @return - The selection mechanism.
	 */
	public SelectionMechanism getMechanism()
	{
		return mechanism;
	}

	/**
	 * Setter for the selection mechanism used in selection.
	 * @param mechanism - The desired selection mechanism.
	 */
	public void setMechanism(SelectionMechanism mechanism)
	{
		this.mechanism = mechanism;
	}
}
