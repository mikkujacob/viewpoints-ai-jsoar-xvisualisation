package Viewpoints.FrontEnd.Audio.TaggedSetSelection;

import java.util.EnumSet;

/**
 * A wrapper for an EnumeratedSet object that contains the tags for the 
 * object which instantiates it. 
 * @author mikhail.jacob
 *
 */
public class TaggedSet
{
	/**
	 * The EnumSet object that contains the tags associated with the 
	 * object that instantiated this TaggedSet object.
	 */
	private EnumSet<Tag> tagSet;
	
	/**
	 * Public constructor for this class taking a variable number 
	 * of Tag enumerated constants as input.
	 * @param tags - Variable number of Tag enums.
	 */
	public TaggedSet(Tag... tags)
	{
		tagSet = EnumSet.of(tags[0], tags);
	}
	
	/**
	 * Public copy constructor for this class taking an existing 
	 * EnumSet as input.
	 * @param tagSet - Existing EnumSet to copy.
	 */
	public TaggedSet(EnumSet<Tag> tagSet)
	{
		tagSet = EnumSet.copyOf(tagSet);
	}
	
	/**
	 * Public copy constructor for this class taking an existing 
	 * TaggedSet object as input.
	 * @param taggedSet - Existing TaggedSet object to copy.
	 */
	public TaggedSet(TaggedSet taggedSet)
	{
		tagSet = EnumSet.copyOf(taggedSet.getTagSet());
	}

	/**
	 * Getter for the inner EnumSet object.
	 * @return - The inner EnumSet object.
	 */
	public EnumSet<Tag> getTagSet()
	{
		return tagSet;
	}
	
	/**
	 * Method to convert an object of this class to a 
	 * String representation.
	 */
	public String toString()
	{
		return tagSet.toString();
	}
}
