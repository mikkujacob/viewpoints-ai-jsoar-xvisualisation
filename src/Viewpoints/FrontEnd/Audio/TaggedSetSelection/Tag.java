package Viewpoints.FrontEnd.Audio.TaggedSetSelection;

/**
 * Enumerated class containing set of all possible tags.
 * @author mikhail.jacob
 *
 */
public enum Tag
{
	//TEMPO
	LOW_TEMPO,
	MEDIUM_TEMPO,
	HIGH_TEMPO,
	
	//ENERGY
	LOW_ENERGY,
	MEDIUM_ENERGY,
	HIGH_ENERGY,
	
	//KEY_SIG
	E_MIN,
	C_MAJ,
	F,
	G,
	D_MIN,
	
	//INSTRUMENT_TYPE
	DRUM,
	HAT_CRASH,
	BASS,
	CHORD,
	MELODY,
	LEAD,
	
	//WITHIN THE LEAD
	HI,
	HI2,
	MID,
	MID2,
	
	// CHORD_PROGRESSION_FEEL: 
	CLICHE,
	CLICHE2,
	CREEPY,
	SIMPLE,
	
	//MEASURES
	THIRTY_TWO,
	SIXTEEN,
	EIGHT,
	FOUR,
	
	//AESTHETIC
	BREAK_BEAT,
	AMBIENT,
	FO_FO,
	
	// CHIP
	CHIPTUNE;
	
}