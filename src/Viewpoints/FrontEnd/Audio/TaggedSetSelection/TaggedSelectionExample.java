package Viewpoints.FrontEnd.Audio.TaggedSetSelection;

import Viewpoints.FrontEnd.Audio.TaggedSetSelection.TaggedSetSelector.SelectionMechanism;

/**
 * Class the defines an example to test and understand how the 
 * TaggedSetSelection library works. The objects, rules, and 
 * other properties can be changed to understand the library better.
 * @author mikhail.jacob
 *
 */
public class TaggedSelectionExample
{
	/**
	 * The TaggedSetSelector object that does selection.
	 */
	TaggedSetSelector<TestObject> selector;
	
	/**
	 * Public parameterized constructor for this class given 
	 * a selection mechanism to use to select the net TestObject.
	 * @param mechanism - The desired selection mechanism.
	 */
	public TaggedSelectionExample(SelectionMechanism mechanism)
	{
		selector = new TaggedSetSelector<TestObject>(mechanism);
	}
	
	/**
	 * The main method that starts execution and testing for this class.
	 * @param args Command line parameters.
	 */
	public static void main(String[] args)
	{
		TaggedSelectionExample example = new TaggedSelectionExample(SelectionMechanism.RANDOM);
		
		TestObject object1 = example.new TestObject(Tag.HIGH_ENERGY, Tag.HIGH_TEMPO);
		TestObject object2 = example.new TestObject(Tag.HIGH_ENERGY, Tag.HIGH_TEMPO);
		TestObject object3 = example.new TestObject(Tag.LOW_ENERGY, Tag.HIGH_TEMPO);
		TestObject object4 = example.new TestObject(Tag.LOW_ENERGY, Tag.HIGH_TEMPO);
		TestObject object5 = example.new TestObject(Tag.LOW_ENERGY, Tag.LOW_TEMPO);
		TestObject object6 = example.new TestObject(Tag.LOW_ENERGY, Tag.LOW_TEMPO);
		TestObject object7 = example.new TestObject(Tag.MEDIUM_ENERGY, Tag.HIGH_TEMPO);
		TestObject object8 = example.new TestObject(Tag.MEDIUM_ENERGY);
		TestObject object9 = example.new TestObject(Tag.HIGH_ENERGY);
		TestObject object10 = example.new TestObject(Tag.HIGH_ENERGY, Tag.MEDIUM_TEMPO);
		
		SelectionRule rule1 = new SelectionRule(new TaggedSet(Tag.HIGH_ENERGY, Tag.HIGH_TEMPO), new TaggedSet(Tag.MEDIUM_ENERGY, Tag.MEDIUM_TEMPO), 10f);
		SelectionRule rule2 = new SelectionRule(new TaggedSet(Tag.HIGH_ENERGY, Tag.HIGH_TEMPO), new TaggedSet(Tag.LOW_ENERGY, Tag.HIGH_TEMPO), 5f);
		SelectionRule rule3 = new SelectionRule(new TaggedSet(Tag.MEDIUM_TEMPO), new TaggedSet(Tag.LOW_ENERGY), 3f);
		SelectionRule rule4 = new SelectionRule(new TaggedSet(Tag.MEDIUM_ENERGY, Tag.HIGH_TEMPO), new TaggedSet(Tag.HIGH_ENERGY, Tag.HIGH_TEMPO));
		SelectionRule rule5 = new SelectionRule(new TaggedSet(Tag.HIGH_ENERGY), new TaggedSet(Tag.LOW_ENERGY));
		
		
		// adds the object and the tags to the selector to the hash table corresponding to the object
		example.getSelector().addObjectTaggedSetToTable(object1, object1.getTaggedSet());
		example.getSelector().addObjectTaggedSetToTable(object2, object2.getTaggedSet());
		example.getSelector().addObjectTaggedSetToTable(object3, object3.getTaggedSet());
		example.getSelector().addObjectTaggedSetToTable(object4, object4.getTaggedSet());
		example.getSelector().addObjectTaggedSetToTable(object5, object5.getTaggedSet());
		example.getSelector().addObjectTaggedSetToTable(object6, object6.getTaggedSet());
		example.getSelector().addObjectTaggedSetToTable(object7, object7.getTaggedSet());
		example.getSelector().addObjectTaggedSetToTable(object8, object8.getTaggedSet());
		example.getSelector().addObjectTaggedSetToTable(object9, object9.getTaggedSet());
		example.getSelector().addObjectTaggedSetToTable(object10, object10.getTaggedSet());
		
		example.getSelector().addSelectionRule(rule1);
		example.getSelector().addSelectionRule(rule2);
		example.getSelector().addSelectionRule(rule3);
		example.getSelector().addSelectionRule(rule4);
		example.getSelector().addSelectionRule(rule5);
		
		
		// default is unweighted random
		System.out.println("Select Next From Object 1 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object1));
//		System.out.println();
//		System.out.println("Select Next From Object 2 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object2));
//		System.out.println();
//		System.out.println("Select Next From Object 3 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object3));
//		System.out.println();
//		System.out.println("Select Next From Object 4 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object4));
//		System.out.println();
//		System.out.println("Select Next From Object 5 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object5));
//		System.out.println();
//		System.out.println("Select Next From Object 6 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object6));
//		System.out.println();
//		System.out.println("Select Next From Object 7 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object7));
//		System.out.println();
//		System.out.println("Select Next From Object 8 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object8));
//		System.out.println();
//		System.out.println("Select Next From Object 9 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object9));
//		System.out.println();
//		System.out.println("Select Next From Object 10 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object10));
		System.out.println();
		
		example.getSelector().setMechanism(SelectionMechanism.WEIGHTED_RANDOM);
		
		System.out.println("Select Next From Object 1 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object1));
//		System.out.println();
//		System.out.println("Select Next From Object 2 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object2));
//		System.out.println();
//		System.out.println("Select Next From Object 3 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object3));
//		System.out.println();
//		System.out.println("Select Next From Object 4 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object4));
//		System.out.println();
//		System.out.println("Select Next From Object 5 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object5));
//		System.out.println();
//		System.out.println("Select Next From Object 6 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object6));
//		System.out.println();
//		System.out.println("Select Next From Object 7 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object7));
//		System.out.println();
//		System.out.println("Select Next From Object 8 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object8));
//		System.out.println();
//		System.out.println("Select Next From Object 9 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object9));
//		System.out.println();
//		System.out.println("Select Next From Object 10 using " + example.getSelector().getMechanism().toString() + ": " + example.getSelector().selectNextFromTags(object10));
	}
	
	/**
	 * Inner class defining a test object to be used with this example. 
	 * In other code this could be a Variable object or a Track or a 
	 * Script (pretty much anything).
	 * @author mikhail.jacob
	 *
	 */
	class TestObject
	{
		/**
		 * The TaggedSet object that has to be included in order to use 
		 * TaggedSetSelector.
		 */
		private TaggedSet taggedSet;
		
		/**
		 * Public parameterized constructor with variable arguments to 
		 * add variable number of tags. You can not add no parameters 
		 * without breaking the library however.
		 * @param tags - The desired tags to add to this object.
		 */
		public TestObject(Tag... tags)
		{
			taggedSet = new TaggedSet(tags);
		}

		/**
		 * Getter for the TaggedSet object.
		 * @return - The TaggedSet object.
		 */
		public TaggedSet getTaggedSet()
		{
			return taggedSet;
		}
		
		/**
		 * Method to convert an object of this class to a 
		 * String representation.
		 */
		public String toString()
		{
			return taggedSet.toString();
		}
	}

	/**
	 * Getter for the TaggedSetSelector object used to select 
	 * the next object given sets of tags, etc.
	 * @return - The TaggedSetSelector object.
	 */
	public TaggedSetSelector<TestObject> getSelector()
	{
		return selector;
	}
}
