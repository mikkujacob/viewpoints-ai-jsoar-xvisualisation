package Viewpoints.FrontEnd.Audio.TaggedSetSelection;

/**
 * Class that defines selection rules between objects with sets of tags. 
 * A TaggedSet object in firstSet enables selection of a TaggedSet object 
 * in secondSet with some weight.
 * @author mikhail.jacob
 *
 */
public class SelectionRule
{
	/**
	 * The LHS of the selection rule with one directional semantics. 
	 */
	private TaggedSet firstSet;
	
	/**
	 * The RHS of the selection rule with one directional semantics.
	 */
	private TaggedSet secondSet;
	
	/**
	 * The weight with which this rule applies.
	 */
	private float weight;
	
	/**
	 * The default weight amount if not specified.
	 */
	private static final float defaultWeight = 1.0f;
	
	/**
	 * Public parameterized constructor for this class that 
	 * instantiates the rule with a default weight.
	 * @param firstSet - The first TaggedSet.
	 * @param secondSet - The second TaggedSet.
	 */
	public SelectionRule(TaggedSet firstSet, TaggedSet secondSet)
	{
		this(firstSet, secondSet, defaultWeight);
	}
	
	/**
	 * Public parameterized constructor for this class that
	 * instantiates the rule with a specified weight.
	 * @param firstSet - The first TaggedSet.
	 * @param secondSet - The second TaggedSet.
	 * @param weight - The specified weight.
	 */
	public SelectionRule(TaggedSet firstSet, TaggedSet secondSet, float weight)
	{
		this.firstSet = firstSet;
		this.secondSet = secondSet;
		this.weight = weight;
	}

	/**
	 * Getter for the weight for this rule.
	 * @return - The weight for this rule.
	 */
	public float getWeight()
	{
		return weight;
	}

	/**
	 * Setter for the weight for this rule.
	 * @param weight - The desired weight for this rule.
	 */
	public void setWeight(float weight)
	{
		this.weight = weight;
	}

	/**
	 * Getter for the first TaggedSet.
	 * @return - The first TaggedSet.
	 */
	public TaggedSet getFirstSet()
	{
		return firstSet;
	}

	/**
	 * Getter for the second TaggedSet.
	 * @return - The second TaggedSet.
	 */
	public TaggedSet getSecondSet()
	{
		return secondSet;
	}
	
	/**
	 * Method to convert an object of this class to a 
	 * String representation.
	 */
	public String toString()
	{
		return "(" + firstSet.toString() + " --> " + secondSet.toString() + ") : " + weight;
	}
}
