package Viewpoints.FrontEnd.Audio.AudioPlayer;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;


import processing.core.PApplet;
import ddf.minim.AudioPlayer;
import ddf.minim.Minim;

public class MusicControl
{
	private static String SOUNDS_LIB = "sounds" + File.separator;
	private ArrayList<String> soundFileNames = new ArrayList<String>();
	private int currentIndex = 0;
	private PApplet parent;
	private Boolean isInit = false;
	private Boolean isPlaying = false;
	private Boolean isRunnable = true;
	private Boolean play = false;
	private Boolean skip = false;
	private Boolean shuffle = false;
	
	private Thread audioThread = new Thread(new Runnable()
	{
		@Override
		public void run()
		{
			try{
				Minim minim;
				AudioPlayer musicPlayer;
				
				//Init minim
				minim = new Minim(parent);
				//Init audioPlayer with first audio file
				currentIndex = -1;
				musicPlayer = minim.loadFile(nextFileName());
				
				while(isRunnable) {
					if(skip) {
						musicPlayer.pause();
						musicPlayer.close();
						if(!shuffle) {
							musicPlayer = minim.loadFile(nextFileName());
						}
						else {
							musicPlayer = minim.loadFile(nextRandomFileName());
						}
						//Done skipping
						skip = false;
					}
					
					//Play
					if(play && !musicPlayer.isPlaying()) {
						musicPlayer.play();
					}
					//Pause
					else if(!play && musicPlayer.isPlaying()) {
						musicPlayer.pause();
					}
					
					//Play status
					isPlaying = musicPlayer.isPlaying();
				}
				
				minim.stop();
			}
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		private String nextFileName() {
			currentIndex = ++currentIndex % soundFileNames.size();
			return soundFileNames.get(currentIndex);
		}
		
		private String nextRandomFileName() {
			currentIndex = (new Random()).nextInt(soundFileNames.size());
			return soundFileNames.get(currentIndex);
		}
	});
	
	public MusicControl(PApplet parent) {
		this.parent = parent;
		init();
	}
	
	private Boolean loadSoundFiles() {
		//Load all audio files in library
		String soundFile;
		File soundsFolder = new File(SOUNDS_LIB);
		File[] soundsFileList = soundsFolder.listFiles(); 
		for (int i = 0; i < soundsFileList.length; i++) {
			if (soundsFileList[i].isFile()) {
				soundFile = soundsFileList[i].getName();
				if (soundFile.toLowerCase().endsWith(".mp3")) { //TODO MODIFY WITH OTHER FORMAT SUPPORT					
					this.soundFileNames.add(SOUNDS_LIB + soundFile);
				}
			}
		}
		
		currentIndex = 0;
		
		if(this.soundFileNames.size() == 0) {
			System.out.println("No Rehearsal Gesture Files In Folder: " + SOUNDS_LIB);
			return false;
		}
		else {
			System.out.println("Loaded " + this.soundFileNames.size() + " Sound Files");
			return true;
		}
	}
	
	public void play() {
		this.play = true;
	}
	
	public void pause() {
		this.play = false;
	}
	
	public Boolean isPlaying() {
		return this.isPlaying;
	}
	
	public void togglePlay() {
		this.play = !this.play;
	}
	
	public void nextTrack() {
		this.skip = true;
	}
	
	public void shuffle(Boolean value) {
		this.shuffle = value;
	}
	
	public void toggleShuffle() {
		this.shuffle = !this.shuffle;
	}
	
	public void shutdown() {
		this.isRunnable = false;
	}
	
	public void init() {
		isInit = loadSoundFiles();
		
		if(isInit) {
			audioThread.start();
		}
	}
}
