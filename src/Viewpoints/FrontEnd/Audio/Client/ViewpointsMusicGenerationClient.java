package Viewpoints.FrontEnd.Audio.Client;

import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.JSValue;
import com.teamdev.jxbrowser.chromium.events.FinishLoadingEvent;
import com.teamdev.jxbrowser.chromium.events.LoadAdapter;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;

import Viewpoints.FrontEnd.Audio.EarSketch.Script;
import Viewpoints.FrontEnd.Audio.EarSketch.Variable;
import Viewpoints.FrontEnd.Audio.EarSketch.Function.FitMedia;
import Viewpoints.FrontEnd.Audio.EarSketch.Function.SetEffect;
import Viewpoints.FrontEnd.Audio.EarSketch.Function.SetEffect.EFFECT;
import Viewpoints.FrontEnd.Audio.EarSketch.Function.SetEffect.EFFECT.*;
import Viewpoints.FrontEnd.Audio.TaggedSetSelection.SelectionRule;
import Viewpoints.FrontEnd.Audio.TaggedSetSelection.Tag;
import Viewpoints.FrontEnd.Audio.TaggedSetSelection.TaggedSet;
import Viewpoints.FrontEnd.Audio.TaggedSetSelection.TaggedSetSelector;
import Viewpoints.FrontEnd.Audio.TaggedSetSelection.TaggedSetSelector.SelectionMechanism;
import Viewpoints.FrontEnd.Communication.Socket.MusicGenerationSocketManager;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;


/**
 * Class for which our Earsketch script is run
 * This is where all the Scripts, Variables, FitMedias,
 * SetEffects,and Sonification Parameters come together.
 * @author scottwise
 */
public class ViewpointsMusicGenerationClient
{
	/**
	 * SETUP VARIABLES
	 */
	private Browser browser;
	private MusicGenerationSocketManager socketManager;
	private long refreshRateMilliseconds = 16000;
	private Script script;
	private static TaggedSetSelector<Variable> tss; 
	private int playThroughs = 0; 
	
	/**
	 *  TODO: Find this value via JxBrowser javascript function call
	 */
	private static final double endMeasure = 33; 
	
	/**
	 * EMA for every 5 seconds in JxBrowser
	 */
	private ExponentialMovingAverage smoothnessEma = new ExponentialMovingAverage(.5);
	private ExponentialMovingAverage energyEma = new ExponentialMovingAverage(.5);
	
	
	/**
	 * PreviousValues for sFX
	 */
	private Float previousPanValue;
	private Float previousDistoValue; 
	private Float previousRingmod;
	private Float previousTremolo; 
	private Float previousFilter; 
	private Float previousFlanger; 
	private Float previousWah; 
	
	
	/**
	 * Example use of ALL VARIABLES FOR SOUNDS BUILT
	 * TODO: Set Up GUI for building variables
	 */
	//1drums
	static Variable var0 = new Variable("drums0", "WISESM0_DRUMS0",Tag.DRUM,Tag.FO_FO,Tag.EIGHT);
	static Variable var1 = new Variable("drums1", "WISESM0_BLANK",Tag.HAT_CRASH);
	static Variable var2 = new Variable("drums2", "WISESM0_DRUMS2",Tag.DRUM,Tag.FO_FO,Tag.EIGHT);
	static Variable var3 = new Variable("drums3", "WISESM0_DRUMS3",Tag.DRUM,Tag.FO_FO,Tag.EIGHT);
	static Variable var4 = new Variable("drums4", "WISESM0_DRUMS4",Tag.DRUM,Tag.FO_FO,Tag.EIGHT);
	static Variable var5 = new Variable("drums5", "WISESM0_DRUMS5",Tag.DRUM,Tag.FO_FO,Tag.EIGHT);

	//1drums
	static Variable var6 = new Variable("drumsa0", "WISESM0_DRUMSA0",Tag.DRUM,Tag.AMBIENT,Tag.SIXTEEN);
	static Variable var7 = new Variable("drumsa1", "WISESM0_DRUMSA1",Tag.DRUM,Tag.AMBIENT,Tag.SIXTEEN);
	
	//2crashes
	static Variable var8 = new Variable("hats0", "WISESM0_BLANK",Tag.HAT_CRASH);
	static Variable var9 = new Variable("hats1", "WISESM0_BLANK",Tag.HAT_CRASH);
	static Variable var10 = new Variable("hats2", "WISESM0_BLANK",Tag.HAT_CRASH);
	static Variable var11 = new Variable("hats3", "WISESM0_BLANK",Tag.HAT_CRASH);
	static Variable var12 = new Variable("hats4", "WISESM0_BLANK",Tag.HAT_CRASH);
	static Variable var13 = new Variable("hats5", "WISESM0_BLANK",Tag.HAT_CRASH);
	static Variable var14 = new Variable("hats6", "WISESM0_BLANK",Tag.HAT_CRASH);
	static Variable var15 = new Variable("hats7", "WISESM0_BLANK",Tag.HAT_CRASH);
	
	//3bass
	static Variable var16 = new Variable("d_bass0", "WISESM0_D_BASS_F0",Tag.D_MIN,Tag.BASS,Tag.FO_FO,Tag.EIGHT);
	static Variable var17 = new Variable("d_bass1", "WISESM0_D_BASS_F1",Tag.CREEPY);
	static Variable var18 = new Variable("d_bass2", "WISESM0_D_BASS_FA0",Tag.D_MIN,Tag.BASS,Tag.FO_FO,Tag.AMBIENT,Tag.EIGHT);
	static Variable var19 = new Variable("d_bass3", "WISESM0_BLANK",Tag.HAT_CRASH);
	static Variable var20 = new Variable("d_bass4", "WISESM0_BLANK",Tag.HAT_CRASH);
	static Variable var21 = new Variable("d_bass5", "WISESM0_BLANK",Tag.HAT_CRASH);
	static Variable var22 = new Variable("d_bass6", "WISESM0_BLANK",Tag.HAT_CRASH);
	static Variable var23 = new Variable("d_bass7", "WISESM0_BLANK",Tag.HAT_CRASH);
	
	//4chords
	static Variable var24 = new Variable("d_chord0", "WISESM0_D_CHORD_F0",Tag.D_MIN,Tag.CHORD,Tag.FO_FO,Tag.EIGHT);
	static Variable var25 = new Variable("d_chord1", "WISESM0_D_CHORD_FA0",Tag.D_MIN,Tag.CHORD,Tag.FO_FO,Tag.AMBIENT,Tag.EIGHT);
	static Variable var26 = new Variable("d_chord2", "WISESM0_D_CHORD_FA1",Tag.D_MIN,Tag.CHORD,Tag.FO_FO,Tag.AMBIENT,Tag.EIGHT);
	static Variable var27 = new Variable("d_chord3", "WISESM0_D_CHORD_FA2",Tag.D_MIN,Tag.CHORD,Tag.FO_FO,Tag.AMBIENT,Tag.EIGHT);
	static Variable var28 = new Variable("d_chord4", "WISESM0_BLANK",Tag.HAT_CRASH);
	static Variable var29 = new Variable("d_chord5", "WISESM0_BLANK",Tag.HAT_CRASH);
	static Variable var30 = new Variable("d_chord6", "WISESM0_BLANK",Tag.HAT_CRASH);
	static Variable var31 = new Variable("d_chord7", "WISESM0_BLANK",Tag.HAT_CRASH);
	
	//5annoyingmids
	static Variable var32 = new Variable("d_lead0", "WISESM0_D_LEAD_MID0",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.EIGHT,Tag.MID);
	static Variable var33 = new Variable("d_lead1", "WISESM0_D_LEAD_MID1",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.EIGHT,Tag.MID);
	static Variable var34 = new Variable("d_lead2", "WISESM0_D_LEAD_MID2",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.EIGHT,Tag.MID);
	static Variable var35 = new Variable("d_lead3", "WISESM0_D_DEEP2",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.EIGHT,Tag.MID);
	static Variable var36 = new Variable("d_lead4", "WISESM0_BLANK",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.EIGHT,Tag.MID);
	static Variable var37 = new Variable("d_lead5", "WISESM0_BLANK",Tag.MID);
	static Variable var38 = new Variable("d_lead6", "WISESM0_BLANK",Tag.MID);
	static Variable var39 = new Variable("d_lead7", "WISESM0_BLANK",Tag.MID);
	
	//6abstractHis
	static Variable var40 = new Variable("d_lead_0", "WISESM0_D_LEAD_MID2_0",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.SIXTEEN,Tag.MID2);
	static Variable var41 = new Variable("d_lead_1", "WISESM0_D_LEAD_MID2_1",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.SIXTEEN,Tag.MID2);
	static Variable var42 = new Variable("d_lead_2", "WISESM0_D_LEAD_MID2_2",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.SIXTEEN,Tag.MID2);
	static Variable var43 = new Variable("d_lead_3", "WISESM0_D_LEAD_MID2_3",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.SIXTEEN,Tag.MID2);
	static Variable var44 = new Variable("d_lead_4", "WISESM0_BLANK",Tag.HAT_CRASH);
	static Variable var45 = new Variable("d_lead_5", "WISESM0_BLANK",Tag.HAT_CRASH);
	static Variable var46 = new Variable("d_lead_6", "WISESM0_BLANK",Tag.HAT_CRASH);
	static Variable var47 = new Variable("d_lead_7", "WISESM0_BLANK",Tag.HAT_CRASH);
	
	//7annoyinghis
	static Variable var48 = new Variable("d_lead0_a", "WISESM0_D_LEAD_HI0",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.EIGHT,Tag.HI);
	static Variable var49 = new Variable("d_lead1_a", "WISESM0_D_LEAD_HI1",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.EIGHT,Tag.HI);
	static Variable var50 = new Variable("d_lead2_a", "WISESM0_D_LEAD_HI2",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.EIGHT,Tag.HI);
	static Variable var51 = new Variable("d_lead3_a", "WISESM0_D_LEAD_HI3",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.EIGHT,Tag.HI);
	static Variable var52 = new Variable("d_lead4_a", "WISESM0_BLANK",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.EIGHT,Tag.HI);
	static Variable var53 = new Variable("d_lead5_a", "WISESM0_BLANK",Tag.HI);
	static Variable var54 = new Variable("d_lead6_a", "WISESM0_BLANK",Tag.HI);
	static Variable var55 = new Variable("d_lead7_a", "WISESM0_BLANK",Tag.HI);
	
	//8abstractHis
	static Variable var56 = new Variable("d_lead2_0", "WISESM0_D_LEAD_HI2_0",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.EIGHT,Tag.HI2);
	static Variable var57 = new Variable("d_lead2_1", "WISESM0_D_LEAD_HI2_1",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.EIGHT,Tag.HI2);
	static Variable var58 = new Variable("d_lead2_2", "WISESM0_D_LEAD_HI2_2",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.EIGHT,Tag.HI2);
	static Variable var59 = new Variable("d_lead2_3", "WISESM0_D_LEAD_HI2_3",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.EIGHT,Tag.HI2);
	static Variable var60 = new Variable("d_lead2_4", "WISESM0_D_LEAD_HI2_4",Tag.D_MIN,Tag.LEAD,Tag.FO_FO,Tag.EIGHT,Tag.HI2);
	static Variable var61 = new Variable("d_lead2_5", "WISESM0_BLANK",Tag.HI2);
	static Variable var62 = new Variable("d_lead2_6", "WISESM0_BLANK",Tag.HI2);
	static Variable var63 = new Variable("d_lead2_7", "WISESM0_BLANK",Tag.HI2);
	
	//9Chiptunes
	static Variable var64 = new Variable("chip0", "EIGHT_BIT_VIDEO_GAME_LOOP_015",Tag.CHIPTUNE);
	static Variable var65 = new Variable("chip1", "EIGHT_BIT_VIDEO_GAME_LOOP_024",Tag.CHIPTUNE);
	static Variable var66 = new Variable("chip2", "EIGHT_BIT_VIDEO_SPEAKNSPELL_BEAT_003",Tag.CHIPTUNE);
	static Variable var67 = new Variable("chip3", "EIGHT_BIT_VIDEO_GAME_LOOP_023",Tag.CHIPTUNE);
	static Variable var68 = new Variable("chip4", "EIGHT_BIT_VIDEO_GAME_LOOP_011",Tag.CHIPTUNE);
	static Variable var69 = new Variable("chip5", "EIGHT_BIT_VIDEO_GAME_LOOP_007",Tag.CHIPTUNE);
	static Variable var70 = new Variable("chip6", "EIGHT_BIT_VIDEO_GAME_LOOP_001",Tag.CHIPTUNE);
	static Variable var71 = new Variable("chip7", "EIGHT_BIT_VIDEO_GAME_LOOP_005",Tag.CHIPTUNE); 

	
	/**
	 * EXAMPLE EARSKETCH FITMEDIA INSTANTIATIONS
	 */
	static FitMedia fm1 = new FitMedia(var0, 1, 1, (int)endMeasure); // Drums
	static FitMedia fm2 = new FitMedia(var8, 2, 1, (int)endMeasure); // HiHats
	static FitMedia fm3 = new FitMedia(var18, 3, 1, (int)endMeasure);// Bass
	static FitMedia fm4 = new FitMedia(var24, 4, 1, (int)endMeasure);// Chord Progression 
	static FitMedia fm5 = new FitMedia(var32, 5, 1, (int)endMeasure);// Melody1 - muted
	static FitMedia fm6 = new FitMedia(var40, 6, 1, (int)endMeasure);// Melody2
	static FitMedia fm7 = new FitMedia(var48, 7, 1, (int)endMeasure);// Melody3 - muted
	static FitMedia fm8 = new FitMedia(var56, 8, 1, (int)endMeasure);// Melody4 - muted
	static FitMedia fm9 = new FitMedia(var64, 9, 1, (int)endMeasure);// ChipTune - muted
	
	
	/**
	 * EXAMPLE INITIAL SETUP OF THE USED EFFECTS:
	 * TODO: A first effect must always be set in order to modulate a track effect. 
	 * The javascript gods are not on our side when running in jxbrowser
	 */
	static SetEffect panEffect = new SetEffect(1,EFFECT.PAN,PAN.LEFT_RIGHT.name(),0);
	static SetEffect panEffect2 = new SetEffect(1,EFFECT.PAN,PAN.LEFT_RIGHT.name(),100,1,-100,33);
	
	static SetEffect distoEffect = new SetEffect(1,EFFECT.DISTORTION,DISTORTION.DISTO_GAIN.name(),0);
	static SetEffect distoEffect2 = new SetEffect(1,EFFECT.DISTORTION,DISTORTION.DISTO_GAIN.name(),0,1,50,33);
	
	static SetEffect ringModEffect = new SetEffect(1,EFFECT.RINGMOD,RINGMOD.RINGMOD_MODFREQ.name(),0);
	static SetEffect ringModEffect2 = new SetEffect(1,EFFECT.RINGMOD,RINGMOD.RINGMOD_MODFREQ.name(),0,1,100,33);
	
	static SetEffect bandPassEffect = new SetEffect(1,EFFECT.BANDPASS,BANDPASS.BANDPASS_FREQ.name(),0);
	static SetEffect bandPassEffect2 = new SetEffect(1,EFFECT.BANDPASS,BANDPASS.BANDPASS_FREQ.name(),100,1,-100,33);
	
	static SetEffect reverbEffect = new SetEffect(1,EFFECT.REVERB,REVERB.REVERB_TIME.name(),0);
	static SetEffect reverbEffect2 = new SetEffect(1,EFFECT.REVERB,REVERB.REVERB_TIME.name(),100,1,-100,33);
	
	static SetEffect tremoloEffect = new SetEffect(4,EFFECT.TREMOLO,TREMOLO.TREMOLO_FREQ.name(),0);
	static SetEffect tremoloEffect2 = new SetEffect(4,EFFECT.TREMOLO,TREMOLO.TREMOLO_FREQ.name(),0,1,100,33);
	
	static SetEffect filterEffect = new SetEffect(6,EFFECT.FILTER,FILTER.FILTER_FREQ .name(),200);
	static SetEffect filterEffect2 = new SetEffect(6,EFFECT.FILTER,FILTER.FILTER_FREQ.name(),20,1,10000,33);
	
	static SetEffect flangerEffect = new SetEffect(7,EFFECT.FLANGER,FLANGER.FLANGER_LENGTH.name(),0);
	static SetEffect flangerEffect2 = new SetEffect(7,EFFECT.FLANGER,FLANGER.FLANGER_LENGTH.name(),0,1,200,33);
	
	static SetEffect phaserEffect = new SetEffect(1,EFFECT.PHASER,PHASER.PHASER_RATE.name(),0);
	static SetEffect phaserEffect2 = new SetEffect(1,EFFECT.PHASER,PHASER.PHASER_RATE.name(),0,1,10,33);
	
	static SetEffect wahEffect = new SetEffect(1,EFFECT.WAH,WAH.WAH_POSITION.name(),0);
	static SetEffect wahEffect2 = new SetEffect(1,EFFECT.WAH,WAH.WAH_POSITION.name(),0,1,1,33);

	
	public static void main(String[] args) throws Exception
	{
		ViewpointsMusicGenerationClient m = new ViewpointsMusicGenerationClient();
		
		m.startBrowser(m);
	}

	public ViewpointsMusicGenerationClient()
	{
		socketManager = new MusicGenerationSocketManager();
		initializeBrowser();
		initializeScript();
		tss = new TaggedSetSelector<Variable>(SelectionMechanism.RANDOM); 
		initializeRules();
	}
	
	
	/**
	 * Example RULE SETUPS
	 * TODO: Add weights to tags
	 * Weight is scaled 0 - 10f based and added to the 2nd tag
	 */
	private void initializeRules() {

		SelectionRule rule0 = new SelectionRule(new TaggedSet(Tag.HAT_CRASH),
							  new TaggedSet(Tag.HAT_CRASH));
		SelectionRule rule1 = new SelectionRule(new TaggedSet(Tag.DRUM),
				  				new TaggedSet(Tag.DRUM));
		SelectionRule rule1_25 = new SelectionRule(new TaggedSet(Tag.DRUM,Tag.AMBIENT,Tag.SIXTEEN),
  								 new TaggedSet(Tag.DRUM,Tag.AMBIENT,Tag.SIXTEEN));
		SelectionRule rule1_5 = new SelectionRule(new TaggedSet(Tag.DRUM,Tag.FO_FO,Tag.EIGHT),
  								new TaggedSet(Tag.DRUM,Tag.FO_FO,Tag.EIGHT));
		SelectionRule rule2 = new SelectionRule(new TaggedSet(Tag.D_MIN,Tag.BASS,Tag.FO_FO,Tag.EIGHT),
								new TaggedSet(Tag.D_MIN,Tag.BASS,Tag.FO_FO,Tag.EIGHT));
		SelectionRule rule3 = new SelectionRule(new TaggedSet(Tag.D_MIN,Tag.CHORD,Tag.FO_FO,Tag.EIGHT),
				  				new TaggedSet(Tag.D_MIN,Tag.CHORD,Tag.FO_FO,Tag.EIGHT));
		SelectionRule rule4 = new SelectionRule(new TaggedSet(Tag.MID),
								new TaggedSet(Tag.MID));
		SelectionRule rule5 = new SelectionRule(new TaggedSet(Tag.MID2),
	  							new TaggedSet(Tag.MID2));
		SelectionRule rule6 = new SelectionRule(new TaggedSet(Tag.HI),
								new TaggedSet(Tag.HI));
		SelectionRule rule7 = new SelectionRule(new TaggedSet(Tag.HI2),
								new TaggedSet(Tag.HI2));
		SelectionRule rule8 = new SelectionRule(new TaggedSet(Tag.CHIPTUNE),
											new TaggedSet(Tag.CHIPTUNE));
		
		
		for (int i=0; i< Variable.getAllVars().size();i++) {
			tss.addObjectTaggedSetToTable(Variable.getAllVars().get(i),
										  Variable.getAllVars().get(i).getTaggedSet()); 
		}
		
		tss.addSelectionRule(rule0);
		tss.addSelectionRule(rule1);
		tss.addSelectionRule(rule1_25);
		tss.addSelectionRule(rule1_5);
		tss.addSelectionRule(rule2);
		tss.addSelectionRule(rule3);
		tss.addSelectionRule(rule4);
		tss.addSelectionRule(rule5);
		tss.addSelectionRule(rule6);
		tss.addSelectionRule(rule7);
	}
		
	private void initializeBrowser()
	{
		browser = new Browser();
		BrowserView view = new BrowserView(browser);
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.add(view, BorderLayout.CENTER);
		frame.setSize(1200, 720);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	private void initializeScript()
	{
		script = new Script(128);
		
		/**
		 * Add all variables and fx to script
		 * so we can string them
		 */
		for (int i = 0; i<Variable.getAllVars().size(); i++) {
			script.addVar(Variable.getAllVars().get(i));
		}
		for (int i = 0; i<FitMedia.getFms().size(); i++) {
			script.addFM(FitMedia.getFms().get(i));
		}		
				
		/**
		 * All effects in the Earsketch Script
		 * script.addSFX(ringModEffect);
		 * script.addSFX(ringModEffect2);
		 */		

	}
	
	/**
	 * Getter for the TaggedSetSelector object used to select 
	 * the next object given sets of tags, etc.
	 * @return - The TaggedSetSelector object.
	 */
	public TaggedSetSelector<Variable> getSelector()
	{
		return tss;
	}

	/**
	 * Method where entire script is played
	 * 
	 * 
	 * @param m
	 */
	private void startBrowser(final ViewpointsMusicGenerationClient m)
	{

		browser.addLoadListener(new LoadAdapter()
		{
			public void onFinishLoadingFrame(FinishLoadingEvent event)
			{
				if (event.isMainFrame())
				{
					
					while (true)
					{
						
						/**Do every frame */
						double currentPlayPos = getCurrentPlayPositionInMeasures(browser); 
						if (currentPlayPos < 1) {
							currentPlayPos = 1; 
						}
						if (true) {
							m.muteTrack(5);
							m.muteTrack(7);
							m.muteTrack(8);
							m.muteTrack(9);
						}
						
						compileandExecuteNewScript(browser,script);
						
						/**
						 * These are cleared so we aren't reduplicating FX
						 * There is most likely a better way to do this
						 * This must occur after the compile and execute so
						 * as not to remove the duplicated FX
						 */
						Script.inMainAdds.clear();

						/** Get Sonificication Parameters via socket */ 
						if(socketManager.getCurrentSonificationParameters() != null)
						{
							System.out.println("Current Set of Sonification Parameters:\n" + socketManager.getCurrentSonificationParameters().convertToString());

							/** Direct Sonification Values */ 
							float period = socketManager.getCurrentSonificationParameters().getPeriod();
							float energy = socketManager.getCurrentSonificationParameters().getEnergy();
							float smoothness = socketManager.getCurrentSonificationParameters().getSmoothness();
							float phase = socketManager.getCurrentSonificationParameters().getPhase();
							float leftHandVelocity = socketManager.getCurrentSonificationParameters().getVelocityLeftHand();
							float rightHandVelocity = socketManager.getCurrentSonificationParameters().getVelocityRightHand();
							float leftHandAcceleration = socketManager.getCurrentSonificationParameters().getAccelerationLeftHand();
							float rightHandAcceleration = socketManager.getCurrentSonificationParameters().getAccelerationRightHand();
							float kineticEnergySum = socketManager.getCurrentSonificationParameters().getKineticEnergyTotal();

							
							/** Pseudo Average of Values coming in every 5 seconds*/ 
							double averageEnergy = energyEma.average(energy);
							double averageSmoothness = smoothnessEma.average(smoothness);

							/** Mapped Values*/ 
							float panValue = socketManager.getCurrentSonificationParameters().getPanValues();
							float ditsoValue = socketManager.getCurrentSonificationParameters().getDistortionValues();
							float ringModValue = socketManager.getCurrentSonificationParameters().getRingModValues();
							float tremoloValue = socketManager.getCurrentSonificationParameters().getTremeloValues();
							float filterValue = socketManager.getCurrentSonificationParameters().getFilterValues();
							float flangerValue = socketManager.getCurrentSonificationParameters().getFlangerValues();
							float wahValue = socketManager.getCurrentSonificationParameters().getWahValues();
							if (filterValue < 20) {
								filterValue = 21; 
							}
							
							if (previousPanValue == null) {
								previousPanValue = (float)panValue;
							}
							if (previousDistoValue == null) {
								previousDistoValue = (float)ditsoValue;
							}
							if (previousRingmod == null) {
								previousRingmod = (float)ringModValue;
							}
							if (previousTremolo == null) {
								previousTremolo = (float)tremoloValue;
							}
							if (previousFilter == null) {
								previousFilter = (float)filterValue;
							}
							if (previousFlanger == null) {
								previousFlanger = (float)flangerValue;
							}
							if (previousWah == null) {
								previousWah = (float)wahValue;
							}
 
							if (currentPlayPos+16f > 32) {
								currentPlayPos = -7f; 
							}
							
							/** Mapped Values for effects
							 * 	For some reason the current play position is always 8 measures off
							 *  */							
							panEffect2.setEffectStartValue(previousPanValue);
							panEffect2.setEffectEndValue(panValue);
							panEffect2.setEffectStartLocation((int) currentPlayPos+8f);
							panEffect2.setEffectEndLocation((int) (currentPlayPos+16f));
							previousPanValue = panValue; 
							
							distoEffect2.setEffectStartValue(previousDistoValue);
							distoEffect2.setEffectEndValue(ditsoValue);
							distoEffect2.setEffectStartLocation((int) currentPlayPos+8f);
							distoEffect2.setEffectEndLocation((int) (currentPlayPos+16f));
							previousDistoValue = ditsoValue;
							
							ringModEffect2.setEffectStartValue(previousRingmod);
							ringModEffect2.setEffectEndValue(ringModValue);
							ringModEffect2.setEffectStartLocation((int) currentPlayPos+8f);
							ringModEffect2.setEffectEndLocation((int) (currentPlayPos+16f));
							previousRingmod = ringModValue;
							
							tremoloEffect2.setEffectStartValue(previousTremolo);
							tremoloEffect2.setEffectEndValue(tremoloValue);
							tremoloEffect2.setEffectStartLocation((int) currentPlayPos+8f);
							tremoloEffect2.setEffectEndLocation((int) (currentPlayPos+16f));
							previousTremolo = tremoloValue;
							
							filterEffect2.setEffectStartValue(previousFilter);
							filterEffect2.setEffectEndValue(filterValue);
							filterEffect2.setEffectStartLocation((int) currentPlayPos+8f);
							filterEffect2.setEffectEndLocation((int) (currentPlayPos+16f));
							previousFilter = previousFilter;
							
							flangerEffect2.setEffectStartValue(previousFlanger);
							flangerEffect2.setEffectEndValue(flangerValue);
							flangerEffect2.setEffectStartLocation((int) currentPlayPos+8f);
							flangerEffect2.setEffectEndLocation((int) (currentPlayPos+16f));
							previousFlanger = flangerValue;
							
							wahEffect2.setEffectStartValue(previousWah);
							wahEffect2.setEffectEndValue(wahValue);
							wahEffect2.setEffectStartLocation((int) currentPlayPos+8f);
							wahEffect2.setEffectEndLocation((int) (currentPlayPos+16f));
							previousWah = wahValue;
							
							
							/**
							 * Using these loops to set effects on all the clips
							 * This is hacky
							 * @author scottwise
							 */
							for (int i=1;i<=6;i++){
								Script.duplicateEffect(filterEffect,i); 
								Script.duplicateEffect(filterEffect2,i);
							}
							
							for (int i=1;i<=6;i++){
								Script.duplicateEffect(ringModEffect,i); 
								Script.duplicateEffect(ringModEffect2,i);
							}
							
							
							
							/** Simple Threshold triggers for swapping tagged clips */
							if (averageEnergy<.5) {
								// fm1.setClipName(m.getSelector().selectNextFromTags(ViewpointsMusicGenerationClient.var0));
								// fm4.setClipName(m.getSelector().selectNextFromTags(ViewpointsMusicGenerationClient.var25));
								// fm6.setClipName(m.getSelector().selectNextFromTags(ViewpointsMusicGenerationClient.var40));
							}

							if (averageSmoothness < .9) {
								// fm6.setClipName(m.getSelector().selectNextFromTags(ViewpointsMusicGenerationClient.var40));
							}
							
						}
						
						try
						{
							Thread.sleep(refreshRateMilliseconds);
						}
						catch (InterruptedException e)
						{
							System.out.println("Got woken up early but no worries.");
						}
					}
				}
			}
		});
		
		browser.loadURL("http://earsketch.gatech.edu/earsketch2/");
	}
	
	/**
	 * All Javascript/Angular Methods are below:
	 * TODO: Method to find all unmuted tracks,etc. 
	 * 
	 */
	
	
	/**
	 * Get play position float
	 * @return - The current Ersketch play position
	 */
	private double getCurrentPlayPositionInMeasures(Browser browser) {
        JSValue playPosition = browser.executeJavaScriptAndReturnValue(
        		"angular.element($('#content')).scope().getCurrentPlayPositionInMeasures()");
        double playPos = playPosition.getNumber(); 
        return playPos;
	}
	
	/**
	 * Compiles and runs Earsketch script
	 */
	private void compileandExecuteNewScript(Browser browser, Script script)
	{
		browser.executeJavaScript(
				"angular.element($('#container')).scope().editor.setValue('" + script.toString() + "');");
		browser.executeJavaScript(
				"angular.element($('#container')).scope().compileCode();");
	}
	
	
	/**
	 * Plays Earsketch script
	 */
	private void playScript(Browser browser) {
		browser.executeJavaScript(
				"angular.element($('#content')).scope().play();");
	}
	
	/**
	 * Used for picking random mute sections
	 * with seed of 5
	 */
	public static int randInt(int min, int max) {
	    Random rand = new Random(5);
	    int randomNum = rand.nextInt((max - min) + 1) + min;
	    return randomNum;
	}

	
	/**
	 * Mutes Earsketch track
	 * 
	 */
	private void muteTrack(int fmclipIndex) {
		browser.executeJavaScript(
				"angular.element($('#content')).scope().muteTrack("+fmclipIndex+");");
	}
	
	/**
	 * Unmutes Earsketch track
	 * 
	 */
	private void unmuteTrack(int fmclipIndex) {
		browser.executeJavaScript(
				"angular.element($('#content')).scope().unmuteTrack("+fmclipIndex+");");
	}
	
	
	/**
	 * Unused methods 
	 * For some reason it returns 2 more than the actual amount
	 * so I subtract 2 from the number
	 */
	private double getNumberOfFMTracks(Browser browser) {
		JSValue numTracks = browser.executeJavaScriptAndReturnValue(
				"angular.element($('#content')).scope().tracks.length"); 
		double num = numTracks.getNumber() - 2;
		return num; 
	}
	
	/**
	 * Gets the measure length of the script
	 * @return - float of end measure
	 */
	private double getScriptEndMeasureNumber(Browser browser) {
		JSValue endMeasure = browser.executeJavaScriptAndReturnValue(
				"angular.element($('#content')).scope().playLength");
		return endMeasure.getNumber();
	}
	
	
	/**
	 * Basic moving average filter
	 * 
	 */
	private class ExponentialMovingAverage {
	    private double alpha;
	    private Double oldValue;
	    public ExponentialMovingAverage(double alpha) {
	        this.alpha = alpha;
	    }

	    public double average(double value) {
	        if (oldValue == null) {
	            oldValue = value;
	            return value;
	        }
	        double newValue = oldValue + alpha * (value - oldValue);
	        oldValue = newValue;
	        return newValue;
	    }
	}
	
}





