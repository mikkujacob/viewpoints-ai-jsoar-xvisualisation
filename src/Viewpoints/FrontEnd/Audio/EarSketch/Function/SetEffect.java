package Viewpoints.FrontEnd.Audio.EarSketch.Function;

import Viewpoints.FrontEnd.Audio.EarSketch.Script;

/**
 * Class for Earsketch SetEffect Function
 * This may be used to set various effects 
 * to corresponding FitMedias or MakeBeats
 * 
 * TODO: NEED a way to remove FX on the fly
 * TODO: NEED a way to add FX on the fly
 * @author scottwise
 */
public class SetEffect {
	
	private int trackNum; 
 	private EFFECT effectType; 
 	private String effectParameter;
 	private float effectValue; 
 	private float effectStartValue;
 	private float effectStartLocation;
 	private float effectEndValue;
 	private float effectEndLocation;
 	private int paramLength;
 	
 	/**
	 * An enumerated set of effects available in EarSketch
	 * PitchShift is an Earsketch Backend effect which may cause serious lag
	 * May not need an Effect class, but I wasn't sure how to structure at the time
	 * For sum-enums
	 * @author scottwise
	 */
 	public static class EFFECT {
		public static final EFFECT BANDPASS = new EFFECT("BANDPASS");
		public static final EFFECT CHORUS = new EFFECT("CHORUS");
		public static final EFFECT COMPRESSOR = new EFFECT("COMPRESSOR");
		public static final EFFECT DELAY = new EFFECT("DELAY");
		public static final EFFECT DISTORTION = new EFFECT("DISTORTION");
		public static final EFFECT EQ3BAND = new EFFECT("EQ3BAND");
		public static final EFFECT FILTER = new EFFECT("FILTER");
		public static final EFFECT FLANGER = new EFFECT("FLANGER");
		public static final EFFECT PAN = new EFFECT("PAN");
		public static final EFFECT PHASER = new EFFECT("PHASER");
		public static final EFFECT PITCHSHIFT = new EFFECT("PITCHSHIFT");
		public static final EFFECT REVERB = new EFFECT("REVERB");
		public static final EFFECT RINGMOD = new EFFECT("RINGMOD");
		public static final EFFECT TREMOLO = new EFFECT("TREMOLO");
		public static final EFFECT VOLUME = new EFFECT("VOLUME");
		public static final EFFECT WAH = new EFFECT("WAH");
		String myEffect; 
		
		
		public EFFECT(String myEffect) {
			this.myEffect = myEffect; 
		}
		
		public String toString() {
			return myEffect; 
		}
 		
 		public enum BANDPASS {
 			BANDPASS_FREQ,
 			BANDPASS_WIDTH,
 			MIX,
 			BYPASS;
 		}
 		
 		public enum CHORUS {
 			CHORUS_LENGTH,
 			CHORUS_NUMVOICES,
 			CHORUS_RATE,
 			MIX,
 			CHORUS_MOD;
 		}
 		
 		public enum COMPRESSOR {
 			COMPRESSOR_THRESHOLD,
 			COMPRESSOR_RATIO,
 			BYPASS;
 		}
 		
 		public enum DELAY {
 			DELAY_TIME,
 			DELAY_FEEDBACK,
 			MIX;
 		}
 		
 		public enum DISTORTION {
 			DISTO_GAIN,
 			MIX,
 			BYPASS;
 		}
 		
 		public enum EQ3BAND {
 			EQ3BAND_LOWGAIN,
 			EQ3BAND_LOWFREQ,
 			EQ3BAND_MIDGAIN,
 			EQ3BAND_MIDFREQ,
 			EQ3BAND_HIGHGAIN,
 			EQ3BAND_HIGHFREQ,
 			MIX,
 			BYPASS; 
 		}
 		
 		public enum FILTER {
 			FILTER_FREQ,
 			FILTER_RESONANCE,
 			MIX,
 			BYPASS;
 		}
 		
 		public enum FLANGER {
 			FLANGER_LENGTH,
 			FLANGER_FEEDBACK,
 			FLANGER_RATE,
 			MIX,
 			BYPASS;
 		}
 		
 		public enum PAN {
 			LEFT_RIGHT,
 			BYPASS;
 		}
 		
 		public enum PHASER {
 			PHASER_RATE,
 			PHASER_RANGEMIN,
 			PHASER_RANGEMAX,
 			PHASER_FEEDBACK,
 			MIX,
 			BYPASS; 
 		}
 		
 		public enum PITCHSHIFT {
 			PITCHSHIFT_SHIFT,
 			BYPASS; 
 		}
 		
 		public enum REVERB {
 			REVERB_TIME,
 			REVER_DAMPFREQ,
 			MIX,
 			BYPASS; 
 		}
 		
 		public enum RINGMOD {
 			RINGMOD_MODFREQ,
 			RINGMOD_FEEDBACK,
 			MIX,
 			BYPASS;
 		}
 		
 		public enum TREMOLO {
 			TREMOLO_FREQ,
 			TREMOLO_AMOUNT,
 			MIX,
 			BYPASS; 
 		}
 		
 		public enum VOLUME {
 			GAIN,
 			BYPASS; 
 		}
 		
 		public enum WAH {
 			WAH_POSITION,
 			MIX,
 			BYPASS; 
 		}
		
	}
	
 	/**
 	 * TODO: Add tagging to FX
 	 * Tagging may allow users to control
 	 * onto which tracks these FX are placed
 	 */
	public SetEffect(int trackNum, EFFECT effectType, String effectParameter, float effectValue) {
		this.trackNum = trackNum;
		this.effectType = effectType;
		this.effectParameter = effectParameter; 
		this.effectValue = effectValue; 
		paramLength = 4; 
	}
	
	public SetEffect(int trackNum, EFFECT effectType, String effectParameter, float effectStartValue, float effectStartLocation, float effectEndValue, float effectEndLocation) {
		this.trackNum = trackNum; 
		this.effectType = effectType;
		this.effectParameter = effectParameter; 
		this.effectStartValue = effectStartValue;
		this.effectStartLocation = effectStartLocation;
		this.effectEndValue = effectEndValue; 
		this.effectEndLocation = effectEndLocation; 
		paramLength = 7; 
	}
	
	public int getTrackNum() {
		return trackNum;
	}

	public void setTrackNum(int trackNum) {
		this.trackNum = trackNum;
	}

	public EFFECT getEffectType() {
		return effectType;
	}

	public void setEffectType(EFFECT effectType) {
		this.effectType = effectType;
	}

	public String getEffectParameter() {
		return effectParameter;
	}

	public void setEffectParameter(String effectParameter) {
		this.effectParameter = effectParameter;
	}

	public float getEffectValue() {
		return effectValue;
	}

	public void setEffectValue(float effectValue) {
		this.effectValue = effectValue;
	}

	public float getEffectStartValue() {
		return effectStartValue;
	}

	public void setEffectStartValue(float effectStartValue) {
		this.effectStartValue = effectStartValue;
	}

	public float getEffectStartLocation() {
		return effectStartLocation;
	}

	public void setEffectStartLocation(float currentPlayPos) {
		this.effectStartLocation = currentPlayPos;
	}

	public float getEffectEndValue() {
		return effectEndValue;
	}

	public void setEffectEndValue(float effectEndValue) {
		this.effectEndValue = effectEndValue;
	}

	public float getEffectEndLocation() {
		return effectEndLocation;
	}

	public void setEffectEndLocation(float f) {
		this.effectEndLocation = f;
	}

	public int getParamLength() {
		return paramLength;
	}

	public void setParamLength(int paramLength) {
		this.paramLength = paramLength;
	}

	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("setEffect("); 
		if (paramLength == 4) {
			result.append(this.trackNum + ",");
			result.append(effectType.toString() + ",");
			result.append(effectParameter.toString() + ",");
			result.append(effectValue);
		}
		else {
			result.append(this.trackNum + ",");
			result.append(effectType.toString() + ",");
			result.append(effectParameter.toString() + ",");
			result.append(effectStartValue + ",");
			result.append(effectStartLocation + ",");
			result.append(effectEndValue + ",");
			result.append(effectEndLocation); 
		}
		
		result.append(");"); 
		return result.toString();
	}
	
}
