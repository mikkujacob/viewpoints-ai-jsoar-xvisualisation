package Viewpoints.FrontEnd.Audio.EarSketch.Function;

import java.util.ArrayList;

import Viewpoints.FrontEnd.Audio.EarSketch.Variable;
import Viewpoints.FrontEnd.Audio.TaggedSetSelection.Tag;

/**
 * Class for Earsketch FitMedia Function
 * These are used to lay the timeline in EarSketch
 * TODO: Add Tagging
 * @author scottwise
 */
public class FitMedia {

	private String clipName;
	private int trackNumber;
	private int startMeasure; 
	private int endMeasure;
	private static ArrayList<FitMedia> allFms = new ArrayList<FitMedia>(); 
	
	public FitMedia(String cn, int tn, int sm, int em, Tag... tags) {
		this.clipName = cn;
		this.trackNumber = tn;
		this.startMeasure = sm;
		this.endMeasure = em; 
		allFms.add(this); 
	}
	
	public FitMedia(Variable cn, int tn, int sm, int em, Tag... tags) {
		this.clipName = cn.getVarName();
		this.trackNumber = tn;
		this.startMeasure = sm;
		this.endMeasure = em; 
		allFms.add(this); 
	}
	
	
	public void setClipName(Variable var) {
		this.clipName = var.getVarName();
	}
	
	public void setTrackNumber(int tn) {
		this.trackNumber = tn; 
	}
	
	public void setStartMeasure(int sm) {
		this.startMeasure = sm; 
	}
	
	public void setEndMeasure(int em) {
		this.endMeasure = em;
	}
	
	public String getClipName() {
		return clipName;
	}
	
	public int getTrackNumber() {
		return trackNumber; 
	}
	
	public int getStartMeasure() {
		return startMeasure; 
	}
	
	public int getEndMeasure() {
		return endMeasure;
	}	
	
	public static ArrayList<FitMedia> getFms() {
		return allFms; 
	}
	
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("fitMedia("); 
		result.append(clipName + ",");
		result.append(trackNumber + ",");
		result.append(startMeasure + ",");
		result.append(endMeasure);
		result.append(");"); 
		return result.toString();
	}
	
}
