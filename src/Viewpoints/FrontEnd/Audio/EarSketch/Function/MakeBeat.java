package Viewpoints.FrontEnd.Audio.EarSketch.Function;

import Viewpoints.FrontEnd.Audio.EarSketch.Variable;


/**
 * Class for Earsketch MakeBeat Function
 * These are a way to create beats out of string structures
 * Consult the Earsketch curriculum for details
 * TODO: Add Tagging
 * @author scottwise
 */
public class MakeBeat {
	
	private String clipName;
	private int trackNumber;
	private int measureNumber; 
	private String rhythmicStructure;
	
	
	public MakeBeat(String cn, int tn, int mn, String rs){
		this.clipName = cn;
		this.trackNumber = tn;
		this.measureNumber = mn;
		this.rhythmicStructure = rs; 
	}
	
	public MakeBeat(Variable cn, int tn, int mn, Variable rs){
		this.clipName = cn.getVarName();
		this.trackNumber = tn;
		this.measureNumber = mn;
		this.rhythmicStructure = rs.getVarName(); 
	}
	
	public void setClipName(Variable cn) {
		this.clipName = cn.getVarName(); 
	}
	
	public void setTrackNumber(int tn) {
		this.trackNumber = tn; 
	}
	
	public void setMeasureNumber(int mn) {
		this.measureNumber = mn; 
	}
	
	public void setRhythmicStructure(Variable rs) {
		this.rhythmicStructure = rs.getVarName(); 
	}
	
	public void setRhythmicStructure(String rs) {
		this.rhythmicStructure = rs; 
	}
	
	
	public String getClipName() {
		return clipName;
	}
	
	public int getTrackNumber() {
		return trackNumber;
	}
	
	public int getMeasureNumber() {
		return measureNumber; 
	}
	
	public String getRhythmicStructure() {
		return rhythmicStructure;
	}
	
	
	
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("makeBeat("); 
		result.append(clipName + ",");
		result.append(trackNumber + ",");
		result.append(measureNumber + ",");
		result.append(rhythmicStructure);
		result.append(");"); 
		return result.toString();
	}
	
}	
