package Viewpoints.FrontEnd.Audio.EarSketch;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import Viewpoints.FrontEnd.Audio.EarSketch.Function.FitMedia;
import Viewpoints.FrontEnd.Audio.EarSketch.Function.MakeBeat;
import Viewpoints.FrontEnd.Audio.EarSketch.Function.SetEffect;
import Viewpoints.FrontEnd.Audio.EarSketch.Function.SetEffect.EFFECT;

/**
 * Script class for generating scripts in Earsketch
 * Houses all the Effects and FitMedia
 * @author scottwise
 */
public class Script {

	public static ArrayList<String> inMainAdds = new ArrayList<String>(); 
	public static ArrayList<MakeBeat> mb = new ArrayList<MakeBeat>();
	public static ArrayList<FitMedia> fm = new ArrayList<FitMedia>();
	public static ArrayList<SetEffect> sfx = new ArrayList<SetEffect>();
	public static ArrayList<Variable> variables = new ArrayList<Variable>(); 
	public static ArrayList<String> oscillationFX = new ArrayList<String>(); 
	public int tempo = 120; 
	public int addFunctions = 0; 
	
	public Script(int tempo) {
		this.tempo = tempo;
	}
	
	public Script(int tempo, Variable... vars) {
		this.tempo = tempo; 
		for(int i = 0; i < vars.length; i++) {
			variables.add(vars[i]);
		}
	}
	
	public void setTempo(int tempo) {
		this.tempo = tempo;
	}

	public int getTempo() {
		return tempo;
	}
	
	public void addFM(FitMedia val) {
		this.fm.add(val); 
	}
	
	public void addMB(MakeBeat val) {
		this.mb.add(val); 
	}
	
	public void addVar(Variable var) {
		this.variables.add(var); 
	}
	
	public void addSFX(SetEffect setfx) {
		this.sfx.add(setfx); 
	}
	
	public String buildScript() {
		String s = ""; 
		return s; 
	}
	
	public ArrayList<FitMedia> getFMList() {
		return this.fm;
	}
	
	public ArrayList<Variable> getVarList() {
		return this.variables;
	}
	
	public boolean isEmpty() {
		if (this.fm.isEmpty()) return true;
		return false;
	}
	
	
	public void purgeScript() {
		this.mb.clear();
		this.fm.clear(); 
		this.sfx.clear();
	}
	
	public void getAllScriptPieces(Script s) {
		this.variables.addAll(s.getVarList());
		this.fm.addAll(s.getFMList());
	}
	
	public void removeEffect(SetEffect fx) {
		sfx.remove(fx);
	}
	
	public void removeFMClip(int index) {
		fm.remove(index); 
	}	
	

	
	// These may occur if zones are defined: 
	public void removeESTrack(int i) {
	}
	public void buildTransitionTrack() {
	}
	public void removeTransitionFX() {
	}
	public void buildTransition(Script s1, Script s2) {
	}
	
	/**
	 * Used to duplicate effects in browser loop
	 * This can help avoid having to initialize effects in the script
	 * @param fx
	 * @param trackNumber
	 */
	public static void duplicateEffect(SetEffect fx, int trackNumber) {
		StringBuilder result = new StringBuilder();
		result.append("setEffect("); 
		if (fx.getParamLength() == 4) {
			result.append(trackNumber + ",");
			result.append(fx.getEffectType().toString() + ",");
			result.append(fx.getEffectParameter().toString() + ",");
			result.append(fx.getEffectValue());
		}
		else {
			result.append(trackNumber + ",");
			result.append(fx.getEffectType().toString() + ",");
			result.append(fx.getEffectParameter().toString() + ",");
			result.append(fx.getEffectStartValue() + ",");
			result.append(fx.getEffectStartLocation() + ",");
			result.append(fx.getEffectEndValue() + ",");
			result.append(fx.getEffectEndLocation()); 
		}
		
		result.append(");"); 
		String res = result.toString();
//		sfx.add(new SetEffect(trackNumber,fx.getEffectType(),fx.getEffectParameter(),
//				fx.getEffectStartValue(),fx.getEffectStartLocation(),fx.getEffectEndValue(),fx.getEffectEndLocation()));
		Script.inMainAdds.add(res);
	}
	
	/**
	 * 
	 * @param fx
	 * @param pannedfx
	 * @param step
	 * @param endmeasure
	 * Not used as of right now
	 */
	public void setOscillatingLoopedEffect(SetEffect fx, SetEffect pannedfx, int step,int endmeasure) {
		StringBuilder fin = new StringBuilder(); 
		fin.append(fx.toString()); // the original start point and end point
		fin.append("\\nfor(j = "+step+"; j<"+endmeasure+"; j=j+"+step*2+") {\\n");
		fin.append("\\tsetEffect("+pannedfx.getTrackNum()+","+pannedfx.getEffectType()+","
					+pannedfx.getEffectParameter()+","+pannedfx.getEffectStartValue()+",j,"
					+pannedfx.getEffectEndValue()+",j+"+step+");\\n"); 
		fin.append("\\tsetEffect("+fx.getTrackNum()+","+fx.getEffectType()+","
				+fx.getEffectParameter()+","+fx.getEffectStartValue()+",j+"+step+","
				+fx.getEffectEndValue()+",j+"+step*2+");\\n"); 
		fin.append("}");
		oscillationFX.add(fin.toString());
	}
	
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("init();\\n");
		result.append("setTempo("+this.tempo+");\\n");
		for (int i = 0; i<this.variables.size();i++) {
			result.append(this.variables.get(i).toString()+"\\n");
		}
		for (int i = 0; i<this.mb.size();i++) {
			result.append(this.mb.get(i).toString() + "\\n"); 
		}
		for (int i = 0; i<this.fm.size();i++) {
			result.append(this.fm.get(i).toString() + "\\n"); 
		}
		for (int i = 0; i<this.sfx.size();i++) {
			result.append(this.sfx.get(i).toString() + "\\n"); 
		}
		for (int i = 0; i<this.oscillationFX.size();i++) {
			result.append(this.oscillationFX.get(i) + "\\n");
		}
		for (int i = 0; i<inMainAdds.size();i++) {
			result.append(inMainAdds.get(i) + "\\n");
		}
		
		result.append("finish();\\n"); 
		return result.toString();
	}
}
