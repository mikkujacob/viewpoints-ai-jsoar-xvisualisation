package Viewpoints.FrontEnd.Audio.EarSketch;

import java.util.ArrayList;

import Viewpoints.FrontEnd.Audio.EarSketch.Function.FitMedia;
import Viewpoints.FrontEnd.Audio.TaggedSetSelection.Tag;
import Viewpoints.FrontEnd.Audio.TaggedSetSelection.TaggedSet;


/**
 * Class for Defining Earsketch Variables
 * Each instance is added to a list
 * These Variables can be used as a first 
 * argument for FitMedia
 * @author scottwise
 */
public class Variable {
	private String varName;
	private String param;
	private TaggedSet taggedSet;
	private static ArrayList<Variable> allVars = new ArrayList<Variable>(); 

	
	public Variable(String varName, String param) {
		this.varName = varName;
		this.param = param;
		allVars.add(this);
	}
	
	public Variable(String varName, String param, Tag... tags) {
		this.varName = varName;
		this.param = param;
		this.taggedSet = new TaggedSet(tags);
		allVars.add(this);
	}
	
	public TaggedSet getTaggedSet()
	{
		return taggedSet;
	}
	
	public String getVarName() {
		return varName;
	}
	
	public String getParam() {
		return param;
	}
	
	public void setVarName(String varName) {
		this.varName = varName;
	}
	
	public void setParam(String param) {
		this.param = param;
	}
	
	public static ArrayList<Variable> getAllVars() {
		return allVars; 
	}
	
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("var " + varName + " = ");
		result.append("\""+param+"\"");
		result.append(";");
		return result.toString();
	}

}
