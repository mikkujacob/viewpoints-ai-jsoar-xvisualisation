package Viewpoints.FrontEnd.Input.MotionInput;

import processing.core.*;

import SimpleOpenNI.SimpleOpenNI;
import Viewpoints.FrontEnd.Clock.Clock;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.UserPixels;

public class KinectMotionInput implements MotionInput {
	private SimpleOpenNI kinect = null;
	private boolean isInit = false;
	private int userNum = -1;
	private float maxZ, minZ;
	
	public KinectMotionInput(SimpleOpenNI kinect) {
		this(kinect, 300, 25000);
	}

	public KinectMotionInput(SimpleOpenNI kinect, float minZ, float maxZ) {
		this.minZ = minZ;
		this.maxZ = maxZ;
		this.kinect = kinect;
	}
	
	public boolean init() {
		if (!kinect.init()) return false;
		isInit = kinect.enableDepth();
		isInit = isInit && kinect.enableUser();
		isInit = isInit && kinect.enableRGB();
		kinect.alternativeViewPointDepthToImage();
		return isInit;
	}
	
	public boolean isInit() {
		return isInit;
	}
	
	public boolean isAnyoneOnScreen(PGraphics pgraphics) {
		PImage userimage = kinect.userImage();
		userimage.loadPixels();
		for (int i = 0; i < userimage.pixels.length; i++) {
			int color = userimage.pixels[i];
			float r = pgraphics.red(color);
			float g = pgraphics.blue(color);
			float b = pgraphics.green(color);
			if (r != g || g != b)
			{
				return true;
			}
		}
		return false;
	}
	
	public Body next(Clock clock) {
		kinect.update();
		int[] userList = kinect.getUsers();
		int nearestI = -1;
		float nearestZ = Float.MAX_VALUE;
		for(int i = 0; i < userList.length; i++)
		{
			//Find the nearest user to the kinect by looking at the Z coordinate of their Neck joint.
			if (kinect.isTrackingSkeleton(userList[i])) {
				PVector jointPos = new PVector();
				kinect.getJointPositionSkeleton(userList[i],SimpleOpenNI.SKEL_NECK,jointPos);
				if(jointPos.z > minZ && jointPos.z < maxZ && jointPos.z < nearestZ){
					nearestI = i;
					nearestZ = jointPos.z;
				}
			}
		}
		if(nearestI > -1){
			userNum = userList[nearestI];//The nearest user is userNum
			Body userPose = new Body();
			userPose.initialize(kinect, userList[nearestI], clock.time());
			return userPose;//Return the skeletal position of the nearest user
		}
		return null;
	}
	
//	public BodyOutline[] getUserPixels(boolean isNearestUserOnly)
//	{
//		int user[] = kinect.getUsers();
//		if(user.length == 0)
//			return null;
//		int [] userMap = kinect.userMap();
//		BodyOutline[] userPixels = new BodyOutline[2];
//		userPixels[0] = new BodyOutline();//The user outline
//		userPixels[1] = new BodyOutline();//The user mask
//
//		// this breaks our array down into rows  
//		for(int y = 1; y < 480; y++){  
//			// this breaks our array down into specific pixels in each row  
//			int leftUser = userMap[y * 640];
//			for(int x = 1; x < 640; x++){  
//				int i = x + y * 640;  
//				int[] outlinePoint = new int[2];
//				int[] maskPoint = new int[4];
//				int curUser = userMap[i];
//				int upUser = userMap[x + (y-1) * 640];
//				
//				//This next line is true at a boundary between values, example user1 and user2 or no user and user 1, etc.
//				//If it is a boundary then add to outline point array
//				if (((!isNearestUserOnly) || (userMap[i] == userNum || upUser == userNum || leftUser == userNum)) && (curUser != leftUser || curUser != upUser))
//				{
//					outlinePoint[0] = x;
//					outlinePoint[1] = y;
//					userPixels[0].add(outlinePoint);
//				}
//				
//				//If there is any user at all, aka any non zero user pixel (or if nearest user pixel is non zero)
//				if (((!isNearestUserOnly) || (userMap[i] == userNum)) && (curUser != 0))
//				{
//					maskPoint[0] = x;
//					maskPoint[1] = y;
//					maskPoint[2] = Math.round(kinect.depthMapRealWorld()[i].z);
//					maskPoint[3] = kinect.rgbImage().get(x, y);
//					//Add it to the outline array
//					userPixels[1].add(maskPoint);
//				}
//				leftUser = curUser;
//			}
//		}
//		
//		return userPixels;
//	}
	
//	public BodyOutline getUserOutline(boolean isNearestUserOnly)
//	{
//		int user[] = kinect.getUsers();
//		if(user.length == 0)
//			return null;
//		int [] userMap = kinect.userMap();
//		BodyOutline outline = new BodyOutline();
//
//		// this breaks our array down into rows  
//		for(int y = 1; y < 480; y++)
//		{
//			// this breaks our array down into specific pixels in each row  
//			int leftUser = userMap[y * 640];
//			for(int x = 1; x < 640; x++)
//			{
//				int i = x + y * 640;  
//				int[] outlinePoint = new int[2];
//				int curUser = userMap[i];
//				int upUser = userMap[x + (y-1) * 640];
//				//This next line is true at a boundary between values, example user1 and user2 or no user and user 1, etc.
//				//If it is a boundary then add to outline point array
//				if (((!isNearestUserOnly) || (userMap[i] == userNum || upUser == userNum || leftUser == userNum)) && (curUser != leftUser || curUser != upUser))
//				{
//					outlinePoint[0] = x;
//					outlinePoint[1] = y;
//					outline.add(outlinePoint);
//				}
//				leftUser = curUser;
//			}
//		}
//		
//		return outline;
//	}
//	
//	public BodyOutline getUserMask(boolean isNearestUserOnly)
//	{
//		int user[] = kinect.getUsers();
//		if(user.length == 0)
//			return null;
//		int [] userMap = kinect.userMap();
//		BodyOutline outline = new BodyOutline();
//		
//		// this breaks our array down into rows  
//		for(int y = 1; y < 480; y++)
//		{
//			// this breaks our array down into specific pixels in each row
//			for(int x = 1; x < 640; x++)
//			{  
//				int i = x + y * 640;  
//				int[] point = new int[4];
//				int curUser = userMap[i];
//				
//				//If there is any user at all, aka any non zero user pixel (or if nearest user pixel is non zero)
//				if (((!isNearestUserOnly) || (userMap[i] == userNum)) && (curUser != 0))
//				{
//					point[0] = x;
//					point[1] = y;
//					point[2] = Math.round(kinect.depthMapRealWorld()[i].z);
//					point[3] = kinect.rgbImage().get(x, y);
//					//Add it to the outline array
//					outline.add(point);
//				}
//			}
//		}
//		
//		return outline;
//	}
	
	public PImage getRGBImage()
	{
		if(isInit)
		{
			return kinect.rgbImage();
		}
		return null;
	}
	
	public PImage getDepthImage()
	{
		if(isInit)
		{
			return kinect.depthImage();
		}
		return null;
	}
	
	public UserPixels getUserPixels()
	{
		int user[] = kinect.getUsers();
		if(isInit && user.length > 0)
		{
			return new UserPixels(kinect.userMap(), userNum);
		}
		return null;
	}
	
	public SimpleOpenNI getKinect(){
		return kinect;
	}
	
	public int getUser(){
		return userNum;
	}
}
