package Viewpoints.FrontEnd.Input.MotionInput;

import java.util.*;

import processing.core.PApplet;
import processing.core.PVector;
import SimpleOpenNI.SimpleOpenNI;
import Viewpoints.FrontEnd.Clock.Clock;
import Viewpoints.FrontEnd.Communication.File.FileUtilities;
import Viewpoints.FrontEnd.Gesture.JointSpaceGesture.JointSpaceGesture;
import Viewpoints.FrontEnd.Shared.*;

public class MotionCapturer extends PApplet {
	/**
	 * Generated serial number
	 */
	private static final long serialVersionUID = 9001349788197790464L;
	private Clock clock;
	private KinectMotionInput kinectInput = null;
	ArrayList<Body> captured = new ArrayList<Body>();
	
	public void setup() {
		size(displayWidth, displayHeight);
		clock = new Clock();
		kinectInput = new KinectMotionInput(new SimpleOpenNI(this));
		if (!kinectInput.init()) {
			background(255, 0, 0);
			noLoop();
			kinectInput = null;
		}
		stroke(255);
		strokeWeight(1);
	}
	
	public void draw() {
		if (null != kinectInput) {
			clock.check();
			background(0);
			translate(width / 2, height / 2);
			Body pose = kinectInput.next(clock);
			if (null != pose && kinectInput.isAnyoneOnScreen(this.g)) {
				//System.out.println("got pose");
				captured.add(pose);
				drawBody(pose);
			} else {
				if (!captured.isEmpty()) {
					System.out.println("out");
					JointSpaceGesture jsg = new JointSpaceGesture(captured);
					FileUtilities.serializeJointsGesture("Capture.jsg", jsg);
					captured.clear();
				}
			}
		}
	}
	
	public boolean sketchFullScreen() {
		return true;
	}
	
	public void onNewUser(SimpleOpenNI context, int userId)
	{
		System.out.println("onNewUser - userId: " + userId);
		context.startTrackingSkeleton(userId);
	}
	
	public static void main(String[] args) {
		PApplet.main("Viewpoints.MotionInput.MotionCapturer", new String[]{"--full-screen", "--display=1"});
	}
	
	public PVector getScreenPos(PVector pos) {
		PVector screenPos = PVector.mult(pos, 0.3f); //new PVector();
		screenPos.y = - screenPos.y;
		screenPos.z = 800.0f / screenPos.z;
		screenPos.x *= screenPos.z;
		screenPos.y *= screenPos.z;
		return screenPos;
	}
	
	public void drawBody(Body body) 
	{
		strokeWeight(1);
		
		for (Map.Entry<LIDX, Pair<JIDX, JIDX>> limbEnds : LIDX.LIMB_ENDS().entrySet()) {
			PVector pos1 = getScreenPos(body.get(limbEnds.getValue().first));
			PVector pos2 = getScreenPos(body.get(limbEnds.getValue().second));
			line(pos1.x, pos1.y, pos2.x, pos2.y);
		}
	}
	
	public void drawVector(PVector origin, PVector vec) {
		strokeWeight(1);
		PVector pos1 = getScreenPos(origin);
		PVector pos2 = getScreenPos(PVector.add(origin, vec));
		line(pos1.x, pos1.y, pos2.x, pos2.y);
	}
}
