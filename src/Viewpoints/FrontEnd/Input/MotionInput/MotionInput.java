package Viewpoints.FrontEnd.Input.MotionInput;

import Viewpoints.FrontEnd.Clock.Clock;
import Viewpoints.FrontEnd.Shared.*;

public interface MotionInput {
	public boolean init();
	public boolean isInit();
	public Body next(Clock clock);
}
