package Viewpoints.FrontEnd.Input.MotionInput;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import Viewpoints.FrontEnd.Application.ViewpointsFrontEnd;
import Viewpoints.FrontEnd.Clock.Clock;
import Viewpoints.FrontEnd.Communication.File.FileUtilities;
import Viewpoints.FrontEnd.Gesture.JointSpaceGesture.JointSpaceGesture;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.JointSpaceGestureInterpolator;

public class FileMotionInput implements MotionInput {
	private String filename = null;
	private boolean isInit = false;
	private String PROJECT_HOME;
	protected JointSpaceGestureInterpolator motionRecord = null;
	
	public FileMotionInput(String filename) {
		this.filename = filename;
		PROJECT_HOME = System.getProperty("user.dir");
		if(PROJECT_HOME.equalsIgnoreCase(null)) {
			try {
				PROJECT_HOME = new java.io.File( "." ).getCanonicalPath();
			}
			catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public void changeFile(String filename) {
		this.filename = filename;
		init();
	}

	public boolean init() {
		JointSpaceGesture jsg = FileUtilities.deserializeJointsGesture(filename);
		if (null != jsg) {
			motionRecord = new JointSpaceGestureInterpolator(jsg.getGestureFramesList());
			isInit = true;
		}
		return isInit;
	}
	
	public boolean isJSG(){
		if(filename.substring(filename.length() - 3).equals("jsg"))
			return true;
		return false;
	}
	public boolean isInit() {
		return isInit;
	}
	
	public void setReplaySpeed(float f) {
		motionRecord.setReplaySpeed(f);
	}

	public Body next(Clock clock) {
		float deltatime = clock.deltatime();
//		System.out.println("Frame displayed: " + this.motionRecord.replayFrame(deltatime).toString());
		return motionRecord.replayFrame(deltatime);
	}
	
	public Body next(float f) {
		return motionRecord.at(f);
	}
	public void repeat(boolean repeat) {
		motionRecord.setIsRepetitionOn(repeat);
	}
	
	public boolean getRepeat() {
		return motionRecord.isRepetitionOn();
	}
	
	public Body replayAt(float f) {
		return motionRecord.replayFrame(f);
	}
	
	public float getTime(){
		return motionRecord.getTimePlayed();
	}
	
	public float getDuration(){
		return motionRecord.getBasicDuration();
	}
	
	public void increment(float f) {
		motionRecord.replayFrame(motionRecord.getTimePlayed() + f);
	}
	
	/**
	 * Get the current time and log it. This should be used over "getTime()" when doing manual annotations. 
	 * The log file is formatted to be used for retraining the classifier.
	 * @file The file to log the current time to.
	 */
	public float getTimeAndLog(String file){
		float percent = motionRecord.percentPassed();
		float time = motionRecord.getTimePlayed();
		PrintWriter out = null;
		try {
			System.out.println("File path: " + file);
			int firstSlashIndex = 0;
			firstSlashIndex = file.indexOf(File.separator);
			String filesuffix = file.substring(firstSlashIndex + 1);
//			out = new PrintWriter(new FileWriter(ViewpointsFrontEnd.PROJECT_HOME + File.separator + "gestures" + File.separator + "time_log.txt", true));
//			File jsg = new File(file);
//			File outFile = new File("study-annotations" + File.separator + jsg.getName() + "_study_time_log.txt");
			File outFile = new File("study-annotations" + File.separator + filesuffix + "_study_time_log.txt");
//			System.out.println("New file path: " + "study-annotations" + File.separator + filesuffix + "_study_time_log.txt");
			if (!outFile.exists()) {
				outFile.createNewFile();
			}
			out = new PrintWriter(new FileWriter(outFile, true));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		out.println("File: " + file);
		out.println("Time: " + time);
		out.println("Percent: " + percent);
		out.close();
		return time;
	}
}
