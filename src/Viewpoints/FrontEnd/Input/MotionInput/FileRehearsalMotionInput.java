package Viewpoints.FrontEnd.Input.MotionInput;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import processing.core.PVector;
import Viewpoints.FrontEnd.Clock.Clock;
import Viewpoints.FrontEnd.Communication.File.FileUtilities;
import Viewpoints.FrontEnd.Gesture.JointSpaceGesture.JointSpaceGesture;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.JIDX;
import Viewpoints.FrontEnd.Shared.JointSpaceGestureInterpolator;

public class FileRehearsalMotionInput implements MotionInput {
	private boolean isInit = false;
	private JointSpaceGestureInterpolator motionRecord = null;
	private ArrayList<JointSpaceGesture> gestureList = new ArrayList<JointSpaceGesture>();
	private int currentGestureIndex = 0;
	private int playCount = 0;
	private final int MAX_PLAY_COUNT = 10;
	private final float REPLAY_SPEED = 0.75f;
	private float previousTimeLapsed = 0;
	private float timeLapsed = 0;
	private int rehearsalCompleteRepetitions = 0;
	
	public FileRehearsalMotionInput(String filename, int limit) {
		String rehearsalFile;
		File rehearsalGestureFolder = new File(filename);
		File[] rehearsalGestureFileList = rehearsalGestureFolder.listFiles(); 
		if(limit > 0){
			//has repeats
			for (int i = 0; i < limit; i++) {
				int index = new Random().nextInt(rehearsalGestureFileList.length);
				if (rehearsalGestureFileList[index].isFile()) {
					rehearsalFile = rehearsalGestureFileList[index].getName();
					if (rehearsalFile.toLowerCase().endsWith(".jsg")) {
	//					System.out.println("Imported Rehearsal Gesture File: " + filename + rehearsalFile);					
						this.gestureList.add(FileUtilities.deserializeJointsGesture(filename + rehearsalFile));
					}
				}
			}
		}else{
			for (int i = 0; i < rehearsalGestureFileList.length; i++) {
				if (rehearsalGestureFileList[i].isFile()) {
					rehearsalFile = rehearsalGestureFileList[i].getName();
					if (rehearsalFile.toLowerCase().endsWith(".jsg")) {
	//					System.out.println("Imported Rehearsal Gesture File: " + filename + rehearsalFile);					
						this.gestureList.add(FileUtilities.deserializeJointsGesture(filename + rehearsalFile));
					}
				}
			}
		}
		if(this.gestureList.size() == 0) {
			System.out.println("No Rehearsal Gesture Files In Folder: " + filename);
		}
		else {
			System.out.println("Loaded " + this.gestureList.size() + " Rehearsal Gesture Files");
		}
	}
	
	@Override
	public boolean init() {
		JointSpaceGesture jsg = this.gestureList.get(this.currentGestureIndex);
		if (null != jsg) {
			this.motionRecord = new JointSpaceGestureInterpolator(jsg.getGestureFramesList());
			this.motionRecord.setReplaySpeed(REPLAY_SPEED);
			System.out.println("Loaded new Rehearsal Gesture at index " + this.currentGestureIndex);
			this.isInit = true;
		} else {
			System.out.println("Failed to load new Rehearsal Gesture at index " + this.currentGestureIndex);
		}
		return this.isInit;
	}
	
	private boolean initNextGesture() {
		this.isInit = false;
		int previousIndex = this.currentGestureIndex;
		this.currentGestureIndex = ++this.currentGestureIndex % this.gestureList.size();
		if(this.currentGestureIndex < previousIndex) {
			this.rehearsalCompleteRepetitions++;
		}
		JointSpaceGesture jsg = this.gestureList.get(this.currentGestureIndex);
		if (null != jsg) {
			this.motionRecord = new JointSpaceGestureInterpolator(jsg.getGestureFramesList());
			this.motionRecord.setReplaySpeed(REPLAY_SPEED);
			System.out.println("Loaded next Rehearsal Gesture at index " + this.currentGestureIndex);
			this.isInit = true;
		} else {
			System.out.println("Failed to load next Rehearsal Gesture at index " + this.currentGestureIndex);
		}
		return this.isInit;
	}

	@Override
	public boolean isInit() {
		return this.isInit;
	}

	@Override
	public Body next(Clock clock) {
		this.previousTimeLapsed = this.timeLapsed;
		this.timeLapsed = this.motionRecord.getTimeInCurrentCycle();
//		System.out.println("Time Lapsed: " + timeLapsed);
		if(this.timeLapsed < this.previousTimeLapsed) {
			++this.playCount;
//			System.out.println("Rehearsal Gesture Played Once. Play Count: " + this.playCount);
		}
		if(this.playCount > this.MAX_PLAY_COUNT) {
			System.out.println("Rehearsal Gesture Played More Than Play Max Play Count");
			if(initNextGesture()) {
				System.out.println("Rehearsing next gesture");
				this.playCount = 0;
			}
		}
		float deltatime = clock.deltatime();
//		System.out.println("Frame displayed: " + this.motionRecord.replayFrame(deltatime).toString());
		return this.motionRecord.replayFrame(deltatime);
	}

	public float getPercentRehearsed() {
		float current = this.currentGestureIndex + 1;
		float total = this.gestureList.size();
		float repetitions = rehearsalCompleteRepetitions;
		
		return (repetitions + (current / total)) * 100;
	}
	
	public static Body getIdealUserPose()
	{
		Body idealUserPose = new Body();
		//Set Skeleton Coordinates for ideal user Body
		idealUserPose.set(JIDX.HEAD,new PVector(61.86306f, 800.0279f, 1792.6035f));
		idealUserPose.set(JIDX.NECK,new PVector(49.31178f, 482.05545f, 1871.1399f));
		idealUserPose.set(JIDX.LEFT_SHOULDER,new PVector(-117.19889f, 473.39166f, 1895.8171f));
		idealUserPose.set(JIDX.LEFT_ELBOW,new PVector(-234.26807f, 214.76712f, 1940.3701f));
		idealUserPose.set(JIDX.LEFT_HAND,new PVector(-502.1734f, 154.27269f, 1752.4586f));
		idealUserPose.set(JIDX.LEFT_FINGERTIP,new PVector(61.86306f, 800.0279f, 1792.6035f));
		idealUserPose.set(JIDX.RIGHT_SHOULDER,new PVector(215.82245f, 490.7192f, 1846.4626f));
		idealUserPose.set(JIDX.RIGHT_ELBOW,new PVector(380.55743f, 234.44539f, 1845.75f));
		idealUserPose.set(JIDX.RIGHT_HAND,new PVector(641.58655f, 210.31554f, 1670.9052f));
		idealUserPose.set(JIDX.RIGHT_FINGERTIP,new PVector(61.86306f, 800.0279f, 1792.6035f));
		idealUserPose.set(JIDX.TORSO,new PVector(56.47932f, 240.32893f, 1834.6368f));
		idealUserPose.set(JIDX.LEFT_HIP,new PVector(-43.330074f, -6.9635806f, 1813.9879f));
		idealUserPose.set(JIDX.LEFT_KNEE,new PVector(-31.571592f, -493.167f, 1824.4634f));
		idealUserPose.set(JIDX.LEFT_FOOT,new PVector(18.669317f, -967.7855f, 1913.0208f));
		idealUserPose.set(JIDX.RIGHT_HIP,new PVector(170.6238f, 4.1684284f, 1782.2798f));
		idealUserPose.set(JIDX.RIGHT_KNEE,new PVector(193.66336f, -464.59735f, 1739.4097f));
		idealUserPose.set(JIDX.RIGHT_FOOT,new PVector(221.68254f, -936.3788f, 1838.4629f));
		return idealUserPose;
	}
	
	public void randStart(){
		currentGestureIndex = new Random().nextInt(this.gestureList.size());
	}
}
