package Viewpoints.FrontEnd.Input.MotionInput;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import Viewpoints.FrontEnd.Clock.Clock;
import Viewpoints.FrontEnd.Communication.File.FileUtilities;
import Viewpoints.FrontEnd.Gesture.JointSpaceGesture.JointSpaceGesture;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.JointSpaceGestureInterpolator;

public class FileCueingMotionInput implements MotionInput
{
	private boolean isInit = false;
	private JointSpaceGestureInterpolator motionRecord = null;
	private ArrayList<JointSpaceGesture> gestureList = new ArrayList<JointSpaceGesture>();
	private int currentGestureIndex = 0;
//	private int playCount = 0;
//	private final int MAX_PLAY_COUNT = 1;
	private final float REPLAY_SPEED = 0.5f;
	private float previousTimeLapsed = 0;
	private float timeLapsed = 0;
	private boolean isPlaying = false;
	private Body currentBody = null;

	public FileCueingMotionInput(String filename, int limit)
	{
		String rehearsalFile;
		File rehearsalGestureFolder = new File(filename);
		File[] rehearsalGestureFileList = rehearsalGestureFolder.listFiles();
		if (limit > 0)
		{
			// has repeats
			for (int i = 0; i < limit; i++)
			{
				int index = new Random().nextInt(rehearsalGestureFileList.length);
				if (rehearsalGestureFileList[index].isFile())
				{
					rehearsalFile = rehearsalGestureFileList[index].getName();
					if (rehearsalFile.toLowerCase().endsWith(".jsg"))
					{
						// System.out.println("Imported Rehearsal Gesture File:
						// " + filename + rehearsalFile);
						this.gestureList.add(FileUtilities.deserializeJointsGesture(filename + rehearsalFile));
					}
				}
			}
		}
		else
		{
			for (int i = 0; i < rehearsalGestureFileList.length; i++)
			{
				if (rehearsalGestureFileList[i].isFile())
				{
					rehearsalFile = rehearsalGestureFileList[i].getName();
					if (rehearsalFile.toLowerCase().endsWith(".jsg"))
					{
//						 System.out.println("Imported Cued Gesture File:
						// " + filename + rehearsalFile);
						this.gestureList.add(FileUtilities.deserializeJointsGesture(filename + rehearsalFile));
					}
				}
			}
		}
		if (this.gestureList.size() == 0)
		{
			System.out.println("No Cued Gesture Files In Folder: " + filename);
		}
		else
		{
			System.out.println("Loaded " + this.gestureList.size() + " Cued Gesture Files");
		}
	}

	@Override
	public boolean init()
	{
		JointSpaceGesture jsg = this.gestureList.get(this.currentGestureIndex);
		if (null != jsg)
		{
			this.motionRecord = new JointSpaceGestureInterpolator(jsg.getGestureFramesList());
			this.motionRecord.setReplaySpeed(REPLAY_SPEED);
			this.motionRecord.setDirectRepetition(false);
			System.out.println("Loaded new Cued Gesture at index " + this.currentGestureIndex);
			this.isInit = true;
		}
		else
		{
			System.out.println("Failed to load new Cued Gesture at index " + this.currentGestureIndex);
		}
		return this.isInit;
	}
	
	private boolean initNextGesture()
	{
		this.isInit = false;
//		int previousIndex = this.currentGestureIndex;
		this.currentGestureIndex = ++this.currentGestureIndex % this.gestureList.size();
//		if (this.currentGestureIndex < previousIndex)
//		{
//			this.rehearsalCompleteRepetitions++;
//		}
		JointSpaceGesture jsg = this.gestureList.get(this.currentGestureIndex);
		if (null != jsg)
		{
			this.motionRecord = new JointSpaceGestureInterpolator(jsg.getGestureFramesList());
			this.motionRecord.setReplaySpeed(REPLAY_SPEED);
			this.motionRecord.setDirectRepetition(false);
			System.out.println("Loaded next cued gesture at index " + this.currentGestureIndex);
			this.isInit = true;
		}
		else
		{
			System.out.println("Failed to load next cued gesture at index " + this.currentGestureIndex);
		}
		return this.isInit;
	}

	@Override
	public boolean isInit()
	{
		return this.isInit;
	}

	@Override
	public Body next(Clock clock)
	{
		this.previousTimeLapsed = this.timeLapsed;
		this.timeLapsed = this.motionRecord.getTimeInCurrentCycle();
		// System.out.println("Time Lapsed: " + timeLapsed);
		if (this.timeLapsed < this.previousTimeLapsed || !isPlaying)
		{
//			++this.playCount;
//			System.out.println("Cued Gesture Played Once. Play Count: " + this.playCount);
			this.isPlaying = false;
			return currentBody;
		}
		float deltatime = clock.deltatime();
		System.out.println("Frame displayed: "/* + this.motionRecord.replayFrame(deltatime).toString()*/);
		currentBody = this.motionRecord.replayFrame(deltatime);
		return currentBody;
	}
	
	public void cueNext()
	{
		if (initNextGesture())
		{
			System.out.println("Cueing next gesture");
//			this.playCount = 0;
			this.isPlaying = true;
		}
	}
}
