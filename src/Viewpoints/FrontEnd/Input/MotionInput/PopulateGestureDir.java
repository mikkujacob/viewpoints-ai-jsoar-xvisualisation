package Viewpoints.FrontEnd.Input.MotionInput;

import java.util.*;

import Viewpoints.FrontEnd.Shared.*;

import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public class PopulateGestureDir {
	private static String GESTURE_DIR = "gestures";
	private static String[] SOURCE_DIRS = {"training-gestures/angry-golden",
											"training-gestures/joy-golden",
											"training-gestures/fear-golden",
											"training-gestures/sad-golden",
											"training-gestures/noemotion-verified"};
	private static String[] EXCLUDE_DIRS = {"training-gestures/high-smooth-golden",
											"training-gestures/normal-smooth-golden",
											"training-gestures/low-smooth-golden"};
	
	private static Set<String> excludedFiles() {
		Set<String> exset = new HashSet<String>();
		for (String exdir : EXCLUDE_DIRS) {
			File exdirfile = new File(exdir);
			for (String file : exdirfile.list()) {
				exset.add(file);
			}
		}
		return exset;
	}
	
	private static List<Pair<String, String>> targetFiles(Set<String> exset) {
		List<Pair<String, String>> inlist = new ArrayList<Pair<String, String>>();
		for (String indir : SOURCE_DIRS) {
			File indirfile = new File(indir);
			for (String file : indirfile.list()) {
				if (!exset.contains(file)) {
					inlist.add(Pair.of(indir, file));
				}
			}
		}
		return inlist;
	}
	
	private static void cloneFiles(List<Pair<String, String>> inlist) {
		for (Pair<String, String> initem : inlist) {
			String dir = initem.first;
			String file = initem.second;
			Path sourcepath = FileSystems.getDefault().getPath(dir, file);
			Path targetpath = FileSystems.getDefault().getPath(GESTURE_DIR, file);
			try {
				Files.copy(sourcepath, targetpath, StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException e) {
				System.out.println("Copy error with "+sourcepath.toString());
			}
		}
	}
	
	public static void main(String[] args) {
		Set<String> exset = excludedFiles();
		List<Pair<String, String>> targetlist = targetFiles(exset);
		cloneFiles(targetlist);
	}
}
