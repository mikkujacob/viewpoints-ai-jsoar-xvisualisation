package Viewpoints.FrontEnd.Communication.Socket;

import Viewpoints.FrontEnd.Communication.Socket.Shared.BasicSocket;
import Viewpoints.FrontEnd.Communication.Socket.Shared.BasicSocket.TYPE;

/**
 * An abstract parent class for socket use cases that require both sending and receiving data over a socket.
 * The class wraps two BasicSocket objects that are used for the communication, various details for initializing them, 
 * the method to create them, a run loop that spawns two child run loops for processing data over them, and methods 
 * for controlling their execution. Any derived classes must define the addSocketDataFromInputSocketToOutputQueue() method,
 * which is done as addOutputToQueue(inputSocket.receive(<U extends AbstractSocketData>.class, true)); for most cases.
 * Any derived classes must also define the removeSocketDataFromInputQueueToOutputSocket() method. This is done
 * as outputSocket.sendFiltered(removeInputFromQueue(), <T extends AbstractSocketData>.class, this.filter, true);
 * for most cases.
 * @author mikhail.jacob
 *
 * @param <T> The wrapper for data that is input to the socket for sending.
 * @param <U> The wrapper for data that is output from the socket after receiving.
 */
public abstract class AbstractInputOutputSocket<T extends AbstractSocketData, U extends AbstractSocketData> extends AbstractSocketIO<T, U>
{
	/**
	 * A socket used to receive data.
	 */
	protected BasicSocket inputSocket;
	
	/**
	 * Whether the socket is running or not.
	 */
	private boolean isInputSocketRunning = true;

	/**
	 * A socket used to receive data.
	 */
	protected BasicSocket outputSocket;

	/**
	 * Whether the socket is running or not.
	 */
	private boolean isOutputSocketRunning = true;

	/**
	 * The address of the socket.
	 */
	protected String inputAddress;

	/**
	 * The type of socket used to receive data.
	 */
	protected TYPE inputSocketType;

	/**
	 * Whether the socket should bind or connect.
	 */
	protected boolean isInputBind;

	/**
	 * The filter used to subscribe to data that publishers are sending.
	 */
	protected String inputFilter;

	/**
	 * The address of the socket.
	 */
	protected String outputAddress;

	/**
	 * The type of socket used to send data.
	 */
	protected TYPE outputSocketType;

	/**
	 * Whether the socket should bind or connect.
	 */
	protected boolean isOutputBind;

	/**
	 * The filter used to publish data for subscribers that are listening for it.
	 */
	protected String outputFilter;
	
	/**
	 * The public constructor for this class that takes in the address, socket type, isBind, and filter for each socket it constructs.
	 * @param inputAddress The address to receive data from.
	 * @param inputSocketType The type of socket to create.
	 * @param isInputBind Whether to bind to the address or connect.
	 * @param inputFilter The filter used to subscribe to data in order to receive what publishers send.
	 * @param outputAddress The address to send data to.
	 * @param outputSocketType The type of socket to create.
	 * @param isOutputBind Whether to bind to the address or connect.
	 * @param outputFilter The filter used to publish data under in order for subscribers to receive it.
	 */
	public AbstractInputOutputSocket(String inputAddress, TYPE inputSocketType, boolean isInputBind, String inputFilter, String outputAddress, TYPE outputSocketType, boolean isOutputBind, String outputFilter)
	{
		super();
		this.inputAddress = inputAddress;
		this.inputSocketType = inputSocketType;
		this.isInputBind = isInputBind;
		this.inputFilter = inputFilter;
		this.outputAddress = outputAddress;
		this.outputSocketType = outputSocketType;
		this.isOutputBind = isOutputBind;
		this.outputFilter = outputFilter;
		initialize();
	}
	
	/**
	 * The run loop that processes data to be sent and received. The main thread spawns two child 
	 * threads that handle sending and receiving separately. The receiving thread constantly receives 
	 * data that is then removed from the outputQueue onto which it was placed for that purpose. The 
	 * sending thread constantly sends data that has been added to the inputQueue for that purpose.
	 */
	@Override
	public void run()
	{
		new Thread(new Runnable()
		{
			
			@Override
			public void run()
			{
				while(true)
				{
					if(isOutputSocketRunning)
					{
						removeSocketDataFromInputQueueToOutputSocket();
					}
					else
					{
						break;
					}
				}
				
				outputSocket.stop();
			}
		}).start();
		
		new Thread(new Runnable()
		{
			
			@Override
			public void run()
			{
				while(true)
				{
					if(isInputSocketRunning)
					{
						addSocketDataFromInputSocketToOutputQueue();
					}
					else
					{
						break;
					}
				}
				
				inputSocket.stop();
			}
		}).start();
		
		while(true)
		{
			if(!(isInputSocketRunning || isOutputSocketRunning))
			{
				break;
			}
		}
	}

	/**
	 * The method that actually creates the sockets. 
	 */
	@Override
	public void createSockets()
	{
		inputSocket = new BasicSocket(inputAddress, inputSocketType, isInputBind);
		inputSocket.subscribe(inputFilter);
		outputSocket = new BasicSocket(outputAddress, outputSocketType, isOutputBind);
	}
	
	/**
	 * An abstract method that is defined by derived classes in order to add data to the outputQueue internally after receiving it over the socket with the correct data type.
	 */
	public abstract void addSocketDataFromInputSocketToOutputQueue();
	
	/**
	 * An abstract method that is defined by derived classes in order to remove data from the inputQueue internally in order to send over the socket according to the correct data type.
	 */
	public abstract void removeSocketDataFromInputQueueToOutputSocket();

	/**
	 * Whether the socket is running currently.
	 * @return Whether the socket is running currently.
	 */
	public boolean isInputSocketRunning()
	{
		return isInputSocketRunning;
	}

	/**
	 * Set whether the socket should be running.
	 * @param isOutputSocketRunning Whether the socket should be running.
	 */
	public void setInputSocketRunning(boolean isInputSocketRunning)
	{
		this.isInputSocketRunning = isInputSocketRunning;
	}

	/**
	 * Whether the socket is running currently.
	 * @return Whether the socket is running currently.
	 */
	public boolean isOutputSocketRunning()
	{
		return isOutputSocketRunning;
	}

	/**
	 * Set whether the socket should be running.
	 * @param isOutputSocketRunning Whether the socket should be running.
	 */
	public void setOutputSocketRunning(boolean isOutputSocketRunning)
	{
		this.isOutputSocketRunning = isOutputSocketRunning;
	}
}