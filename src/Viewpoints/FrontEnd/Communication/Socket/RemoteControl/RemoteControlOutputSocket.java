package Viewpoints.FrontEnd.Communication.Socket.RemoteControl;

import java.util.Random;

import Viewpoints.FrontEnd.Communication.Socket.AbstractOutputSocket;
import Viewpoints.FrontEnd.Communication.Socket.SocketConstants;
import Viewpoints.FrontEnd.Communication.Socket.RemoteControl.RemoteControlData.RemoteControlPreset;
import Viewpoints.FrontEnd.Communication.Socket.Shared.BasicSocket.TYPE;

public class RemoteControlOutputSocket extends AbstractOutputSocket<RemoteControlData, RemoteControlData>
{

	public RemoteControlOutputSocket(String address, TYPE socketType, boolean isBind, String filter)
	{
		super(address, socketType, isBind, filter);
	}

	public static void main(String[] args)
	{
		RemoteControlOutputSocket remoteControlIO = new RemoteControlOutputSocket(SocketConstants.REMOTE_CONTROL_ADDRESS, SocketConstants.REMOTE_CONTROL_SERVER_TYPE, SocketConstants.IS_REMOTE_CONTROL_SERVER_BIND, SocketConstants.REMOTE_CONTROL_FILTER);
		int maxSendCount = 5000;
		int sendCount = 0;
		RemoteControlData sendingData;
		Random random = new Random();
		
		while(true)
		{
			sendingData = new RemoteControlData();
			sendingData.setPreset(RemoteControlPreset.values()[random.nextInt(RemoteControlPreset.values().length)]);
			remoteControlIO.addInputToQueue(sendingData);
			sendCount++;
			System.out.println("Added an object to inputQueue " + sendCount);
			System.out.println("Object to inputQueue is " + sendingData.convertToString());
			
			if(sendCount >= maxSendCount)
			{
				break;
			}
		}
		remoteControlIO.setOutputSocketRunning(false);
	}

	@Override
	public void removeSocketDataFromInputQueueToOutputSocket()
	{
		Object object = removeInputFromQueue();
		if(object != null)
		{
			outputSocket.sendFiltered(object, RemoteControlData.class, this.filter, true);
		}
	}

}
