package Viewpoints.FrontEnd.Communication.Socket.RemoteControl;

import Viewpoints.FrontEnd.Communication.Socket.AbstractSocketData;

public class RemoteControlData implements AbstractSocketData
{
	public static enum RemoteControlPreset
	{
		ONE,
		TWO,
		THREE,
		FOUR,
		FIVE,
		SIX,
		SEVEN,
		EIGHT,
		NINE,
		ZERO,
		A,
		S,
		D,
		F,
		G,
		H,
		J,
		K,
		L,
		Q,
		W,
		E,
		R,
		T,
		Y,
		U,
		I,
		O,
		P,
		Z,
		X,
		C,
		V,
		B,
		N,
		M,
		NONE
	}
	
	private RemoteControlPreset preset;
	
	public RemoteControlData()
	{
		preset = RemoteControlPreset.NONE;
	}
	
	public RemoteControlData(RemoteControlPreset preset)
	{
		this.preset = preset;
	}
	
	@Override
	public String convertToString()
	{
		return preset.toString();
	}

	public RemoteControlPreset getPreset()
	{
		return preset;
	}

	public void setPreset(RemoteControlPreset preset)
	{
		this.preset = preset;
	}
}
