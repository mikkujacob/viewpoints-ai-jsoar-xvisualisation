package Viewpoints.FrontEnd.Communication.Socket.RemoteControl;

import Viewpoints.FrontEnd.Communication.Socket.AbstractInputSocket;
import Viewpoints.FrontEnd.Communication.Socket.SocketConstants;
import Viewpoints.FrontEnd.Communication.Socket.Shared.BasicSocket.TYPE;

public class RemoteControlInputSocket extends AbstractInputSocket<RemoteControlData, RemoteControlData>
{

	public RemoteControlInputSocket(String address, TYPE socketType, boolean isBind, String filter)
	{
		super(address, socketType, isBind, filter);
	}

	public static void main(String[] args)
	{
		RemoteControlInputSocket remoteControlIO = new RemoteControlInputSocket(SocketConstants.REMOTE_CONTROL_ADDRESS, SocketConstants.REMOTE_CONTROL_CLIENT_TYPE, SocketConstants.IS_REMOTE_CONTROL_CLIENT_BIND, SocketConstants.REMOTE_CONTROL_FILTER);
		int maxReceiveCount = 500;
		int receiveCount = 0;
		RemoteControlData receivedData;
		while(true)
		{
			receivedData = remoteControlIO.removeOutputFromQueue();
			
			if(receivedData != null)
			{
				receiveCount++;
				System.out.println("Received an object from outputQueue " + receiveCount);
				System.out.println("Object from outputQueue is " + receivedData.convertToString());
				receivedData = null;
			}
			
			if(receiveCount >= maxReceiveCount)
			{
				break;
			}
		}
		remoteControlIO.setInputSocketRunning(false);
	}

	@Override
	public void addSocketDataFromInputSocketToOutputQueue()
	{
		addOutputToQueue(inputSocket.receive(RemoteControlData.class, true));
	}

}
