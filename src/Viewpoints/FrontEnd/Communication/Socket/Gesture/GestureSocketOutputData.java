package Viewpoints.FrontEnd.Communication.Socket.Gesture;

import Viewpoints.FrontEnd.Communication.Socket.AbstractSocketData;
import Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture.PredicateSpaceGesture;
import Viewpoints.FrontEnd.Shared.Body;

public class GestureSocketOutputData implements AbstractSocketData
{
	/**
	 * A Body that contains a PVector representation of the 3D positions of a set of enumerated joints at a point in time.
	 */
	private PredicateSpaceGesture gesture;
	
	
	/**
	 * Public constructor for creating this object.
	 * @param body The body to be wrapped.
	 */
	public GestureSocketOutputData(PredicateSpaceGesture gesture)
	{
		this.gesture = gesture;
	}

	/**
	 * A method to convert the body to a String representation.
	 * @return The converted String.
	 */
	@Override
	public String convertToString()
	{
		return gesture.toString();
	}

	/**
	 * Getter for the wrapped body object.
	 * @return The body object.
	 */
	public PredicateSpaceGesture getGesture()
	{
		return gesture;
	}

	/**
	 * Setter for the wrapped body object.
	 * @param body The body object to set.
	 */
	public void setBody(PredicateSpaceGesture gesture)
	{
		this.gesture = gesture;
	}
}
