package Viewpoints.FrontEnd.Communication.Socket;

import Viewpoints.FrontEnd.Communication.Socket.Shared.BasicSocket;
import Viewpoints.FrontEnd.Communication.Socket.Shared.BasicSocket.TYPE;

/**
 * An abstract parent class for socket use cases that only require sending data over a socket and not receiving any data.
 * The class wraps a BasicSocket that is used for the communication, various details for initializing it, 
 * the method to create it, a run loop for processing data over it, and methods for controlling its execution.
 * Any derived classes must define the removeSocketDataFromInputQueueToOutputSocket() method. This is done
 * as outputSocket.sendFiltered(removeInputFromQueue(), <T extends AbstractSocketData>.class, this.filter, true);
 * for most cases.
 * @author mikhail.jacob
 *
 * @param <T> A dummy type that should be kept the same as the class used for U.
 * @param <U> The wrapper for data that is output from the socket after receiving.
 */
public abstract class AbstractOutputSocket<T extends AbstractSocketData, U extends AbstractSocketData> extends AbstractSocketIO<T, U>
{
	/**
	 * A socket used to send data.
	 */
	protected BasicSocket outputSocket;
	
	/**
	 * Whether the socket is running or not.
	 */
	private boolean isOutputSocketRunning = true;

	/**
	 * The address of the socket.
	 */
	protected String address;

	/**
	 * The type of socket used to send data.
	 */
	protected TYPE socketType;

	/**
	 * Whether the socket should bind or connect.
	 */
	protected boolean isBind;

	/**
	 * The filter used to publish data for subscribers that are listening for it.
	 */
	protected String filter;
	
	/**
	 * The public constructor for this class that takes in the address, socket type, isBind, and filter for the socket it constructs.
	 * @param address The address to send data to.
	 * @param socketType The type of socket to create.
	 * @param isBind Whether to bind to the address or connect.
	 * @param filter The filter used to publish data under in order for subscribers to receive it. 
	 */
	public AbstractOutputSocket(String address, TYPE socketType, boolean isBind, String filter)
	{
		super();
		this.address = address;
		this.socketType = socketType;
		this.isBind = isBind;
		this.filter = filter;
		initialize();
	}

	/**
	 * The run loop that processes data and constantly sends data that has been added to the inputQueue for that purpose.
	 */
	@Override
	public void run()
	{
		while(true)
		{
			if(isOutputSocketRunning)
			{
				removeSocketDataFromInputQueueToOutputSocket();
			}
			else
			{
				break;
			}
		}
		
		outputSocket.stop();
	}

	/**
	 * The method that actually creates the socket.
	 */
	@Override
	public void createSockets()
	{
		outputSocket = new BasicSocket(address, socketType, isBind);
	}
	
	/**
	 * An abstract method that is defined by derived classes in order to remove data from the inputQueue internally in order to send over the socket according to the correct data type. 
	 */
	public abstract void removeSocketDataFromInputQueueToOutputSocket();

	/**
	 * Whether the socket is running currently.
	 * @return Whether the socket is running currently.
	 */
	public boolean isOutputSocketRunning()
	{
		return isOutputSocketRunning;
	}

	/**
	 * Set whether the socket should be running.
	 * @param isOutputSocketRunning Whether the socket should be running.
	 */
	public void setOutputSocketRunning(boolean isOutputSocketRunning)
	{
		this.isOutputSocketRunning = isOutputSocketRunning;
	}
}