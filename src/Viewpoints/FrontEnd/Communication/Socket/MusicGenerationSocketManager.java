package Viewpoints.FrontEnd.Communication.Socket;

import Viewpoints.FrontEnd.Communication.Socket.SonificationParameters.SonificationParametersData;
import Viewpoints.FrontEnd.Communication.Socket.SonificationParameters.SonificationParametersInputSocket;

/**
 * The SocketManager class creates sockets for communication between GENII
 * and other applications, data sinks, and data sources. 
 * @author mikhail.jacob
 *
 */
public class MusicGenerationSocketManager implements Runnable
{
	/**
	 * Is the SocketManager thread currently running. Toggle this to control the thread.
	 */
	private boolean isRunning;
	
	/**
	 * The minimum time the system would take to send new information across sockets.
	 */
	private final long refreshTimeMilliseconds = 3000;
	
	/**
	 * Thread to control the running of this class.
	 */
	private Thread socketManagerThread;
	
	/**
	 * The current set of sonification parameters.
	 */
	private SonificationParametersData currentSonificationParameters;
	
	/**
	 * An output socket that receives sonification parameters from an external transmitter.
	 */
	private SonificationParametersInputSocket sonificationParametersIO;
	
	/**
	 * Public constructor that instantiates, initializes, and starts running the sockets.
	 */
	public MusicGenerationSocketManager()
	{
		isRunning = true;
		sonificationParametersIO = new SonificationParametersInputSocket(
				SocketConstants.SONIFICATION_PARAMETERS_ADDRESS, 
				SocketConstants.SONIFICATION_PARAMETERS_CLIENT_TYPE, 
				SocketConstants.IS_SONIFICATION_PARAMETERS_CLIENT_BIND, 
				SocketConstants.SONIFICATION_PARAMETERS_FILTER);
		
		socketManagerThread = new Thread(this);
		socketManagerThread.start();
	}

	/**
	 * The main run loop of the socket manager that tries (until toggled off) 
	 * to send data every refreshTimeMilliseconds milliseconds.
	 */
	@Override
	public void run()
	{
		while(true)
		{
			if(!isRunning)
			{
				break;
			}
			
			currentSonificationParameters = sonificationParametersIO.removeOutputFromQueue();
			
			try
			{
				Thread.sleep(refreshTimeMilliseconds);
			}
			catch (InterruptedException e)
			{
				//Woke up ahead of schedule.
			}
		}
	}

	/**
	 * Gets whether the thread is currently running or not.
	 * @return
	 */
	public boolean isRunning()
	{
		return isRunning;
	}

	/**
	 * Sets / toggles whether the thread should be running or not.
	 * @param isRunning the desired state of the thread.
	 */
	public void setRunning(boolean isRunning)
	{
		this.isRunning = isRunning;
	}

	/**
	 * Gets the current set of sonification parameters.
	 * @return Current set of sonification parameters.
	 */
	public SonificationParametersData getCurrentSonificationParameters()
	{
		return currentSonificationParameters;
	}

	/**
	 * Sets the current set of sonification parameters to the desired value.
	 * @param currentSonificationParameters - desired set of sonification parameters.
	 */
	public void setCurrentSonificationParameters(SonificationParametersData currentSonificationParameters)
	{
		this.currentSonificationParameters = currentSonificationParameters;
	}
}