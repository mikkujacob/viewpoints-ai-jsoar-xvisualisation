package Viewpoints.FrontEnd.Communication.Socket.Perception;

import Viewpoints.FrontEnd.Communication.Socket.AbstractInputSocket;
import Viewpoints.FrontEnd.Communication.Socket.SocketConstants;
import Viewpoints.FrontEnd.Communication.Socket.Shared.BasicSocket.TYPE;

/**
 * Class the concretely defines an input socket for perception.
 * @author mikhail.jacob
 *
 */
public class PerceptionInputSocket extends AbstractInputSocket<PerceptionSocketOutputData, PerceptionSocketOutputData>
{
	/**
	 * Public constructor that constructs, initializes, and starts running the PerceptionInputSocket.
	 * @param address The address to receive data from.
	 * @param socketType The type of socket to create.
	 * @param isBind Whether to bind to the address or connect.
	 * @param filter The filter used to subscribe to data in order to receive what publishers send.
	 */
	public PerceptionInputSocket(String address, TYPE socketType, boolean isBind, String filter)
	{
		super(address, socketType, isBind, filter);
	}
	
	/**
	 * Main method to start execution to test this class.
	 * @param args Runtime arguments.
	 */
	public static void main(String[] args)
	{
		PerceptionInputSocket perceptionIO = new PerceptionInputSocket(SocketConstants.PERCEPTION_ADDRESS, TYPE.SUB, true, SocketConstants.PERCEPTION_FILTER);
//		int maxReceiveCount = 500;
//		int receiveCount = 0;
		PerceptionSocketOutputData receivedData;
		
		while(true)
		{
			// System.out.println("test");
			receivedData = perceptionIO.removeOutputFromQueue();
			// System.out.println(receivedData);
			if(receivedData != null)
			{
//				receiveCount++;
//				System.out.println("Received an object from outputQueue " + receiveCount);
				System.out.println("Object from outputQueue is " + receivedData.convertToString());
				receivedData = null;
			}
			
//			if(receiveCount >= maxReceiveCount)
//			{
//				break;
//			}
		}
//		perceptionIO.setInputSocketRunning(false);
	}

	/**
	 * Method to add data to the outputQueue internally after receiving it over the socket with the PerceptionSocketOutputData class definition.
	 */
	@Override
	public void addSocketDataFromInputSocketToOutputQueue()
	{
		addOutputToQueue(inputSocket.receive(PerceptionSocketOutputData.class, true));
	}
}