package Viewpoints.FrontEnd.Communication.Socket.Action;

import Viewpoints.FrontEnd.Communication.Socket.AbstractOutputSocket;
import Viewpoints.FrontEnd.Communication.Socket.SocketConstants;
import Viewpoints.FrontEnd.Communication.Socket.Shared.BasicSocket.TYPE;
import Viewpoints.FrontEnd.Input.MotionInput.FileRehearsalMotionInput;

/**
 * Class that concretely defines an output socket for actions.
 * @author mikhail.jacob
 *
 */
public class ActionOutputSocket extends AbstractOutputSocket<ActionSocketInputData, ActionSocketInputData>
{
	/**
	 * Public constructor that constructs, initializes, and starts running the ActionOutputSocket.
	 * @param address The address to send data to.
	 * @param socketType The type of socket to create.
	 * @param isBind Whether to bind to the address or connect.
	 * @param filter The filter used to publish data under in order for subscribers to receive it.
	 */
	public ActionOutputSocket(String address, TYPE socketType, boolean isBind, String filter)
	{
		super(address, socketType, isBind, filter);
	}
	
	/**
	 * Main method to start execution to test this class.
	 * @param args
	 */
	public static void main(String[] args)
	{
		ActionOutputSocket actionIO = new ActionOutputSocket(SocketConstants.ACTION_ADDRESS, TYPE.PUB, true, SocketConstants.ACTION_FILTER);
//		int maxSendCount = 5000;
//		int sendCount = 0;
		ActionSocketInputData sendingData;
		
		while(true)
		{
			sendingData = new ActionSocketInputData(FileRehearsalMotionInput.getIdealUserPose());
			actionIO.addInputToQueue(sendingData);
//			sendCount++;
//			System.out.println("Added an object to inputQueue " + sendCount);
			System.out.println("Object to inputQueue is " + sendingData.convertToString());
			
//			if(sendCount >= maxSendCount)
//			{
//				break;
//			}
		}
		//actionIO.setOutputSocketRunning(false);
	}

	/**
	 * Method to remove data from the inputQueue internally in order to send over the socket according to the ActionSocketInputData class definition.
	 */
	@Override
	public void removeSocketDataFromInputQueueToOutputSocket()
	{
		outputSocket.sendFiltered(removeInputFromQueue(), ActionSocketInputData.class, this.filter, true);
	}
}
