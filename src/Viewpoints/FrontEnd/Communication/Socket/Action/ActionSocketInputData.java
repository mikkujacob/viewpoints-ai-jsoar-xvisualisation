package Viewpoints.FrontEnd.Communication.Socket.Action;

import Viewpoints.FrontEnd.Communication.Socket.AbstractSocketData;
import Viewpoints.FrontEnd.Shared.Body;

/**
 * Class that wraps the objects that are input to the ActionOutputSocket to be sent by it.
 * @author mikhail.jacob
 *
 */
public class ActionSocketInputData implements AbstractSocketData
{
	/**
	 * A Body that contains a PVector representation of the 3D positions of a set of enumerated joints at a point in time.
	 */
	private Body body;
	
	/**
	 * Public constructor for creating this object.
	 * @param body The body to be wrapped.
	 */
	public ActionSocketInputData(Body body)
	{
		this.body = body;
	}

	/**
	 * A method to convert the body to a String representation.
	 * @return The converted String.
	 */
	@Override
	public String convertToString()
	{
		return body.toString();
	}

	/**
	 * Getter for the wrapped body object.
	 * @return The body object.
	 */
	public Body getBody()
	{
		return body;
	}

	/**
	 * Setter for the wrapped body object.
	 * @param body The body object to set.
	 */
	public void setBody(Body body)
	{
		this.body = body;
	}
}
