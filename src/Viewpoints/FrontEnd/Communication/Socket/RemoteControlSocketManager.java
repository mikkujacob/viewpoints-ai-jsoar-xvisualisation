package Viewpoints.FrontEnd.Communication.Socket;

import Viewpoints.FrontEnd.Communication.Socket.RemoteControl.RemoteControlData;
import Viewpoints.FrontEnd.Communication.Socket.RemoteControl.RemoteControlData.RemoteControlPreset;
import Viewpoints.FrontEnd.Communication.Socket.RemoteControl.RemoteControlOutputSocket;

public class RemoteControlSocketManager
{
	/**
	 * An output socket that sends sonification parameters to an external receiver.
	 */
	private RemoteControlOutputSocket remoteControlIO;
	
	/**
	 * Public constructor that instantiates, initializes, and starts running the sockets.
	 */
	public RemoteControlSocketManager()
	{
		remoteControlIO = new RemoteControlOutputSocket(
				SocketConstants.REMOTE_CONTROL_ADDRESS, 
				SocketConstants.REMOTE_CONTROL_SERVER_TYPE, 
				SocketConstants.IS_REMOTE_CONTROL_SERVER_BIND, 
				SocketConstants.REMOTE_CONTROL_FILTER);
	}
	
	public void togglePreset(RemoteControlPreset targetPreset)
	{
		remoteControlIO.addInputToQueue(new RemoteControlData(targetPreset));
	}
}
