package Viewpoints.FrontEnd.Communication.Socket;

import Viewpoints.FrontEnd.Communication.Socket.Shared.BasicSocket.TYPE;

/**
 * Class defines constants and other shared resources that are used to instantiate sockets.
 * @author mikhail.jacob
 *
 */
public class SocketConstants
{
	//SocketManager Constants
	
	//ADDRESS CONSTANTS
	
	//Default transport is assumed to be TCP
	//Default address is LOCALHOST or 127.0.0.1
	//EPHEMERAL PORT RANGE: 49152–65535
	
	/**
	 * The address string for the Action server socket.
	 */
	public static final String ACTION_ADDRESS = "tcp://localhost:50000";

	/**
	 * The address string for the Perception client socket.
	 */
	public static final String PERCEPTION_ADDRESS = "tcp://localhost:50010";
	
	/**
	 * The address string for the GESTURE server socket.
	 */
	public static final String GESTURE_ADDRESS = "tcp://localhost:50020";
	
	/**
	 * The address string for the Sonification Parameters.
	 */
//	public static final String SONIFICATION_PARAMETERS_ADDRESS = "tcp://128.61.77.105:80";
	public static final String SONIFICATION_PARAMETERS_ADDRESS = "tcp://localhost:50030";
	
	/**
	 * The address string for the Remote Controlling installation machines.
	 */
//	public static final String REMOTE_CONTROL_ADDRESS = "tcp://128.61.77.105:80";
	public static final String REMOTE_CONTROL_ADDRESS = "tcp://localhost:50040";

	//FILTER CONSTANTS
	
	/**
	 * The string used to subscribe to percepts from the publisher socket.
	 */
	public static final String PERCEPTION_FILTER = "PERCEPTION";
	
	/**
	 * The string used to subscribe to acts from the publisher socket.
	 */
	public static final String ACTION_FILTER = "ACTION";
	
	/**
	 * The string used to subscribe to gestures from the publisher socket.
	 */
	public static final String GESTURE_FILTER = "GESTURE";
	
	/**
	 * The string used to subscribe to sonification parameters from the publisher socket.
	 */
	public static final String SONIFICATION_PARAMETERS_FILTER = "SONIFICATION";
	
	/**
	 * The string used to subscribe to remote control signals from the publisher socket.
	 */
	public static final String REMOTE_CONTROL_FILTER = "REMOTE_CONTROL";
	
	//SOCKET TYPE CONSTANTS
	
	//PUB, //PUBLISHER sends data to optional SUBSCRIBERs
	//SUB, //SUBSCRIBER receives filtered data from a PUBLISHER
	
	/**
	 * The socket type of the perception socket.
	 */
	public static final TYPE PERCEPTION_TYPE = TYPE.SUB;

	/**
	 * The socket type of the action socket.
	 */
	public static final TYPE ACTION_TYPE = TYPE.PUB;
	
	/**
	 * The socket type of the gesture socket.
	 */
	public static final TYPE GESTURE_TYPE = TYPE.PUB;
	
	/**
	 * The socket type of the sonification parameters server socket.
	 */
	public static final TYPE SONIFICATION_PARAMETERS_SERVER_TYPE = TYPE.PUB;
	
	/**
	 * The socket type of the sonification parameters client socket.
	 */
	public static final TYPE SONIFICATION_PARAMETERS_CLIENT_TYPE = TYPE.SUB;
	
	/**
	 * The socket type of the remote control server socket.
	 */
	public static final TYPE REMOTE_CONTROL_SERVER_TYPE = TYPE.PUB;
	
	/**
	 * The socket type of the remote control client socket.
	 */
	public static final TYPE REMOTE_CONTROL_CLIENT_TYPE = TYPE.SUB;
	
	//IS BIND CONSTANTS

	/**
	 * Whether the perception socket should bind or connect.
	 */
	public static final boolean IS_PERCEPTION_BIND = false;

	/**
	 * Whether the action socket should bind or connect.
	 */
	public static final boolean IS_ACTION_BIND = true;
	
	/**
	 * Whether the gesture socket should bind or connect.
	 */
	public static final boolean IS_GESTURE_BIND = true;
	
	/**
	 * Whether the sonification parameters server socket should bind or connect.
	 */
	public static final boolean IS_SONIFICATION_PARAMETERS_SERVER_BIND = true;
	
	/**
	 * Whether the sonification parameters client socket should bind or connect.
	 */
	public static final boolean IS_SONIFICATION_PARAMETERS_CLIENT_BIND = false;
	
	/**
	 * Whether the remote control server socket should bind or connect.
	 */
	public static final boolean IS_REMOTE_CONTROL_SERVER_BIND = true;
	
	/**
	 * Whether the remote control client socket should bind or connect.
	 */
	public static final boolean IS_REMOTE_CONTROL_CLIENT_BIND = false;
}
