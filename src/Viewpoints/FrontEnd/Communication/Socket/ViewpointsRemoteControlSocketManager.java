package Viewpoints.FrontEnd.Communication.Socket;

import Viewpoints.FrontEnd.Communication.Socket.RemoteControl.RemoteControlData;
import Viewpoints.FrontEnd.Communication.Socket.RemoteControl.RemoteControlInputSocket;

public class ViewpointsRemoteControlSocketManager implements Runnable
{
	/**
	 * Is the SocketManager thread currently running. Toggle this to control the thread.
	 */
	private boolean isRunning;
	
	/**
	 * The minimum time the system would take to send new information across sockets.
	 */
	private final long refreshTimeMilliseconds = 1000;
	
	/**
	 * Thread to control the running of this class.
	 */
	private Thread socketManagerThread;
	
	/**
	 * The current set of sonification parameters.
	 */
	private RemoteControlData currentRemoteControlData;
	
	/**
	 * An output socket that receives sonification parameters from an external transmitter.
	 */
	private RemoteControlInputSocket remoteControlIO;
	
	/**
	 * Public constructor that instantiates, initializes, and starts running the sockets.
	 */
	public ViewpointsRemoteControlSocketManager()
	{
		isRunning = true;
		remoteControlIO = new RemoteControlInputSocket(
				SocketConstants.REMOTE_CONTROL_ADDRESS, 
				SocketConstants.REMOTE_CONTROL_CLIENT_TYPE, 
				SocketConstants.IS_REMOTE_CONTROL_CLIENT_BIND, 
				SocketConstants.REMOTE_CONTROL_FILTER);
		
		socketManagerThread = new Thread(this);
		socketManagerThread.start();
	}

	/**
	 * The main run loop of the socket manager that tries (until toggled off) 
	 * to send data every refreshTimeMilliseconds milliseconds.
	 */
	@Override
	public void run()
	{
		while(true)
		{
			if(!isRunning)
			{
				break;
			}
			
			currentRemoteControlData = remoteControlIO.removeOutputFromQueue();
			
			try
			{
				Thread.sleep(refreshTimeMilliseconds);
			}
			catch (InterruptedException e)
			{
				//Woke up ahead of schedule.
			}
		}
	}

	/**
	 * Gets whether the thread is currently running or not.
	 * @return
	 */
	public boolean isRunning()
	{
		return isRunning;
	}

	/**
	 * Sets / toggles whether the thread should be running or not.
	 * @param isRunning the desired state of the thread.
	 */
	public void setRunning(boolean isRunning)
	{
		this.isRunning = isRunning;
	}

	/**
	 * Gets the current set of sonification parameters.
	 * @return Current set of sonification parameters.
	 */
	public RemoteControlData getCurrentRemoteControlData()
	{
		return currentRemoteControlData;
	}

	/**
	 * Sets the current set of sonification parameters to the desired value.
	 * @param currentRemoteControlData - desired set of sonification parameters.
	 */
	public void setCurrentRemoteControlData(RemoteControlData currentRemoteControlData)
	{
		this.currentRemoteControlData = currentRemoteControlData;
	}
}
