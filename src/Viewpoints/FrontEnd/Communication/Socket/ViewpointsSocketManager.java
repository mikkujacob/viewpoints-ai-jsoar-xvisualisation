package Viewpoints.FrontEnd.Communication.Socket;

import Viewpoints.FrontEnd.Communication.Socket.SonificationParameters.SonificationParametersData;
import Viewpoints.FrontEnd.Communication.Socket.SonificationParameters.SonificationParametersOutputSocket;
import Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture.PredicateSpaceGesture;
import Viewpoints.FrontEnd.Model.AestheticsModel.ViewpointsAestheticsModel;
import Viewpoints.FrontEnd.Shared.JIDX;

/**
 * The SocketManager class creates sockets for communication between GENII
 * and other applications, data sinks, and data sources. 
 * @author mikhail.jacob
 *
 */
public class ViewpointsSocketManager implements Runnable
{
	/**
	 * Is the SocketManager thread currently running. Toggle this to control the thread.
	 */
	private boolean isRunning;
	
	/**
	 * The minimum time the system would take to send new information across sockets.
	 */
	private final long refreshTimeMilliseconds = 3000;
	
	/**
	 * Thread to control the running of this class.
	 */
	private Thread socketManagerThread;
	
//	/**
//	 * An input socket that receives percepts from an external transmitter.
//	 */
//	private PerceptionInputSocket perceptionIO;
//	
//	/**
//	 * An output socket that sends actions to an external receiver.
//	 */
//	private ActionOutputSocket actionIO;
	
	/**
	 * An output socket that sends sonification parameters to an external receiver.
	 */
	private SonificationParametersOutputSocket sonificationParametersIO;
	
	/**
	 * Public constructor that instantiates, initializes, and starts running the sockets.
	 */
	public ViewpointsSocketManager()
	{
//		perceptionIO = new PerceptionInputSocket(SocketConstants.PERCEPTION_ADDRESS, SocketConstants.PERCEPTION_TYPE, SocketConstants.IS_PERCEPTION_BIND, SocketConstants.PERCEPTION_FILTER);
//		actionIO = new ActionOutputSocket(SocketConstants.ACTION_ADDRESS, SocketConstants.ACTION_TYPE, SocketConstants.IS_ACTION_BIND, SocketConstants.ACTION_FILTER);
		
		isRunning = true;
		sonificationParametersIO = new SonificationParametersOutputSocket(
				SocketConstants.SONIFICATION_PARAMETERS_ADDRESS, 
				SocketConstants.SONIFICATION_PARAMETERS_SERVER_TYPE, 
				SocketConstants.IS_SONIFICATION_PARAMETERS_SERVER_BIND, 
				SocketConstants.SONIFICATION_PARAMETERS_FILTER);
		
		socketManagerThread = new Thread(this);
		socketManagerThread.start();
	}

	/**
	 * The main run loop of the socket manager that tries (until toggled off) 
	 * to send data every refreshTimeMilliseconds milliseconds.
	 */
	@Override
	public void run()
	{
		while(true)
		{
			if(!isRunning)
			{
				break;
			}
			
			sonificationParametersIO.addInputToQueue(getSonificationParametersData());
			
			try
			{
				Thread.sleep(refreshTimeMilliseconds);
			}
			catch (InterruptedException e)
			{
				//Woke up ahead of schedule.
			}
		}
	}
	
	/**
	 * Method that gets sonification parameters data from the ViewpointsAestheticModel 
	 * class and creates a new data object containing the values for that time. 
	 * @return The SonificationParametersData object containing the data.
	 */
	private SonificationParametersData getSonificationParametersData()
	{
		SonificationParametersData data;
		try
		{
			data = new SonificationParametersData();
			
			//viewpointsTracker
			data.setEnergy(ViewpointsAestheticsModel.getViewpointsTracker().getEnergy());
			data.setSmoothness(ViewpointsAestheticsModel.getViewpointsTracker().getSmoothness());
			
			//rhythmTracker
			data.setHasRhythm(ViewpointsAestheticsModel.getRhythmTracker().hasRhythm());
			data.setPeriod(ViewpointsAestheticsModel.getRhythmTracker().getPeriod());
			data.setPhase(ViewpointsAestheticsModel.getRhythmTracker().getPhase());
			
			//postSegmentationTracker
			data.setPredicateSpaceGesture((PredicateSpaceGesture) ViewpointsAestheticsModel.getPostSegmentationTracker().getValue("gesture"));
			
			//emotionTracker
			data.setEmotion((String) ViewpointsAestheticsModel.getEmotionTracker().getValue("emotion"));
			
			//activityTracker
			data.setVelocityTotal(ViewpointsAestheticsModel.getActivityTracker().getVelocity());
			data.setVelocityRightHand(ViewpointsAestheticsModel.getActivityTracker().getVelocity(JIDX.RIGHT_HAND));
			data.setVelocityLeftHand(ViewpointsAestheticsModel.getActivityTracker().getVelocity(JIDX.LEFT_HAND));
			data.setAccelerationTotal(ViewpointsAestheticsModel.getActivityTracker().getAcceleration());
			data.setAccelerationRightHand(ViewpointsAestheticsModel.getActivityTracker().getAcceleration(JIDX.RIGHT_HAND));
			data.setAccelerationLeftHand(ViewpointsAestheticsModel.getActivityTracker().getAcceleration(JIDX.LEFT_HAND));
			data.setForceTotal(ViewpointsAestheticsModel.getActivityTracker().getForce());
			data.setForceRightHand(ViewpointsAestheticsModel.getActivityTracker().getForce(JIDX.RIGHT_HAND));
			data.setForceLeftHand(ViewpointsAestheticsModel.getActivityTracker().getForce(JIDX.LEFT_HAND));
			data.setMomentumTotal(ViewpointsAestheticsModel.getActivityTracker().getMomentum());
			data.setMomentumRightHand(ViewpointsAestheticsModel.getActivityTracker().getMomentum(JIDX.RIGHT_HAND));
			data.setMomentumLeftHand(ViewpointsAestheticsModel.getActivityTracker().getMomentum(JIDX.LEFT_HAND));
			data.setKineticEnergyTotal(ViewpointsAestheticsModel.getActivityTracker().getKineticEnergy());
			data.setKineticEnergyRightHand(ViewpointsAestheticsModel.getActivityTracker().getKineticEnergy(JIDX.RIGHT_HAND));
			data.setKineticEnergyLeftHand(ViewpointsAestheticsModel.getActivityTracker().getKineticEnergy(JIDX.LEFT_HAND));
			
			//humanStillnessTracker
			data.setHumanStill(ViewpointsAestheticsModel.getHumanStillness().isStill());
			
			//agentStillnessTracker
			data.setAgentStill(ViewpointsAestheticsModel.getAgentStillness().isStill());
		}
		catch (Exception e)
		{
			System.out.println("ERROR!!! Not getting sonification data from ViewpointsAestheticModel trackers.");
			e.printStackTrace();
			return new SonificationParametersData();
		}
		
//		System.out.println("Data being sent is:\n" + data.convertToString() + "\n");
		return data;
	}

	/**
	 * Whether the thread is currently running or not.
	 * @return
	 */
	public boolean isRunning()
	{
		return isRunning;
	}

	/**
	 * Toggle / set whether the thread should be running or not.
	 * @param isRunning the desired state of the thread.
	 */
	public void setRunning(boolean isRunning)
	{
		this.isRunning = isRunning;
	}
}