package Viewpoints.FrontEnd.Communication.Socket.Rhythm;

import Viewpoints.FrontEnd.Communication.Socket.AbstractSocketData;
import Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture.PredicateSpaceGesture;
import Viewpoints.FrontEnd.Shared.Body;

public class RhythmSocketInputData implements AbstractSocketData
{
	/**
	 * A Body that contains a PVector representation of the 3D positions of a set of enumerated joints at a point in time.
	 */
	private PredicateSpaceGesture body;
	
	/**
	 * Public constructor for creating this object.
	 * @param body The body to be wrapped.
	 */
	public RhythmSocketInputData(PredicateSpaceGesture body)
	{
		this.body = body;
	}

	/**
	 * A method to convert the body to a String representation.
	 * @return The converted String.
	 */
	@Override
	public String convertToString()
	{
		return body.toString();
	}

	/**
	 * Getter for the wrapped body object.
	 * @return The body object.
	 */
	public PredicateSpaceGesture getBody()
	{
		return body;
	}

	/**
	 * Setter for the wrapped body object.
	 * @param body The body object to set.
	 */
	public void setBody(PredicateSpaceGesture body)
	{
		this.body = body;
	}
}
