package Viewpoints.FrontEnd.Communication.Socket;

import Viewpoints.FrontEnd.Communication.Socket.Shared.BasicSocket;
import Viewpoints.FrontEnd.Communication.Socket.Shared.BasicSocket.TYPE;

/**
 * An abstract parent class for socket use cases that only require receiving data over a socket and not sending any data.
 * The class wraps a BasicSocket that is used for the communication, various details for initializing it, 
 * the method to create it, a run loop for processing data over it, and methods for controlling its execution.
 * Any derived classes must define the addSocketDataFromInputSocketToOutputQueue() method. This is done
 * as addOutputToQueue(inputSocket.receive(<U extends AbstractSocketData>.class, true)); for most cases.
 * @author mikhail.jacob
 *
 * @param <T> The wrapper for data that is input to the socket for sending.
 * @param <U> A dummy type that should be kept the same as the class used for T.
 */
public abstract class AbstractInputSocket<T extends AbstractSocketData, U extends AbstractSocketData> extends AbstractSocketIO<T, U>
{
	/**
	 * A socket used to receive data.
	 */
	protected BasicSocket inputSocket;
	
	/**
	 * Whether the socket is running or not.
	 */
	private boolean isInputSocketRunning = true;

	/**
	 * The address of the socket.
	 */
	protected String address;

	/**
	 * The type of socket used to receive data.
	 */
	protected TYPE socketType;

	/**
	 * Whether the socket should bind or connect.
	 */
	protected boolean isBind;

	/**
	 * The filter used to subscribe to data that publishers are sending.
	 */
	protected String filter;
	
	/**
	 * The public constructor for this class that takes in the address, socket type, isBind, and filter for the socket it constructs.
	 * @param address The address to receive data from.
	 * @param socketType The type of socket to create.
	 * @param isBind Whether to bind to the address or connect.
	 * @param filter The filter used to subscribe to data in order to receive what publishers send.
	 */
	public AbstractInputSocket(String address, TYPE socketType, boolean isBind, String filter)
	{
		super();
		this.address = address;
		this.socketType = socketType;
		this.isBind = isBind;
		this.filter = filter;
		initialize();
	}

	/**
	 * The run loop that processes data and constantly receives data that is then removed from the outputQueue onto which it was placed for that purpose.
	 */
	@Override
	public void run()
	{
		while(true)
		{
			if(isInputSocketRunning)
			{
				addSocketDataFromInputSocketToOutputQueue();
			}
			else
			{
				break;
			}
		}
		
		inputSocket.stop();
	}

	/**
	 * The method that actually creates the socket.
	 */
	@Override
	public void createSockets()
	{
		inputSocket = new BasicSocket(address, socketType, isBind);
		inputSocket.subscribe(filter);
	}
	
	/**
	 * An abstract method that is defined by derived classes in order to add data to the outputQueue internally after receiving it over the socket with the correct data type.
	 */
	public abstract void addSocketDataFromInputSocketToOutputQueue();

	/**
	 * Whether the socket is running currently.
	 * @return Whether the socket is running currently.
	 */
	public boolean isInputSocketRunning()
	{
		return isInputSocketRunning;
	}

	/**
	 * Set whether the socket should be running.
	 * @param isOutputSocketRunning Whether the socket should be running.
	 */
	public void setInputSocketRunning(boolean isInputSocketRunning)
	{
		this.isInputSocketRunning = isInputSocketRunning;
	}
}