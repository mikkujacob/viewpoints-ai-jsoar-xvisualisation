package Viewpoints.FrontEnd.Communication.Socket.SonificationParameters;

import Viewpoints.FrontEnd.Communication.Socket.AbstractSocketData;
import Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture.PredicateSpaceGesture;

public class SonificationParametersData implements AbstractSocketData
{
	//viewpointsTracker
	private float energy;
	private float smoothness;
	
	//rhythmTracker
	private boolean hasRhythm;
	private float period;
	private float phase;
	
	//postSegmentationTracker
	private PredicateSpaceGesture predicateSpaceGesture;
	
	//emotionTracker
	private String emotion;
	
	//activityTracker
	private float velocityTotal;
	private float velocityRightHand;
	private float velocityLeftHand;
	private float accelerationTotal;
	private float accelerationRightHand;
	private float accelerationLeftHand;
	private float forceTotal;
	private float forceRightHand;
	private float forceLeftHand;
	private float momentumTotal;
	private float momentumRightHand;
	private float momentumLeftHand;
	private float kineticEnergyTotal;
	private float kineticEnergyRightHand;
	private float kineticEnergyLeftHand;
	
	//humanStillnessTracker
	private boolean isHumanStill;
	
	//agentStillnessTracker
	private boolean isAgentStill;
	
	//ema
	private ExponentialMovingAverage velocityave = new ExponentialMovingAverage(.5f); 
	private ExponentialMovingAverage accelerationave = new ExponentialMovingAverage(.5f); 
	private ExponentialMovingAverage momentumave = new ExponentialMovingAverage(.5f); 
	private ExponentialMovingAverage kineticave = new ExponentialMovingAverage(.5f); 
	
	
	public SonificationParametersData()
	{
		energy = 0f;
		smoothness = 0f;
		hasRhythm = false;
		period = 0f;
		phase = 0f;
		predicateSpaceGesture = new PredicateSpaceGesture();
		emotion = "none";
		velocityTotal = 0f;
		velocityRightHand = 0f;
		velocityLeftHand = 0f;
		accelerationTotal = 0f;
		accelerationRightHand = 0f;
		accelerationLeftHand = 0f;
		forceTotal = 0f;
		forceRightHand = 0f;
		forceLeftHand = 0f;
		momentumTotal = 0f;
		momentumRightHand = 0f;
		momentumLeftHand = 0f;
		kineticEnergyTotal = 0f;
		kineticEnergyRightHand = 0f;
		kineticEnergyLeftHand = 0f;
		isHumanStill = true;
		isAgentStill = true;
	}
	
	public float scaleRange(float oldValue, float oldMin, float oldMax, float newMin, float newMax)
	{
		return (((oldValue - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
	}
	
	@Override
	public String convertToString()
	{
		String result = "Energy: " + energy + "\n";
		result += "Smoothness: " + smoothness + "\n";
		result += "Has Rhythm: " + hasRhythm + "\n";
		result += "Period: " + period + "\n";
		result += "Phase: " + phase + "\n";
		result += "Last Predicate Space Gesture: " + predicateSpaceGesture + "\n";
		result += "Last Emotion: " + emotion + "\n";
		result += "Velocity Total: " + velocityTotal + "\n";
		result += "Velocity Right Hand: " + velocityRightHand + "\n";
		result += "Velocity Left Hand: " + velocityLeftHand + "\n";
		result += "Acceleration Total: " + accelerationTotal + "\n";
		result += "Acceleration Right Hand: " + accelerationRightHand + "\n";
		result += "Acceleration Left Hand: " + accelerationLeftHand + "\n";
		result += "Force Total: " + forceTotal + "\n";
		result += "Force Right Hand: " + forceRightHand + "\n";
		result += "Force Left Hand: " + forceLeftHand + "\n";
		result += "Momentum Total: " + momentumTotal + "\n";
		result += "Momentum Right Hand: " + momentumRightHand + "\n";
		result += "Momentum Left Hand: " + momentumLeftHand + "\n";
		result += "Kinectic Energy Total: " + kineticEnergyTotal + "\n";
		result += "Kinectic Energy Right Hand: " + kineticEnergyRightHand + "\n";
		result += "Kinectic Energy Left Hand: " + kineticEnergyLeftHand + "\n";
		result += "Is Human Still: " + isHumanStill + "\n";
		result += "Is Agent Still: " + isAgentStill;
		
		return result;
	}
	

//	float v = velocityave.average(velocityTotal);  
//	float a = accelerationave.average(accelerationTotal);  
//	float m = momentumave.average(momentumTotal);  
//	float k = kineticave.average(kineticEnergyTotal);  
	
	// PAN - LEFT_RIGHT -100,100
	public float getPanValues() {
		// maybe use the difference between left and right hand velocity?
		// float rl = velocityRightHand - velocityLeftHand; 
		return scaleRange(energy,0,1,-100,100); 
	}
	
	// DISTORTION - DISTO_GAIN (0,50) 
	public float getDistortionValues() {
//		return scaleRange(energy,0,1,0,50); 
//		return scaleRange(smoothness,0,1,0,50); 
//		return scaleRange(momentumTotal,0,1000,0,50); 
		return scaleRange(accelerationTotal,0,3000,0,50); 
//		return scaleRange(velocityTotal,0,400,0,50); 
	}
	
	// RINGMOD - RINGMOD_MODFREQ (0,100)
	public float getRingModValues() {
//		return scaleRange(energy,0,1,0,100); 
//		return scaleRange(smoothness,0,1,0,100); 
//		return scaleRange(momentumTotal,0,1000,0,100); 
//		return scaleRange(accelerationTotal,0,3000,0,100); 
		return scaleRange(velocityTotal,0,400,0,100); 
	}
	
	// TREMELO - TREMOLO_FREQ (0,20) - It's actually 0 - 100, but 0-20 is the sweet range
	public float getTremeloValues() {
//		return scaleRange(energy,0,1,0,20); 
//		return scaleRange(smoothness,0,1,0,20); 
//		return scaleRange(momentumTotal,0,1000,0,20); 
//		return scaleRange(a,0,3000,0,20); 
		return scaleRange(velocityTotal,0,400,0,20); 
	}	
	
	// FLANGER - FLANGER_LENGTH (0,200) 
	public float getFlangerValues() {
//		return scaleRange(energy,0,1,0,200); 
//		return scaleRange(smoothness,0,1,0,200); 
		return scaleRange(momentumTotal,0,1000,0,200); 
//		return scaleRange(accelerationTotal,0,3000,0,200); 
//		return scaleRange(velocityTotal,0,400,0,200);  
	}	
	
	// WAH - WAH_POSITION(0,1)  
	public float getWahValues() {
//		return scaleRange(energy,0,1,0,1); 
		return scaleRange(smoothness,0,1,0,1); 
//		return scaleRange(momentumTotal,0,1000,0,1); 
//		return scaleRange(accelerationTotal,0,3000,0,1); 
//		return scaleRange(velocityTotal,0,400,0,1);  
	}	
	
	
	// FILTER - FILTER_FREQ (20,10000) - the actual range is 20,000, but was a little too large
	public float getFilterValues() {
		// Phase
//		return scaleRange(energy,0,1,20,10000); 
//		return scaleRange(smoothness,0,1,20,10000); 
//		return scaleRange(momentumTotal,0,1000,20,10000); 
//		return scaleRange(accelerationTotal,0,3000,20,10000); 
		return scaleRange(kineticEnergyTotal,0,5000,1000,10000);  
	}	
	

	public float getEnergy()
	{
		return energy;
	}

	public void setEnergy(float energy)
	{
		this.energy = energy;
	}

	public float getSmoothness()
	{
		return smoothness;
	}

	public void setSmoothness(float smoothness)
	{
		this.smoothness = smoothness;
	}

	public boolean isHasRhythm()
	{
		return hasRhythm;
	}

	public void setHasRhythm(boolean hasRhythm)
	{
		this.hasRhythm = hasRhythm;
	}

	public float getPeriod()
	{
		return period;
	}

	public void setPeriod(float period)
	{
		this.period = period;
	}

	public float getPhase()
	{
		return phase;
	}

	public void setPhase(float phase)
	{
		this.phase = phase;
	}

	public PredicateSpaceGesture getPredicateSpaceGesture()
	{
		return predicateSpaceGesture;
	}

	public void setPredicateSpaceGesture(PredicateSpaceGesture predicateSpaceGesture)
	{
		this.predicateSpaceGesture = predicateSpaceGesture;
	}

	public String getEmotion()
	{
		return emotion;
	}

	public void setEmotion(String emotion)
	{
		this.emotion = emotion;
	}

	public float getVelocityTotal()
	{
		return velocityTotal;
	}

	public void setVelocityTotal(float velocityTotal)
	{
		this.velocityTotal = velocityTotal;
	}

	public float getVelocityRightHand()
	{
		return velocityRightHand;
	}

	public void setVelocityRightHand(float velocityRightHand)
	{
		this.velocityRightHand = velocityRightHand;
	}

	public float getVelocityLeftHand()
	{
		return velocityLeftHand;
	}

	public void setVelocityLeftHand(float velocityLeftHand)
	{
		this.velocityLeftHand = velocityLeftHand;
	}

	public float getAccelerationTotal()
	{
		return accelerationTotal;
	}

	public void setAccelerationTotal(float accelerationTotal)
	{
		this.accelerationTotal = accelerationTotal;
	}

	public float getAccelerationRightHand()
	{
		return accelerationRightHand;
	}

	public void setAccelerationRightHand(float accelerationRightHand)
	{
		this.accelerationRightHand = accelerationRightHand;
	}

	public float getAccelerationLeftHand()
	{
		return accelerationLeftHand;
	}

	public void setAccelerationLeftHand(float accelerationLeftHand)
	{
		this.accelerationLeftHand = accelerationLeftHand;
	}

	public float getForceTotal()
	{
		return forceTotal;
	}

	public void setForceTotal(float forceTotal)
	{
		this.forceTotal = forceTotal;
	}

	public float getForceRightHand()
	{
		return forceRightHand;
	}

	public void setForceRightHand(float forceRightHand)
	{
		this.forceRightHand = forceRightHand;
	}

	public float getForceLeftHand()
	{
		return forceLeftHand;
	}

	public void setForceLeftHand(float forceLeftHand)
	{
		this.forceLeftHand = forceLeftHand;
	}

	public float getMomentumTotal()
	{
		return momentumTotal;
	}

	public void setMomentumTotal(float momentumTotal)
	{
		this.momentumTotal = momentumTotal;
	}

	public float getMomentumRightHand()
	{
		return momentumRightHand;
	}

	public void setMomentumRightHand(float momentumRightHand)
	{
		this.momentumRightHand = momentumRightHand;
	}

	public float getMomentumLeftHand()
	{
		return momentumLeftHand;
	}

	public void setMomentumLeftHand(float momentumLeftHand)
	{
		this.momentumLeftHand = momentumLeftHand;
	}

	public float getKineticEnergyTotal()
	{
		return kineticEnergyTotal;
	}

	public void setKineticEnergyTotal(float kineticEnergyTotal)
	{
		this.kineticEnergyTotal = kineticEnergyTotal;
	}

	public float getKineticEnergyRightHand()
	{
		return kineticEnergyRightHand;
	}

	public void setKineticEnergyRightHand(float kineticEnergyRightHand)
	{
		this.kineticEnergyRightHand = kineticEnergyRightHand;
	}

	public float getKineticEnergyLeftHand()
	{
		return kineticEnergyLeftHand;
	}

	public void setKineticEnergyLeftHand(float kineticEnergyLeftHand)
	{
		this.kineticEnergyLeftHand = kineticEnergyLeftHand;
	}

	public boolean isHumanStill()
	{
		return isHumanStill;
	}

	public void setHumanStill(boolean isHumanStill)
	{
		this.isHumanStill = isHumanStill;
	}

	public boolean isAgentStill()
	{
		return isAgentStill;
	}

	public void setAgentStill(boolean isAgentStill)
	{
		this.isAgentStill = isAgentStill;
	}
	
	private class ExponentialMovingAverage {
	    private float alpha;
	    private Float oldValue;
	    public ExponentialMovingAverage(float alpha) {
	        this.alpha = alpha;
	    }

	    public float average(float value) {
	        if (oldValue == null) {
	            oldValue = value;
	            return value;
	        }
	        float newValue = oldValue + alpha * (value - oldValue);
	        oldValue = newValue;
	        return newValue;
	    }
	}

}
