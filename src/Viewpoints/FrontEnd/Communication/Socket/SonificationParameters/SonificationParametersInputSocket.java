package Viewpoints.FrontEnd.Communication.Socket.SonificationParameters;

import Viewpoints.FrontEnd.Communication.Socket.AbstractInputSocket;
import Viewpoints.FrontEnd.Communication.Socket.SocketConstants;
import Viewpoints.FrontEnd.Communication.Socket.Shared.BasicSocket.TYPE;

public class SonificationParametersInputSocket extends AbstractInputSocket<SonificationParametersData, SonificationParametersData>
{

	/**
	 * Public constructor that constructs, initializes, and starts running the SonificationParametersInputSocket.
	 * @param address The address to receive data from.
	 * @param socketType The type of socket to create.
	 * @param isBind Whether to bind to the address or connect.
	 * @param filter The filter used to subscribe to data in order to receive what publishers send.
	 */
	public SonificationParametersInputSocket(String address, TYPE socketType, boolean isBind, String filter)
	{
		super(address, socketType, isBind, filter);
	}

	/**
	 * Main method to start execution to test this class.
	 * @param args Runtime arguments.
	 */
	public static void main(String[] args)
	{
		SonificationParametersInputSocket sonificationParametersIO = new SonificationParametersInputSocket(SocketConstants.SONIFICATION_PARAMETERS_ADDRESS, SocketConstants.SONIFICATION_PARAMETERS_CLIENT_TYPE, SocketConstants.IS_SONIFICATION_PARAMETERS_CLIENT_BIND, SocketConstants.SONIFICATION_PARAMETERS_FILTER);
		int maxReceiveCount = 500;
		int receiveCount = 0;
		SonificationParametersData receivedData;
		while(true)
		{
			receivedData = sonificationParametersIO.removeOutputFromQueue();
			
			if(receivedData != null)
			{
				receiveCount++;
				System.out.println("Received an object from outputQueue " + receiveCount);
				System.out.println("Object from outputQueue is " + receivedData.convertToString());
				receivedData = null;
			}
			
			if(receiveCount >= maxReceiveCount)
			{
				break;
			}
		}
		sonificationParametersIO.setInputSocketRunning(false);
	}

	/**
	 * Method to add data to the outputQueue internally after receiving it over the socket with the SonificationParametersData class definition.
	 */
	@Override
	public void addSocketDataFromInputSocketToOutputQueue()
	{
		addOutputToQueue(inputSocket.receive(SonificationParametersData.class, true));
	}
}
