package Viewpoints.FrontEnd.Communication.Socket.SonificationParameters;

import Viewpoints.FrontEnd.Communication.Socket.AbstractOutputSocket;
import Viewpoints.FrontEnd.Communication.Socket.SocketConstants;
import Viewpoints.FrontEnd.Communication.Socket.Shared.BasicSocket.TYPE;

public class SonificationParametersOutputSocket extends AbstractOutputSocket<SonificationParametersData, SonificationParametersData>
{

	/**
	 * Public constructor that constructs, initializes, and starts running the SonificationParametersOutputSocket.
	 * @param address The address to send data to.
	 * @param socketType The type of socket to create.
	 * @param isBind Whether to bind to the address or connect.
	 * @param filter The filter used to publish data under in order for subscribers to receive it.
	 */
	public SonificationParametersOutputSocket(String address, TYPE socketType, boolean isBind, String filter)
	{
		super(address, socketType, isBind, filter);
	}

	/**
	 * Main method to start execution to test this class.
	 * @param args Runtime arguments.
	 */
	public static void main(String[] args)
	{
		SonificationParametersOutputSocket sonificationParametersIO = new SonificationParametersOutputSocket(SocketConstants.SONIFICATION_PARAMETERS_ADDRESS, SocketConstants.SONIFICATION_PARAMETERS_SERVER_TYPE, SocketConstants.IS_SONIFICATION_PARAMETERS_SERVER_BIND, SocketConstants.SONIFICATION_PARAMETERS_FILTER);
		int maxSendCount = 5000;
		int sendCount = 0;
		SonificationParametersData sendingData;
		
		while(true)
		{
			sendingData = new SonificationParametersData();
			sonificationParametersIO.addInputToQueue(sendingData);
			sendCount++;
			System.out.println("Added an object to inputQueue " + sendCount);
			System.out.println("Object to inputQueue is " + sendingData.convertToString());
			
			if(sendCount >= maxSendCount)
			{
				break;
			}
		}
		sonificationParametersIO.setOutputSocketRunning(false);
	}

	/**
	 * Method to remove data from the inputQueue internally in order to send over the socket according to the SonificationParametersData class definition.
	 */
	@Override
	public void removeSocketDataFromInputQueueToOutputSocket()
	{
		Object object = removeInputFromQueue();
		if(object != null)
		{
			outputSocket.sendFiltered(object, SonificationParametersData.class, this.filter, true);
		}
	}

}
