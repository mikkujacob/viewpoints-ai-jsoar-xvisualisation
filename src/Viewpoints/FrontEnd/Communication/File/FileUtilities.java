package Viewpoints.FrontEnd.Communication.File;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import Viewpoints.BackEnd.GestureStorage.GestureFileTable;
import Viewpoints.FrontEnd.Gesture.JointSpaceGesture.JointSpaceGesture;
import Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture.PredicateSpaceGesture;

/**
 * Utility class meant for methods related to writing gestures to files
 * 
 * @author mjacob6
 * 
 */
public class FileUtilities
{

	/**
	 * Write to a text file.
	 * @param path
	 * @param outputGesture
	 */
	public static void writeToText(String path, PredicateSpaceGesture outputGesture)
	{

		FileWriter fstream = null;
		BufferedWriter out = null;

		try
		{
			fstream = new FileWriter(path, false);
			out = new BufferedWriter(fstream);

			// System.out.println("Writing: " + outputGesture.toString());

			out.write(outputGesture.toString());

			System.out.println("Done Writing " + path);
		}
		catch (Exception e)
		{
//			System.err.println("failed to output Gesture: " + e.getMessage());
		}
		finally
		{
			try
			{
				out.close();
			}
			catch (IOException e)
			{
				// e.printStackTrace();
			}
		}
	}

	/**
	 * Read from a text file.
	 * @param path
	 * @return
	 */
	public static PredicateSpaceGesture readFromText(String path)
	{

		FileReader fstream = null;
		BufferedReader in = null;
		String inputGesture = "";
		try
		{
			fstream = new FileReader(path);
			in = new BufferedReader(fstream);
			String line = "";

			while ((line = in.readLine()) != null)
			{
				inputGesture += line + "\n";
			}

			// System.out.println("Done Reading: " + inputGesture);
		}
		catch (Exception e)
		{
//			System.err.println("failed to output Gesture: " + e.getMessage());
		}
		finally
		{
			try
			{
				if (null != in)
					in.close();

			}
			catch (IOException e)
			{
				// e.printStackTrace();
			}
		}

		// System.out.println(Gesture.fromString(inputGesture));
		return PredicateSpaceGesture.fromString(inputGesture);
	}

	/**
	 * Serialize Predicate Space Gesture
	 * 
	 * @param path
	 * @param outputGesture
	 * @return
	 */
	public static Boolean serializeGesture(String path, PredicateSpaceGesture outputGesture)
	{
		FileOutputStream fileoutstream = null;
		ObjectOutputStream objectoutstream = null;
		try
		{
			fileoutstream = new FileOutputStream(path);
			objectoutstream = new ObjectOutputStream(
					fileoutstream);
			objectoutstream.writeObject(outputGesture);
			objectoutstream.flush();
			objectoutstream.close();
			fileoutstream.flush();
			fileoutstream.close();
		}
		catch (IOException e)
		{
//			System.out.println("Exception in serializeGesture");
//			e.printStackTrace();
			return false;
		}
		finally
		{
			try
			{
				if (null != objectoutstream)
				{
					objectoutstream.close();
				}
				if (null != fileoutstream)
				{
					fileoutstream.close();
				}
			}
			catch (Exception e)
			{
				System.out.println("Exception in finally of serializeGesture");
				e.printStackTrace();
				return null;
			}
		}

//		System.out.println("Done Writing Predicate Gesture: " + path);

		return true;
	}

	/**
	 * Deserialize Predicate Space Gesture
	 * 
	 * @param path
	 * @return
	 */
	public static PredicateSpaceGesture deserializeGesture(String path)
	{
		PredicateSpaceGesture readGesture = new PredicateSpaceGesture();
		FileInputStream fileinstream = null;
		ObjectInputStream objectinstream = null;

		try
		{
			File infile = new File(path);
			fileinstream = new FileInputStream(infile);

			if (fileinstream.available() == 0)
			{
				fileinstream.close();
				return null;
			}

			objectinstream = new ObjectInputStream(fileinstream);
			readGesture = (PredicateSpaceGesture) objectinstream.readObject();
			objectinstream.close();
		}
		catch (IOException | ClassNotFoundException e)
		{
//			System.out.println("Exception in deserializeGesture");
//			e.printStackTrace();
			return null;
		}
		finally
		{
			try
			{
				if (null != objectinstream)
				{
					objectinstream.close();
				}
				if (null != fileinstream)
				{
					fileinstream.close();
				}
			}
			catch (Exception e)
			{
				System.out.println("Exception in finally of deserializeGesture with path of " + path);
				e.printStackTrace();
				return null;
			}
		}
//		System.out.println("Done Reading Predicate Gesture: " + path);

		return readGesture;
	}

	/**
	 * Clear a text file.
	 * 
	 * @param path
	 */
	public static void clearText(String path)
	{

		FileWriter fstream = null;
		BufferedWriter out = null;

		try
		{
			fstream = new FileWriter(path, false);
			out = new BufferedWriter(fstream);

			// System.out.println("Clearing");

			out.write("");

//			System.out.println("Done Clearing " + path);
		}
		catch (Exception e)
		{
//			System.err.println("Exception in clear text: " + e.getMessage());
//			e.printStackTrace();
		}
		finally
		{
			try
			{
				if (null != out)
				{
					out.close();
				}
				if (null != fstream)
				{
					fstream.close();
				}
			}
			catch (IOException e)
			{
				System.out.println("Exception in finally of clear text");
				e.printStackTrace();
			}
		}
	}

	/**
	 * Serialize Joint Space Gesture
	 * 
	 * @param path
	 * @param outputGestureJoints
	 * @return
	 */
	public static Boolean serializeJointsGesture(String path,
			JointSpaceGesture outputGestureJoints)
	{
		FileOutputStream fileoutstream = null;
		ObjectOutputStream objectoutstream = null;
		try
		{
			fileoutstream = new FileOutputStream(path);
			objectoutstream = new ObjectOutputStream(
					fileoutstream);
			objectoutstream.writeObject(outputGestureJoints);
			objectoutstream.flush();
			objectoutstream.close();
			fileoutstream.flush();
			fileoutstream.close();
		}
		catch (IOException e)
		{
//			System.out.println("Exception in serializeJointsGesture");
//			e.printStackTrace();
			return false;
		}
		finally
		{
			try
			{
				if (null != objectoutstream)
				{
					objectoutstream.close();
				}
				if (null != fileoutstream)
				{
					fileoutstream.close();
				}
			}
			catch (Exception e)
			{
				System.out.println("Exception in finally of serializeJointsGesture");
				e.printStackTrace();
				return null;
			}
		}

//		System.out.println("Done Writing Joints Gesture: " + path);

		return true;
	}

	/**
	 * Deserialize Joint Space Gesture
	 * 
	 * @param path
	 * @return
	 */
	public static JointSpaceGesture deserializeJointsGesture(String path)
	{
		JointSpaceGesture readGesture = new JointSpaceGesture();
		FileInputStream fileinstream = null;
		ObjectInputStream objectinstream = null;
		try
		{
			File infile = new File(path);
			fileinstream = new FileInputStream(infile);

			if (fileinstream.available() == 0)
			{
				fileinstream.close();
				return null;
			}

			objectinstream = new ObjectInputStream(fileinstream);

			readGesture = (JointSpaceGesture) objectinstream.readObject();
			objectinstream.close();
		}
		catch (IOException | ClassNotFoundException e)
		{
			System.out.println("Exception in deserializeJointsGesture at " + path);
			FileUtilities.clearText(path);
			return null;
		}
		finally
		{
			try
			{
				if (null != objectinstream)
				{
					objectinstream.close();
				}
				if (null != fileinstream)
				{
					fileinstream.close();
				}
			}
			catch (Exception e)
			{
				System.out.println("Exception in finally of deserializeJointsGesture");
				e.printStackTrace();
				return null;
			}
		}

//		System.out.println("Done Reading Joints Gesture: " + path);
		if(readGesture == null)
			return null;
		readGesture.autotiming();
		return readGesture;
	}



	/**
	 * Serialize Gesture File Table
	 * 
	 * @param path
	 * @param outputGestureFileTable
	 * @return
	 */
	public static Boolean serializeGestureFileTable(String path,
			GestureFileTable outputGestureFileTable)
	{
		FileOutputStream fileoutstream = null;
		ObjectOutputStream objectoutstream = null;
		try
		{
			fileoutstream = new FileOutputStream(path);
			objectoutstream = new ObjectOutputStream(
					fileoutstream);
			objectoutstream.writeObject(outputGestureFileTable);
			objectoutstream.flush();
			objectoutstream.close();
			fileoutstream.flush();
			fileoutstream.close();
		}
		catch (IOException e)
		{
//			System.out.println("Exception in serializeGestureFileTable");
//			e.printStackTrace();
			return false;
		}
		finally
		{
			try
			{
				if (null != objectoutstream)
				{
					objectoutstream.close();
				}
				if (null != fileoutstream)
				{
					fileoutstream.close();
				}
			}
			catch (Exception e)
			{
				System.out.println("Exception in finally of serializeGestureFileTable");
				e.printStackTrace();
				return null;
			}
		}

//		System.out.println("Done Writing Gesture File Table: " + path);

		return true;
	}

	/**
	 * Deserialize Gesture File Table
	 * 
	 * @param path
	 * @return
	 */
	public static GestureFileTable deserializeGestureFileTable(String path)
	{
		GestureFileTable readGestureFileTable = new GestureFileTable();
		FileInputStream fileinstream = null;
		ObjectInputStream objectinstream = null;
		try
		{
			File infile = new File(path);
			fileinstream = new FileInputStream(infile);

			if (fileinstream.available() == 0)
			{
				fileinstream.close();
				return null;
			}

			objectinstream = new ObjectInputStream(fileinstream);
			readGestureFileTable = (GestureFileTable) objectinstream.readObject();
			objectinstream.close();
		}
		catch (IOException | ClassNotFoundException e)
		{
//			System.out.println("Exception in deserializeGestureFileTable");
//			e.printStackTrace();
			return null;
		}
		finally
		{
			try
			{
				if (null != objectinstream)
				{
					objectinstream.close();
				}
				if (null != fileinstream)
				{
					fileinstream.close();
				}
			}
			catch (Exception e)
			{
				System.out.println("Exception in finally of deserializeGestureFileTable");
				e.printStackTrace();
				return null;
			}
		}

//		System.out.println("Done Reading Gesture File Table: " + path);
		return readGestureFileTable;
	}
	
	/**
	 * Write a text message to a file. Overwrite existing text in file.
	 * 
	 * @param path
	 *            Path to file.
	 * @param text
	 *            Text message to write to file.
	 */
	public static void writeToFile(String path, String text)
	{
		writeToFile(path, text, false);
	}

	/**
	 * Write a text message to a file.
	 * 
	 * @param path
	 *            Path to file.
	 * @param text
	 *            Text message to write to file.
	 * @param append
	 *            Boolean to enable appending current text message to existing
	 *            text in file.
	 */
	public static void writeToFile(String path, String text, boolean append)
	{
		File pathFile = new File(path);
		FileWriter fstream = null;
		BufferedWriter out = null;
		try
		{
			pathFile.createNewFile();
			fstream = new FileWriter(path, append);

			out = new BufferedWriter(fstream);

			out.write(text);

			// System.out.println("Done Writing " + path);
		}
		catch (Exception e)
		{
//			System.err.println("Failed to write text: " + e.getMessage());
//			System.err.println("Path: " + path);
		}
		finally
		{
			try
			{
				out.close();
			}
			catch (IOException e)
			{
				 e.printStackTrace();
			}
		}
	}
	
	/**
	 * Annotate gesture timestamps to a csv file.
	 * @param writer
	 * @param time
	 * @param start
	 * @param type
	 * @return
	 */
	public static FileWriter annotate(FileWriter writer, String directory, String videoName, float time, boolean start, String type) {
		try {
			if (writer == null) {
				File gestureLibraryFile = new File(directory);
				if (!gestureLibraryFile.exists()) {
					gestureLibraryFile.mkdir();
				}
				//String out = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss").format(new Date());
				//File file = new File("gesture-timestamps/" + out + type + ".csv");
				File file =  new File(directory + "/" + videoName + type + ".csv");
				if (file.exists()) {
					writer = new FileWriter(file, true);
					writer.write('\n');
					writer.write('\n');
				} else {
					writer = new FileWriter(file, true);
					writer.write("Start");
					writer.write(',');
					writer.write("End");
					writer.write('\n');
				}
			}
			
			writer.append(time + "");
			
			if (start) {
				writer.append(',');
			} else {
				writer.append('\n');
			}

			writer.flush();
			
			return writer;
			
		} catch(Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Clear all the text files being used by front end and back end applications.
	 * TODO: (Lauren) Remove this param and just figure out the home directory.
	 * @param home Local directory for the project.
	 */
	public static void flush(String home) {
		clearText(home + File.separator + "file_communications" + File.separator + "JSOARJointsToVisualization.txt");
		clearText(home + File.separator + "file_communications" + File.separator + "JSOARPredicatesToVisualization.txt");
		clearText(home + File.separator + "file_communications" + File.separator + "KinectJointsToJSOAR.txt");
		clearText(home + File.separator + "file_communications" + File.separator + "KinectPredicatesToJSOAR.txt");
		clearText(home + File.separator + "file_communications" + File.separator + "requestRandomResponse.txt");
		clearText(home + File.separator + "file_communications" + File.separator + "response-time.txt");
		clearText(home + File.separator + "file_communications" + File.separator + "restart_commands.txt");
		clearText(home + File.separator + "file_communications" + File.separator + "time_log.txt");
	}
}
