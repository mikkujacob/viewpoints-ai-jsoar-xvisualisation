package Viewpoints.FrontEnd.ResponseLogic;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import Viewpoints.FrontEnd.Clock.Clock;
import Viewpoints.FrontEnd.Communication.File.FileUtilities;
import Viewpoints.FrontEnd.Gesture.JointSpaceGesture.JointSpaceGesture;
import Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture.PredicateSpaceGesture;
import Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture.TransformPredicates;
import Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture.TransformPredicates.TransformPredicateInterface;
import Viewpoints.FrontEnd.Model.AestheticsModel.ViewpointsAestheticsModel;
import Viewpoints.FrontEnd.Shared.*;
import processing.core.PVector;

public class SoarResponseLogic extends ResponseLogic
{
	public String kinectPredicatesOutPath = "file_communications/KinectPredicatesToJSOAR.txt";
	public String kinectJointsOutPath = "file_communications/KinectJointsToJSOAR.txt";
	public String jsoarPredicatesInPath = "file_communications/JSOARPredicatesToVisualization.txt";
	public String jsoarJointsInPath = "file_communications/JSOARJointsToVisualization.txt";
	public static int restartTimeThreshold = 5000;
	private String PROJECT_HOME;
	private float responseTime = 0f;

	private JointSpaceGestureInterpolator currentResponse = null;

	private Clock clock;
	
	public SoarResponseLogic()
	{
		clock = new Clock();
		PROJECT_HOME = System.getProperty("user.dir");
		if (PROJECT_HOME.equalsIgnoreCase(null))
		{
			try
			{
				PROJECT_HOME = new java.io.File(".").getCanonicalPath();
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}

			if (PROJECT_HOME.equalsIgnoreCase(null))
			{
				System.out.println("ERROR! PROJECT_HOME IS NULL");
			}
		}

		if (PROJECT_HOME.endsWith(File.separator + "bin"))
		{
			PROJECT_HOME = PROJECT_HOME.substring(0, PROJECT_HOME.length() - 4);
		}

		kinectPredicatesOutPath = PROJECT_HOME + File.separator + kinectPredicatesOutPath;
		kinectJointsOutPath = PROJECT_HOME + File.separator + kinectJointsOutPath;
		jsoarPredicatesInPath = PROJECT_HOME + File.separator + jsoarPredicatesInPath;
		jsoarJointsInPath = PROJECT_HOME + File.separator + jsoarJointsInPath;
	}

	@Override
	protected JointSpaceGestureInterpolator evaluateResponse(JointSpaceGesture input,
			JointSpaceGesture normalizedInput)
	{
		//Start timer clock
		clock.checkMillis();
		
		//TODO: REDO JointSpaceGesture to PredicateSpaceGesture transform.
		//1. Create a new class CLassifierPredicateSpaceGestureTracker that transforms JSG to PSG with method transformJSGToPSG(JointSpaceGesture sourceJSG);
		//2. Follow same logic as Ivan's other ClassifierXYZTracker classes for takePose(), takePoses(), reset(), requestClassification(), isTracking() and getValue().
		//3. Within transformJSGToPSG(), do:
		//	a. Create empty PSG.
		//	b. Create lists of instantaneous predicate histories (eg. smoothness, tempo, energy, etc).
		//	c. Create sliding window history of past Body frames (size equals N history)
		//	d. Create list of PredicateFrames objects.
		//	e. In loop with parameter current Body frame, do:
		//		i. Create empty PredicateFrame object.
		//		ii. Classify instantaneous predicate (eg. smoothness, tempo, energy, etc).
		//		iii. Add to instantaneous predicate history list
		//		iv. Add to current PredicateFrame object.
		//	f. Calculate aggregate values of predicates from instantaneous predicate histories.
		//	g. Create Gesture object from aggregate values of predicates and list of PredicateFrames objects.
		//	h. Return Gesture object.
		
		// Classify performative emotion in gesture
		String emotion = "";
		//TODO: (Lauren) This shouldn't be an issue but check if the emotions are checking.
		ViewpointsAestheticsModel.getEmotionTracker().takePoses(input.getGestureFramesList());
		// emotionTracker.takePoses(normalizedInput.getGestureFramesList());
		ViewpointsAestheticsModel.getEmotionTracker().requestClassification();
		emotion = (String) ViewpointsAestheticsModel.getEmotionTracker().getValue("emotion");

		System.out.println("Emotion Tracked: " + emotion);

		// Convert JSG into PSG
		ViewpointsAestheticsModel.getPostSegmentationTracker().takePoses(input.getGestureFramesList());
		ViewpointsAestheticsModel.getPostSegmentationTracker().requestClassification();
		PredicateSpaceGesture predicateSpaceGesture = (PredicateSpaceGesture) ViewpointsAestheticsModel.getPostSegmentationTracker().getValue("gesture");

		predicateSpaceGesture.setEmotionFromString(emotion);

//		System.out.println(predicateSpaceGesture.toString());
//		System.out.println("Pred Hist Count: " + predicateSpaceGesture.All_Predicate_History.size());
//		System.out.println("JSG Frame Count: " + normalizedInput.getGestureFramesList().size());

		// Write out Predicate Space Gesture to Soar PSG in file
		Boolean isGestureWritten = FileUtilities.serializeGesture(
				kinectPredicatesOutPath, predicateSpaceGesture);

		// Write out Joint Space Gesture to Soar JSG in file
		isGestureWritten = isGestureWritten
				&& FileUtilities.serializeJointsGesture(kinectJointsOutPath, normalizedInput);

		if (!isGestureWritten)
		{
			System.out.println("Error: Did not write dual space gestures to Soar");
		}

		JointSpaceGesture jsGesture = null;
		PredicateSpaceGesture psGesture = null;
		Clock currentResponseTime = new Clock();

		currentResponseTime.start();
		//TODO: (Lauren) This could be a major issue.
		while (null == jsGesture || null == psGesture)
		{
			//System.out.println("stuck in while....");
			// Read in JSG File from Soar JSG out file
			if (null == jsGesture)
			{
				jsGesture = FileUtilities.deserializeJointsGesture(jsoarJointsInPath);
				if (null != jsGesture)
				{
					FileUtilities.clearText(jsoarJointsInPath);
				}
			}

			// Read in PSG File from Soar PSG out file
			if (null == psGesture)
			{
				psGesture = FileUtilities.deserializeGesture(jsoarPredicatesInPath);
				if (null != psGesture)
				{
					FileUtilities.clearText(jsoarPredicatesInPath);
				}
			}
			
			//TODO:(Lauren) change this with different clocks and get deltatime
			currentResponseTime.checkMillis();
			if (currentResponseTime.time() > Math.max(restartTimeThreshold, responseTime + 10000)) {
				break;
			}
		}

//		System.out.println(psGesture.toString());
//		System.out.println("Joint Space Gesture: " + jsGesture.toString());

		//TODO: (Lauren) use this time as the response time
		clock.checkMillis();
		System.out.println("Response Time (ms): " + clock.deltatime());
		responseTime = clock.deltatime();
		//Reset the response time if it becomes too large.
		if (responseTime > restartTimeThreshold * 6) {
			responseTime = 0;
		}

		log(clock.deltatime());
		
		/*
		 * @Sasi - This is where you would do the following:
		 * 
		 * 1. Read in the PredicateSpaceGesture and the JointSpaceGesture sequentially.
		 * 2. Map the PSG values to physical transforms of the JSG according to some set of transforms you've created.
		 * 3. Transform the skeletal data (JSGs).
		 * 4. Create a new LeaderRecord currentResponse from the transformed JSG.
		 * */
		//read in PSG and JSG
		
		JointSpaceGesture currentGesture = jsGesture;
		
		//assign PSG
		//TODO: fix potential synchronization errors
		setPSG(psGesture);
		
		//Handle transforms of the skeleton and joints of the gesture here.
		//(Handling the Viewpoints / Effects transformations in RhythmVAI.java)
		//for(int i = 0; i < psGesture.transforms.size(); i++)
		{
//			if(psGesture.transforms.get(i) instanceof TransformPredicates.RESPONSE_MODE)
//			{
//				currentGesture = glyphResponse(psGesture.transforms.get(i), jsGesture);
//			}
			
//			if(psGesture.transforms.get(i) instanceof TransformPredicates.ADD_CROUCHING)
//			{
//				currentGesture = crouchResponse(psGesture.transforms.get(i), jsGesture);
//			}
			
		}
		
//		currentGesture = crouchResponse(psGesture.transforms.get(0), jsGesture);
		
		// transform JSG based on frames
		
		
		
		if (currentGesture != null){
			currentResponse = new JointSpaceGestureInterpolator(currentGesture.getGestureFramesList());
			currentResponse.setIsRepetitionOn(true);
			currentResponse.setDirectRepetition(true);
			return currentResponse;
		} else {
			return null;
		}
	}
	
	//@Sasi - There's a conversion problem happening from Real-World Coordinates that the Kinect gives us in the joints
	//to the coordinates of the screen. You need to convert between them. In RhythmVAI.java there is a method
	//PVector getScreenPos(PVector pos) that does that in one direction. If you're modifying the JSG files directly
	//you'll need to convert them in the opposite direction (from desired screen coordinates or offsets to real world coordinates in the joints). :)
	public JointSpaceGesture crouchResponse(TransformPredicateInterface transformPredicateInterface, JointSpaceGesture jsGesture) {
			ArrayList<Body> frames = jsGesture.getGestureFramesList();
			Body currentBody = frames.get(0);
			PVector rLocation = currentBody.get(JIDX.RIGHT_HIP);
			PVector lLocation = currentBody.get(JIDX.LEFT_HIP);
			PVector rKneeLocation = currentBody.get(JIDX.RIGHT_KNEE);
			PVector lKneeLocation = currentBody.get(JIDX.LEFT_KNEE);
			PVector torso = currentBody.get(JIDX.TORSO);
			
			for(int i = 0; i < jsGesture.getGestureFramesList().size() - 1; i++)
			{
				
				torso.y -= 10;
				rLocation.x-=10;
				rLocation.x-=10;
				rLocation.y-=10;
				rLocation.y-=10;
				
				lLocation.x-=10;
				lLocation.x-=10;
				lLocation.y-=10;
				lLocation.y-=10;
				
				rKneeLocation.y -= 20;
				lKneeLocation.y -= 20;
				currentBody.set(JIDX.LEFT_HIP, lLocation);
				currentBody.set(JIDX.RIGHT_HIP, rLocation);
				currentBody.set(JIDX.LEFT_KNEE, lKneeLocation);
				currentBody.set(JIDX.RIGHT_KNEE, rKneeLocation);
				currentBody.set(JIDX.TORSO, torso);
				frames.set(i + 1, currentBody);
			}
		
		
		return jsGesture;
	}
	
	//@Sasi - Nix this from here. It's handled outside :)
	public JointSpaceGesture glyphResponse(TransformPredicateInterface transformPredicateInterface, JointSpaceGesture jsGesture)
	{
		for(int i = 0; i < jsGesture.getGestureFramesList().size(); i++) {
			
			if(transformPredicateInterface == TransformPredicates.RESPONSE_MODE.REPEAT) {
				//set vaiGlyph to user glyph
				
			}
			
			if(transformPredicateInterface == TransformPredicates.RESPONSE_MODE.VARY) {
				//transform the current glyph
			}
			
			if(transformPredicateInterface == TransformPredicates.RESPONSE_MODE.NEW) {
				//display next glyph from list
				
			}
			
			if(transformPredicateInterface == TransformPredicates.RESPONSE_MODE.EMOTION) {
				//get new random glyph from list
			}
			
			if(transformPredicateInterface == TransformPredicates.RESPONSE_MODE.PATTERN) {
				//alternate glyphs
			}
		}
		
		return jsGesture;
	}
	
	public JointSpaceGestureInterpolator requestRandomResponse(){
		JointSpaceGesture jsGesture = null;
		Clock timer = new Clock();
		while (null == jsGesture)
		{
			timer.check();
			if(timer.time() > .3)
				return null;
			// Read in JSG File from Soar JSG out file
			if (null == jsGesture)
			{
				jsGesture = FileUtilities.deserializeJointsGesture(jsoarJointsInPath);
				if (null != jsGesture)
				{
					FileUtilities.clearText(jsoarJointsInPath);
				}
			}
		}
		
		currentResponse = new JointSpaceGestureInterpolator(jsGesture.getGestureFramesList());
		currentResponse.setIsRepetitionOn(true);
		currentResponse.setDirectRepetition(true);
		return currentResponse;
	}
	//TODO: CHANGE HARDCODED PATH
	private void log(float time) {
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(PROJECT_HOME + File.separator + "file_communications" + File.separator + "response-time.txt", false));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		out.println(time);
		out.close();
	}
}
