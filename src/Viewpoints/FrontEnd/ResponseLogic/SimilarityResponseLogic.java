package Viewpoints.FrontEnd.ResponseLogic;

import Viewpoints.FrontEnd.Gesture.JointSpaceGesture.JointSpaceGesture;
import Viewpoints.FrontEnd.Model.ObjectiveParameters.ObjectiveParametersMetric;
import Viewpoints.FrontEnd.Model.ObjectiveParameters.TargetKeyset;
import Viewpoints.FrontEnd.Shared.*;

public class SimilarityResponseLogic extends ResponseLogic {
	private ObjectiveParametersMetric metric = new ObjectiveParametersMetric(TargetKeyset.getBasicSimilarityKeyset(), 
																			"gesture-segments", "gesture-segments",
																			"runtime-gesture-segments", "runtime-gesture-segments");
	private JointSpaceGestureInterpolator currentResponse = null;
	
	public SimilarityResponseLogic() {
		metric.load();
	}
	
	@Override
	protected JointSpaceGestureInterpolator evaluateResponse(JointSpaceGesture input, JointSpaceGesture normalizedInput) {
		JointSpaceGesture similar = metric.selectClosest(normalizedInput);
		if (metric.size() < 200) {
			metric.memorizeLastGesture();
		}
		currentResponse = new JointSpaceGestureInterpolator(similar.getGestureFramesList());
		currentResponse.setIsRepetitionOn(true);
		currentResponse.setDirectRepetition(true);
		return currentResponse;
	}
}
