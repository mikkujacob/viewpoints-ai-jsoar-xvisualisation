package Viewpoints.FrontEnd.ResponseLogic;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import Viewpoints.FrontEnd.Communication.File.FileUtilities;
import Viewpoints.FrontEnd.Gesture.JointSpaceGesture.JointSpaceGesture;
import Viewpoints.FrontEnd.Shared.*;

public class RandomResponseLogic extends ResponseLogic {
	private ArrayList<JointSpaceGesture> gestureList = new ArrayList<JointSpaceGesture>();
	private JointSpaceGestureInterpolator currentResponse = null;
	private Random rand;
	
	public RandomResponseLogic(String filename) {
		String rehearsalFile;
		File rehearsalGestureFolder = new File(filename);
		File[] rehearsalGestureFileList = rehearsalGestureFolder.listFiles(); 
		for (int i = 0; i < rehearsalGestureFileList.length; i++) {
			if (rehearsalGestureFileList[i].isFile()) {
				rehearsalFile = rehearsalGestureFileList[i].getName();
				if (rehearsalFile.toLowerCase().endsWith(".jsg")) {
//					System.out.println("Imported Rehearsal Gesture File: " + filename + rehearsalFile);					
					this.gestureList.add(FileUtilities.deserializeJointsGesture(filename + rehearsalFile));
				}
			}
		}
		if(this.gestureList.size() == 0) {
			System.out.println("No Random Response Files In Folder: " + filename);
		}
		else {
			System.out.println("Loaded " + this.gestureList.size() + " Random Response Files");
		}
		
		rand = new Random();
	}
	
	@Override
	protected JointSpaceGestureInterpolator evaluateResponse(JointSpaceGesture input, JointSpaceGesture normalizedInput) {
		this.currentResponse = new JointSpaceGestureInterpolator(gestureList.get(rand.nextInt(gestureList.size())).getGestureFramesList());
		return currentResponse;
	}
}
