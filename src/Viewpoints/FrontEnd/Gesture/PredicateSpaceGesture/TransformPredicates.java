package Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import Viewpoints.FrontEnd.Shared.LIMB;

public class TransformPredicates implements Serializable
{
	/**
	 * Do not change unless you want to intentionally break serializability
	 * with previous versions of this class
	 */
	private static final long serialVersionUID = 8945391762181021322L;

	public interface TransformPredicateInterface{}
	
	private static HashMap<String, String> _framePredicateModifiedBy = null;
	
	public static String framePredicateModifiedBy(String transformPredicate) {
		if (null == _framePredicateModifiedBy) {
			_framePredicateModifiedBy = new HashMap<String, String>();
			_framePredicateModifiedBy.put("TRANSFORM_TEMPO", "TEMPO");
			_framePredicateModifiedBy.put("TRANSFORM_ENERGY", "ENERGY");
			_framePredicateModifiedBy.put("TRANSFORM_SMOOTHNESS", "SMOOTHNESS");
		}
		
		return 	_framePredicateModifiedBy.get(transformPredicate);
	}
	
	private static Enum<?>[] rangeFor(Enum<?> enumElement) {
		return enumElement.getClass().getEnumConstants();
	}
	
	private static int ordinal(Enum<?> enumElement, Enum<?>[] range) {
		for (int i = 0; i < range.length; i++) {
			if (enumElement.equals(range[i])) {
				return i;
			}
		}
		return -1;
	}
	
	public static void alterFramePredicates(TransformPredicateInterface transform, FramePredicates predicates) {
		String nameOfTransformed = transform.getClass().getSimpleName();
		String nameOfModified = framePredicateModifiedBy(nameOfTransformed);
		if (null == nameOfModified) {
			return;
		}
		try {
			Enum<?> modified = (Enum<?>)predicates.get(nameOfModified);
			Enum<?>[] modifiedRange = rangeFor(modified);
			Enum<?>[] modifierRange = rangeFor((Enum<?>)transform);
			int idxModified = ordinal(modified, modifiedRange);
			int idxModifier = ordinal((Enum<?>)transform, modifierRange);
			int idxNew = idxModified + idxModifier + 1 - modifierRange.length/2;
			if (idxNew < 0) {
				idxNew = 0;
			} else if (idxNew > modifiedRange.length - 1) {
				idxNew = modifiedRange.length - 1;
			}
			predicates.set(nameOfModified, modifiedRange[idxNew]);
		} catch (Exception e) {
			return;
		}
	}
	
	public enum REFLECT_TYPE
	{
		VERTICALLY,
		TRANSVERSALLY,
		LONGITUDINALLY
	};
	
	//Response Mode Predicate
	public enum RESPONSE_MODE implements TransformPredicateInterface
	{
		NO_OP,
		NEW,
		REPEAT,
		VARY,
		PATTERN,
		EXAGGERATE,
		BREAK,
		UNSET,
		EMOTION
	};
	
	//Absolute Transforms
	public enum TRANSFORM_DURATION_ABSOLUTE implements TransformPredicateInterface
	{
		VERY_SHORT,
		SHORT,
		MEDIUM,
		LONG,
		VERY_LONG
	};
	
	public enum TRANSFORM_TEMPO_ABSOLUTE implements TransformPredicateInterface
	{
		ALMOST_STILL,
		SLOW,
		MEDIUM,
		FAST,
		EXTREMELY_FAST
	};
	
	public enum TRANSFORM_SMOOTHNESS_ABSOLUTE implements TransformPredicateInterface
	{
		SMOOTH,
		REGULAR,
		STACCATO
	};
	
	public enum TRANSFORM_ENERGY_ABSOLUTE implements TransformPredicateInterface
	{
		ALMOST_NONE,
		LOW,
		MEDIUM,
		HIGH,
		EXTREMELY_HIGH
	};
	
	public enum TRANSFORM_SIZE_ABSOLUTE implements TransformPredicateInterface
	{
		SMALL,
		MEDIUM,
		LARGE
	};
	
	public enum TRANSFORM_QUADRANT_ABSOLUTE implements TransformPredicateInterface
	{
		TOP_RIGHT,
		TOP_LEFT,
		BOTTOM_RIGHT,
		BOTTOM_LEFT
	};
	
	public enum TRANSFORM_DISTANCE_FROM_CENTER_ABSOLUTE implements TransformPredicateInterface
	{
		VERY_NEAR,
		NEAR,
		MEDIUM,
		FAR,
		VERY_FAR
	};
	
	public enum TRANSFORM_HEIGHT_ABSOLUTE implements TransformPredicateInterface
	{
		SHORT,
		MEDIUM,
		TALL
	};
	
	public enum TRANSFORM_DISTANCE_AGENT_FROM_HUMAN_ABSOLUTE implements TransformPredicateInterface
	{
		VERY_NEAR,
		NEAR,
		MEDIUM,
		FAR,
		VERY_FAR
	};
	
	//Relative Transforms
	public enum TRANSFORM_DURATION implements TransformPredicateInterface
	{
		MUCH_SHORTER,
		SHORTER,
		LONGER,
		MUCH_LONGER
	};
	
	public enum TRANSFORM_TEMPO implements TransformPredicateInterface
	{
		MUCH_SLOWER,
		SLOWER,
		FASTER,
		MUCH_FASTER
	};
	
	public enum TRANSFORM_SMOOTHNESS implements TransformPredicateInterface
	{
		MORE_SMOOTH,
		LESS_SMOOTH
	};
	
	public enum TRANSFORM_ENERGY implements TransformPredicateInterface
	{
		MUCH_LESS_ENERGY,
		LESS_ENERGY,
		MORE_ENERGY,
		MUCH_MORE_ENERGY
	};
	
	public enum TRANSFORM_BODY_SYMMETRY implements TransformPredicateInterface
	{
		TRUE
	};
	
	public enum TRANSFORM_SIZE implements TransformPredicateInterface
	{
		MUCH_SMALLER,
		SMALLER,
		LARGER,
		MUCH_LARGER
	};
	
	public enum TRANSFORM_QUADRANT implements TransformPredicateInterface
	{
		TO_LEFT,
		TO_RIGHT,
		TO_TOP,
		TO_BOTTOM,
		TO_TOP_LEFT,
		TO_TOP_RIGHT,
		TO_BOTTOM_LEFT,
		TO_BOTTOM_RIGHT
	};
	
	public enum TRANSFORM_DISTANCE_FROM_CENTER implements TransformPredicateInterface
	{
		MUCH_LESS_DISTANCE,
		LESS_DISTANCE,
		MORE_DISTANCE,
		MUCH_MORE_DISTANCE
	};
	
	public enum TRANSFORM_HEIGHT implements TransformPredicateInterface
	{
		LESS_HEIGHT,
		MORE_HEIGHT
	};
	
	public enum TRANSFORM_DISTANCE_AGENT_FROM_HUMAN implements TransformPredicateInterface
	{
		MUCH_LESS_DISTANCE,
		LESS_DISTANCE,
		MORE_DISTANCE,
		MUCH_MORE_DISTANCE
	};
	
	public enum TRANSFORM_FACING implements TransformPredicateInterface
	{
		TO_HUMAN,
		AWAY_FROM_HUMAN,
		SYMETRICALLY_OPPOSITE
	};
	
	public enum ADD_REFLECT_LIMB implements TransformPredicateInterface
	{
		LEFT_ARM_LONGITUDINALLY,
		LEFT_ARM_VERTICALLY,
		LEFT_ARM_TRANSVERSALLY,
		LEFT_LEG_LONGITUDINALLY,
		LEFT_LEG_VERTICALLY,
		LEFT_LEG_TRANSVERSALLY,
		RIGHT_ARM_LONGITUDINALLY,
		RIGHT_ARM_VERTICALLY,
		RIGHT_ARM_TRANSVERSALLY,
		RIGHT_LEG_LONGITUDINALLY,
		RIGHT_LEG_VERTICALLY,
		RIGHT_LEG_TRANSVERSALLY;

		public LIMB getLimb() {
			String name = this.name();
			String[] split = name.split("_");
			
			return LIMB.valueOf(split[0] + "_" + split[1]);
		}

		public REFLECT_TYPE getReflectType() {
			String name = this.name();
			String[] split = name.split("_");
			
			return REFLECT_TYPE.valueOf(split[2]);
		}
	};
	
	//Deprecated
	@Deprecated
	public enum ADD_MODIFY_LIMB_MOTION_SAME_SET implements TransformPredicateInterface
	{
		REFLECT_RIGHT_HAND,
		REFLECT_RIGHT_LEG,
		REFLECT_LEFT_HAND,
		REFLECT_LEFT_LEG,
		COPY_RIGHT_HAND,
		COPY_RIGHT_LEG,
		COPY_LEFT_HAND,
		COPY_LEFT_LEG
	};

	//Deprecated
	@Deprecated
	public enum ADD_MODIFY_LIMB_MOTION_OTHER_SET implements TransformPredicateInterface
	{
		REFLECT_RIGHT_HAND,
		REFLECT_RIGHT_LEG,
		REFLECT_LEFT_HAND,
		REFLECT_LEFT_LEG,
		COPY_RIGHT_HAND,
		COPY_RIGHT_LEG,
		COPY_LEFT_HAND,
		COPY_LEFT_LEG
	};
	
	public enum ADD_SWITCH_LIMBS implements TransformPredicateInterface
	{
		RIGHT_ARM_LEFT_ARM,
		RIGHT_ARM_RIGHT_LEG,
		RIGHT_ARM_LEFT_LEG,
		LEFT_ARM_RIGHT_ARM,
		LEFT_ARM_RIGHT_LEG,
		LEFT_ARM_LEFT_LEG,
		RIGHT_LEG_LEFT_LEG,
		RIGHT_LEG_RIGHT_ARM,
		RIGHT_LEG_LEFT_ARM,
		LEFT_LEG_RIGHT_LEG,
		LEFT_LEG_RIGHT_ARM,
		LEFT_LEG_LEFT_ARM;

		public LIMB getLimb1() {
			String name = this.name();
			String[] split = name.split("_");
			
			return LIMB.valueOf(split[0] + "_" + split[1]);
		}

		public LIMB getLimb2() {
			String name = this.name();
			String[] split = name.split("_");
			
			return LIMB.valueOf(split[2] + "_" + split[3]);
		}
	};

	public enum ADD_COPY_LIMB implements TransformPredicateInterface
	{
		RIGHT_ARM_LEFT_ARM,
		RIGHT_ARM_RIGHT_LEG,
		RIGHT_ARM_LEFT_LEG,
		RIGHT_ARM_LEFT_ARM_RIGHT_LEG,
		RIGHT_ARM_LEFT_ARM_LEFT_LEG,
		RIGHT_ARM_RIGHT_LEG_LEFT_LEG,
		RIGHT_ARM_LEFT_ARM_RIGHT_LEG_LEFT_LEG,
		LEFT_ARM_RIGHT_ARM,
		LEFT_ARM_RIGHT_LEG,
		LEFT_ARM_LEFT_LEG,
		LEFT_ARM_RIGHT_ARM_RIGHT_LEG,
		LEFT_ARM_RIGHT_ARM_LEFT_LEG,
		LEFT_ARM_RIGHT_LEG_LEFT_LEG,
		LEFT_ARM_RIGHT_ARM_RIGHT_LEG_LEFT_LEG,
		RIGHT_LEG_LEFT_LEG,
		RIGHT_LEG_RIGHT_ARM,
		RIGHT_LEG_LEFT_ARM,
		RIGHT_LEG_LEFT_LEG_RIGHT_ARM,
		RIGHT_LEG_LEFT_LEG_LEFT_ARM,
		RIGHT_LEG_RIGHT_ARM_LEFT_ARM,
		RIGHT_LEG_LEFT_LEG_RIGHT_ARM_LEFT_ARM,
		LEFT_LEG_RIGHT_LEG,
		LEFT_LEG_RIGHT_ARM,
		LEFT_LEG_LEFT_ARM,
		LEFT_LEG_RIGHT_LEG_RIGHT_ARM,
		LEFT_LEG_RIGHT_LEG_LEFT_ARM,
		LEFT_LEG_RIGHT_ARM_LEFT_ARM,
		LEFT_LEG_RIGHT_LEG_RIGHT_ARM_LEFT_ARM;

		public LIMB getSource() {
			String name = this.name();
			String[] split = name.split("_");
			
			return LIMB.valueOf(split[0] + "_" + split[1]);
		}

		public ArrayList<LIMB> getTarget() {
			String name = this.name();
			String[] split = name.split("_");
			ArrayList<LIMB> limbs = new ArrayList<LIMB>();
			
			limbs.add(LIMB.valueOf(split[2] + "_" + split[3]));
			
			if(split.length > 4)
			{
				limbs.add(LIMB.valueOf(split[4] + "_" + split[5]));
			}
			
			if(split.length > 6)
			{
				limbs.add(LIMB.valueOf(split[6] + "_" + split[7]));
			}
			
			return limbs;
		}
	};
	
	public enum ADD_REPEAT implements TransformPredicateInterface
	{
		FEW,
		MEDIUM,
		MANY
	};
	
	public enum ADD_JUMPING implements TransformPredicateInterface
	{
		TRUE
	};
	
	public enum ADD_CROUCHING implements TransformPredicateInterface
	{
		TRUE
	};
	
	public enum ADD_SPINNING implements TransformPredicateInterface
	{
		TRUE
	};
	
	public enum ADD_RADIAL_SYMMETRIC_SPIN implements TransformPredicateInterface
	{
		TRUE
	};
	
	public static TransformPredicateInterface parseTransformPredicates(String enumName, String enumValue)
	{
		if (enumName.equals("TRANSFORM_DURATION")) {
			return TRANSFORM_DURATION.valueOf(enumValue);
		}
		else if (enumName.equals("TRANSFORM_TEMPO")) {
			return TRANSFORM_TEMPO.valueOf(enumValue);
		}
		else if (enumName.equals("TRANSFORM_SMOOTHNESS")) {
			return TRANSFORM_SMOOTHNESS.valueOf(enumValue);
		}
		else if (enumName.equals("TRANSFORM_ENERGY")) {
			return TRANSFORM_ENERGY.valueOf(enumValue);
		}
		else if (enumName.equals("TRANSFORM_BODY_SYMMETRY")) {
			return TRANSFORM_BODY_SYMMETRY.valueOf(enumValue);
		}
		else if (enumName.equals("TRANSFORM_SIZE")) {
			return TRANSFORM_SIZE.valueOf(enumValue);
		}
		else if (enumName.equals("TRANSFORM_QUADRANT")) {
			return TRANSFORM_QUADRANT.valueOf(enumValue);
		}
		else if (enumName.equals("TRANSFORM_DISTANCE_FROM_CENTER")) {
			return TRANSFORM_DISTANCE_FROM_CENTER.valueOf(enumValue);
		}
		else if (enumName.equals("TRANSFORM_HEIGHT")) {
			return TRANSFORM_HEIGHT.valueOf(enumValue);
		}
		else if (enumName.equals("TRANSFORM_DISTANCE_AGENT_FROM_HUMAN")) {
			return TRANSFORM_DISTANCE_AGENT_FROM_HUMAN.valueOf(enumValue);
		}
		else if (enumName.equals("TRANSFORM_FACING")) {
			return TRANSFORM_FACING.valueOf(enumValue);
		}
		else if (enumName.equals("ADD_REFLECT_LIMB")) {
			return ADD_REFLECT_LIMB.valueOf(enumValue);
		}
		else if (enumName.equals("ADD_SWITCH_LIMBS")) {
			return ADD_SWITCH_LIMBS.valueOf(enumValue);
		}
		else if (enumName.equals("ADD_COPY_LIMB")) {
			return ADD_COPY_LIMB.valueOf(enumValue);
		}
		else if (enumName.equals("ADD_MODIFY_LIMB_MOTION_SAME_SET")) {
			System.err.println("DEPRECATED! Switch to newer operation");
			return ADD_MODIFY_LIMB_MOTION_SAME_SET.valueOf(enumValue);
		}
		else if (enumName.equals("ADD_MODIFY_LIMB_MOTION_OTHER_SET")) {
			System.err.println("DEPRECATED! Switch to newer operation");
			return ADD_MODIFY_LIMB_MOTION_OTHER_SET.valueOf(enumValue);
		}
		else if (enumName.equals("ADD_REPEAT")) {
			return ADD_REPEAT.valueOf(enumValue);
		}
		else if (enumName.equals("ADD_JUMPING")) {
			return ADD_JUMPING.valueOf(enumValue);
		}
		else if (enumName.equals("ADD_CROUCHING")) {
			return ADD_CROUCHING.valueOf(enumValue);
		}
		else if (enumName.equals("ADD_SPINNING")) {
			return ADD_SPINNING.valueOf(enumValue);
		}
		else if (enumName.equals("ADD_RADIAL_SYMMETRIC_SPIN")) {
			return ADD_RADIAL_SYMMETRIC_SPIN.valueOf(enumValue);
		}
		else if (enumName.equals("RESPONSE_MODE")) {
			return RESPONSE_MODE.valueOf(enumValue);
		}
		
		System.out.println("Error: Unknown enumName");
		return null;
	}
}
