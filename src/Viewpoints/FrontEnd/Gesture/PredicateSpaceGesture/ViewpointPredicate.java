package Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.JIDX;
import processing.core.PVector;

public abstract class ViewpointPredicate implements Serializable {
	/**
	 * Do not change unless you want to intentionally break serializability
	 * with previous versions of this class
	 */
	private static final long serialVersionUID = -1027861961215620161L;
	final static int ASSUMED_FRAMERATE = 30;
	float predNumber;
	Predicate predValue;

	final static int valueMultiplier = 1; // used to scale all predicate values if changing framerate

	// define set of gesture predicates to use
	final static ArrayList<JIDX> vpJoints = new ArrayList<JIDX>(Arrays.asList(
			JIDX.HEAD,
			JIDX.LEFT_SHOULDER, JIDX.LEFT_ELBOW, JIDX.LEFT_HAND, JIDX.LEFT_HIP, JIDX.LEFT_KNEE, JIDX.LEFT_FOOT,
			JIDX.RIGHT_SHOULDER, JIDX.RIGHT_ELBOW, JIDX.RIGHT_HAND, JIDX.RIGHT_HIP, JIDX.RIGHT_KNEE, JIDX.RIGHT_FOOT
			));
		
	public ViewpointPredicate(ArrayList<Body> skelPositions) {
		this.predNumber = calcPredNumber(skelPositions);
		this.predValue = classifyPredicate(this.predNumber);
	}
	
	public ViewpointPredicate(float predNumber) {
		this.predNumber = predNumber;
		this.predValue = classifyPredicate(this.predNumber);
	}
	
	protected float toTargetRange(float rawValue, float[] rawLevels, float[] targetLevels) {
		if (rawValue <= rawLevels[0]) return targetLevels[0];

		for (int i = 1; i < rawLevels.length; i++) {
			if (rawValue < rawLevels[i]) {
				float percent = (rawValue - rawLevels[i-1]) / (rawLevels[i] - rawLevels[i-1]);
				return targetLevels[i-1] + percent * (targetLevels[i] - targetLevels[i-1]);
			}
		}
		
		return targetLevels[targetLevels.length - 1];
	}

	/**
	 * Given a numeric value for the predicate return it's enumerated value
	 * @param predNumber
	 * @return
	 */
	public abstract Predicate classifyPredicate(float predNumber);

	
	/**
	 * Given a sequence of skeleton joints (most recent last) return a predicate number - from 0 to 1, with 0.5 corresponding to medium value
	 * for the given class type
	 * @param skelPositions
	 * @return
	 */
	public abstract float calcPredNumber(ArrayList<Body> skelPositions);

	/**
	 * Given a sequence of skeleton joints (most recent last) return a predicate enumerated value
	 * for the given class type
	 * 
	 * @param skelPositions
	 * @return
	 */
	public Predicate calcPredicate(ArrayList<Body> skelPositions) {
		float mPredNumber = calcPredNumber(skelPositions);
		Predicate mPredValue = classifyPredicate(mPredNumber);
		return mPredValue;
	}


	/**
	 * Compute difference in position between two sets of skeleton information.
	 * @param inCurSkel
	 * @param inPrevSkel
	 * @return
	 */
	public HashMap<JIDX, PVector> getMvmtDiff  (Map<JIDX, PVector> inCurSkel,
												float iCurrTimestamp,
												Map<JIDX, PVector> inPrevSkel,
												float iPrevTimestamp) {
		float factor = 1.0f/(iCurrTimestamp - iPrevTimestamp);
		HashMap<JIDX, PVector> diffVecs = new HashMap<JIDX, PVector>();
		for (JIDX ji : JIDX.ALL_JIDX) {
			PVector delta = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			delta.mult(factor);
			diffVecs.put(ji, delta);
		}
		return diffVecs;
	}
	
	public HashMap<JIDX, PVector> getLeftLegMvmtDiff(Map<JIDX, PVector> inCurSkel, Map<JIDX, PVector> inPrevSkel) {
		HashMap<JIDX, PVector> diffVecs = new HashMap<JIDX, PVector>();
		JIDX ji = JIDX.LEFT_FOOT;
			diffVecs.put(ji,PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji)));
		ji = JIDX.LEFT_KNEE;
			diffVecs.put(ji,PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji)));
		ji = JIDX.LEFT_HIP;
			diffVecs.put(ji,PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji)));
		return diffVecs;
	}
	
	public HashMap<JIDX, PVector> getRightLegMvmtDiff(Map<JIDX, PVector> inCurSkel, Map<JIDX, PVector> inPrevSkel) {
		HashMap<JIDX, PVector> diffVecs = new HashMap<JIDX, PVector>();
		JIDX ji = JIDX.RIGHT_FOOT;
			diffVecs.put(ji,PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji)));
		ji = JIDX.RIGHT_KNEE;
			diffVecs.put(ji,PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji)));
		ji = JIDX.RIGHT_HIP;
			diffVecs.put(ji,PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji)));
		return diffVecs;
	}
	
	public HashMap<JIDX, PVector> getLeftHandMvmtDiff(Map<JIDX, PVector> inCurSkel, Map<JIDX, PVector> inPrevSkel) {
		HashMap<JIDX, PVector> diffVecs = new HashMap<JIDX, PVector>();
		JIDX ji = JIDX.LEFT_SHOULDER;
			diffVecs.put(ji,PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji)));
		ji = JIDX.LEFT_ELBOW;
			diffVecs.put(ji,PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji)));
		ji = JIDX.LEFT_HAND;
			diffVecs.put(ji,PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji)));
		return diffVecs;
	}
	
	public HashMap<JIDX, PVector> getRightHandMvmtDiff(Map<JIDX, PVector> inCurSkel, Map<JIDX, PVector> inPrevSkel) {
		HashMap<JIDX, PVector> diffVecs = new HashMap<JIDX, PVector>();
		JIDX ji = JIDX.RIGHT_SHOULDER;
			diffVecs.put(ji,PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji)));
		ji = JIDX.RIGHT_ELBOW;
			diffVecs.put(ji,PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji)));
		ji = JIDX.RIGHT_HAND;
			diffVecs.put(ji,PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji)));
		return diffVecs;
	}
	
	public HashMap<JIDX, PVector> getLeftLegTransverseMvmtDiff(Map<JIDX, PVector> inCurSkel, Map<JIDX, PVector> inPrevSkel) {
		HashMap<JIDX, PVector> diffVecs = new HashMap<JIDX, PVector>();
		JIDX ji = JIDX.LEFT_FOOT;
			PVector difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.z = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.LEFT_KNEE;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.z = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.LEFT_HIP;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.z = 0;
			diffVecs.put(ji, difference);
		return diffVecs;
	}
	
	public HashMap<JIDX, PVector> getRightLegTransverseMvmtDiff(Map<JIDX, PVector> inCurSkel, Map<JIDX, PVector> inPrevSkel) {
		HashMap<JIDX, PVector> diffVecs = new HashMap<JIDX, PVector>();
		JIDX ji = JIDX.RIGHT_FOOT;
			PVector difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.z = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.RIGHT_KNEE;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.z = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.RIGHT_HIP;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.z = 0;
			diffVecs.put(ji, difference);
		return diffVecs;
	}
	
	public HashMap<JIDX, PVector> getLeftHandTransverseMvmtDiff(Map<JIDX, PVector> inCurSkel, Map<JIDX, PVector> inPrevSkel) {
		HashMap<JIDX, PVector> diffVecs = new HashMap<JIDX, PVector>();
		JIDX ji = JIDX.LEFT_SHOULDER;
			PVector difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.z = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.LEFT_ELBOW;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.z = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.LEFT_HAND;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.z = 0;
			diffVecs.put(ji, difference);
		return diffVecs;
	}
	
	public HashMap<JIDX, PVector> getRightHandTransverseMvmtDiff(Map<JIDX, PVector> inCurSkel, Map<JIDX, PVector> inPrevSkel) {
		HashMap<JIDX, PVector> diffVecs = new HashMap<JIDX, PVector>();
		JIDX ji = JIDX.RIGHT_SHOULDER;
			PVector difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.z = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.RIGHT_ELBOW;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.z = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.RIGHT_HAND;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.z = 0;
			diffVecs.put(ji, difference);
		return diffVecs;
	}
	
	public HashMap<JIDX, PVector> getLeftLegLongitudinalMvmtDiff(Map<JIDX, PVector> inCurSkel, Map<JIDX, PVector> inPrevSkel) {
		HashMap<JIDX, PVector> diffVecs = new HashMap<JIDX, PVector>();
		JIDX ji = JIDX.LEFT_FOOT;
			PVector difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.LEFT_KNEE;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.LEFT_HIP;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		return diffVecs;
	}
	
	public HashMap<JIDX, PVector> getRightLegLongitudinalMvmtDiff(Map<JIDX, PVector> inCurSkel, Map<JIDX, PVector> inPrevSkel) {
		HashMap<JIDX, PVector> diffVecs = new HashMap<JIDX, PVector>();
		JIDX ji = JIDX.RIGHT_FOOT;
			PVector difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.RIGHT_KNEE;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.RIGHT_HIP;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		return diffVecs;
	}
	
	public HashMap<JIDX, PVector> getLeftHandLongitudinalMvmtDiff(Map<JIDX, PVector> inCurSkel, Map<JIDX, PVector> inPrevSkel) {
		HashMap<JIDX, PVector> diffVecs = new HashMap<JIDX, PVector>();
		JIDX ji = JIDX.LEFT_SHOULDER;
			PVector difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.LEFT_ELBOW;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.LEFT_HAND;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		return diffVecs;
	}
	
	public HashMap<JIDX, PVector> getRightHandLongitudinalMvmtDiff(Map<JIDX, PVector> inCurSkel, Map<JIDX, PVector> inPrevSkel) {
		HashMap<JIDX, PVector> diffVecs = new HashMap<JIDX, PVector>();
		JIDX ji = JIDX.RIGHT_SHOULDER;
			PVector difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.RIGHT_ELBOW;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.RIGHT_HAND;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.y = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		return diffVecs;
	}
	
	public HashMap<JIDX, PVector> getLeftLegVerticalMvmtDiff(Map<JIDX, PVector> inCurSkel, Map<JIDX, PVector> inPrevSkel) {
		HashMap<JIDX, PVector> diffVecs = new HashMap<JIDX, PVector>();
		JIDX ji = JIDX.LEFT_FOOT;
			PVector difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.z = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.LEFT_KNEE;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.z = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.LEFT_HIP;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.z = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		return diffVecs;
	}
	
	public HashMap<JIDX, PVector> getRightLegVerticalMvmtDiff(Map<JIDX, PVector> inCurSkel, Map<JIDX, PVector> inPrevSkel) {
		HashMap<JIDX, PVector> diffVecs = new HashMap<JIDX, PVector>();
		JIDX ji = JIDX.RIGHT_FOOT;
			PVector difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.z = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.RIGHT_KNEE;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.z = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.RIGHT_HIP;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.z = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		return diffVecs;
	}
	
	public HashMap<JIDX, PVector> getLeftHandVerticalMvmtDiff(Map<JIDX, PVector> inCurSkel, Map<JIDX, PVector> inPrevSkel) {
		HashMap<JIDX, PVector> diffVecs = new HashMap<JIDX, PVector>();
		JIDX ji = JIDX.LEFT_SHOULDER;
			PVector difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.z = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.LEFT_ELBOW;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.z = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.LEFT_HAND;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.z = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		return diffVecs;
	}
	
	public HashMap<JIDX, PVector> getRightHandVerticalMvmtDiff(Map<JIDX, PVector> inCurSkel, Map<JIDX, PVector> inPrevSkel) {
		HashMap<JIDX, PVector> diffVecs = new HashMap<JIDX, PVector>();
		JIDX ji = JIDX.RIGHT_SHOULDER;
			PVector difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.z = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.RIGHT_ELBOW;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.z = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		ji = JIDX.RIGHT_HAND;
			difference = PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji));
			difference.z = 0;
			difference.x = 0;
			diffVecs.put(ji, difference);
		return diffVecs;
	}
	
	
	/**
	 * Returns the values of the most recent numFrames predicates given a list of predicates
	 * @param predicateList
	 * @param numFrames
	 * @return
	 */
	public static <T extends ViewpointPredicate> List<Float> getPredicateValueHist(ArrayList<T> predicateList, int numFrames) {
		numFrames = Math.max(0, numFrames);
		List<T> predicateSublist = predicateList.subList(Math.max(0, predicateList.size()-numFrames), predicateList.size());
		List<Float> predicateValues = new ArrayList<Float>();
		for (int i=0; i<predicateSublist.size(); i++) {
			predicateValues.add(predicateSublist.get(i).getPredNumber());
		}
		return predicateValues;
	}
	
	/**
	 * Compute the running maximum value of the most recent numFrames predicates given the list of predicates
	 * @param predicateList
	 * @param numFrames
	 * @return
	 */
	public static <T extends ViewpointPredicate> float predicateRunningMax(ArrayList<T> predicateList, int numFrames) {
		return runningMax(getPredicateValueHist(predicateList, numFrames));
	}
	
	/**
	 * Compute the running average value of the most recent numFrames predicates given the list of predicates
	 * @param predicateList
	 * @param numFrames
	 * @return
	 */
	public static <T extends ViewpointPredicate> float predicateRunningAvg(ArrayList<T> predicateList, int numFrames) {
		return runningAvg(getPredicateValueHist(predicateList, numFrames));
	}
	
	/**
	 * Compute the running average value of the most recent numFrames predicates given the list of predicates,
	 * indicating whether to include or exclude nonzero values
	 * @param predicateList
	 * @param numFrames
	 * @param nonzero
	 * @return
	 */
	public static <T extends ViewpointPredicate> float predicateRunningAvg(ArrayList<T> predicateList, int numFrames, boolean nonzero) {
		return runningAvg(getPredicateValueHist(predicateList, numFrames), nonzero);
	}


	/**
	 * Returns maximum value from a HashMap of joints to floats
	 * @param inVals
	 * @return
	 */
	public static float getMax(HashMap<JIDX, Float> inVals) {
		return Collections.max(inVals.values());
	}

	/**
	 * Returns JIDX on the joint with maximum value
	 * @param inVals
	 * @return
	 */
	public static JIDX whichMax(HashMap<JIDX, Float> inVals) {
		Float maxval = Float.MIN_VALUE;
		JIDX maxJidx = null;
		for (JIDX ji : inVals.keySet()) {
			if (inVals.get(ji) > maxval) {
				maxval = inVals.get(ji);
				maxJidx = ji;
			}
		}
		return maxJidx;
	}

	public static Float runningAvg(List<Float> list) {
		if (list.size() < 1) {
			return 0f;
		}

		Float avg = 0f;
		for (int i=0; i<list.size(); i++) {
			avg += list.get(i);
		}
		return avg / list.size();
	}

	/**
	 * Returns running average of a history of values. 
	 * Optionally only uses values that are non-zero.
	 * @param list
	 * @param nonzero
	 * @return
	 */
	public static Float runningAvg(List<Float> list, boolean nonzero) {
		if (nonzero == true) {
			List<Float> newHist = new ArrayList<Float>();
			for (int i=0; i<list.size(); i++) {
				if (list.get(i) > Math.pow(10,-3)) {
					newHist.add(list.get(i));
				}
			}
			return runningAvg(newHist);
		}
		return runningAvg(list);
	}

	/**
	 * Returns maximum value from a history of values.
	 * @param list
	 * @return
	 */
	public static Float runningMax(List<Float> list) {
		return Collections.max(list);
	}
	
	public String toString() {
		// match on class name to get truncated version of enum name, rather than full class path
		String longClassName = this.predValue.getClass().toString();
		String shortClassName = "";
		
		Pattern predicateNamePattern = Pattern.compile("\\$([\\w\\_]+)");
		Matcher predicateNameMatcher;
		predicateNameMatcher = predicateNamePattern.matcher(longClassName);
		if (predicateNameMatcher.find()) {
			shortClassName = predicateNameMatcher.group(1);
		}
		
		String outString = shortClassName + ": " + this.predValue.toString() + " (" + this.predNumber + ")";
		return outString;
	}

	
	public float getPredNumber() {
		return this.predNumber;
	}
	
	public Predicate getPredValue() {
		return this.predValue;
	}
}
