package Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture;

public interface Predicate {
	String name();
	String toString();
}
