package Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture;

import java.util.ArrayList;

import Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture.Predicates.DURATION;
import Viewpoints.FrontEnd.Shared.Body;

public class DurationPredicate extends ViewpointPredicate {
	/**
	 * Generated serial number.
	 */
	private static final long serialVersionUID = -5630165120483221706L;
	private static final float[] rawLevels    = {0.0f, 
												 10.0f / ASSUMED_FRAMERATE, 
												 20.0f / ASSUMED_FRAMERATE, 
												 30.0f / ASSUMED_FRAMERATE,
												 40.0f / ASSUMED_FRAMERATE,
												 60.0f / ASSUMED_FRAMERATE};
	private static final float[] scaledLevels = {0.0f, 0.15f, 0.35f, 0.5f, 0.65f, 1.0f};
	
	public DurationPredicate(ArrayList<Body> skelPositions) {
		super(skelPositions);
	}
	
	public DurationPredicate(float predNumber) {
		super(predNumber);
	}


	/**
	 * Given a numeric value for the predicate return it's enumerated value
	 * @param predNumber
	 * @return
	 */
	public DURATION classifyPredicate(float predNumber) {
		if (predNumber < 0.15f) {
			return DURATION.VERY_SHORT;
		} else if (predNumber < 0.35f) {
			return DURATION.SHORT;
		} else if (predNumber < 0.65f) {
			return DURATION.MEDIUM;
		} else if (predNumber < 1.0f) {
			return DURATION.LONG;
		} else {
			return DURATION.VERY_LONG;
		}
	}

	/**
	 * Given a sequence of skeleton joints (most recent last) return a predicate number
	 * for the given class type
	 * @param skelPositions
	 * @return
	 */
	public float calcPredNumber(ArrayList<Body> skelPositions) {
		return toTargetRange(skelPositions.get(skelPositions.size() - 1).getTimestamp() - skelPositions.get(0).getTimestamp(),
							rawLevels, scaledLevels);
	}
}
