package Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import Viewpoints.FrontEnd.Shared.JIDX;
import processing.core.PVector;

public class Predicates implements Serializable
{
	/**
	 * Do not change unless you want to intentionally break serializability
	 * with previous versions of this class
	 */
	private static final long serialVersionUID = -2320950865395314483L;

	final static int valueMultiplier = 1; // used to scale all predicate values if changing framerate
	
	// define set of gesture predicates to use
	final static ArrayList<JIDX> vpJoints = new ArrayList<JIDX>(Arrays.asList(
			JIDX.HEAD,
			JIDX.LEFT_SHOULDER, JIDX.LEFT_ELBOW, JIDX.LEFT_HAND, JIDX.LEFT_HIP, JIDX.LEFT_KNEE, JIDX.LEFT_FOOT,
			JIDX.RIGHT_SHOULDER, JIDX.RIGHT_ELBOW, JIDX.RIGHT_HAND, JIDX.RIGHT_HIP, JIDX.RIGHT_KNEE, JIDX.RIGHT_FOOT
			));
	
	//ViewPoints Predicates
	public enum DURATION implements Predicate
	{
		NONE,
		VERY_SHORT,
		SHORT,
		MEDIUM,
		LONG,
		VERY_LONG
	};
	
	public enum TEMPO implements Predicate
	{
		NONE,
		ALMOST_STILL,
		SLOW,
		MEDIUM,
		FAST,
		EXTREMELY_FAST
	};
	
	public enum EMOTION implements Predicate
	{
		NONE,
		ANGRY,
		JOY,
		SAD,
		FEAR
	};
	
	public enum LEFT_LEG_STILL implements Predicate
	{
		TRUE,
		FALSE
	};
	
	public enum RIGHT_LEG_STILL implements Predicate
	{
		TRUE,
		FALSE
	};
	
	public enum LEFT_HAND_STILL implements Predicate
	{
		TRUE,
		FALSE
	};
	
	public enum RIGHT_HAND_STILL implements Predicate
	{
		TRUE,
		FALSE
	};
	
	public enum LEFT_LEG_TRANSVERSE implements Predicate
	{
		TRUE,
		FALSE
	};
	
	public enum RIGHT_LEG_TRANSVERSE implements Predicate
	{
		TRUE,
		FALSE
	};
	
	public enum LEFT_HAND_TRANSVERSE implements Predicate
	{
		TRUE,
		FALSE
	};
	
	public enum RIGHT_HAND_TRANSVERSE implements Predicate
	{
		TRUE,
		FALSE
	};
	
	public enum LEFT_LEG_LONGITUDINAL implements Predicate
	{
		TRUE,
		FALSE
	};
	
	public enum RIGHT_LEG_LONGITUDINAL implements Predicate
	{
		TRUE,
		FALSE
	};
	
	public enum LEFT_HAND_LONGITUDINAL implements Predicate
	{
		TRUE,
		FALSE
	};
	
	public enum RIGHT_HAND_LONGITUDINAL implements Predicate
	{
		TRUE,
		FALSE
	};
	
	public enum LEFT_LEG_VERTICAL implements Predicate
	{
		TRUE,
		FALSE
	};
	
	public enum RIGHT_LEG_VERTICAL implements Predicate
	{
		TRUE,
		FALSE
	};
	
	public enum LEFT_HAND_VERTICAL implements Predicate
	{
		TRUE,
		FALSE
	};
	
	public enum RIGHT_HAND_VERTICAL implements Predicate
	{
		TRUE,
		FALSE
	};
	
	
	public enum SMOOTHNESS implements Predicate
	{
		NONE,
		SMOOTH,
		REGULAR,
		STACCATO
	};
	
	public enum ENERGY implements Predicate
	{
		NONE,
		ALMOST_NONE,
		LOW,
		MEDIUM,
		HIGH,
		EXTREMELY_HIGH
	};
	
	//Gesture Predicates
	public enum LEFTARM_CURVE
	{	
		NONE,
		STRAIGHT,
		BENT
	};
	
	public enum LEFTHAND_POS
	{
		NONE,
		IN,
		OUT
	};
	
	public enum LEFTHAND_HEIGHT
	{
		NONE,
		UP,
		DOWN,
		AT_MOUTH
	};
	
	public enum RIGHTARM_CURVE
	{
		NONE,
		STRAIGHT,
		BENT
	};
	
	public enum RIGHTHAND_POS
	{
		NONE,
		IN,
		OUT
	};
	
	public enum RIGHTHAND_HEIGHT
	{
		NONE,
		UP,
		DOWN,
		AT_MOUTH
	};
	
	public enum BOTHARM_CURVE
	{
		NONE,
		STRAIGHT,
		BENT
	};
	
	public enum LEFTLEG_CURVE
	{
		NONE,
		STRAIGHT,
		BENT
	};
	
	public enum RIGHTLEG_CURVE
	{
		NONE,
		STRAIGHT,
		BENT
	};
	
	public enum BODY_SYMMETRIC
	{
		NONE,
		SYMMETRIC,
		ASYMMETRIC
	};
	
	public enum HANDS_TOGETHER
	{
		NONE,
		TOGETHER,
		APART
	};
	
	public enum ARMS_CROSSED
	{
		NONE,
		CROSSED,
		NOT_CROSSED
	};
	
	public enum BOTH_HANDS_BY_CHEST
	{
		NONE,
		BY_CHEST,
		NOT_BY_CHEST
	};
	
	public enum FACING
	{
		NONE,
		FAR_LEFT,
		SLIGHT_LEFT,
		CENTER,
		SLIGHT_RIGHT,
		FAR_RIGHT
	};
	
	public enum HEIGHT
	{
		NONE,
		SHORT,
		MEDIUM,
		TALL
	};
	
	public enum QUADRANT
	{
		NONE,
		TOP_RIGHT,
		TOP_LEFT,
		BOTTOM_RIGHT,
		BOTTOM_LEFT
	};
	
	public enum DIST_CENTER
	{
		NONE,
		NEAR,
		MEDIUM,
		FAR
	};
	
	public enum SIZE
	{
		NONE,
		SMALL,
		MEDIUM,
		LARGE
	};
	
	//FUTURE WORK
//	public enum FloorPath
//	{
//		LINE,
//		GRID,
//		CURVE
//	};
	
	/**
	 * Given two skeleton positions compute the tempo of moving between them
	 * 
	 * DEFINITION: Tempo measures the rate of movement during a gesture.
	 * 
	 * @param inCurSkel
	 * @param inPrevSkel
	 * @return
	 */
	public static Float calcTempo(HashMap<JIDX, PVector> inCurSkel, HashMap<JIDX, PVector> inPrevSkel) {
		HashMap<JIDX, PVector> diffSkel = getMvmtDiff(inCurSkel, inPrevSkel);
		
		HashMap<JIDX, Float> tempos = new HashMap<JIDX, Float>();

		for (JIDX ji : diffSkel.keySet()) {
			if (vpJoints.contains(ji)) {
				tempos.put(ji, Math.abs(diffSkel.get(ji).mag()));
			}
		}
		float tempoVal = getMax(tempos) * valueMultiplier;
		return tempoVal;
	}
	
	/**
	 * Converts a tempo value into a predicate
	 * @param inValue
	 * @return
	 */
	public static TEMPO classifyTempo(float inValue) {
		if (inValue < 30) {
			return TEMPO.ALMOST_STILL;
		} else if (inValue < 60) {
			return TEMPO.SLOW;
		} else if (inValue < 120) {
			return TEMPO.MEDIUM;
		} else if (inValue < 180) {
			return TEMPO.FAST;
		} else if (inValue >= 240){
			return TEMPO.EXTREMELY_FAST;
		}
		return TEMPO.NONE; // something went wrong
	}
	
	/**
	 * Given two skeleton positions compute the energy moving between them.
	 * 
	 * DEFINITION: Energy measures the overall movement (velocity) of a gesture.
	 * 
	 * @param inCurSkel
	 * @param inPrevSkel
	 * @return
	 */
	public static Float calcEnergy(HashMap<JIDX, PVector> inCurSkel, HashMap<JIDX, PVector> inPrevSkel){
		HashMap<JIDX, PVector> diffSkel = getMvmtDiff(inCurSkel, inPrevSkel);

		float energy = 0f;

		for (JIDX ji : diffSkel.keySet()) {
			if (vpJoints.contains(ji)) {
				energy += Math.abs(diffSkel.get(ji).mag());
			}
		}
		return energy * valueMultiplier;
	}
	
	/**
	 * Converts an energy value into a predicate
	 * @param inValue
	 * @return
	 */
	public static ENERGY classifyEnergy(float inValue) {
		if (inValue < 300) {
			return ENERGY.ALMOST_NONE;
		} else if (inValue < 1000) {
			return ENERGY.LOW;
		} else if (inValue < 2000) {
			return ENERGY.MEDIUM;
		} else if (inValue < 3000) {
			return ENERGY.HIGH;
		} else if (inValue >= 4000) {
			return ENERGY.EXTREMELY_HIGH;
		}
		return ENERGY.NONE;
	}
	
	/**
	 * Given a series of three skeleton positions compute the smoothness moving between them 
	 * 
	 * DEFINITION: Smoothness measures the acceleration of a gesture. 
	 * 	Conveys how much a gesture has sharp changes in velocity. 
	 * 
	 * @param inCurSkel
	 * @param inPrevSkel
	 * @param inPrev2Skel
	 * @return
	 */
//	public static Float calcSmoothness(PVector[] inCurSkel, PVector[] inPrevSkel, PVector[] inPrev2Skel){
	public static Float calcSmoothness(HashMap<JIDX, PVector> inCurSkel, 
			HashMap<JIDX, PVector> inPrevSkel, HashMap<JIDX, PVector> inPrev2Skel){
		// get velocity between each pair of skeleton positions
		HashMap<JIDX, PVector> diffSkel = getMvmtDiff(inCurSkel, inPrevSkel);
		HashMap<JIDX, PVector> diff2Skel = getMvmtDiff(inPrevSkel, inPrev2Skel);

		// get acceleration by taking difference of velocities
		HashMap<JIDX, PVector> accSkel = getMvmtDiff(diffSkel, diff2Skel);
		
		HashMap<JIDX,Float> smoothnesses = new HashMap<JIDX,Float>();
		for (JIDX ji : accSkel.keySet()) {
			if (vpJoints.contains(ji)) {
				smoothnesses.put(ji, Math.abs(accSkel.get(ji).mag()));
			}
		}
		float smoothness = getMax(smoothnesses) * valueMultiplier; // smoothness is maximum acceleration
		return smoothness;
	}

	/**
	 * Converts a smoothness value into a predicate
	 * @param inValue
	 * @return
	 */
	public static SMOOTHNESS classifySmoothness(float inValue){
		if (inValue < 110) {
			return SMOOTHNESS.SMOOTH;
		} else if (inValue < 330) {
			return SMOOTHNESS.REGULAR;
		} else if (inValue >= 330){
			return SMOOTHNESS.STACCATO;
		}
		return SMOOTHNESS.NONE;
	}

	/**
	 * Given an ArrayList of predicates during a gesture compute the duration of that gesture.
	 * 
	 * DEFINITION: Duration measures how long a gesture lasts. 
	 * 
	 * @param gesture
	 * @return
	 */
	public static Integer calcDuration(ArrayList<FramePredicates> gestureFrames) {
		int numframes = gestureFrames.size();
		return numframes;
	}
	
	/**
	 * Given a gesture compute the duration of the gesture.
	 * 
	 * DEFINITION: Duration measures how long a gesture lasts. 
	 * 
	 * @param gesture
	 * @return
	 */
	public static Integer calcDuration(PredicateSpaceGesture gesture) {
		int numframes = gesture.getAll_Predicate_History().size();
		
//		DURATION out = classifyDuration(numframes);
		return numframes;
	}
	
	/**
	 * Converts a duration value (length of gesture history) into a predicate 
	 * @param inValue
	 * @return
	 */
	public static DURATION classifyDuration(float inValue){
		if (inValue < 10) {
			return DURATION.VERY_SHORT;
		} else if (inValue < 20) {
			return DURATION.SHORT;
		} else if (inValue < 40) {
			return DURATION.MEDIUM;
		} else if (inValue < 60) {
			return DURATION.LONG;
		} else if (inValue >= 60){
			return DURATION.VERY_LONG;
		}
		return DURATION.NONE;
	}
	
	
	/**
	 * Compute difference in position between two sets of skeleton information.
	 * @param inCurSkel
	 * @param inPrevSkel
	 * @return
	 */
	public static HashMap<JIDX, PVector> getMvmtDiff(HashMap<JIDX, PVector> inCurSkel, HashMap<JIDX, PVector> inPrevSkel) {
		if (inCurSkel.size() != inPrevSkel.size()) {
			throw new Error("trying to take difference of different length skeletons!");
		}
		
		HashMap<JIDX, PVector> diffVecs = new HashMap<JIDX, PVector>();
		for (JIDX ji : inCurSkel.keySet()) {
			diffVecs.put(ji,PVector.sub(inCurSkel.get(ji), inPrevSkel.get(ji)));
		}
		return diffVecs;
	}
	
	/**
	 * Returns maximum value from a HashMap of joints to floats
	 * @param inVals
	 * @return
	 */
	public static float getMax(HashMap<JIDX, Float> inVals) {
		return Collections.max(inVals.values());
	}
	
	/**
	 * Returns JIDX on the joint with maximum value
	 * @param inVals
	 * @return
	 */
	public static JIDX whichMax(HashMap<JIDX, Float> inVals) {
		Float maxval = Float.MIN_VALUE;
		JIDX maxJidx = null;
		for (JIDX ji : inVals.keySet()) {
			if (inVals.get(ji) > maxval) {
				maxval = inVals.get(ji);
				maxJidx = ji;
			}
		}
		return maxJidx;
	}
	
	public static Float runningAvg(List<Float> list) {
		if (list.size() < 1) {
			return 0f;
		}
		
		Float avg = 0f;
		for (int i=0; i<list.size(); i++) {
			avg += list.get(i);
		}
		return avg / list.size();
	}
	
	/**
	 * Returns running average of a history of values. 
	 * Optionally only uses values that are non-zero.
	 * @param list
	 * @param nonzero
	 * @return
	 */
	public static Float runningAvg(List<Float> list, boolean nonzero) {
		if (nonzero == true) {
			List<Float> newHist = new ArrayList<Float>();
			for (int i=0; i<list.size(); i++) {
				if (list.get(i) > Math.pow(10,-3)) {
					newHist.add(list.get(i));
				}
			}
			return runningAvg(newHist);
		}
		return runningAvg(list);
	}
	
	/**
	 * Returns maximum value from a history of values.
	 * @param list
	 * @return
	 */
	public static Float runningMax(List<Float> list) {
		return Collections.max(list);
	}
	
	public static void main(String[] args)
	{
		System.out.println(DURATION.LONG.name());
	}
}
