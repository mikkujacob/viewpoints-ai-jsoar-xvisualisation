package Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture;

import java.util.ArrayList;
import java.util.HashMap;

import processing.core.PVector;
import Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture.Predicates.ENERGY;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.JIDX;

public class EnergyPredicate extends ViewpointPredicate {
	/**
	 * Generated serial number.
	 */
	private static final long serialVersionUID = -6163853273020651115L;
	private static final float[] rawLevels    = {0.0f, 1500.0f, 3000.0f, 4000.0f, 6000.0f, 8000.0f};
	private static final float[] scaledLevels = {0.0f, 0.15f,   0.35f,   0.5f,    0.65f,   1.0f};
	
	public EnergyPredicate(ArrayList<Body> skelPositions) {
		super(skelPositions);
	}

	public EnergyPredicate(float predNumber) {
		super(predNumber);
	}
	
	/**
	 * Given a numeric value for the predicate return it's enumerated value
	 * @param predNumber
	 * @return
	 */
	public ENERGY classifyPredicate(float predNumber) {
		if (predNumber < 0.15f) {
			return ENERGY.ALMOST_NONE;
		} else if (predNumber < 0.35f) {
			return ENERGY.LOW;
		} else if (predNumber < 0.65f) {
			return ENERGY.MEDIUM;
		} else if (predNumber < 1.0f) {
			return ENERGY.HIGH;
		} else {
			return ENERGY.EXTREMELY_HIGH;
		}
	}
	
	/**
	 * Given a sequence of skeleton joints (most recent last) return a predicate number
	 * for the given class type
	 * @param skelPositions
	 * @return
	 */
	public float calcPredNumber(ArrayList<Body> skelPositions) {
		if (skelPositions.size() < 2) {
			throw new Error("calcPredNumber: too few (< 2) skeletons !");
		}
		Body inCurSkel = skelPositions.get(skelPositions.size()-1);
		Body inPrevSkel = skelPositions.get(skelPositions.size()-2);
		
		HashMap<JIDX, PVector> diffSkel = getMvmtDiff(inCurSkel, inCurSkel.getTimestamp(),
													  inPrevSkel, inPrevSkel.getTimestamp());

		float energy = 0f;

		for (JIDX ji : diffSkel.keySet()) {
			if (vpJoints.contains(ji)) {
				energy += diffSkel.get(ji).mag();
			}
		}
		return toTargetRange(energy * valueMultiplier, rawLevels, scaledLevels);
	}
}
