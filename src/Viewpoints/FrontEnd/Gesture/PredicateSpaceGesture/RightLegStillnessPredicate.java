package Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture;

import java.util.ArrayList;
import java.util.HashMap;

import processing.core.PVector;
import Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture.Predicates.*;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.JIDX;

public class RightLegStillnessPredicate extends ViewpointPredicate {
	/**
	 * Generated serial number
	 */
	private static final long serialVersionUID = -8130161558200132177L;
	private static final float[] rawLevels    = {0.0f, 30 * ASSUMED_FRAMERATE, 60 * ASSUMED_FRAMERATE};
	private static final float[] scaledLevels = {0.0f, 0.5f,                   1.0f};
	
	public RightLegStillnessPredicate(ArrayList<Body> skelPositions) {
		super(skelPositions);
	}

	public RightLegStillnessPredicate(float predNumber) {
		super(predNumber);
	}


	/**
	 * Given a numeric value for the predicate return it's enumerated value
	 * @param predNumber
	 * @return
	 */
	public RIGHT_LEG_STILL classifyPredicate(float predNumber) {
		if (predNumber < 0.5f) {
			//System.out.println("RIGHT LEG STILL");
			return RIGHT_LEG_STILL.TRUE;
		} else {
			//System.out.println("RIGHT LEG MOVING");
			return RIGHT_LEG_STILL.FALSE;
		}
			
	}
	
	/**
	 * Given a sequence of skeleton joints (most recent last) return a predicate number
	 * for the given class type
	 * @param skelPositions
	 * @return
	 */
	public float calcPredNumber(ArrayList<Body> skelPositions) {
		if (skelPositions.size() < 2) {
			throw new Error("calcPredNumber: too few (< 2) skeletons !");
		}
		Body inCurSkel = skelPositions.get(skelPositions.size()-1);
		Body inPrevSkel = skelPositions.get(skelPositions.size()-2);
		
		HashMap<JIDX, PVector> diffSkel = getRightLegMvmtDiff(inCurSkel, inPrevSkel);
		
		HashMap<JIDX, Float> tempos = new HashMap<JIDX, Float>();

		for (JIDX ji : diffSkel.keySet()) {
			if (vpJoints.contains(ji)) {
				tempos.put(ji, Math.abs(diffSkel.get(ji).mag()));
			}
		}
		float tempoVal = getMax(tempos) * valueMultiplier;
		tempoVal /= (inCurSkel.getTimestamp() - inPrevSkel.getTimestamp());
		//System.out.println("Tempo Val: "+tempoVal);
		return toTargetRange(tempoVal, rawLevels, scaledLevels);
	}
}
