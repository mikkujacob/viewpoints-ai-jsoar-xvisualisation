package Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture;

import java.util.ArrayList;
import java.util.HashMap;

import processing.core.PVector;
import Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture.Predicates.TEMPO;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.JIDX;

public class TempoPredicate extends ViewpointPredicate {
	/**
	 * Generated serial number
	 */
	private static final long serialVersionUID = 5553234702459732692L;
	private static final float[] rawLevels    = {0.0f, 
		 										30.0f * ASSUMED_FRAMERATE, 
		 										50.0f * ASSUMED_FRAMERATE, 
		 										65.0f * ASSUMED_FRAMERATE,
		 										80.0f * ASSUMED_FRAMERATE,
		 										120.0f / ASSUMED_FRAMERATE};
	private static final float[] scaledLevels = {0.0f, 0.15f, 0.35f, 0.5f, 0.65f, 1.0f};
	
	public TempoPredicate(ArrayList<Body> skelPositions) {
		super(skelPositions);
	}

	public TempoPredicate(float predNumber) {
		super(predNumber);
	}


	/**
	 * Given a numeric value for the predicate return it's enumerated value
	 * @param predNumber
	 * @return
	 */
	public TEMPO classifyPredicate(float predNumber) {
		if (predNumber < 0.15f) {
			return TEMPO.ALMOST_STILL;
		} else if (predNumber < 0.35f) {
			return TEMPO.SLOW;
		} else if (predNumber < 0.65f) {
			return TEMPO.MEDIUM;
		} else if (predNumber < 1.0f) {
			return TEMPO.FAST;
		} else {
			return TEMPO.EXTREMELY_FAST;
		}
	}
	
	/**
	 * Given a sequence of skeleton joints (most recent last) return a predicate number
	 * for the given class type
	 * @param skelPositions
	 * @return
	 */
	public float calcPredNumber(ArrayList<Body> skelPositions) {
		if (skelPositions.size() < 2) {
			throw new Error("calcPredNumber: too few (< 2) skeletons !");
		}
		Body inCurSkel = skelPositions.get(skelPositions.size()-1);
		Body inPrevSkel = skelPositions.get(skelPositions.size()-2);
		
		HashMap<JIDX, PVector> diffSkel = getMvmtDiff(	inCurSkel, inCurSkel.getTimestamp(), 
														inPrevSkel, inPrevSkel.getTimestamp());
		
		HashMap<JIDX, Float> tempos = new HashMap<JIDX, Float>();

		for (JIDX ji : diffSkel.keySet()) {
			if (vpJoints.contains(ji)) {
				tempos.put(ji, diffSkel.get(ji).mag());
			}
		}
		
		float tempoVal = getMax(tempos) * valueMultiplier;
		//System.out.println("Tempo Val: "+tempoVal);
		return toTargetRange(tempoVal, rawLevels, scaledLevels);
	}
}
