package Viewpoints.FrontEnd.Segmentation;

public enum SegmentMethods {
	ACTIVITY, RHYTHM, STILLNESS, ALL;
}
