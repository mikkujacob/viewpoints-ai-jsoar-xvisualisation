package Viewpoints.FrontEnd.Segmentation;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import processing.core.PVector;
import Viewpoints.FrontEnd.Clock.Clock;
import Viewpoints.FrontEnd.Model.AestheticsModel.ViewpointsAestheticsModel;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.JIDX;

/**
 * Gesture segmenting method dependent on detected a rhythmic motion. Uses the RhythmTracker to determine rhythm.
 * @author Lauren Winston
 *
 */
public class RhythmSegmenter extends GestureSegmenter {
	private static final float FORGETTING_TIME = 10.0f;
	private static final float JOINT_DEVIATION_TOLERANCE = 0.25f;
	private static final float REQUIRED_IN_TRACK_DEVIATION_FACTOR = 2.0f;
	private ArrayDeque<Body> trackOfMotion = null;
	private boolean trackIsSegment = false;
	private Clock gestureSegmentLength;
	
	public RhythmSegmenter() {
		super();
		trackOfMotion = new ArrayDeque<Body>();
	}

	@Override
	public void takePose(Body body) {
		trackIsSegment = false;
		updateTrack(body);
	}
	
	@Override
	public boolean isTracking() {
		return ViewpointsAestheticsModel.getRhythmTracker().hasRhythm();
	}
	
	@Override
	public boolean segmentStarted() {
		boolean started = ViewpointsAestheticsModel.getRhythmTracker().rhythmJustStarted();
		boolean period = false;

		/*
		 * Update the timer for gesture length.
		 */
		if (started) {
			gestureSegmentLength = new Clock();
			gestureSegmentLength.start();
		} else if (ViewpointsAestheticsModel.getRhythmTracker().hasRhythm()) {
			if (gestureSegmentLength == null) {
				gestureSegmentLength = new Clock();
				gestureSegmentLength.start();
				period = true;
			}
		}

		return started || period;
	}

	@Override
	public boolean segmentEnded() {
		boolean ended = ViewpointsAestheticsModel.getRhythmTracker().rhythmJustEnded();
		boolean period = false;
		
		/*
		 * Force an ended segment if the gesture length has exceeded the period.
		 */
		if (gestureSegmentLength != null) {
			gestureSegmentLength.check();
			if (gestureSegmentLength.time() > ViewpointsAestheticsModel.getRhythmTracker().getPeriod()) {
				period = true;
			}
		}

		/*
		 * Reset the clock if the time has expired.
		 */
		if (ended || period) {
			gestureSegmentLength = null;
		}
		
		return ended || period;
	}
	
	@Override
	public ArrayList<Body> getSegment() {
		/*if(!trackIsSegment) {
			return null;
		}*/

		segment = new ArrayList<Body>();
		if (trackOfMotion != null) {
			segment.addAll(trackOfMotion);
		}

		return segment;
	}
	
	//TODO: (Lauren) Make the naming clearer across clear() and reset()
	public void clear(){
		if (this.trackOfMotion != null) {
			this.trackOfMotion.clear();
		}
	}

	@Override
	public void reset() {
		this.trackOfMotion = null;
	}

	private void updateTrack(Body body) {
		if (null == trackOfMotion)
			return;
		
		trackOfMotion.add(body);
		
		while (body.getTimestamp() - trackOfMotion.getFirst().getTimestamp() > FORGETTING_TIME) {
			trackOfMotion.poll();
		}
		
		ArrayList<Float> activeBeats = new ArrayList<Float>();
		ViewpointsAestheticsModel.getRhythmTracker().collectActiveBeats(activeBeats);
		Collections.sort(activeBeats);
		
		for (Float activePeriod: activeBeats) {
			Body historyBody = regress(activePeriod);
			if (null == historyBody) continue;
			//Continue if historyBody does not meet certain similarlity constraints
			if (!body.isSimilar(historyBody, JOINT_DEVIATION_TOLERANCE)) continue;
			if (!sufficientDeviationWithinTrack(body, historyBody)) continue;

			//Before track is cut to segment, it is ensured that the body is similar enough.
			cutTrackToSegment(historyBody);
			break;
		}
	}
	
	/**
	 * 
	 * @param deltaTime The active period.
	 * @return Body closest to the active period.
	 */
	private Body regress(float deltaTime) {
		if (trackOfMotion.isEmpty()) return null;
		float targetTime = trackOfMotion.getLast().getTimestamp() - deltaTime;
		Iterator<Body> historyIterator = trackOfMotion.descendingIterator();
		Body up = null;
		Body dn = null;
		while (historyIterator.hasNext()) {
			up = dn;
			dn = historyIterator.next();
			if (dn.getTimestamp() < targetTime)
				break;
		}
		if (null == dn) return null;
		if (null == up) return dn;
		if (targetTime - dn.getTimestamp() < up.getTimestamp() - targetTime) {
			return dn;
		} else {
			return up;
		}
	}

	private float deviationValue(Body poseA, Body poseB) {
		PVector centerA = poseA.get(JIDX.TORSO);
		PVector centerB = poseB.get(JIDX.TORSO);
		float deviationValue = 0;
		for (JIDX jidx : JIDX.ALL_JIDX) {
			if (JIDX.TORSO == jidx) continue;
			PVector radialA = PVector.sub(poseA.get(jidx), centerA);
			PVector radialB = PVector.sub(poseB.get(jidx), centerB);
			PVector deviation = PVector.sub(radialB, radialA);
			float deviationMag = deviation.mag();
			if (deviationMag > deviationValue) deviationValue = deviationMag;
		}
		return deviationValue;
	}
	
	private boolean sufficientDeviationWithinTrack(Body recentEnd, Body oldEnd) {
		float endDifference = deviationValue(recentEnd, oldEnd);
		boolean inSegment = false;
		for (Body pose : trackOfMotion) {
			if (pose == oldEnd) {
				inSegment = true;
			}
			if (inSegment) {
				float inSegmentDeviation = deviationValue(pose, oldEnd);
				if (inSegmentDeviation > REQUIRED_IN_TRACK_DEVIATION_FACTOR * endDifference) return true;
			}
		}
		return false;
	}
	
	private void cutTrackToSegment(Body segmentStart) {
		trackIsSegment = true;
		//Smoothes the track so that the start and end of the gesture segment are the same.
		while (segmentStart != trackOfMotion.getFirst()) {
			trackOfMotion.poll();
		}
	}
}
