package Viewpoints.FrontEnd.Segmentation;

import java.util.ArrayList;

import Viewpoints.FrontEnd.Model.AestheticsModel.ViewpointsAestheticsModel;
import Viewpoints.FrontEnd.Model.Trackers.BodyStillnessTracker;
import Viewpoints.FrontEnd.Shared.Body;

/**
 * Segments a gesture according to the stillness of the body.
 * @author Lauren
 *
 */
public class StillnessSegmenter extends GestureSegmenter {

	private boolean segmentStarted = false;
	private boolean currentStillness = false;
	private boolean previousStillness = false;
	private boolean segmentEnded = false;

	@Override
	public void reset() {
		segment.clear();
		segmentStarted = false;
		currentStillness = false;
		previousStillness = false;
		segmentEnded = false;
	}

	@Override
	public boolean segmentStarted() {
		return segmentStarted;
	}

	@Override
	public boolean segmentEnded() {
		return segmentEnded;
	}

	@Override
	public ArrayList<Body> getSegment() {
		return segment;
	}

	@Override
	public void takePose(Body body) {
		BodyStillnessTracker tracker = ViewpointsAestheticsModel.getHumanStillness();
		previousStillness = currentStillness;
		currentStillness = tracker.isStill();
		segmentStarted = false;
		segmentEnded = false;
		
		//was moving and is now still
		if (currentStillness && !previousStillness) {
			segmentEnded = true;
		} else if (!currentStillness && previousStillness) { //was still and is now moving
			segmentStarted = true;
		}
		
		
		if (!currentStillness) {
			segment.add(body);
		} else {
			reset();
		}
	}

	/**
	 * Returns true when the system is moving, false when the system is still. 
	 */
	@Override
	public boolean isTracking() {
		return segment.size() != 0;
	}

}
