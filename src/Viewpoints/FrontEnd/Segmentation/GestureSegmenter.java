package Viewpoints.FrontEnd.Segmentation;

import java.util.ArrayList;
import java.util.Date;

import Viewpoints.FrontEnd.Application.ViewpointsFrontEnd;
import Viewpoints.FrontEnd.Communication.File.FileUtilities;
import Viewpoints.FrontEnd.Gesture.JointSpaceGesture.JointSpaceGesture;
import Viewpoints.FrontEnd.Shared.Body;

/**
 * 
 * Defines a method for automatically segmenting gestures. For example, rhythmic or activity-based.
 * 
 * @author Lauren Winston
 *
 */

public abstract class GestureSegmenter {
	protected boolean previousStillness;
	protected boolean currentStillness;
	protected boolean forcedStart;
	protected boolean forcedEnd;
	protected ArrayList<Body> segment = new ArrayList<Body>();
	public static int totalGesturesSerialized = 0;
	public static int maxSerializableGestures = 100;
	
	/**
	 * Resets the parameters in the segmenter.
	 */
	public abstract void reset();
	
	/**
	 * @return If a gesture has begun being tracked.
	 */
	public abstract boolean segmentStarted();
	
	/**
	 * @return If a gesture has just stopped being tracked.
	 */
	public abstract boolean segmentEnded();
	
	/**
	 * @return A list of body segments composing the entire gesture.
	 */
	public abstract ArrayList<Body> getSegment();
	
	/**
	 * Used to update the segmenter with the current body information.
	 * @param body
	 */
	public abstract void takePose(Body body);
	
	/**
	 * @return If a gesture segment is currently being tracked.
	 */
	public abstract boolean isTracking();
	
	/**
	 * Modifies the visualization using the given effects controller if needed. 
	 */
	public void effects() {} 

	/**
	 * Serialize a set of gestures.
	 */
	public void saveGesture() {
		if (segment == null) {
			return;
		}

		if (segment.size() == 0) {
			return;
		}

		//This if statement is just to prevent memory overflow if the application is left running.
		if (totalGesturesSerialized < maxSerializableGestures) {
			JointSpaceGesture jsg = new JointSpaceGesture(segment);
			String fileName = ViewpointsFrontEnd.SEGMENTATION_METHOD + "-" + ViewpointsFrontEnd.getPlaybackName() + "-" + (new Date()).toString().replace(':','-') + ".jsg";
			fileName = fileName.replaceAll(" ", "");
			Boolean isGestureWritten = FileUtilities.serializeJointsGesture
					("gesture-captured" + "/" + fileName, jsg);
			if (isGestureWritten) {
				totalGesturesSerialized++;
			}
		}
	}
}
