package Viewpoints.FrontEnd.Segmentation;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import processing.core.PVector;
import Viewpoints.FrontEnd.Application.ClassifierTrainer;
import Viewpoints.FrontEnd.Clock.Clock;
import Viewpoints.FrontEnd.Model.AestheticsModel.ViewpointsAestheticsModel;
import Viewpoints.FrontEnd.Model.Trackers.BodyStillnessTracker;
import Viewpoints.FrontEnd.Model.VAIClassifier.Algorithm;
import Viewpoints.FrontEnd.Model.VAIClassifier.Data;
import Viewpoints.FrontEnd.Model.VAIClassifier.VAILearner;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.JIDX;

/**
 * Gesture segmenting method based off a measure of local minima at "activity"
 * @author Lauren Winston
 *
 */
public class ActivitySegmenter extends GestureSegmenter {
	private int start = -1;
	private ClassifierCommThread classifierCommThread = null;
	private ArrayList<Float> forceSmoothed = new ArrayList<Float>();
	private ArrayList<Float> forceSmoothedHist = new ArrayList<Float>();
	private LinkedHashMap<JIDX, int[][]> activityMins;    //binary for classification
	private int[] forceMins;       //binary for classification
	private ArrayList<int[]> segmentCoal = new ArrayList<int[]>();       // coalesce binary
	// 0 neck, 1 left_hand, 2 right_hand, 3 left_elbow, 4 right_elbow, 5 left_shoulder, 6 right_shoulder, 7 left_torso, 8 right_torso, 9 left_hip, 10 right_hip, 11 left_knee, 12 right_knee
	// 13 left_waist, 14 right_waist, 15 left_leg, 16 right_leg, 17 left_larm, 18 right_larm, 19 left_uarm, 20 right_uarm, 21 left_harm, 22 right_harm, 23 left_llarm, 24 right_llarm
	// 25 left_uuarm, 26 right_uuarm, 27 lower_left, 28 lower_right, 29 upper_left, 30 upper_right, 31 lower_body, 32 upper_body, 33 body
	private ArrayList<HashMap<String, PVector>> segmentHist = new ArrayList<HashMap<String, PVector>>();
	private ArrayList<int[]> marked = new ArrayList<int[]>();
	private boolean getData = false;
	private boolean shouldStart = false;
	private boolean shouldEnd = false;
	private boolean hasStarted = false;
	private boolean hasEnded = false;
	private Clock boundaryGapTimer;
	private JIDX jointSets[][] = { {JIDX.HEAD, JIDX.NECK, JIDX.TORSO},
			{JIDX.LEFT_FINGERTIP, JIDX.LEFT_HAND, JIDX.LEFT_ELBOW},
			{JIDX.RIGHT_FINGERTIP, JIDX.RIGHT_HAND, JIDX.RIGHT_ELBOW},
			{JIDX.LEFT_HAND, JIDX.LEFT_ELBOW, JIDX.LEFT_SHOULDER},
			{JIDX.RIGHT_HAND, JIDX.RIGHT_ELBOW, JIDX.RIGHT_SHOULDER},
			{JIDX.LEFT_ELBOW, JIDX.LEFT_SHOULDER, JIDX.NECK},
			{JIDX.RIGHT_ELBOW, JIDX.RIGHT_SHOULDER, JIDX.NECK},
			{JIDX.NECK, JIDX.TORSO, JIDX.LEFT_HIP},
			{JIDX.NECK, JIDX.TORSO, JIDX.RIGHT_HIP},
			{JIDX.TORSO, JIDX.LEFT_HIP, JIDX.LEFT_KNEE},
			{JIDX.TORSO, JIDX.RIGHT_HIP, JIDX.RIGHT_KNEE},
			{JIDX.LEFT_HIP, JIDX.LEFT_KNEE, JIDX.LEFT_FOOT},
			{JIDX.RIGHT_HIP, JIDX.RIGHT_KNEE, JIDX.RIGHT_FOOT} };
	
	private static VAILearner classifier;
	Clock clock;
	
	private float velocitySum;
	private float kineticEnergySum;
	private float momentumSum;
	private float forceSum;
	
	public ActivitySegmenter() {
		this(false);
	}

	public ActivitySegmenter(boolean getData)
	{
		clock = new Clock();
		this.getData = getData;
		try {
			classifier = VAILearner.create(Data.BOUNDARY, Algorithm.RANDOM_FOREST);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
				
		try
		{
			classifierCommThread = new ClassifierCommThread();
			classifierCommThread.start();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	@Override
	public void takePose(Body pose) {
		if (getData) {
			ViewpointsAestheticsModel.getActivityTracker().takePose(pose);
		}

		if(pose == null){
			reset();
			return;
		}
		clock.check();
		
		if(boundaryGapTimer != null){
			boundaryGapTimer.check();
			if(boundaryGapTimer.time() > 5){
				endGesture(0);
				reset();
				return;
			}
		}
		hasStarted = false;
		hasEnded = false;
		forceSum = 0;
		velocitySum = 0;
		momentumSum = 0;
		kineticEnergySum = 0;
		
		if (ViewpointsAestheticsModel.getActivityTracker().isExit()) {
			return;
		}

		if(ViewpointsAestheticsModel.getActivityTracker().getVelocityHistory().size() > 1){ //if ActivityTracker has velocity
			forceSmoothed.add(ViewpointsAestheticsModel.getActivityTracker().getForce());
		}
		
		ArrayList<Float> forceHist = ViewpointsAestheticsModel.getActivityTracker().getForceHistory();
		ArrayList<LinkedHashMap<JIDX, float[]>> activityHist = ViewpointsAestheticsModel.getActivityTracker().getActivityHistory();
		if(forceHist.size() > 6 && activityHist.size() > 6) {
			activityMins = getActivityMins(activityHist);
			forceSmoothed = gaussianSmoothing(forceSmoothed);
			forceHist = gaussianSmoothing(forceHist);
			forceMins = getMins(forceHist, 0f);
		}
		
		//HashMap<JIDX, PVector> jointAngles = new HashMap<JIDX, PVector>();
		PVector a = null, b = null, cross;
		HashMap<String, PVector> segmentNorms = new HashMap<String, PVector>();
		int count = 0;
		int[] coal = new int[34];
		boolean canCoal = (segmentHist.size() > 0) ? true : false;
		for(JIDX[] joints : jointSets){
			count++;
			//double angle = getAngle(PVector.sub(pose.get(joints[1]), pose.get(joints[0])), PVector.sub(pose.get(joints[1]), pose.get(joints[2])));
			a = PVector.sub(pose.get(joints[1]), pose.get(joints[0]));
			b = PVector.sub(pose.get(joints[1]), pose.get(joints[2]));
			a.normalize();
			b.normalize();
			cross = a.cross(b);
			//jointAngles.put(joints[1], cross);
			String modifier = "";
			if(count < 8){
				modifier = "UPPER_";
			}else{
				modifier = "LOWER_";
				if(joints[1] == JIDX.TORSO){
					if(joints[2] == JIDX.LEFT_HIP)
						modifier += "LEFT_";
					else
						modifier += "RIGHT_";
				}
			}
			segmentNorms.put(modifier + joints[1].toString(), cross);
			if(canCoal)
				coal[count - 1] = getCoal(modifier + joints[1].toString(), cross);
		}

		//lower body coalesce
		String modifier = "LOWER_LEFT_";
		for(int i = 0; i < 2; i++){
			segmentNorms.get(modifier + "TORSO").normalize(a);
			segmentNorms.get(modifier + "HIP").normalize(b);
			cross = a.cross(b);
			segmentNorms.put(modifier + "WAIST", cross);
			if(canCoal){
				if(coal[7+i] == 1 && coal[9+i] == 1)
					coal[13+i] = getCoal(modifier + "WAIST", cross);
				else
					coal[13+i] = 0;
			}
			
			segmentNorms.get(modifier + "HIP").normalize(a);
			segmentNorms.get(modifier + "KNEE").normalize(b);
			cross = a.cross(b);
			segmentNorms.put(modifier + "LEG", cross);
			if(canCoal){
				if(coal[9+i] == 1 && coal[11+i] == 1)
					coal[15+i] = getCoal(modifier + "LEG", cross);
				else
					coal[15+i] = 0;
			}
			
			segmentNorms.get(modifier + "WAIST").normalize(a);
			segmentNorms.get(modifier + "LEG").normalize(b);
			cross = a.cross(b);
			segmentNorms.put(modifier.substring(0, modifier.length()-1), cross);
			if(canCoal){
				if(coal[13+i] == 1 && coal[15+i] == 1)
					coal[27+i] = getCoal(modifier.substring(0, modifier.length()-1), cross);
				else
					coal[27+i] = 0;
			}
			
			modifier = modifier.substring(0, modifier.length()-5);
			modifier += "RIGHT_";
		}
		
		segmentNorms.get("LOWER_LEFT").normalize(a);
		segmentNorms.get("LOWER_RIGHT").normalize(b);
		cross = a.cross(b);
		segmentNorms.put("LOWER_BODY", cross);
		if(canCoal){
			if(coal[27] == 1 && coal[28] == 1)
				coal[31] = getCoal("LOWER_BODY", cross);
			else
				coal[31] = 0;
		}
		
		//upper body coalesce
		modifier = "UPPER_LEFT_";
		for(int i = 0; i < 2; i++){
			segmentNorms.get(modifier + "HAND").normalize(a);
			segmentNorms.get(modifier + "ELBOW").normalize(b);
			cross = a.cross(b);
			segmentNorms.put(modifier + "lARM", cross);
			if(canCoal){
				if(coal[1+i] == 1 && coal[3+i] == 1)
					coal[17+i] = getCoal(modifier + "lARM", cross);
				else
					coal[17+i] = 0;
			}
			
			segmentNorms.get(modifier + "ELBOW").normalize(a);
			segmentNorms.get(modifier + "SHOULDER").normalize(b);
			cross = a.cross(b);
			segmentNorms.put(modifier + "uARM", cross);
			if(canCoal){
				if(coal[3+i] == 1 && coal[5+i] == 1)
					coal[19+i] = getCoal(modifier + "uARM", cross);
				else
					coal[19+i] = 0;
			}

			segmentNorms.get(modifier + "SHOULDER").normalize(a);
			if(i == 0)
				segmentNorms.get(modifier.substring(0, modifier.length()-5) + "NECK").normalize(b);
			else 	
				segmentNorms.get(modifier.substring(0, modifier.length()-6) + "NECK").normalize(b);

			cross = a.cross(b);
			segmentNorms.put(modifier + "hARM", cross);
			if(canCoal){
				if(coal[5+i] == 1 && coal[0] == 1)
					coal[21+i] = getCoal(modifier + "hARM", cross);
				else
					coal[21+i] = 0;
			}
			
			segmentNorms.get(modifier + "lARM").normalize(a);
			segmentNorms.get(modifier + "uARM").normalize(b);
			cross = a.cross(b);
			segmentNorms.put(modifier + "llARM", cross);
			if(canCoal){
				if(coal[17+i] == 1 && coal[19+i] == 1)
					coal[23+i] = getCoal(modifier + "llARM", cross);
				else
					coal[23+i] = 0;
			}
			
			segmentNorms.get(modifier + "uARM").normalize(a);
			segmentNorms.get(modifier + "hARM").normalize(b);
			cross = a.cross(b);
			segmentNorms.put(modifier + "uuARM", cross);
			if(canCoal){
				if(coal[19+i] == 1 && coal[21+i] == 1)
					coal[25+i] = getCoal(modifier + "uuARM", cross);
				else
					coal[25+i] = 0;
			}
				
			segmentNorms.get(modifier + "llARM").normalize(a);
			segmentNorms.get(modifier + "uuARM").normalize(b);
			cross = a.cross(b);
			segmentNorms.put(modifier.substring(0, modifier.length()-1), cross);
			if(canCoal){
				if(coal[23+i] == 1 && coal[25+i] == 1)
					coal[29+i] = getCoal(modifier.substring(0, modifier.length()-1), cross);
				else
					coal[29+i] = 0;
			}
			
			modifier = modifier.substring(0, modifier.length()-5);
			modifier += "RIGHT_";
		}
		
		segmentNorms.get("UPPER_LEFT").normalize(a);
		segmentNorms.get("UPPER_RIGHT").normalize(b);
		cross = a.cross(b);
		segmentNorms.put("UPPER_BODY", cross);
		if(canCoal){
			if(coal[29] == 1 && coal[30] == 1)
				coal[32] = getCoal("UPPER_BODY", cross);
			else
				coal[32] = 0;
		}
		
		//upper and lower body coalesce
		segmentNorms.get("UPPER_BODY").normalize(a);
		segmentNorms.get("LOWER_BODY").normalize(b);
		cross = a.cross(b);
		segmentNorms.put("BODY", cross);
		if(canCoal){
			if(coal[31] == 1 && coal[32] == 1)
				coal[33] = getCoal("BODY", cross);
			else
				coal[33] = 0;
		}

		segmentHist.add(segmentNorms);
		segmentCoal.add(coal);

		if (!getData) {
			if(forceMins != null && forceMins[forceMins.length-7] == 1){
				mark(0, forceMins.length-7);
			}
		}

		if(getData){
			ArrayList<Integer> temp = new ArrayList<Integer>();
			for(int i = 0; i < marked.size(); i++){
				if(segmentCoal.size() > i + 10){
					if(activityMins == null || (marked.get(i)[1] == 1 && forceMins[marked.get(i)[0]] == 0)){
						temp.add(i);
						break;
					}
					PrintWriter out = null;
					try {
						FileWriter file = new FileWriter(ClassifierTrainer.PROJECT_HOME + File.separator + "new_models" + File.separator + "data" + File.separator + "NewGestures.txt", true);
						out = new PrintWriter(file);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					for(int j = -1; j < 2; j++){
						for(int res : segmentCoal.get(marked.get(i)[0] + j)){
							out.print(res + ",");
						}
						for (int[][] act : activityMins.values()) {
							out.print(act[0][marked.get(i)[0] + j] + ",");
							out.print(act[1][marked.get(i)[0] + j] + ",");
							out.print(act[2][marked.get(i)[0] + j] + ",");
						}
					}
	
					out.print(marked.get(i)[1]);
					out.println();
					out.close();
					temp.add(i);
					break;
				}
			}
			for(Integer i : temp){
				marked.remove(i.intValue());
			}
		}else{
			ArrayList<Integer> temp = new ArrayList<Integer>();
			for(int i = 0; i < marked.size(); i++){
				if(segmentCoal.size() > marked.get(i)[0] + 10){
					if(activityMins == null || (marked.get(i)[1] == 1 && forceMins[marked.get(i)[0]] == 0) || (boundaryGapTimer != null && boundaryGapTimer.time() < .66)){
						temp.add(i);
						break;
					}
					double[] instance = new double[238];
					
					int index = 0;
					for(int j = -1; j < 2; j++){
						for(int res : segmentCoal.get(marked.get(i)[0] + j)){
							instance[index] = (double)res;
							index++;
						}
						
						for (int[][] act : activityMins.values()) {
							instance[index] = (double)act[0][marked.get(i)[0] + j];
							index ++;
							instance[index] = (double)act[1][marked.get(i)[0] + j];
							index ++;
							instance[index] = (double)act[2][marked.get(i)[0] + j];
							index ++;
						}
					}
					instance[index] = (double)0;
					try {
						double[] bound = classifier.classify(instance);
						if(bound[1] > 0.4){
							if(boundaryGapTimer == null && !currentStillness){
								startGesture(i);
							}else {
								if (segment == null && !currentStillness) {
									startGesture(i);
								} else if (boundaryGapTimer != null){
									endGesture(i);
								} else {
								}
							}
							marked.clear();
						}else{
							if (shouldStart) {
								startGesture(i);
							} else if (shouldEnd) {
								endGesture(i);
							} else {
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					temp.add(i);	
					break;
				} else {
					 if (shouldStart) {
						 startGesture(i);
					 } else if (shouldEnd) {
						 endGesture(i);
					 } else {
					 }
				}
			}
			
			Collections.sort(temp);
			Collections.reverse(temp);
			if(marked.size() >= temp.size()){
				for(int i = 0; i < temp.size(); i++){
					marked.remove(temp.get(i).intValue());
				}
			}
		}
	}
	

	/**
	 * 
	 * @param i is the index of marked to begin the gesture with
	 */
	public void startGesture(int i) {
		if (boundaryGapTimer == null) {
			shouldStart = false;
			boundaryGapTimer = new Clock();
			start = marked.get(i)[0];
			//System.out.println("start: " + clock.time());
			hasStarted = true;
		}
	}
	
	/**
	 * 
	 * @param i is the index of marked to end the gesture with
	 */
	public void endGesture(int i) {
		ArrayList<Body> bodyHistory = ViewpointsAestheticsModel.getActivityTracker().getBodyHistory();
		if (segment != null && bodyHistory != null && boundaryGapTimer != null && marked.size() > i) {
			boundaryGapTimer = null;
			for(int k = start; k < marked.get(i)[0]; k++) {
				if (k < bodyHistory.size() && k >= 0) {
					segment.add(bodyHistory.get(k));
				} else {
					break;
				}
			}
			//System.out.println("end: " + clock.time());
			shouldEnd = false;
			hasEnded = true;
		}
	}

	@Override
	//TODO: (Lauren) is this the right result for this method?
	public boolean isTracking() {
		return boundaryGapTimer != null;
	}
	
	@Override
	public boolean segmentStarted() {
		return hasStarted;
	}

	@Override
	public boolean segmentEnded() {
		return hasEnded;
	}
	
	public boolean getStart(){
		return boundaryGapTimer != null;
	}
	
	public float getTime() {
		return clock.time();
	}
	
	public float getEnd(){
		return boundaryGapTimer.time() - boundaryGapTimer.iniTime();
	}
	
	@Override
	public ArrayList<Body> getSegment(){
		if(segment.size() < 1)
			return null;
		return segment;
	}
	
	public void mark(int boundary, int frame){
		if(segmentCoal.size() > 6 && (frame != -1 ? frame > 6 : true)){
			int[] arr = new int[2];
			if(frame == -1) {
				arr[0] = segmentCoal.size()-1;
			}
			else {
				arr[0] = frame;
			}
			arr[1] = boundary;
			marked.add(arr);
		}
	}
	
	/*
	 * Smoothes the values by using a weighted average of each value's neighbors.
	 */
	public ArrayList<Float> smoothing(ArrayList<Float> original){
		float weightNum = .6f;  //weights
		float weightNeigh1 = .1f;
		float weightNeigh2=.07f;
		float weightNeigh3=.03f;
		ArrayList<Float> smoothed = new ArrayList<Float>();
		for(int i = 0; i < 4; i++){
			smoothed.add(original.get(i));
		}
		for (int i = 4; i < original.size()-3; i++) {
			smoothed.add(original.get(i)*weightNum + original.get(i-1)*weightNeigh1 + original.get(i-2)*weightNeigh2 
	       + original.get(i-3)*weightNeigh3 + original.get(i+1)*weightNeigh1+  original.get(i+2)*weightNeigh2 +original.get(i+3)*weightNeigh3);  
		}

		int startSize = original.size() - 3;
		while (startSize < 4) {
			startSize++;
		}

		for(int i = startSize; i < original.size(); i++){
			smoothed.add(original.get(i));
		}
		return smoothed;
	}
	
	public ArrayList<Float> gaussianSmoothing(ArrayList<Float> orig) {
		float weightNum = .9545f;  //weights
		float weightNeigh1 = .02275f;
		float weightNeigh2=0f;
		float weightNeigh3=0f;

		ArrayList<Float> smoothed = new ArrayList<Float>();
		for(int i = 0; i < 4; i++){
			smoothed.add(orig.get(i));
		}
		for (int i = 4; i < orig.size()-3; i++) {
			smoothed.add(orig.get(i)*weightNum + orig.get(i-1)*weightNeigh1 + orig.get(i-2)*weightNeigh2 
	       + orig.get(i-3)*weightNeigh3 + orig.get(i+1)*weightNeigh1+  orig.get(i+2)*weightNeigh2 +orig.get(i+3)*weightNeigh3);  
		}
		
		int startSize = orig.size() - 3;
		while (startSize < 4) {
			startSize++;
		}

		for(int i = startSize; i < orig.size(); i++){
			smoothed.add(orig.get(i));
		}

		return smoothed;
	}
	
	public LinkedHashMap<JIDX, int[][]> getActivityMins(ArrayList<LinkedHashMap<JIDX, float[]>> hist){
		LinkedHashMap<JIDX, int[][]> activityMins = new LinkedHashMap<JIDX, int[][]>();
		LinkedHashMap<JIDX, ArrayList<ArrayList<Float>>> converted = new LinkedHashMap<JIDX, ArrayList<ArrayList<Float>>>();
		for (JIDX ji : hist.get(1).keySet()) {
			ArrayList<ArrayList<Float>> arr = new ArrayList<ArrayList<Float>>();
			ArrayList<Float> empty =  new ArrayList<Float>();
			empty.add(-1f);
			arr.add(empty);
			empty =  new ArrayList<Float>();
			empty.add(-1f);
			arr.add(empty);
			empty =  new ArrayList<Float>();
			empty.add(-1f);
			arr.add(empty);
			converted.put(ji, arr);
		}
		for (JIDX ji : hist.get(1).keySet()) {
			activityMins.put(ji, new int[3][]);
		}
		for(HashMap<JIDX, float[]> frame : hist){
			if(frame == null)
				continue;
			for (Map.Entry<JIDX, float[]> entry : frame.entrySet()) {
			    JIDX segment = entry.getKey();
			    float[] activity = entry.getValue();
			    converted.get(segment).get(0).add(activity[0]);
			    converted.get(segment).get(1).add(activity[1]);
			    converted.get(segment).get(2).add(activity[2]);
			}
		}
		for (Map.Entry<JIDX, ArrayList<ArrayList<Float>>> entry : converted.entrySet()) {
			activityMins.get(entry.getKey())[0] = getMins(entry.getValue().get(0), 15f);
			activityMins.get(entry.getKey())[1] = getMins(entry.getValue().get(1), 10f);
			activityMins.get(entry.getKey())[2] = getMins(entry.getValue().get(2), 10f);
		}
		return activityMins;
	}
	
	public int[] getMins(ArrayList<Float> arr, float threshold){
		int min[] = new int[arr.size()];
		int start = 2;
		if(arr.get(0) == null || arr.get(0) == -1){
			arr.set(0, arr.get(1));
			start = 3;
		}
		for(int i = start; i < arr.size()-1; i++){    //find local minima	   
			float left =- 1;
			float mid =- 1;
			float right =- 1;
			    
			left = arr.get(i-1);
			mid = arr.get(i);
			right = arr.get(i+1);
			
		    if (right-mid > threshold && left-mid > threshold || right-mid < threshold && left-mid < threshold) 
		    	min[i]= 1;    //set as 1 if it's a local minima or maxima	
		    else
		    	min[i]= 0;
		}
		return min;
	}
	
	private int getCoal(String segment, PVector cur){
		int res = 0;
		PVector prev = segmentHist.get(segmentHist.size()-1).get(segment);
		PVector diff = PVector.sub(cur, prev);
		if(Math.abs(diff.x) < 0.1 && Math.abs(diff.y) < 0.1 && Math.abs(diff.z) < 0.1){
			res = 1;
		}
		return res;
	}
	
	@Override
	public void reset() {
		classifierCommThread.reset();
		segmentHist.clear();
		marked.clear();
		segmentCoal.clear();
		forceMins = null;
		activityMins = null;
		forceSmoothedHist.addAll(forceSmoothed);
		forceSmoothed.clear();
		boundaryGapTimer = null;
		start = -1;
		segment = new ArrayList<Body>();
		
		hasEnded = false;
		hasStarted = false;
		ViewpointsAestheticsModel.getActivityTracker().reset();
	}

	public void setStillness(boolean newStillness) {
		previousStillness = currentStillness;
		currentStillness = newStillness;
		if (!currentStillness && previousStillness) {
			shouldStart = true;
			shouldEnd = false;
		} else if (currentStillness && !previousStillness) {
			shouldStart = false;
			shouldEnd = true;
		}
	}
	
	public float getVelocity() {
		return velocitySum;
	}
	
	public float getKineticEnergy() {
		return kineticEnergySum;
	}
	
	public float getMomentum() {
		return momentumSum;
	}
	
	public float getForce() {
		return forceSum;
	}

	private static class ClassifierCommThread extends Thread {
		public void run() {
			
		}
				
//		public void requestClassificationOf(ArrayList<Body> history) {
//			synchronized(this) {
//			}
//		}
		
		
		public void reset() {
			synchronized(this) {

			}
		}
	}

	
}
