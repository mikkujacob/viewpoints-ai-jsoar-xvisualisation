package Viewpoints.FrontEnd.Segmentation;

import java.io.FileWriter;
import java.util.ArrayList;

import Viewpoints.FrontEnd.Clock.Clock;
import Viewpoints.FrontEnd.Communication.File.FileUtilities;
import Viewpoints.FrontEnd.Model.AestheticsModel.ViewpointsAestheticsModel;
import Viewpoints.FrontEnd.Shared.Body;

/**
 * Manages the different segmentation models. A specific segmenter can be specified. If none are specified, a combination of the different
 * segmenters will be used.
 * @author Lauren
 *
 */

public class SegmentationManager {
	/*
	 * True if captured gestures should be saved to the "gesture-captured" directory.
	 */
	private static boolean SAVE_GESTURES = false;
	
	/**
	 * The minimum length in seconds of a gesture.
	 */
	private static float MIN_GESTURE_LENGTH = .75f;
	
	/**
	 * The maximum length in seconds of a gesture.
	 */
	private static float MAX_GESTURE_LENGTH = 6f;

	/**
	 * Timer that keeps track of the length of the current gesture once
	 * a start has been detected.
	 */
	private Clock gestureLengthTimer;

	private SegmentMethods segmentMethod;
	private ActivitySegmenter activitySeg;
	private RhythmSegmenter rhythmSeg;
	private GestureSegmenter gestureSeg;
	private StillnessSegmenter stillnessSeg;
	private ArrayList<GestureSegmenter> segmenters = new ArrayList<GestureSegmenter>();

	//FileWriters to be used for annotations. Each segmentation method should have it's own writer.
	private FileWriter rhythmWriter;
	private FileWriter activityWriter;
	private FileWriter allWriter;
	private FileWriter stillnessWriter;

	//Logic for switching between the segmenters when using the arbiter
	private boolean forceEnd = false;
	private boolean forceStart = false;
	
	private boolean ended = false;
	private boolean started = false;
	
	/**
	 * The directory that annotations will be saved to.
	 */
	private String annotationWriteDir = "gesture-timestamps/";
	/**
	 * If the arbiter is currently tracking a gesture.
	 */
	private boolean currentlyTracking = false;
	
	//TODO: (Lauren) Use prevResponseLength to check against current lengths for rhythmic motions.
	private float prevResponseLength = 0f;

	public SegmentationManager(SegmentMethods segmentMethod) {
		this.segmentMethod = segmentMethod;
		activitySeg = new ActivitySegmenter();
		rhythmSeg = new RhythmSegmenter();
		stillnessSeg = new StillnessSegmenter();
		
		segmenters.add(activitySeg);
		segmenters.add(rhythmSeg);
		segmenters.add(stillnessSeg);

		switch(this.segmentMethod){
			case ACTIVITY:
				gestureSeg = activitySeg;
				break;
			case RHYTHM:
				gestureSeg = rhythmSeg;
				break;
			case STILLNESS:
				gestureSeg = stillnessSeg;
				break;
			case ALL:
				break;
			default:
				break;
		}
	}
	
	public SegmentationManager() {
		this(SegmentMethods.ALL);
	}
	
	public void reset() {
		if (segmentMethod != SegmentMethods.ALL) {
			gestureSeg.reset();
		} else {
			//TODO: (Lauren) Define what this means for the gesture manager. Are all of the segmenters reset?
		}
	}
	
	public void preSegmentationUpdate(Body userPose){
		if (segmentMethod != SegmentMethods.ALL) {
			gestureSeg.takePose(userPose);
		} else {
			for (GestureSegmenter segmenter : segmenters) {
				segmenter.takePose(userPose);
			}
		}
	}
	
	public void postSegmentationUpdate() {
		if (ended) {
			saveGesture();
		}

		if (activitySeg.segmentEnded()) {
			activitySeg.reset();
			ViewpointsAestheticsModel.getActivityTracker().reset();
		}
		
		if (rhythmSeg.segmentEnded()) {
			rhythmSeg.clear();
		}
	}
	
	public boolean segmentStarted() {
		if (segmentMethod != SegmentMethods.ALL) {
			return gestureSeg.segmentStarted();
		} else {
			started = false;
			if (stillnessSeg.isTracking()) {
				if (currentlyTracking && activitySeg.segmentStarted()) {
					forceEnd = true;
					started = true;
				} else if (activitySeg.segmentStarted() || (rhythmSeg.segmentStarted() && !activitySeg.isTracking())) {
					started = true;
				} else if (!activitySeg.isTracking() && !currentlyTracking && rhythmSeg.isTracking() && !rhythmSeg.segmentEnded()) {
					forceStart = true;
					started = true;
				}
			}
			
			if (started && gestureLengthTimer == null) {
				gestureLengthTimer = new Clock();
				gestureLengthTimer.start();
			} else if (started) {
				gestureLengthTimer.check();
				prevResponseLength = gestureLengthTimer.time();
				gestureLengthTimer = new Clock();
				gestureLengthTimer.start();
			}
			
			if (started) {
				currentlyTracking = true;
			}

			return started;
		} 
	}
	
	/**
	 * Serialize the captured gesture.
	 */
	public void saveGesture() {
		if (SAVE_GESTURES) {
			if (segmentMethod != SegmentMethods.ALL) {
				gestureSeg.saveGesture();
			} else {
				/*
				 * Save the completed gesture to a jsg gesture file.
				 */
				if (activitySeg.segmentEnded()) {
					activitySeg.saveGesture();
				} else if (rhythmSeg.segmentEnded()) {
					rhythmSeg.saveGesture();
				} else if (rhythmSeg.isTracking() && activitySeg.segmentStarted()) {
					rhythmSeg.saveGesture();
				} else if (activitySeg.isTracking()) {
					activitySeg.saveGesture();
				} else if (rhythmSeg.isTracking()) {
					rhythmSeg.saveGesture();
				}
			}
		}
	}
	
	public boolean segmentEnded() {
		if (segmentMethod != SegmentMethods.ALL) {
			ended = gestureSeg.segmentEnded();
			return ended;
		} else {
			ended = false;
			
			if (stillnessSeg.segmentEnded() && currentlyTracking) {
				ended = true;
			} else if (forceEnd) {
				ended = true;
			} else if (currentlyTracking && ((rhythmSeg.segmentEnded() && !activitySeg.isTracking()) || activitySeg.segmentEnded())) {
				ended = true;
			}
			
			if (ended && gestureLengthTimer != null) {
				gestureLengthTimer.check();
				prevResponseLength = gestureLengthTimer.time();
				gestureLengthTimer = null;
			}
			
			if (ended && forceEnd) {
				forceEnd = false;
			} else if (ended && !started) {
				currentlyTracking = false;
			} else if (forceStart) {
				forceStart = false;
			}
			
			

			return ended;
		}
	}
	
	public boolean isTracking() {
		if (segmentMethod != SegmentMethods.ALL) {
			return gestureSeg.isTracking();
		} else {
			return currentlyTracking;
		}
	}
	
	public ArrayList<Body> getSegment() {
		ArrayList<Body> segment = null;
		if (segmentMethod != SegmentMethods.ALL) {
			segment = gestureSeg.getSegment();
		} else {
			if (stillnessSeg.isTracking() || stillnessSeg.segmentEnded()) {
				if (activitySeg.isTracking() || activitySeg.segmentEnded()) {
					segment = activitySeg.getSegment();
				} else if (rhythmSeg.isTracking() || rhythmSeg.segmentEnded()) {
					segment = rhythmSeg.getSegment();
				}
			}
		}
		
		if (gestureLengthTimer != null) {
			gestureLengthTimer.check();
			if (gestureLengthTimer.time() < MIN_GESTURE_LENGTH) {
				segment = null;
			}
		} 

		return segment;
	}
	
	/**
	 * Use the correct FileWriter to generate and append to a csv file representing the starts
	 * and ends of gesture boundaries.
	 */
	public void annotate(String videoName, float time, boolean isStart) {
		switch(this.segmentMethod){
		case ACTIVITY:
			activityWriter = FileUtilities.annotate(activityWriter, annotationWriteDir, videoName, time, isStart, this.segmentMethod.toString());
			break;
		case RHYTHM:
			rhythmWriter = FileUtilities.annotate(rhythmWriter, annotationWriteDir, videoName, time, isStart, this.segmentMethod.toString());
			break;
		case STILLNESS:
			stillnessWriter = FileUtilities.annotate(stillnessWriter, annotationWriteDir, videoName, time, isStart, this.segmentMethod.toString());
			break;
		case ALL:
			allWriter = FileUtilities.annotate(allWriter, annotationWriteDir, videoName, time, isStart, this.segmentMethod.toString());
			break;
		default:
			break;
		}
	}
	
	/**
	 * Close all file writers.
	 */
	public void close() {
		try {
			activityWriter.close();
			allWriter.close();
			stillnessWriter.close();
			allWriter.close();
		} catch (Exception e) {
			
		}
	}

	public void changeSegmentationMethod(SegmentMethods method) {
		segmentMethod = method;
	}
}
