package Viewpoints.FrontEnd.Shared;

import java.util.HashMap;

public enum LIMB {
	LEFT_LEG,
	RIGHT_LEG,
	LEFT_ARM,
	RIGHT_ARM;
	
	public static final LIMB[] ALL_LIMBS = {LEFT_LEG, RIGHT_LEG, LEFT_ARM, RIGHT_ARM};
	public static HashMap<LIMB, Pair<LIDX, LIDX>> _SUBLIMBS = null;
	
	public static HashMap<LIMB, Pair<LIDX, LIDX>> PART_LIMBS() {
		if (null != _SUBLIMBS)
			return _SUBLIMBS;
		
		_SUBLIMBS = new HashMap<LIMB, Pair<LIDX, LIDX>>();
		_SUBLIMBS.put(LEFT_LEG, Pair.of(LIDX.LEFT_THIGH, LIDX.LEFT_SHIN));
		_SUBLIMBS.put(RIGHT_LEG, Pair.of(LIDX.RIGHT_THIGH, LIDX.RIGHT_SHIN));
		_SUBLIMBS.put(LEFT_ARM, Pair.of(LIDX.LEFT_UPPER_ARM, LIDX.LEFT_FOREARM));
		_SUBLIMBS.put(RIGHT_ARM, Pair.of(LIDX.RIGHT_UPPER_ARM, LIDX.RIGHT_FOREARM));
		return _SUBLIMBS;
	}
	
	public static boolean isArm(LIMB limb) {
		return LEFT_ARM == limb || RIGHT_ARM == limb;
	}
	
	public static boolean isLeg(LIMB limb) {
		return !isArm(limb);
	}
	
	public static boolean isLeft(LIMB limb) {
		return LEFT_ARM == limb || LEFT_LEG == limb;
	}
	
	public static boolean isRight(LIMB limb) {
		return !isLeft(limb);
	}
	
	public static JIDX[] limbJoints(LIMB iLimb) {
		Pair<LIDX, LIDX> limbs = PART_LIMBS().get(iLimb);
		Pair<JIDX, JIDX> rootEnds = LIDX.LIMB_ENDS().get(limbs.first);
		Pair<JIDX, JIDX> farEnds =  LIDX.LIMB_ENDS().get(limbs.second);
		JIDX[] partJidx = {rootEnds.first, rootEnds.second, farEnds.second};
		return partJidx;
	}
}
