package Viewpoints.FrontEnd.Shared;

import java.util.HashMap;
import java.util.Map;

import java.io.Serializable;

import SimpleOpenNI.SimpleOpenNI;
import processing.core.*;

public class Body extends SkeletalData implements Serializable, Cloneable
{
	
	/**
	 * Serial Version UID - DO NOT CHANGE PLEASE!!!
	 */
	private static final long serialVersionUID = 2610605569934643630L;
	
	private HashMap<JIDX, PMatrix3D> orientations = new HashMap<JIDX, PMatrix3D>();

	public void set(JIDX iJidx, PVector iPvector) {
		put(iJidx, iPvector);
	}
	
	public void setJointOrientation(JIDX iJidx, PMatrix3D iPMatrix){
		orientations.put(iJidx, iPMatrix);
	}
	
	public void initialize(SimpleOpenNI context, int userId, float timestamp) {
		orientations = new HashMap<JIDX, PMatrix3D>();
		setTimestamp(timestamp);
		
		PVector jointPos = new PVector();
		PMatrix3D jointOrientation = new PMatrix3D();
		
		// note: jointPos.get() is copying each PVector as they are call-by-reference

		//Get Skeleton Coordinates
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_HEAD,jointPos);
		set(JIDX.HEAD,jointPos.get());
		//PApplet.print(joints.get(JIDX.HEAD)+"\n");
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_NECK,jointPos);
		set(JIDX.NECK,jointPos.get());
		//context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_NECK,jointPos); //Never used this before
		//set(JIDX.LEFT_COLLAR,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_SHOULDER,jointPos);
		set(JIDX.LEFT_SHOULDER,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_ELBOW,jointPos);
		set(JIDX.LEFT_ELBOW,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_HAND,jointPos);
		set(JIDX.LEFT_HAND,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_FINGERTIP,jointPos);  // always (0.0, 0.0, 0.0)
		set(JIDX.LEFT_FINGERTIP,jointPos.get());
		//context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_NECK,jointPos); //Never used this before
		//set(JIDX.RIGHT_COLLAR,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_SHOULDER,jointPos);
		set(JIDX.RIGHT_SHOULDER,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_ELBOW,jointPos);
		set(JIDX.RIGHT_ELBOW,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_HAND,jointPos);
		set(JIDX.RIGHT_HAND,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_FINGERTIP,jointPos);  // always (0.0, 0.0, 0.0)
		set(JIDX.RIGHT_FINGERTIP,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_TORSO,jointPos);
		set(JIDX.TORSO,jointPos.get());
		//context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_TORSO,jointPos); // always (0.0, 0.0, 0.0)
		//set(JIDX.WAIST,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_HIP,jointPos);
		set(JIDX.LEFT_HIP,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_KNEE,jointPos);
		set(JIDX.LEFT_KNEE,jointPos.get());
		//context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_FOOT,jointPos); // always (0.0, 0.0, 0.0)
		//set(JIDX.LEFT_ANKLE,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_LEFT_FOOT,jointPos);
		set(JIDX.LEFT_FOOT,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_HIP,jointPos);
		set(JIDX.RIGHT_HIP,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_KNEE,jointPos);
		set(JIDX.RIGHT_KNEE,jointPos.get());
		//context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_FOOT,jointPos); // always (0.0, 0.0, 0.0)
		//set(JIDX.RIGHT_ANKLE,jointPos.get());
		context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_FOOT,jointPos);
		set(JIDX.RIGHT_FOOT,jointPos.get());
		
		
		
		
		//Get Skeleton Orientation
		context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_HEAD,jointOrientation);
		setJointOrientation(JIDX.HEAD,jointOrientation.get());
		//PApplet.print(joints.get(JIDX.HEAD)+"\n");
		context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_NECK,jointOrientation);
		setJointOrientation(JIDX.NECK,jointOrientation.get());
		//context.getJointPositionSkeleton(userId,SimpleOpenNI.SKEL_NECK,jointPos); //Never used this before
		//set(JIDX.LEFT_COLLAR,jointPos.get());
		context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_LEFT_SHOULDER,jointOrientation);
		setJointOrientation(JIDX.LEFT_SHOULDER,jointOrientation.get());
		context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_LEFT_ELBOW,jointOrientation);
		setJointOrientation(JIDX.LEFT_ELBOW,jointOrientation.get());
		context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_LEFT_HAND,jointOrientation);
		setJointOrientation(JIDX.LEFT_HAND,jointOrientation.get());
		context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_LEFT_FINGERTIP,jointOrientation);  // always (0.0, 0.0, 0.0)
		setJointOrientation(JIDX.LEFT_FINGERTIP,jointOrientation.get());
		//context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_NECK,jointOri); //Never used this before
		//setOri(JIDX.RIGHT_COLLAR,jointOri.get());
		context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_SHOULDER,jointOrientation);
		setJointOrientation(JIDX.RIGHT_SHOULDER,jointOrientation.get());
		context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_ELBOW,jointOrientation);
		setJointOrientation(JIDX.RIGHT_ELBOW,jointOrientation.get());
		context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_HAND,jointOrientation);
		setJointOrientation(JIDX.RIGHT_HAND,jointOrientation.get());
		context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_FINGERTIP,jointOrientation);  // always (0.0, 0.0, 0.0)
		setJointOrientation(JIDX.RIGHT_FINGERTIP,jointOrientation.get());
		context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_TORSO,jointOrientation);
		setJointOrientation(JIDX.TORSO,jointOrientation.get());
		//context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_TORSO,jointOri); // always (0.0, 0.0, 0.0)
		//setOri(JIDX.WAIST,jointOri.get());
		context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_LEFT_HIP,jointOrientation);
		setJointOrientation(JIDX.LEFT_HIP,jointOrientation.get());
		context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_LEFT_KNEE,jointOrientation);
		setJointOrientation(JIDX.LEFT_KNEE,jointOrientation.get());
		//context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_LEFT_FOOT,jointOri); // always (0.0, 0.0, 0.0)
		//setOri(JIDX.LEFT_ANKLE,jointOri.get());
		context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_LEFT_FOOT,jointOrientation);
		setJointOrientation(JIDX.LEFT_FOOT,jointOrientation.get());
		context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_HIP,jointOrientation);
		setJointOrientation(JIDX.RIGHT_HIP,jointOrientation.get());
		context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_KNEE,jointOrientation);
		setJointOrientation(JIDX.RIGHT_KNEE,jointOrientation.get());
		//context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_FOOT,jointOri); // always (0.0, 0.0, 0.0)
		//setOri(JIDX.RIGHT_ANKLE,jointOri.get());
		context.getJointOrientationSkeleton(userId,SimpleOpenNI.SKEL_RIGHT_FOOT,jointOrientation);
		setJointOrientation(JIDX.RIGHT_FOOT,jointOrientation.get());
		
		
		
		
	}
	
	public PVector[] localBasis() {
		PVector localFwddir = PVecUtilities.ortonormalization(orientation(), PVecUtilities.UPVEC);
		PVector localSidedir = PVecUtilities.UPVEC.cross(localFwddir);
		PVector[] localBasis = {localSidedir, PVecUtilities.UPVEC.get(), localFwddir};
		return localBasis;
	}
	
	public PVector center() {
		return get(JIDX.TORSO);
	}
	
	public Body transformed(PVector[] sourceBasis, 
						   	PVector[] targetBasis,
						   	PVector targetCenter) {
		Body transformedPose = new Body();
		PVector center = center();
		for (JIDX jidx : JIDX.ALL_JIDX) {
			if (JIDX.TORSO == jidx) {
				transformedPose.set(JIDX.TORSO, targetCenter.get());
			} else {
				PVector jointPosition = get(jidx);
				PVector radiusVector = PVector.sub(jointPosition, center);
				float[] dissolution = PVecUtilities.dissolve(radiusVector, sourceBasis);
				PVector normalizedPosition = PVecUtilities.assemble(targetBasis, dissolution);
				transformedPose.set(jidx, PVector.add(targetCenter, normalizedPosition));
			}
		}
		transformedPose.setOrientation(this.orientations);
		transformedPose.setTimestamp(getTimestamp());
		return transformedPose;
	}
	
	public Body translated(PVector targetCenter) {
		Body translatedPose = new Body();
		PVector center = center();
		for(JIDX jidx : JIDX.ALL_JIDX) {
			if(JIDX.TORSO == jidx) {
				translatedPose.set(jidx, targetCenter.get());
			}
			else {
				PVector jointPosition = get(jidx);
				PVector radiusVector = PVector.sub(jointPosition, center);
				translatedPose.set(jidx, PVector.add(targetCenter, radiusVector));
			}
		}
		translatedPose.setOrientation(this.orientations);
		return translatedPose;
	}
	
	public PVector orientation()
	{
		return PVecUtilities.normTo3Pt(get(JIDX.TORSO), get(JIDX.LEFT_SHOULDER), get(JIDX.RIGHT_SHOULDER));
	}
	
	public PVector upperUpvec()
	{
		PVector upvec = PVector.sub(get(JIDX.NECK), get(JIDX.TORSO));
		upvec.div(upvec.mag());
		return upvec;
	}
	
	public PVector lowerUpvec() 
	{
		return PVecUtilities.UPVEC;
	}
	
	@Override
	public Object clone()
	{
		Body cln = new Body();

		cln.setTimestamp(getTimestamp());

		for (JIDX jidx : keySet()) {
			
			cln.set(jidx, PVecUtilities.clone(get(jidx)));
		}
		
		
		cln.setOrientation(this.orientations);		
		
		return cln;
	}
	
	private PMatrix3D clonePMatrix3D(PMatrix3D matrix)
	{
		float[] matrixElements = new float[9];
		matrix.get(matrixElements);
		PMatrix3D newMatrix = new PMatrix3D();
		newMatrix.set(matrixElements);
		return newMatrix;
	}
	
	// interpolates a position of the body from the positions and their coefficients given in parameter
	public static Body interpolate(Body body1, Body body2, float coefficient) {
		
		// position of the body to be returned
		Body body = new Body();
		
		// to store the part of the body which position is being computed
		JIDX iJidx;
		
		// browses all parts of the body
		for (Map.Entry<JIDX, PVector> joints_iterator : body1.entrySet()) {
			
			// part of the body to interpolate
			iJidx = joints_iterator.getKey();
			
			// interpolates the position of this part of the body and stores it
			body.set(iJidx,
					PVector.lerp(joints_iterator.getValue(), body2.get(iJidx), coefficient));
			
		}
		// the interpolated position of the body is returned
		return body;
		
	}
	
	public boolean isSimilar(Body other, float relativeDeviationTolerance) {
		PVector centerA = get(JIDX.TORSO);
		PVector centerB = other.get(JIDX.TORSO);
		for (JIDX jidx : JIDX.ALL_JIDX) {
			if (JIDX.TORSO == jidx) continue;
			PVector radialA = PVector.sub(get(jidx), centerA);
			PVector radialB = PVector.sub(other.get(jidx), centerB);
			PVector deviation = PVector.sub(radialB, radialA);
			float referenceMag = Math.max(radialA.mag(), radialB.mag());
			float deviationMag = deviation.mag();
			if (deviationMag > referenceMag * relativeDeviationTolerance) {
				return false;
			}
		}
		return true;
	}
	
	public float characteristicSize() {
		float sumsize = 0.0f;
		
		for (Pair<JIDX, JIDX> jidxpair : LIDX.LIMB_ENDS().values()) {
			sumsize += PVector.sub(get(jidxpair.second), get(jidxpair.first)).mag();
		}
		
		return sumsize / LIDX.LIMB_ENDS().size();
	}
	
	public Body scale(float factor) {
		Body scaled = new Body();
		
		for (JIDX jidx : JIDX.ALL_JIDX) {
			scaled.set(jidx, PVector.mult(get(jidx), factor));
		}
		
		scaled.setOrientation(this.orientations);
		scaled.setTimestamp(getTimestamp());
		return scaled;
	}

	public HashMap<JIDX, PMatrix3D> getOrientation() {
		return this.orientations;
	}

	public void setOrientation(HashMap<JIDX, PMatrix3D> orientation) {
		this.orientations = orientation;
	}
}
