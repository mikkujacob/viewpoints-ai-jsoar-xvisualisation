package Viewpoints.FrontEnd.Shared;

public class Measure {
	private float timestamp;
	private float value;
	
	public Measure(float timestamp, float value) {
		super();
		this.timestamp = timestamp;
		this.value = value;
	}

	public float getTimestamp() {
		return timestamp;
	}

	public float getValue() {
		return value;
	}
}
