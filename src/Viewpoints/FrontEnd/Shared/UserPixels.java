package Viewpoints.FrontEnd.Shared;

import java.util.Arrays;

public class UserPixels 
{
	int[] userPixels;
	int userNumber = -1;
	
	public UserPixels(int size)
	{
		userPixels = new int[size];
	}

	public UserPixels(int size, int userNumber)
	{
		userPixels = new int[size];
		this.userNumber = userNumber;
	}
	
	public UserPixels(int[] pixelData, int userNumber)
	{
		this.userNumber = userNumber;
		userPixels = Arrays.copyOf(pixelData, pixelData.length);
	}

	public int[] getUserPixels()
	{
		return userPixels;
	}

	public void setUserPixels(int[] userPixels)
	{
		this.userPixels = userPixels;
	}

	public int getUserNumber()
	{
		return userNumber;
	}

	public void setUserNumber(int userNumber)
	{
		this.userNumber = userNumber;
	}
}
