package Viewpoints.FrontEnd.Shared;

import java.util.ArrayList;

import processing.core.PVector;

public class BodyDerivative extends SkeletalData implements Cloneable
{
	/**
	 * Generated serial number
	 */
	private static final long serialVersionUID = 2981506184017871572L;

	BodyDerivative(SkeletalData oldSdata, SkeletalData newSdata) {
		setTimestamp((newSdata.getTimestamp() + oldSdata.getTimestamp())/2);
		
		float dT = (newSdata.getTimestamp() - oldSdata.getTimestamp());

		for (JIDX ji : JIDX.ALL_JIDX) {
			if (dT > 0) {
				PVector delta = PVector.sub(newSdata.get(ji), oldSdata.get(ji));
				delta.div(dT);
				put(ji, delta);
			} else {
				put(ji, PVecUtilities.ZERO.get());
			}
		}
	}
	
	public static ArrayList<BodyDerivative> derivative(ArrayList<? extends SkeletalData> frames) {
		ArrayList<BodyDerivative> derivative = new ArrayList<BodyDerivative>();
		for (int i = 0; i < frames.size() - 1; i++) {
			derivative.add(new BodyDerivative(frames.get(i), frames.get(i+1)));
		}
		return derivative;
	}
}
