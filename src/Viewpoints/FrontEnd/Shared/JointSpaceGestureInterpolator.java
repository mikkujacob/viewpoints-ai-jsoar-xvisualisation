package Viewpoints.FrontEnd.Shared;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

// records body positions at certain times and generate body positions
// at intermediate times by interpolation
public class JointSpaceGestureInterpolator implements java.io.Serializable {

	// serial ID of LeaderRecord
	private static final long serialVersionUID = 1L;

	// tree map storing body positions and the time this body has been recorded
	private TreeMap<Float, Body> bodies = new TreeMap<Float, Body>();

	// relative acceleration of the replay against the original scene
	private float replaySpeed = 1;

	// whether the replay has to be repeated or not
	// if yes, it is repeated backward once finished until it is back to the beginning
	private boolean isRepetitionOn = true;
	
	// if true, always repeat from the beginning to the end
	// if false, play from the beginning to the end, then from the end to the beginning, then from the beginning to the end, etc.
	private boolean isDirectRepetition = false;

	private float duration = 0.0f;

	// total time elapsed since the beginning of the recording or the replay
	private float totalTime = 0.0f;
	
	private float percentPassed = 0.0f;

	public JointSpaceGestureInterpolator(ArrayList<Body> iBodies) {
		if (0 != iBodies.size()) {
			float startingTime = iBodies.get(0).getTimestamp();
			
			for (Body body : iBodies) {
				bodies.put(body.getTimestamp() - startingTime, body);
			}
			
			duration = iBodies.get(iBodies.size() - 1).getTimestamp() - iBodies.get(0).getTimestamp();
		}
	}
	
	public void setReplaySpeed(float replaySpeed) {
		this.replaySpeed = replaySpeed;
	}
	
	public float getReplaySpeed() {
		return this.replaySpeed;
	}
	
	// updates whether the replay has to be repeated or not
	public void setIsRepetitionOn(boolean isRepetitionOn) {
		this.isRepetitionOn = isRepetitionOn;
	}
	
	public void setDirectRepetition(boolean isDirectRepetition) {
		this.isDirectRepetition = isDirectRepetition;
	}
	
	public boolean isRepetitionOn() {
		return this.isRepetitionOn;
	}
	
	public boolean isDirectRepetition() {
		return this.isDirectRepetition;
	}

	// sets the speed back to its original value
	public void reinitializeSpeed() {
		replaySpeed = 1;
	}
	
	public Body at(float time) {
		if (time >= bodies.lastKey()) {
			return (Body)bodies.lastEntry().getValue().clone();
		} else if (time <= bodies.firstKey()) {
			return (Body)bodies.firstEntry().getValue().clone();
		} else {
			Map.Entry<Float, Body> floorEntry = bodies.floorEntry(time);
			float floorTime = floorEntry.getKey();
			Body floorBody = floorEntry.getValue();
			if (time == floorTime) return (Body)floorBody.clone();
			Map.Entry<Float, Body> ceilingEntry = bodies.ceilingEntry(time);
			float ceilingTime = ceilingEntry.getKey();
			Body ceilingBody = ceilingEntry.getValue();
			Body result = Body.interpolate(floorBody, ceilingBody, (time - floorTime) / (ceilingTime - floorTime));
			return result;
		}
	}

	// returns the position of the body at this time (real or interpolated)
	public Body replayFrame(float deltaTime) {
		float replay_time = getReplayTime();

		percentPassed = replay_time / duration;
		
		//System.out.println(totalTime);
		
		Body response = at(replay_time);
		
		response.setTimestamp(totalTime);
		totalTime += replaySpeed * deltaTime;
		// the position of the body corresponding to the given time is returned
		return response;
	}
	
	//TODO: (Lauren) add comments
	public Body atFrame(float time) {
		Body response = at(time);
		response.setTimestamp(totalTime);
		return response;
	}
	
	private float getReplayTime() {
		if (!isRepetitionOn) {
			if (totalTime > duration) {
				return duration;
			} else {
				return totalTime;
			}
		} else {
			int nb_cycles = (int)(totalTime / duration);
			float endOfLastCycle = duration * nb_cycles;
			float timeInCurrentCycle = totalTime - endOfLastCycle;
			if (isDirectRepetition || 0 == nb_cycles % 2) {
				return timeInCurrentCycle;
			} else {
				return duration - timeInCurrentCycle;
			}
		}
	}
	
	public float percentPassed() {
		return percentPassed;
	}
	
	public float getBasicDuration() {
		return bodies.lastKey();
	}

	public float getTimePlayed() {
		return totalTime;
	}
	
	public float getTimeInCurrentCycle() {
		int nb_cycles = (int)(totalTime / duration);
		float endOfLastCycle = duration * nb_cycles;
		float timeInCurrentCycle = totalTime - endOfLastCycle;
		return timeInCurrentCycle;
	}
}
