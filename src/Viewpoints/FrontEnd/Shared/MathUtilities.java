package Viewpoints.FrontEnd.Shared;

public class MathUtilities {
	public static float exponentialDrift(float oldValue, float targetValue, float deltatime, float characteristicTime) {
		float delta = targetValue - oldValue;
		delta *= Math.pow(2.0, -deltatime / characteristicTime);
		return targetValue - delta;
	}
	
	// smoothingFactor * location + (1 - smoothingFactor) * previousSmoothed
	public static float calculateEMA(float prevEMA, float currLocation, float smoothingFactor) {
		float currentEMA = prevEMA;
		currentEMA = currentEMA * (float)(1 - smoothingFactor);
		
		currentEMA = currentEMA + (smoothingFactor * currLocation);
		
		return currentEMA;
	}
}
