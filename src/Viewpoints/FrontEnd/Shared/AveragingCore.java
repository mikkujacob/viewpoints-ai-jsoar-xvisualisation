package Viewpoints.FrontEnd.Shared;

import java.util.*;

public class AveragingCore {
	private float width;
	private float normwidth;
	
	public AveragingCore(float width) {
		this.width = width;
		this.normwidth = width/(float)Math.sqrt(-Math.log(0.25));
	}
	
	public ArrayList<Measure> apply(ArrayList<Measure> measures) {
		ArrayList<Measure> averages = new ArrayList<Measure>(measures.size());
		
		for (int i = 0; i < measures.size(); i++) {
			Measure med = measures.get(i);
			float medTime = med.getTimestamp();
			float ave = med.getValue();
			float normalizer = 1;
			
			for (int j = 1; ;j++) {
				boolean lbreak = false;
				boolean rbreak = false;
				
				if (i+j < measures.size() && 
						measures.get(i+j).getTimestamp() - medTime < width) {
					float factor = evaluateCore(medTime, measures.get(i+j).getTimestamp());
					ave += factor * measures.get(i+j).getValue();
					normalizer += factor;
				} else {
					lbreak = true;
				}
				
				if (i-j > -1 && medTime - measures.get(i-j).getTimestamp() < width) {
					float factor = evaluateCore(medTime, measures.get(i-j).getTimestamp());
					ave += factor * measures.get(i-j).getValue();
					normalizer += factor;
				} else {
					rbreak = true;
				}
				
				if (lbreak && rbreak)
					break;
			}
			
			averages.add(new Measure(medTime, ave / normalizer));
		}
		
		return averages;
	}
	
	private float evaluateCore(float center, float target) {
		return (float)Math.exp(-Math.pow((target - center)/normwidth, 2));
	}
}
