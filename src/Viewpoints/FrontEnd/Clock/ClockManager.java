package Viewpoints.FrontEnd.Clock;

/**
 * Contains references to all the clocks used in Viewpoints to maintain consistent access.
 * @author Lauren
 *
 */

public class ClockManager {
	private static Clock mainClock;
	
	public static Clock getMainClock() {
		return mainClock;
	}
	
	public static void setMainClock(Clock c) {
		mainClock = c;
	}
}
