package Viewpoints.FrontEnd.Clock;

/**
 * Class used to keep track of event timing in Viewpoints
 */
public class Clock {
	
	private long iniTime = 0;
	private float checktime = 0.0f;
	private float deltatime = 0.0f;
	
	/**
	 * Constructor
	 */
	public Clock() {
		iniTime = System.currentTimeMillis();
	}
	
	/**
	 * Starts or resets the clock to be initialized at the current time
	 */
	public void start() {
		iniTime = System.currentTimeMillis();
	}
	
	/*
	 * Checks the time, updating deltatime (seconds elapsed in between checks) and 
	 * check time (last time the clock was checked in seconds)
	 */
	public void check() {
		float time = (System.currentTimeMillis() - iniTime) / 1000.0f;
		deltatime = time - checktime;
		checktime = time;
		//deltatime = 0.02f;
		//checktime += deltatime;
	}
	
	/**
	 * Checks the time, updating deltatime (milliseconds elapsed in between checks) and 
	 * check time (last time the clock was checked in milliseconds)
	 */
	public void checkMillis() {
		float time = System.currentTimeMillis() - iniTime;
		deltatime = time - checktime;
		checktime = time;
		//deltatime = 0.02f;
		//checktime += deltatime;
	}
	
	/**
	 * Returns the last time the clock was "checked"
	 * @return the last time the clock was checked
	 */
	public float time() {
		return checktime;
	}
	
	/**
	 * Returns the time elapsed in between checks
	 * @return the time elapsed since the clock was checked
	 */
	public float deltatime() {
		return deltatime;
	}
	
	/**
	 * Returns the time since the clock was started in seconds
	 * @return time since the clock was initialized in seconds
	 */
	public float iniTime(){
		return (System.currentTimeMillis() - iniTime) / 1000.0f;
	}
	
	/**
	 * Returns the time since the clock was started in milliseconds
	 * @return time since the clock was initialized (milliseconds)
	 */
	public float iniTimeMillis(){
		return (System.currentTimeMillis() - iniTime);
	}
}