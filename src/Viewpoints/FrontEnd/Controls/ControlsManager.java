package Viewpoints.FrontEnd.Controls;

import java.util.HashMap;

import SimpleOpenNI.SimpleOpenNI;
import Viewpoints.FrontEnd.Application.ViewpointsFrontEnd;
import Viewpoints.FrontEnd.Communication.Socket.RemoteControl.RemoteControlData.RemoteControlPreset;
import Viewpoints.FrontEnd.Graphics.Debug.Debug;
import Viewpoints.FrontEnd.Graphics.Debug.DebugOverlay;
import Viewpoints.FrontEnd.Graphics.Embodiment.ParticleEmbodiment.Fireflies.Firefly.FireFlyDrawMethod;
import Viewpoints.FrontEnd.Graphics.Embodiment.Shadow.ShadowParameters.MultiShadowType;
import Viewpoints.FrontEnd.Graphics.Embodiment.StencilEmbodiment.StencilEmbodiment.StencilDrawMethod;
import Viewpoints.FrontEnd.Input.MotionInput.FileMotionInput;
import Viewpoints.FrontEnd.Input.MotionInput.FileRehearsalMotionInput;
import Viewpoints.FrontEnd.Input.MotionInput.KinectMotionInput;
import Viewpoints.FrontEnd.Input.MotionInput.MotionInput;
import Viewpoints.FrontEnd.Segmentation.SegmentMethods;
import controlP5.Toggle;
import processing.core.PApplet;
import processing.core.PConstants;

/**
 * Class that contains logic for both keypress and click controls available in VAI
 * @author Sasi
 *
 */
public class ControlsManager {
	// instance variables
	private Debug debug;
	private DebugOverlay overlay;
	private PApplet papplet;
	
	private boolean DEBUG = false;	// show or hide debug toggles
	private boolean DEBUG_TEXT = false;	// show or hide debug text
	private boolean USE_KINECT = true; // use Kinect or files
	private boolean RECORDING_TIMES = false;

	private boolean REHEARSAL_MODE = false;
	private boolean SOAR_RESPONSE_MODE = true; 
	private boolean RANDOM_RESPONSE_MODE = false;
	private boolean REFLECT_VAI_CENTER = true;
	private boolean REFLECT_VAI_MOVEMENTS = true;
	private boolean DRAW_USER = true; // draw user skeleton
	private boolean DRAW_VAI = false; //draw vai skeleton
	private boolean DRAW_SHADOW; // draw user's embodiment, whether shadowEmbodiment, RGB video, or D video.
	private boolean DRAW_EMBODIMENT = true;
	private boolean DRAW_USER_GLYPH = false;
	private boolean DRAW_VAI_GLYPH = false;
	private boolean SHOW_USER_GLYPH = false;
	private boolean SHOW_VAI_GLYPH = false;
	private boolean AI_OFF = false;
	private boolean MONITORING = true;
	private boolean IGNORE_KINECT = false;
	private boolean isBlendedGraphics = true;
	private boolean videoStreaming = false;
	private boolean playing = true;
	private boolean viewpointsAestheticsModelSocketStreaming = true;
	private boolean drawNearestUserEmbodiment = false; // draw only nearest user embodiment
	private boolean drawRGB; // draw user's embodiment, whether shadowEmbodiment, RGB video, or D video.
	private boolean drawDepth; // draw user's embodiment, whether shadowEmbodiment, RGB video, or D video.
	private boolean drawStencil; // draw the stencil embodiment
	private boolean enterGreetingModes = false; // enter greeting modes when first user arrives or last user leaves
	private boolean remoteControlSocketReceiving = true;
	private boolean twoStagesPresent = false;
	private boolean embodimentOnTopStage = false;
	private boolean useCueingFileMotionInput = false;
	private boolean USE_UNITY = false;

	String[] flags = {"USE_KINECT", "REFLECT_VAI_CENTER", "REFLECT_VAI_MOVEMENTS", "DRAW_USER", "DRAW_VAI",
	"DRAW_SHADOW"};
	boolean[] bools = {USE_KINECT, REFLECT_VAI_CENTER, REFLECT_VAI_MOVEMENTS, DRAW_USER, DRAW_VAI,
	DRAW_SHADOW};
	
	private HashMap<String, Boolean> booleanFlags;
	
	//pass in debug object for manager to see if can get away with not having it
	/**
	 * 
	 * @param papplet the frontend of the system
	 */
	public ControlsManager(PApplet papplet) {
//		this.debug = debug;
//		this.overlay = overlay;
		this.papplet = papplet;
		
		booleanFlags = new HashMap<String, Boolean>();
		
		for(int i = 0; i < flags.length; i++) {
			booleanFlags.put(flags[i], bools[i]);
		}

		initializeUserEmbodiment();
	}
	
	/*
	 * Method that checks for if DebugOverlay toggles are clicked
	 * 
	 */
	public void cListen() {
		//TODO: (Sasi) Un-hardcode this
		if(((Toggle)overlay.getCP5().getController("USE_KINECT")).isMouseOver()) {
			USE_KINECT = !USE_KINECT;
			((Toggle)overlay.getCP5().getController("USE_KINECT")).setValue(USE_KINECT);
		}
		
		if(((Toggle)overlay.getCP5().getController("REFLECT_VAI_CENTER")).isMouseOver()) {
			REFLECT_VAI_CENTER = !REFLECT_VAI_CENTER;
			((Toggle)overlay.getCP5().getController("REFLECT_VAI_CENTER")).setValue(REFLECT_VAI_CENTER);
		}
		
		if(((Toggle)overlay.getCP5().getController("REFLECT_VAI_MOVEMENTS")).isMouseOver()) {
			REFLECT_VAI_MOVEMENTS = !REFLECT_VAI_MOVEMENTS;
			((Toggle)overlay.getCP5().getController("REFLECT_VAI_MOVEMENTS")).setValue(REFLECT_VAI_MOVEMENTS);
		}

		if(((Toggle)overlay.getCP5().getController("DRAW_USER")).isMouseOver()) {
			DRAW_USER = !DRAW_USER;
			((Toggle)overlay.getCP5().getController("DRAW_USER")).setValue(DRAW_USER);
		}
				
		if(((Toggle)overlay.getCP5().getController("DRAW_VAI")).isMouseOver()) {
			DRAW_VAI = !DRAW_VAI;
			((Toggle)overlay.getCP5().getController("DRAW_VAI")).setValue(DRAW_VAI);
		}
		
		if(((Toggle)overlay.getCP5().getController("DRAW_SHADOW")).isMouseOver()) {
			DRAW_SHADOW = !DRAW_SHADOW;
			((Toggle)overlay.getCP5().getController("DRAW_SHADOW")).setValue(DRAW_SHADOW);
		}
	}
	
	public void listen()
	{
		if(papplet instanceof ViewpointsFrontEnd)
		{
			listenViewpointsFrontEnd(((ViewpointsFrontEnd)papplet));
		}
	}
	
	/*
	 * Method that contains logic for what to do on key presses
	 */
	public void listenViewpointsFrontEnd(ViewpointsFrontEnd papplet) {
		
		if(papplet.key == 'b' || papplet.key == 'B')
		{
			isBlendedGraphics = !isBlendedGraphics;
			papplet.changeBlendedGraphics();
		}
		
		if(papplet.key == 'f' || papplet.key == 'F')
		{
			papplet.getVaiEffectsController().activeParameters().drawMethod = FireFlyDrawMethod.values()[(papplet.getVaiEffectsController().activeParameters().drawMethod.ordinal() + 1) % FireFlyDrawMethod.values().length];
		}
		
//		if(papplet.key == 'l' || papplet.key == 'L')
//		{
//			papplet.getUserStencilEffectsController().activeParameters().drawMethod = StencilDrawMethod.values()[(papplet.getUserStencilEffectsController().activeParameters().drawMethod.ordinal() + 1) % StencilDrawMethod.values().length];
//		}
		
		if(papplet.key == 'j' || papplet.key == 'J')
		{
			useCueingFileMotionInput = !useCueingFileMotionInput;
		}
		
//		if(papplet.key == '1') {
//			papplet.getUserEffectsController().activeParameters().setShadowPixel(2);
//			debug.addGUIText("SHADOW PIXELATION: " + papplet.getUserEffectsController().activeParameters().getShadowPixel()+1, 5000);
//		}
//		if(papplet.key == '2') {
//			papplet.getUserEffectsController().activeParameters().setShadowPixel(3);
//			debug.addGUIText("SHADOW PIXELATION: " + papplet.getUserEffectsController().activeParameters().getShadowPixel()+1, 5000);
//		}
//		if(papplet.key == '3') {
//			papplet.getUserEffectsController().activeParameters().setShadowPixel(4);
//			debug.addGUIText("SHADOW PIXELATION: " + papplet.getUserEffectsController().activeParameters().getShadowPixel()+1, 5000);
//		}
//		if(papplet.key == '4') {
//			papplet.getUserEffectsController().activeParameters().setShadowPixel(5);
//			debug.addGUIText("SHADOW PIXELATION: " + papplet.getUserEffectsController().activeParameters().getShadowPixel()+1, 5000);
//		}
//		if(papplet.key == '5') {
//			papplet.getUserEffectsController().activeParameters().setShadowPixel(6);
//			debug.addGUIText("SHADOW PIXELATION: " + papplet.getUserEffectsController().activeParameters().getShadowPixel()+1, 5000);
//		}
//		if(papplet.key == '6') {
//			papplet.getUserEffectsController().activeParameters().setShadowPixel(7);
//			debug.addGUIText("SHADOW PIXELATION: " + papplet.getUserEffectsController().activeParameters().getShadowPixel()+1, 5000);
//		}
//		if(papplet.key == '7') {
//			papplet.getUserEffectsController().activeParameters().setShadowPixel(8);
//			debug.addGUIText("SHADOW PIXELATION: " + papplet.getUserEffectsController().activeParameters().getShadowPixel()+1, 5000);
//		}
//		if(papplet.key == '8') {
//			papplet.getUserEffectsController().activeParameters().setShadowPixel(9);
//			debug.addGUIText("SHADOW PIXELATION: " + papplet.getUserEffectsController().activeParameters().getShadowPixel()+1, 5000);
//		}
//		if(papplet.key == '9') {
//			papplet.getUserEffectsController().activeParameters().setShadowPixel(10);
//			debug.addGUIText("SHADOW PIXELATION: " + papplet.getUserEffectsController().activeParameters().getShadowPixel()+1, 5000);
//		}
		
//		if(papplet.key == '1')
//		{
//			papplet.getUserEffectsController().activeParameters().isMultiShadow = false;
//		}
//		else if(papplet.key == '2')
//		{
//			papplet.getUserEffectsController().activeParameters().isMultiShadow = true;
//			papplet.getUserEffectsController().activeParameters().multiShadowType = MultiShadowType.SINGLE_LINE_HORIZONTAL;
//		}
//		else if(papplet.key == '3')
//		{
//			papplet.getUserEffectsController().activeParameters().isMultiShadow = true;
//			papplet.getUserEffectsController().activeParameters().multiShadowType = MultiShadowType.SINGLE_LINE_DIAGONAL;
//		}
//		else if(papplet.key == '4')
//		{
//			papplet.getUserEffectsController().activeParameters().isMultiShadow = true;
//			papplet.getUserEffectsController().activeParameters().multiShadowType = MultiShadowType.DOUBLE_LINE_DIAGONAL;
//		}
//		else if(papplet.key == '5')
//		{
//			papplet.getUserEffectsController().activeParameters().isMultiShadow = true;
//			papplet.getUserEffectsController().activeParameters().multiShadowType = MultiShadowType.X_SHAPE;
//		}
//		else if(papplet.key == '6')
//		{
//			papplet.getUserEffectsController().activeParameters().isFadeModulationActive = !papplet.getUserEffectsController().activeParameters().isFadeModulationActive;
//		}
//		else if(papplet.key == '7')
//		{
//			papplet.getUserEffectsController().activeParameters().isEnergyFadeBothRising = !papplet.getUserEffectsController().activeParameters().isEnergyFadeBothRising;
//		}
//		if(papplet.key == '3')
//		{
//			papplet.currentPreset = RemoteControlPreset.values()[(papplet.currentPreset.ordinal() + 1) % RemoteControlPreset.values().length];
////			System.out.println("PRESET: " + papplet.currentPreset.toString());
//			papplet.handlePresets(papplet.currentPreset);
//		}
//		else if(papplet.key == '1')
//		{
//			int ordinal = papplet.currentPreset.ordinal() - 1;
//			if(ordinal < 0)
//			{
//				ordinal = RemoteControlPreset.values().length - 1;
//			}
//			papplet.currentPreset = RemoteControlPreset.values()[ordinal];
////			System.out.println("PRESET: " + papplet.currentPreset.toString());
//			papplet.handlePresets(papplet.currentPreset);
//		}
		else if(papplet.key == '8')
		{
			DRAW_SHADOW = false;
			drawRGB = true;
			drawDepth = false;
			drawStencil = false;
		}
		else if(papplet.key == '9')
		{
			DRAW_SHADOW = false;
			drawRGB = false;
			drawDepth = true;
			drawStencil = false;
		}
		
		
		if(papplet.key == ' ' && !DEBUG) { //instructions
			debug.addGUIText("Move in a rhythmic motion to trigger a learned response", 5000);
//			papplet.getFileCueingMotionInput().cueNext();
		}
		
//		//Play / Pause Music
//		if(papplet.keyCode == PConstants.RETURN || papplet.keyCode == PConstants.ENTER) {
//			papplet.getAudioController().togglePlay();
//		}
		
//		//TODO: Test if this works
//		if(papplet.key == 'n' || papplet.key == 'N') {
//			papplet.getAudioController().nextTrack();
//		}
//		
//		if(papplet.key == 'f' || papplet.key == 'F') {
//			papplet.getAudioController().toggleShuffle();
//		}
		
		//Toggle Reflect VAI's Center along Y Axis
		if(papplet.key == 'c' || papplet.key == 'C') {
			REFLECT_VAI_CENTER = !REFLECT_VAI_CENTER;
			((Toggle)overlay.getCP5().getController("REFLECT_VAI_CENTER")).toggle();
			debug.addGUIText("REFLECT VAI CENTER: " + REFLECT_VAI_CENTER, 5000);
		}
		//Toggle Reflect VAI's Movements along Y Axis
		if(papplet.key == 'm' || papplet.key == 'M') {
			REFLECT_VAI_MOVEMENTS = !REFLECT_VAI_MOVEMENTS;
			((Toggle)overlay.getCP5().getController("REFLECT_VAI_MOVEMENTS")).toggle();
			debug.addGUIText("REFLECT VAI MOVEMENTS: " + REFLECT_VAI_MOVEMENTS, 5000);
		}
		//Toggle ActivityTracker vs. RhythmTracker
		if (papplet.key == 't' || papplet.key == 'T') {
			ViewpointsFrontEnd.SEGMENTATION_METHOD = SegmentMethods.values()[(ViewpointsFrontEnd.SEGMENTATION_METHOD.ordinal() + 1) % SegmentMethods.values().length];
			ViewpointsFrontEnd.getSegmentationManager().changeSegmentationMethod(ViewpointsFrontEnd.SEGMENTATION_METHOD);
			debug.addGUIText("SEGMENTATION METHOD: " + ViewpointsFrontEnd.SEGMENTATION_METHOD, 5000);
		}

		if(papplet.key == 'g' || papplet.key == 'G')
		{
			if(DRAW_USER_GLYPH && DRAW_VAI_GLYPH)
			{
				DRAW_USER_GLYPH = true;
				DRAW_VAI_GLYPH = false;
				debug.addGUIText("RhythmDRAW_USER_GLYPH: " + DRAW_USER_GLYPH, 500);
				debug.addGUIText("RhythmDRAW_VAI_GLYPH: " + DRAW_VAI_GLYPH, 500);
			}
			else if (DRAW_USER_GLYPH && !DRAW_VAI_GLYPH)
			{
				DRAW_USER_GLYPH = false;
				DRAW_VAI_GLYPH = true;
				debug.addGUIText("RhythmDRAW_USER_GLYPH: " + DRAW_USER_GLYPH, 500);
				debug.addGUIText("RhythmDRAW_VAI_GLYPH: " + DRAW_VAI_GLYPH, 500);
			}
			else if (!DRAW_USER_GLYPH && DRAW_VAI_GLYPH)
			{
				DRAW_USER_GLYPH = false;
				DRAW_VAI_GLYPH = false;
				debug.addGUIText("RhythmDRAW_USER_GLYPH: " + DRAW_USER_GLYPH, 500);
				debug.addGUIText("RhythmDRAW_VAI_GLYPH: " + DRAW_VAI_GLYPH, 500);
			}
			else if (!DRAW_USER_GLYPH && !DRAW_VAI_GLYPH)
			{
				DRAW_USER_GLYPH = true;
				DRAW_VAI_GLYPH = true;
				debug.addGUIText("RhythmDRAW_USER_GLYPH: " + DRAW_USER_GLYPH, 500);
				debug.addGUIText("RhythmDRAW_VAI_GLYPH: " + DRAW_VAI_GLYPH, 500);
			}
		}
		
		//Toggle Draw User Skeleton
		if(papplet.key == 'u' || papplet.key == 'U') {
			DRAW_USER = !DRAW_USER;
			((Toggle)overlay.getCP5().getController("DRAW_USER")).setValue(DRAW_USER);
			debug.addGUIText("DRAW USER: " + DRAW_USER, 5000);
		}
		
		//Toggle Draw vAI Skeleton
		if(papplet.key == 'v' || papplet.key == 'V') {
			DRAW_VAI = !DRAW_VAI;
			((Toggle)overlay.getCP5().getController("DRAW_VAI")).toggle();
			debug.addGUIText("DRAW VAI: " + DRAW_VAI, 5000);
		}

		//Toggle display of fireflies. 
		//TODO: (Lauren) Change this so it works with the new Embodiment class
		if (papplet.key == 'e' || papplet.key =='E') {
			DRAW_EMBODIMENT = !DRAW_EMBODIMENT;
			if (isDEBUG() && !DRAW_EMBODIMENT) {
				DRAW_VAI = true;
			}
		}

		//Cycles between User's ShadowEmbodiment, RGB video, D video, and no user avatar.
		if(papplet.key == 's' || papplet.key == 'S') {
//			DRAW_SHADOW = !DRAW_SHADOW;
			cycleUserEmbodiment();
			((Toggle)overlay.getCP5().getController("DRAW_SHADOW")).toggle();
			debug.addGUIText("DRAW SHADOW: " + DRAW_SHADOW, 5000);
		}
		
		//Toggle between Kinect Usage / Simulated Movement
		//AND between Kinect Usage / Rehearsal Mode
		if(papplet.key == 'k' || papplet.key == 'K' || papplet.key == 'r' || papplet.key == 'R') {
			Boolean biasRehearsal = false;
			playing = true;
			if(papplet.key == 'k' || papplet.key == 'K') {
				USE_KINECT = !USE_KINECT;
				((Toggle)overlay.getCP5().getController("USE_KINECT")).toggle();
				biasRehearsal = false;
			}
			if(papplet.key == 'r' || papplet.key == 'R') {
				REHEARSAL_MODE = !REHEARSAL_MODE;
//				((Toggle)overlay.getCP5().getController("REHEARSAL_MODE")).toggle();
				biasRehearsal = true;
			}
			if(!USE_KINECT && !REHEARSAL_MODE) {
				papplet.setMotionInput(new FileMotionInput(ViewpointsFrontEnd.getPlaybackFile()));
				debug.addGUIText("FILE PLAYBACK MODE: " + !USE_KINECT, 5000);
				IGNORE_KINECT = true;
			} else if(!USE_KINECT && REHEARSAL_MODE) {
				papplet.setMotionInput(new FileRehearsalMotionInput(ViewpointsFrontEnd.getREHEARSAL_GESTURE_LIB(), -1));
				debug.addGUIText("REHEARSAL MODE: " + REHEARSAL_MODE, 5000);
				IGNORE_KINECT = true;
			} else if(USE_KINECT && !REHEARSAL_MODE) {
				if(papplet.getKinectInput() == null)
				{
					ViewpointsFrontEnd.setKinectInput(new KinectMotionInput(new SimpleOpenNI(papplet), papplet.getMinZ(), papplet.getMaxZ()));
					((KinectMotionInput) papplet.getKinectInput()).getKinect().setMirror(true);
				}
				papplet.setMotionInput((MotionInput) papplet.getKinectInput());
				debug.addGUIText("KINECT MODE: " + USE_KINECT, 5000);
				IGNORE_KINECT = false;
			} else if(USE_KINECT && REHEARSAL_MODE) {
				if(biasRehearsal) {
					papplet.setMotionInput(new FileRehearsalMotionInput(ViewpointsFrontEnd.getREHEARSAL_GESTURE_LIB(), -1));
					debug.addGUIText("REHEARSAL MODE: " + REHEARSAL_MODE, 5000);
					IGNORE_KINECT = true;
				}
				else {
					if(papplet.getKinectInput() == null)
					{
						ViewpointsFrontEnd.setKinectInput(new KinectMotionInput(new SimpleOpenNI(papplet), papplet.getMinZ(), papplet.getMaxZ()));
						((KinectMotionInput) papplet.getKinectInput()).getKinect().setMirror(true);
					}
					papplet.setMotionInput(papplet.getKinectInput());
					debug.addGUIText("KINECT MODE: " + USE_KINECT, 5000);	
					IGNORE_KINECT = false;
				}
			}
			//TODO: ADD THIS CALL INTO KEYPRESSED WHEN YOU INCORPORATE THIS FILE INTO IT
//			changeMotionInput();
		}

		//Toggle debug mode;
		if (papplet.key == 'd' || papplet.key == 'D') {
			DEBUG = !DEBUG;
			DEBUG_TEXT = !DEBUG_TEXT;
			if (!USE_KINECT) {
				papplet.getDebugSlider().setLock(!papplet.getDebugSlider().isLock());
				papplet.getDebugSlider().setVisible(!papplet.getDebugSlider().isVisible());
				((FileMotionInput)papplet.getMotionInput()).repeat(!DEBUG);

			}
		//	((Toggle)cp5.getController("DEBUG")).toggle();

			if (!DEBUG) playing = true;
			
			DRAW_USER = true;
			if (!USE_KINECT) {
				DRAW_EMBODIMENT = !DEBUG;
			}
			debug.addGUIText("Debugging: " + DEBUG, 5000);
		}
		
		if (DEBUG && papplet.key == ' ' && !USE_KINECT) {
			playing = !playing;;
		}

		if (DEBUG && !playing && papplet.keyCode == PConstants.LEFT) {
			papplet.getDebugSlider().setValue(papplet.getDebugSlider().getValue() - .01f);
			((FileMotionInput)papplet.getMotionInput()).replayAt(-.01f);
			//addGUIText("Left key pressed", 5000);
		}
//
		if (DEBUG && !playing && papplet.keyCode == PConstants.RIGHT) {
			papplet.getDebugSlider().setValue(papplet.getDebugSlider().getValue() + .01f);
			((FileMotionInput)papplet.getMotionInput()).replayAt(.01f);
			//addGUIText("Right key pressed", 5000);
		}
		
//		//Change the toggle inputs
//		//Toggle Use AI
//		if(papplet.key == 'a' || papplet.key == 'A') {
//			int mode = 0;
//			if(!AI_OFF && !RANDOM_RESPONSE_MODE) { //EXPERIMENT WITH NO AI - MODE 1
//				//TO EXPERIMENT WITH NO AI 
//				AI_OFF = true;
//				RANDOM_RESPONSE_MODE = false;
//				
//				mode = 1;
//			} else if(AI_OFF && RANDOM_RESPONSE_MODE) { //EXPERIMENT WITH FULL AI - MODE 3
//				//TO EXPERIMENT WITH FULL AI
//				AI_OFF = false;
//				RANDOM_RESPONSE_MODE = false;
//				
//				papplet.setResponseLogic(papplet.getBackupSoarResponseLogic());
//				
//				mode = 3;
//				debug.addGUIText("Full AI", 5000);
//			} else if(AI_OFF && !RANDOM_RESPONSE_MODE) { //EXPERIMENT WITH RANDOM AI - MODE 2
//				//TO EXPERIMENT WITH RANDOM AI
//				AI_OFF = true;
//				RANDOM_RESPONSE_MODE = true;
//				
//				papplet.setResponseLogic(papplet.getResponseLogic());
//				
//				mode = 2;
//			}
//			
//			debug.addGUIText("EXPERIMENTAL CONDITION: " + mode, 5000);
//		}
		
		//Modulate min and max distance for Kinect to respond to people.
		if(papplet.key == '.' || papplet.key == '>') {
			if(papplet.getMaxZ() - 100 > papplet.getMinZ()) {
				papplet.setMaxZ(papplet.getMaxZ() - 100);
			}
			debug.addGUIText("MAX Z VALUE: " + papplet.getMaxZ(), 5000);
		}
		if(papplet.key == '/' || papplet.key == '?') {
			if(papplet.getMaxZ() + 100 <= 10000) {
				papplet.setMaxZ(papplet.getMaxZ() + 100);
			}
			debug.addGUIText("MAX Z VALUE: " + papplet.getMaxZ(), 5000);
		}
		if(papplet.key == 'z' || papplet.key == 'Z') {
			if(papplet.getMinZ() - 50 > 0) {
				papplet.setMinZ(papplet.getMinZ() - 50);
			}
			debug.addGUIText("MIN Z VALUE: " + papplet.getMinZ(), 5000);
		}
		if(papplet.key == 'x' || papplet.key == 'X') {
			if(papplet.getMinZ() + 50 < papplet.getMaxZ()) {
				papplet.setMinZ(papplet.getMinZ() + 50);
			}
			debug.addGUIText("MIN Z VALUE: " + papplet.getMinZ(), 5000);
		}
		
		//TODO: (Sasi) Fix this
		
//		//Toggle Randomized VAI Experimentation
//		if(vai.key == 'e' || vai.key == 'E') {
//			if(!RhythmVAI.isRandomizedRoundStarted()) {
//				ArrayList<Integer> tempList = new ArrayList<Integer>();
//				tempList.add(1);
//				tempList.add(2);
//				tempList.add(3);
//				int index;
//				
//				index = random.nextInt(tempList.size());
//				RhythmVAI.experimentalModes.add(tempList.get(index));
//				tempList.remove(index);
//				index = random.nextInt(tempList.size());
//				RhythmVAI.experimentalModes.add(tempList.get(index));
//				tempList.remove(index);
//				index = RhythmVAI.random.nextInt(tempList.size());
//				RhythmVAI.experimentalModes.add(tempList.get(index));
//				tempList.remove(index);
//				
//				RhythmVAI.randomizedRoundStarted = true;
//			}
//			
//			int mode = RhythmVAI.experimentalModes.get(0);
//			RhythmVAI.experimentalModes.remove(0);
//			
//			if(mode == 1) { //EXPERIMENT WITH NO AI - MODE 1
//				RhythmAI_OFF = true;
//				RhythmRANDOM_RESPONSE_MODE = false;
//			} else if(mode == 2) { //EXPERIMENT WITH RANDOM AI - MODE 2
//				RhythmAI_OFF = true;
//				RhythmRANDOM_RESPONSE_MODE = true;
//				
//				RhythmVAI.responseLogic = backupRandomResponseLogic;
//			} else if(mode == 3) { //EXPERIMENT WITH FULL AI - MODE 3
//				RhythmAI_OFF = false;
//				RhythmRANDOM_RESPONSE_MODE = false;
//				
//				RhythmVAI.responseLogic = backupSoarResponseLogic;
//			}
//			if(RhythmVAI.experimentalModes.isEmpty()) {
//				RhythmVAI.randomizedRoundStarted = false;
//			}
//			Rhythmdebug.addGUIText("EXPERIMENTAL CONDITION: " + mode, 5000);
//		
//		}
	}

	// Getters //
	
	public String[] getFlags() {
		return flags;
	}
	
	public HashMap<String, Boolean> getBooleanFlags() {
		return booleanFlags;
	}

	public boolean isDEBUG() {
		return DEBUG;
	}
	
	public boolean isUSE_KINECT() {
		return USE_KINECT;
	}

	public boolean isRECORDING_TIMES(){
		return RECORDING_TIMES;
	}
	
	public boolean isREHEARSAL_MODE() {
		return REHEARSAL_MODE;	
	}

	public boolean isSOAR_RESPONSE_MODE() {
		return SOAR_RESPONSE_MODE;
	}
	
	public boolean isRANDOM_RESPONSE_MODE() {
		return RANDOM_RESPONSE_MODE;
	}
	
	public boolean isREFLECT_VAI_CENTER() {
		return REFLECT_VAI_CENTER;
	}
	
	public boolean isREFLECT_VAI_MOVEMENTS() {
		return REFLECT_VAI_MOVEMENTS;
	}
	
	public boolean isDRAW_USER() {
		return DRAW_USER;
	}
	
	public boolean isDRAW_VAI() {
		return DRAW_VAI;
	}
	
	public boolean isDRAW_SHADOW() {
		return DRAW_SHADOW;
	}
	
	public boolean isDRAW_USER_GLYPH() {
		return DRAW_USER_GLYPH;
	}
	
	public boolean isDRAW_VAI_GLYPH() {
		return DRAW_VAI_GLYPH;
	}
	
	public boolean isAI_OFF() {
		return AI_OFF;
	}

	public boolean isDRAW_EMBODIMENT() {
		return DRAW_EMBODIMENT;
	}
	
	public boolean isIGNORE_KINECT() {
		return IGNORE_KINECT;	
	}
	
	public boolean isMONITORING() {
		return MONITORING;
	}
	
	public boolean isPlaying() {
		return playing;
	}

	public boolean isSHOW_USER_GLYPH() {
		return SHOW_USER_GLYPH;
	}

	public boolean isSHOW_VAI_GLYPH() {
		return SHOW_VAI_GLYPH;
	}
	
	public boolean isVideoStreaming() {
		return videoStreaming;
	}
	
	// Setters //

	
	public void setDEBUG(boolean debug) {
		this.DEBUG = debug;
	}
	
	public void setUSE_KINECT(boolean useKinect) {
		this.USE_KINECT = useKinect;
	}

	public void setRECORDING_TIMES(boolean recordingTimes){
		this.RECORDING_TIMES = recordingTimes;
	}
	
	public void setREHEARSAL_MODE(boolean rehearsalMode) {
		this.REHEARSAL_MODE = rehearsalMode;			
	}

	public void setSOAR_RESPONSE_MODE(boolean soarResponseMode) {
		this.SOAR_RESPONSE_MODE = soarResponseMode;
	}
	
	public void setRANDOM_RESPONSE_MODE(boolean randomResponseMode) {
		this.RANDOM_RESPONSE_MODE = randomResponseMode;
	}
	
	public void setREFLECT_VAI_CENTER(boolean reflectVaiCenter) {
		this.REFLECT_VAI_CENTER = reflectVaiCenter;
	}
	
	public void setREFLECT_VAI_MOVEMENTS(boolean reflectVaiMovements) {
		this.REFLECT_VAI_MOVEMENTS = reflectVaiMovements;
	}
	
	public void setDRAW_USER(boolean drawUser) {
		this.DRAW_USER = drawUser;
	}
	
	public void setDRAW_VAI(boolean drawVai) {
		this.DRAW_VAI = drawVai;
	}
	
	public void setDRAW_SHADOW(boolean drawShadow) {
		this.DRAW_SHADOW = drawShadow;
	}
	
	public void setDRAW_USER_GLYPH(boolean drawUserGlyph) {
		this.DRAW_USER_GLYPH = drawUserGlyph;
	}
	
	public void setDRAW_VAI_GLYPH(boolean drawVaiGlyph) {
		this.DRAW_VAI_GLYPH = drawVaiGlyph;
	}
	
	public void setAI_OFF(boolean aiOff) {
		this.AI_OFF = aiOff;
	}

	public void setDRAW_EMBODIMENT(boolean drawEmbodiment) {
		this.DRAW_EMBODIMENT = drawEmbodiment;
	}
	
	public void setIGNORE_KINECT(boolean ignoreKinect) {
		this.IGNORE_KINECT = ignoreKinect;
	}
	
	public void setMONITORING(boolean monitoring) {
		this.MONITORING = monitoring;
	}
	
	public void setPlaying(boolean playing) {
		this.playing = playing;
	}


	public void setSHOW_USER_GLYPH(boolean showUserGlyph) {
		this.SHOW_USER_GLYPH = showUserGlyph;
	}


	public void setSHOW_VAI_GLYPH(boolean showVaiGlyph) {
		this.SHOW_VAI_GLYPH = showVaiGlyph;
	}
	
	public void setVideoStreaming(boolean videoStreaming) {
		this.videoStreaming = videoStreaming;
	}
	
	public void universalSet(String name, boolean theValue) {
		this.booleanFlags.put(name, theValue);
	}
	
	
	// Debug Object Setters //
	
	public void setDebug(Debug debug) {
		this.debug = debug;
	}
	
	public void setDebugOverlay(DebugOverlay overlay) {
		this.overlay = overlay;
	}
	
	
	// Unsuccessful attempt to elegantly Fixing Toggle Issues //
	public void toggle(boolean bool, String name) {
		bool = !bool;
		((Toggle)overlay.getCP5().getController(name)).setValue(bool);
	}

	public boolean isBlendedGraphics()
	{
		return isBlendedGraphics;
	}

	public void setIsBlendedGraphics(boolean isBlendedGraphics)
	{
		this.isBlendedGraphics = isBlendedGraphics;
	}

	public boolean isViewpointsAestheticsModelSocketStreaming()
	{
		return viewpointsAestheticsModelSocketStreaming;
	}

	public void setViewpointsAestheticsModelSocketStreaming(
			boolean viewpointsAestheticsModelSocketStreaming)
	{
		this.viewpointsAestheticsModelSocketStreaming = viewpointsAestheticsModelSocketStreaming;
	}
	
	public void initializeUserEmbodiment()
	{
		DRAW_SHADOW = true;
		drawRGB = false;
		drawDepth = false;
		drawStencil = false;
	}
	
	public void cycleUserEmbodiment()
	{
		if(DRAW_SHADOW)
		{
			DRAW_SHADOW = false;
			drawRGB = true;
			drawDepth = false;
			drawStencil = false;
		}
		else if(drawRGB && !DRAW_SHADOW)
		{
			DRAW_SHADOW = false;
			drawRGB = false;
			drawDepth = true;
			drawStencil = false;
		}
		else if(drawDepth && !drawRGB && !DRAW_SHADOW)
		{
			DRAW_SHADOW = false;
			drawRGB = false;
			drawDepth = false;
			drawStencil = true;
		}
		else if(drawStencil && !drawDepth && !drawRGB && !DRAW_SHADOW)
		{
			DRAW_SHADOW = false;
			drawRGB = false;
			drawDepth = false;
			drawStencil = false;
		}
		else
		{
			DRAW_SHADOW = true;
			drawRGB = false;
			drawDepth = false;
			drawStencil = false;
		}
	}

	public boolean isDrawRGB()
	{
		return drawRGB;
	}

	public void setDrawRGB(boolean drawRGB)
	{
		this.drawRGB = drawRGB;
	}

	public boolean isDrawDepth()
	{
		return drawDepth;
	}

	public void setDrawDepth(boolean drawD)
	{
		this.drawDepth = drawD;
	}

	public boolean isDrawNearestUserEmbodiment()
	{
		return drawNearestUserEmbodiment;
	}

	public void setDrawNearestUserEmbodiment(boolean drawNearestUserEmbodiment)
	{
		this.drawNearestUserEmbodiment = drawNearestUserEmbodiment;
	}

	public boolean isDrawStencil()
	{
		return drawStencil;
	}

	public void setDrawStencil(boolean drawStencil)
	{
		this.drawStencil = drawStencil;
	}

	public boolean isEnterGreetingModes()
	{
		return enterGreetingModes;
	}

	public void setEnterGreetingModes(boolean enterGreetingModes)
	{
		this.enterGreetingModes = enterGreetingModes;
	}

	public boolean isDEBUG_TEXT()
	{
		return DEBUG_TEXT;
	}

	public void setDEBUG_TEXT(boolean dEBUG_TEXT)
	{
		DEBUG_TEXT = dEBUG_TEXT;
	}

	public boolean isRemoteControlSocketReceiving()
	{
		return remoteControlSocketReceiving;
	}

	public void setRemoteControlSocketReceiving(boolean remoteControlSocketReceiving)
	{
		this.remoteControlSocketReceiving = remoteControlSocketReceiving;
	}

	public boolean isTwoStagesPresent()
	{
		return twoStagesPresent;
	}

	public void setTwoStagesPresent(boolean twoStagesPresent)
	{
		this.twoStagesPresent = twoStagesPresent;
	}

	public boolean isEmbodimentOnTopStage()
	{
		return embodimentOnTopStage;
	}

	public void setEmbodimentOnTopStage(boolean embodimentOnTopStage)
	{
		this.embodimentOnTopStage = embodimentOnTopStage;
	}

	public boolean isUseCueingFileMotionInput()
	{
		return useCueingFileMotionInput;
	}

	public void setUseCueingFileMotionInput(boolean useCueingFileMotionInput)
	{
		this.useCueingFileMotionInput = useCueingFileMotionInput;
	}
	
	public boolean isUSE_UNITY() {
		return USE_UNITY;
	}
}