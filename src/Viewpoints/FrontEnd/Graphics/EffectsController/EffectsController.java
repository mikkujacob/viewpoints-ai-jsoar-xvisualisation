package Viewpoints.FrontEnd.Graphics.EffectsController;

import Viewpoints.FrontEnd.Controls.ControlsManager;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.UserPixels;
import processing.core.PImage;

/*
 * Abstract class to define a way to draw both environmental and embodiment effects
 */
public abstract class EffectsController {
	
	private ControlsManager controlsManager;
	
	public EffectsController(ControlsManager controlsManager)
	{
		this.controlsManager = controlsManager;
	}

	public abstract void reset();
	
	public abstract void update(Body userPose, Body vaiPose, UserPixels userPixels, PImage depthImage, PImage rgbImage);
	
	public ControlsManager getControlsManager() {
		return controlsManager;
	}

	public void setControlsManager(ControlsManager controlsManager) {
		this.controlsManager = controlsManager;
	}
}