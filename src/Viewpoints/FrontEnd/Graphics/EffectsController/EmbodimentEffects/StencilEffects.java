package Viewpoints.FrontEnd.Graphics.EffectsController.EmbodimentEffects;

import Viewpoints.FrontEnd.Controls.ControlsManager;
import Viewpoints.FrontEnd.Graphics.EffectsController.EffectsController;
import Viewpoints.FrontEnd.Graphics.Embodiment.StencilEmbodiment.StencilParameters;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.UserPixels;
import processing.core.PImage;

public class StencilEffects extends EffectsController
{
	private StencilParameters stencilParameters;
	private UserPixels userPixels;
	private PImage depthImage;
	private PImage rgbImage;
	
	
	public StencilEffects(ControlsManager controlsManager) {
		super(controlsManager);
		stencilParameters = new StencilParameters();
	}
	
	@Override
	public void reset()
	{
		
	}

	@Override
	public void update(Body userPose, Body vaiPose, UserPixels userPixels, PImage depthImage, PImage rgbImage)
	{
		this.userPixels = userPixels;
		this.depthImage = depthImage;
		this.rgbImage = rgbImage;
	}
	
	public float scaleRange(float oldValue, float oldMin, float oldMax, float newMin, float newMax)
	{
		return (((oldValue - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
	}
	
	public StencilParameters activeParameters()
	{
		return stencilParameters;
	}

	public UserPixels getUserPixels()
	{
		return userPixels;
	}

	public PImage getDepthImage()
	{
		return depthImage;
	}

	public PImage getRgbImage()
	{
		return rgbImage;
	}
}
