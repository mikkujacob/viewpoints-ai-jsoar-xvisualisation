package Viewpoints.FrontEnd.Graphics.EffectsController.EmbodimentEffects;

import Viewpoints.FrontEnd.Controls.ControlsManager;
import Viewpoints.FrontEnd.Graphics.EffectsController.EffectsController;
import Viewpoints.FrontEnd.Graphics.Embodiment.Shadow.ShadowParameters;
import Viewpoints.FrontEnd.Model.AestheticsModel.ViewpointsAestheticsModel;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.UserPixels;
import processing.core.PImage;

public class ShadowEffects extends EffectsController
{
	private ShadowParameters shadowParameters;
	private UserPixels userPixels;
	
	
	public ShadowEffects(ControlsManager controlsManager) {
		super(controlsManager);
		shadowParameters = new ShadowParameters();
	}
	
	@Override
	public void reset()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void update(Body userPose, Body vaiPose, UserPixels userPixels, PImage depthImage, PImage rgbImage)
	{
		this.userPixels = userPixels;
		operateViewpoints();
	}
	
	public void operateViewpoints()
	{
		if (ViewpointsAestheticsModel.getViewpointsTracker().isTracking())
		{
			float energy = ViewpointsAestheticsModel.getViewpointsTracker().getEnergy();
//			float smoothness = ViewpointsAestheticsModel.getViewpointsTracker().getSmoothness();
			
			if(activeParameters().isFadeModulationActive)
			{
				if(activeParameters().isEnergyFadeBothRising)
				{
					//Modulate fade with lack of energy and vice versa
//					System.out.println("ENERGY: " + energy);
					activeParameters().setFade(scaleRange(energy, 0.25f, 0.75f, activeParameters().MIN_FADE, activeParameters().MAX_FADE));
//					System.out.println("FADE: " + activeParameters().FADE);
				}
				else
				{
//					System.out.println("ENERGY: " + (1 - energy));
					activeParameters().setFade(scaleRange(1f - energy, 0.25f, 0.75f, activeParameters().MIN_FADE, activeParameters().MAX_FADE));
//					System.out.println("FADE: " + activeParameters().FADE);
				}
			}
			else
			{
				activeParameters().setFade(activeParameters().DEFAULT_FADE);
			}
		}
	}
	
	public float scaleRange(float oldValue, float oldMin, float oldMax, float newMin, float newMax)
	{
		return (((oldValue - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
	}
	
	public ShadowParameters activeParameters()
	{
		return shadowParameters;
	}

	public UserPixels getUserPixels()
	{
		return userPixels;
	}
}
