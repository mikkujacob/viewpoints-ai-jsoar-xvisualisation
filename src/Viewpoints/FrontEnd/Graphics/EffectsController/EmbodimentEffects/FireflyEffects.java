package Viewpoints.FrontEnd.Graphics.EffectsController.EmbodimentEffects;

import java.awt.Color;

import processing.core.PImage;
import processing.core.PVector;
import Viewpoints.FrontEnd.Clock.ClockManager;
import Viewpoints.FrontEnd.Controls.ControlsManager;
import Viewpoints.FrontEnd.Graphics.EffectsController.EffectsController;
import Viewpoints.FrontEnd.Graphics.Embodiment.ParticleEmbodiment.Fireflies.Firefly;
import Viewpoints.FrontEnd.Graphics.Embodiment.ParticleEmbodiment.Fireflies.FireflyParameters;
import Viewpoints.FrontEnd.Graphics.Field.BodyField;
import Viewpoints.FrontEnd.Graphics.Field.CentralField;
import Viewpoints.FrontEnd.Graphics.Field.Field;
import Viewpoints.FrontEnd.Model.AestheticsModel.ViewpointsAestheticsModel;
import Viewpoints.FrontEnd.Model.Trackers.RhythmTracker;
import Viewpoints.FrontEnd.Shared.*;

/*
 * EffectsController for Firefly embodiment
 */
public class FireflyEffects extends EffectsController {
	public final static PVector CLOUD_CENTER = new PVector(0.0f, 0.0f, 1000.0f);

	private int heartColor = 0;
	private float heartIgnition = 0.0f;
	private float cardio = 0.0f;
	private static final float CARDIO_RHYTHM = 3;
	private static final float TWO_CARDIO_DEV_SQR = (float) (8 * Math.PI * Math.PI / (CARDIO_RHYTHM * CARDIO_RHYTHM));
	private boolean cloudMode = true;
	private PVector heartPosition = null;
	private CentralField cloudField = new CentralField(CLOUD_CENTER);
	private BodyField vaiBodyField = null;
	private float charSize = 0.0f;
	private static FireflyParameters fflyParams = new FireflyParameters();
	private static FireflyParameters fflyCloudParams = new FireflyParameters();
	
	public FireflyEffects(ControlsManager controlsManager) {
		super(controlsManager);
		charSize = 700.0f;
		fflyParams.adjustToScale(charSize / 6, charSize);
		fflyCloudParams.adjustToScale(charSize / 6, charSize);
	}
	
	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}
	
	

	@Override
	public void update(Body userPose, Body vaiPose, UserPixels userPixels, PImage depthImage, PImage rgbImage) {
		if (null != userPose) {
			ViewpointsAestheticsModel.getViewpointsTracker().takePose(userPose);
			operateViewpoints();
		}
		if (null == vaiPose) {
			cloudMode = true;
			ViewpointsAestheticsModel.getViewpointsTracker().reset();
			vaiBodyField = null;
		} else {
			if (cloudMode) {
				cloudMode = false;
				PVector delta = PVector.sub(vaiPose.get(JIDX.TORSO), vaiPose.get(JIDX.RIGHT_FOOT));
				charSize = delta.mag();
				fflyParams.adjustToScale(charSize / 6.0f, charSize / 15.0f);
			}
			heartPosition = PVector.lerp(vaiPose.get(JIDX.TORSO), vaiPose.get(JIDX.NECK), 0.575f);
			if (null == vaiBodyField) {
				vaiBodyField = new BodyField(vaiPose);
			} else {
				vaiBodyField.update(vaiPose);
			}
		}
		RhythmTracker rhythmTracker = ViewpointsAestheticsModel.getRhythmTracker();
		if (rhythmTracker.hasRhythm()) {
			setCardioByPhase(rhythmTracker.getPhase());
			setHeartColor(Color.HSBtoRGB(353 / 360.0f, 0.95f, 0.2f + 0.8f * getCardio()));
			float underignition = 1.0f - getHeartIgnition();
			underignition *= Math.exp(-ClockManager.getMainClock().deltatime());
			setHeartIgnition(1.0f - underignition);
		} else {
			setHeartIgnition((float) (getHeartIgnition() * Math.exp(-ClockManager.getMainClock().deltatime())));
		}
	}
	
	public void operateViewpoints() {
		if (ViewpointsAestheticsModel.getViewpointsTracker().isTracking()) {
			float spectrePitch = 1 - ViewpointsAestheticsModel.getViewpointsTracker().getEnergy();
			//System.out.println(spectrePitch);
			fflyParams.setSpectrePitch(spectrePitch);
			fflyCloudParams.setSpectrePitch(spectrePitch);
			//System.out.println(viewpointsTracker.getSmoothness());
//			float electrization = 0.0f;
			float sparkleness = 0.0f;
			float smoothness = ViewpointsAestheticsModel.getViewpointsTracker().getSmoothness();
			
//			//TODO: FIX HACK
//			smoothness = 0.01f;
			smoothness = spectrePitch;
			
			if (smoothness < 0.5f) {
//				electrization = 1.0f - 2 * smoothness;
				sparkleness = 1.0f - 2 * smoothness;
			}
//			fflyParams.setElectrization(electrization);
//			fflyCloudParams.setElectrization(electrization);
			fflyParams.setSparkleness(sparkleness);
			fflyCloudParams.setSparkleness(sparkleness);
			if (smoothness > 0.5f) {
				fflyParams.setAnxiety(5.0f - (smoothness - 0.5f) * 10);
			} else {
				fflyParams.setAnxiety(5.0f);
			}
			//TODO: FIX HACK
			fflyParams.setAnxiety(5.0f);
		}
	}
	
	// Getters and Setters //
	
	public FireflyParameters activeParameters() {
		if (cloudMode) {
			return fflyCloudParams;
		} else {
			return fflyParams;
		}
	}
	
	public float getCharSize() {
		return charSize;
	}
	
	public PVector getHeartPosition() {
		return heartPosition;
	}
	
	public boolean isCloudMode() {
		return cloudMode;
	}
	
	public Field getControlField() {
		if (cloudMode) {
			return cloudField;
		} else {
			return vaiBodyField;
		}
	}
	
	
	public float getHeartIgnition() {
		return heartIgnition;
	}
	
	public void setHeartIgnition(float ignition) {
		this.heartIgnition = ignition;
	}
	
	public float getCardio() {
		return cardio;
	}
	
	public void setCardioByPhase(float phase) {
		cardio = cardiogram(phase);
	}
	
	protected float cardiogram(float phase) {
		int periods = (int) (phase / (2 * Math.PI));
		float t = (float) (phase - periods * 2 * Math.PI - Math.PI);
		return (float) Math.exp(-Math.pow(t, 2)/TWO_CARDIO_DEV_SQR); //* sin(CARDIO_RHYTHM * t);
	}

	public int getHeartColor() {
		return heartColor;
	}
	
	public void setHeartColor(int color) {
		heartColor = color;
	}

}
