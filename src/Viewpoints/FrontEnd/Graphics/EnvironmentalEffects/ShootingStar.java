package Viewpoints.FrontEnd.Graphics.EnvironmentalEffects;

import java.util.Random;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;
import punktiert.math.Vec;
import punktiert.physics.BConstantForce;

public class ShootingStar
{
	private PApplet papplet;
	private float x;
	private float y;

	Star shootingStar;
	BConstantForce force;

	static PImage star1;
	static PImage star2;
	static PImage star3;
	static PImage star4;
	
	int choice;

	public ShootingStar(float x, float y, PApplet papplet)
	{
		this.papplet = papplet;
		star1 = papplet.loadImage("data/images/hebru_selected_collage/hebru7-right.png");
		star2 = papplet.loadImage("data/images/hebru_selected_collage/hebru6-right.png");
		star3 = papplet.loadImage("data/images/hebru_selected_collage/hebru7-left.png");
		star4 = papplet.loadImage("data/images/hebru_selected_collage/hebru6-left.png");
		this.x = x;
		this.y = y;

		Vec pos = new Vec(this.x, this.y, 7);
		shootingStar = new Star(pos, 5, (int) papplet.random(20) + 2, papplet.g);

		choice = (int) papplet.random(2);
		switch (choice)
		{
			case 0:
			{
				shootingStar.setStarImage(star1);
				break;
			}
			case 1:
			{
				shootingStar.setStarImage(star2);
				break;
			}
		}

		float fx = papplet.random(100) + 300;
		float fy = papplet.random(150) + 50;

		Random random = new Random(); //@Ju-Hwan: Please start using the Random class instead of Processing's random() method.
		
		boolean flipX = random.nextBoolean(), flipY = random.nextBoolean();
		
		if(flipX)
		{
			fx = -1 * fx;
//			System.out.println("Flip fx");
			
			if(choice == 0)
				shootingStar.setStarImage(star3);
			else
				shootingStar.setStarImage(star4);
		}
		
		if(flipY)
		{
//			fy = -1 * fy;
////			System.out.println("Flip fy");
		}

		force = new BConstantForce(new Vec(fx, fy, 0f).normalizeTo(1f));
		force.apply(shootingStar);

	}

	public void draw()
	{

		float currentR = shootingStar.getRadius();
		float newR = currentR - 0.05f;
		if (newR < 0)
		{
			return;
		}
		else
		{
			shootingStar.setRadius(newR);
			shootingStar.update();
			shootingStar.draw();
		}

	}
}
