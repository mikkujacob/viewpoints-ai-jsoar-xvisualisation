package Viewpoints.FrontEnd.Graphics.EnvironmentalEffects;

import processing.core.PGraphics;
import processing.core.PImage;
import punktiert.physics.VParticle;
import punktiert.math.Vec;

public class Star extends VParticle
{
	private PGraphics graphics;
	private PImage starImage; //MAKE SURE TO INITIALIZE THIS FROM THE EXTERNAL CONTEXT BEFORE USING THIS.
	private DrawMethod drawMethod = DrawMethod.STAR_IMAGE;
	private int starColor = (new PGraphics()).color(255);
	
	public static enum DrawMethod
	{
		STAR_IMAGE, //MAKE SURE TO INITIALIZE STARIMAGE FROM THE EXTERNAL CONTEXT BEFORE USING THIS.
		ELLIPSE,
//		SPHERE
	}
	
	public Star(PGraphics graphics)
	{
		super();
		this.graphics = graphics;
		this.starColor = graphics.color(255);
	}
	
	public Star(float x, float y, PGraphics graphics)
	{
		super(x, y);
		this.graphics = graphics;
		this.starColor = graphics.color(255);
	}
	
	public Star(float x, float y, float z, PGraphics graphics)
	{
		super(x, y, z);
		this.graphics = graphics;
		this.starColor = graphics.color(255);
	}
	
	public Star(float x, float y, float z, float w, PGraphics graphics)
	{
		super(x, y, z, w);
		this.graphics = graphics;
		this.starColor = graphics.color(255);
	}
	
	public Star(float x, float y, float z, float w, float r, PGraphics graphics)
	{
		super(x, y, z, w, r);
		this.graphics = graphics;
		this.starColor = graphics.color(255);
	}
	
	public Star(Vec v, PGraphics graphics)
	{
		super(v);
		this.graphics = graphics;
		this.starColor = graphics.color(255);
	}
	
	public Star(Vec v, float w, PGraphics graphics)
	{
		super(v, w);
		this.graphics = graphics;
		this.starColor = graphics.color(255);
	}
	
	public Star(Vec v, float w, float r, PGraphics graphics)
	{
		super(v, w, r);
		this.graphics = graphics;
		this.starColor = graphics.color(255);
	}
	
	public Star(Vec v, float w, float r, int color, PGraphics graphics)
	{
		super(v, w, r);
		this.graphics = graphics;
		this.starColor = color;
	}
	
	public Star(Vec pos, Vec vel, PGraphics graphics)
	{
		super(pos, vel);
		this.graphics = graphics;
		this.starColor = graphics.color(255);
	}
	
	public Star(Vec pos, Vec v, float w, float r, PGraphics graphics)
	{
		super(pos, v, w, r);
		this.graphics = graphics;
		this.starColor = graphics.color(255);
	}
	
	public Star(VParticle p, PGraphics graphics)
	{
		super(p);
		this.graphics = graphics;
		this.starColor = graphics.color(255);
	}
	
	public Star(VParticle p, int color, PGraphics graphics)
	{
		super(p);
		this.graphics = graphics;
		this.starColor = color;
	}
	
	public void draw()
	{
		int oldStroke = graphics.strokeColor;
		int oldFill = graphics.fillColor;
		
		graphics.noStroke();
		graphics.fill(255, 200);
		
		switch(drawMethod)
		{
			case STAR_IMAGE:
			{
				int oldTint = graphics.tintColor;
				int scaleFactor = 4;
				graphics.tint(starColor);
				graphics.image(starImage, x - scaleFactor * radius / 2f, y - scaleFactor * radius / 2f, radius * scaleFactor, radius * scaleFactor);
				graphics.tint(oldTint);
				break;
			}
//			case SPHERE:
//			{
//				papplet.pushMatrix();
//				papplet.translate(x, y, z);
//				
//				papplet.stroke(200, 200, 0, 200);
//				papplet.sphere(radius);
//				
//				papplet.translate(-1 * x, -1 * y, -1 * z);
//				papplet.popMatrix();
//				break;
//			}
			case ELLIPSE:
			default:
			{
				graphics.ellipse(x, y, radius * 2, radius * 2);
			}
		}
		
		graphics.fill(oldFill);
		graphics.stroke(oldStroke);
	}

	public PImage getStarImage()
	{
		return starImage;
	}

	public void setStarImage(PImage starImage)
	{
		this.starImage = starImage;
	}

	public DrawMethod getDrawMethod()
	{
		return drawMethod;
	}

	public void setDrawMethod(DrawMethod drawMethod)
	{
		this.drawMethod = drawMethod;
	}

	public void cycleDrawMethod()
	{
		int next = (this.drawMethod.ordinal() + 1) % DrawMethod.values().length;
		this.drawMethod = DrawMethod.values()[next];
	}
}
