package Viewpoints.FrontEnd.Graphics.EnvironmentalEffects;

import java.util.ArrayList;
import processing.core.PApplet;
import punktiert.math.Vec;
import punktiert.physics.VPhysics;

public class Constellation
{
	private PApplet papplet;
	private ArrayList<Pair<Star>> lines;
	private ArrayList<Star> stars;
	private float averageLineLength;
	private int originX;
	private int originY;
/*	
	public Constellation(PApplet papplet, Star... stars)
	{
		this.papplet = papplet;
		lines = new ArrayList<Pair<Star>>();
		this.stars = new ArrayList<Star>(Arrays.asList(stars));
		averageLineLength = 0f;
	}
	
	public Constellation(PApplet papplet, ArrayList<Star> stars)
	{
		this.papplet = papplet;
		lines = new ArrayList<Pair<Star>>();
		this.stars = new ArrayList<Star>(stars);
		averageLineLength = 0f;
	}
	*/
	public Constellation(PApplet papplet, int x, int y)
	{
		this.papplet = papplet;
		lines = new ArrayList<Pair<Star>>();
		stars = new ArrayList<Star>();
		averageLineLength = 0f;
		originX = x;
		originY = y;
	}
	
	public void draw()
	{
		int oldStroke = papplet.g.strokeColor;
		int oldFill = papplet.g.fillColor;
		
		for(Star star : stars)
		{

			papplet.tint(151, 141, 247, 126);
			star.draw();
			papplet.noTint();
			
//			if (star.x > papplet.width*2)
//			{
//				star.setPreviousPosition(new Vec(-papplet.width, star.y));
//				star.setX(-papplet.width);
//			}
//			if (star.y > papplet.height)
//			{
//				star.setPreviousPosition(new Vec(0f, star.y));
//				star.setY(0f);
//			}
		}
		
		papplet.stroke(255, 200);
		
		for(Pair<Star> line : lines)
		{
			//Check to make sure that lines aren't drawn when the constellation's stars wrap around the screen.
			float distance = distance(line.getFirst().x, line.getFirst().y, 0f, line.getSecond().x, line.getSecond().y, 0f);
			if(distance <= 3 * averageLineLength)
			{	
				papplet.stroke(255, 255, 255, 25);
				papplet.strokeWeight(2);
				float deltaX = (line.getSecond().x - line.getFirst().x)/10.0f;
				float deltaY = (line.getSecond().y - line.getFirst().y)/10.0f;
				papplet.line(line.getFirst().x + deltaX, line.getFirst().y + deltaY, line.getSecond().x - deltaX, line.getSecond().y - deltaY);
			}
		}
		
		papplet.fill(oldFill);
		papplet.stroke(oldStroke);
	}
	
	public void addLine(int first, int second)
	{
		Star firstStar = this.stars.get(first);
		Star secondStar = this.stars.get(second);
		lines.add(new Pair<Star>(firstStar, secondStar));
		float distance = distance(firstStar.x, firstStar.y, firstStar.z, secondStar.x, secondStar.y, secondStar.z);
		averageLineLength += (distance - averageLineLength) / lines.size(); //Incremental average formula
	}
	
	public void addStar(float x, float y, int mass, int radius, VPhysics physics){
		Vec pos = new Vec(originX + x, originY + y, 0);
		Star star = new Star(pos, mass, radius, this.papplet.g);
		this.stars.add(star);
		physics.addParticle(star);
	}
	
	private float distance(float x1, float y1, float z1, float x2, float y2, float z2)
	{
		return PApplet.sqrt(PApplet.sq(x1 - x2) + PApplet.sq(y1 - y2) + PApplet.sq(z1 - z2));
	}

	public ArrayList<Star> getStars()
	{
		return stars;
	}
}
