package Viewpoints.FrontEnd.Graphics.EnvironmentalEffects;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import Viewpoints.FrontEnd.Communication.Video.Syphon.SyphonServer;
//import Viewpoints.FrontEnd.Graphics.EnvironmentalEffects.Star.DrawMethod;
import processing.core.PApplet;
import processing.core.PImage;
import punktiert.math.Vec;
import punktiert.physics.BConstantForce;
import punktiert.physics.BWorldBox;
import punktiert.physics.BehaviorInterface;
import punktiert.physics.VPhysics;

/**
 * Processing sketch to display and control environmental effects like stars,
 * galaxies, comets, nebulae, etc.
 * 
 * @author mikhail.jacob
 *
 */
public class EnvironmentalEffects extends PApplet
{
	/**
	 * Generated serial version UID.
	 */
	private static final long serialVersionUID = 6528154714948826092L;
	
//	Syphon server to stream video to an external application
	private SyphonServer syphonServer;
	
//	Is video streaming using Syphon?
	private static final boolean isVideoStreaming = true;
	
//	Is Windows being used currently?
	private static boolean isWindows;

//	Physics object
	VPhysics physics;

//	Constant Force
	BConstantForce force;

//	Number of stars in the scene
	int amount = 1000;
	
//	What to normalize the force to
	float forceNormalization = .0075f;
	
//	Are the stars at top of the installation / screen?
	private boolean starsAtTop = false;
	
//	Are there two virtual stages of top and bottom?
	private boolean twoStagePresent = false;

//	Constellations
	ArrayList<Constellation> constellations;
	
//	Stars
	ArrayList<Star> stars;
	
//	Shooting stars
	ArrayList<ShootingStar> shootingStars;
	
//	Box to control teleportation, etc. of particles
	BWorldBox box;
	
//	Star images.
	PImage star1;
	PImage star2;
	PImage star3;
	PImage star4;
	
	PImage nebulousStars;
	PImage particleStars;

	PImage hebru1;
	PImage hebru2;
	PImage hebru3;
	PImage hebru4;
	
	PImage starscape1;
	PImage starscape2;
//	PImage starscape3;
	
	//Scale factors
	float scaleX;
	float scaleY;
	float minRad;
	float maxRad;
	
	
	/**
	 * Main method to test and execute the sketch.
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
		PApplet.main(
				"Viewpoints.FrontEnd.Graphics.EnvironmentalEffects.EnvironmentalEffects",
				new String[] { "--full-screen", "--display=1" });
	}
	
//	@Override
//	public boolean sketchFullScreen() {
//		  return true;
//	}

	public void setup()
	{
		size(2 * displayWidth, displayHeight, P3D);
		
		scaleX = width/1920f/2f;
		scaleY = height/1080f;
		minRad = 2;
		maxRad = 15;
		
		box = new BWorldBox(0, 0, 0, 1.5f * width, height / 2, 0);
		box.setWrapSpace(true);
		
		System.out.println("width: " + width);
		System.out.println("height: " + height);
		
		if (System.getProperty("os.name").toLowerCase().indexOf("windows") > -1)  
		{
			isWindows = true;
		}
		else
		{
			isWindows = false;
		}
		
		if(isVideoStreaming)
		{
			if(!isWindows)
			{
				syphonServer = new SyphonServer(this, "EnvironmentalEffects");
			}
		}
		
		star1 = loadImage("data/images/star2.png");
		star2 = loadImage("data/images/star2.png");
		star3 = loadImage("data/images/star3.png");
		star4 = loadImage("data/images/star4.png");
		
		nebulousStars = loadImage("data/images/nebulous-starscape.png");
		particleStars = loadImage("data/images/particle-starscape.png");
		
		hebru1 = loadImage("data/images/hebru1.png");
		hebru2 = loadImage("data/images/hebru2.png");
		hebru3 = loadImage("data/images/hebru3.png");
		hebru4 = loadImage("data/images/hebru4.png");
		
		starscape1 = loadImage("data/images/starscape1-cropped.png");
		starscape2 = loadImage("data/images/starscape2-cropped.png");
//		starscape3 = loadImage("data/images/starscape3-cropped.png");
		
		backgroundXOrigin1 = 0;
		backgroundYOrigin1 = 0;
		backgroundXOrigin2 = width / 2;
		backgroundYOrigin2 = 0;
		
//		frameRate(60);
		noStroke();

		// set up physics
		physics = new VPhysics();
		physics.setfriction(.4f);
		physics.addBehavior(box);
		
		//force = new BConstantForce(new Vec(width * 0.5f, 0f, 0f).normalizeTo(.005f));
		force = new BConstantForce(new Vec(width * 0.5f, 0f, 0f).normalizeTo(forceNormalization));
		physics.addBehavior(force);
		
		stars = new ArrayList<Star>();
		constellations = new ArrayList<Constellation>();
		shootingStars = new ArrayList<ShootingStar>();
		

		// Weighted list for random variable with probability. (used for y
		// position of particles)
		int tenth = height / 20;
		int[] weightedList = { height / 2 - tenth * 2, tenth * 2,
				height / 2 - tenth * 3, height / 2 - tenth * 3, height / 2 - tenth * 3,
				height / 2 - tenth * 4, height / 2 - tenth * 4, height / 2 - tenth * 4,
				height / 2 - tenth * 4, height / 2 - tenth * 5, height / 2 - tenth * 5,
				height / 2 - tenth * 5, height / 2 - tenth * 5, height / 2 - tenth * 5,
				height / 2 - tenth * 6, height / 2 - tenth * 6, height / 2 - tenth * 6,
				height / 2 - tenth * 6, height / 2 - tenth * 6, height / 2 - tenth * 6,
				height / 2 - tenth * 7, height / 2 - tenth * 7, height / 2 - tenth * 7,
				height / 2 - tenth * 7, height / 2 - tenth * 7, height / 2 - tenth * 7,
				height / 2 - tenth * 7, height / 2 - tenth * 8, height / 2 - tenth * 8,
				height / 2 - tenth * 8, height / 2 - tenth * 8, height / 2 - tenth * 8,
				height / 2 - tenth * 8, height / 2 - tenth * 8, height / 2 - tenth * 8,
				height / 2 - tenth * 9, height / 2 - tenth * 9, height / 2 - tenth * 9,
				height / 2 - tenth * 9, height / 2 - tenth * 9, height / 2 - tenth * 9,
				height / 2 - tenth * 9, height / 2 - tenth * 9, height / 2 - tenth * 9,
				height / 2 - tenth * 10, height / 2 - tenth * 10, height / 2 - tenth * 10,
				height / 2 - tenth * 10, height / 2 - tenth * 10, height / 2 - tenth * 10,
				height / 2 - tenth * 10, height / 2 - tenth * 10, height / 2 - tenth * 10,
				height / 2 - tenth * 10 };

		Random random = new Random();
		
		// Random particles
		for (int i = 0; i < amount; i++)
		{
			// val for arbitrary radius
			float rad = random(minRad, maxRad);
			int radIndex = (int) (random(weightedList.length));
			int radY = weightedList[radIndex];
			// vector for position
//			Vec pos = new Vec(random(-width+rad, 2*width - rad), random(radY, radY + tenth), -1 * random(rad, height / 2 - rad));
			Vec pos = new Vec(random(rad, 1.5f*width - rad), random(radY, radY + tenth), 0);
			
			int color = color(255);
			if(rad > 0.8f * maxRad)
			{
				color = color(random.nextInt(256), random.nextInt(256), random.nextInt(256));
			}
			
			// create particle (Vec pos, mass, radius)
			Star star = new Star(pos, 5, rad, color, this.g);
//			star.addBehavior(force);
			int choice = (int)random(4);
			switch(choice)
			{
				case 0:
				{
					star.setStarImage(star1);
					break;
				}
				case 1:
				{
					star.setStarImage(star2);
					break;
				}
				case 2:
				{
					star.setStarImage(star3);
					break;
				}
				case 3:
				{
					star.setStarImage(star4);
					break;
				}
			}
			// add particle to physics engine
			physics.addParticle(star);
			stars.add(star);
		}
		
		// Constellation particles
		//Gemini
		float constellationWidth = map(width*1/15, -1*width, 2 * width, 0, 1.5f * width);
		Constellation gemini = new Constellation(this, (int)constellationWidth, height / 2 * 1/15);
		gemini.addStar(0*scaleX, 0*scaleY, 5, 6, physics);//Pollux
		gemini.addStar(10*scaleX, 20*scaleY, 5, 4, physics);
		gemini.addStar(-20*scaleX, 30*scaleY, 5, 4, physics);
		gemini.addStar(20*scaleX, 80*scaleY, 5, 8, physics);
		gemini.addStar(2*scaleX, 115*scaleY, 5, 6, physics);//Left thigh
		gemini.addStar(43*scaleX, 175*scaleY, 5, 4, physics);
		gemini.addStar(45*scaleX, 105*scaleY, 5, 4, physics);//Right thigh
		gemini.addStar(70*scaleX, 155*scaleY, 5, 5, physics);
		gemini.addStar(40*scaleX, 20*scaleY, 5, 4, physics);//Holding hands
		gemini.addStar(85*scaleX, 10*scaleY, 5, 4, physics);
		gemini.addStar(40*scaleX, -20*scaleY, 5, 6, physics);//Castor
		gemini.addStar(130*scaleX, -5*scaleY, 5, 6, physics);
		gemini.addStar(110*scaleX, 80*scaleY, 5, 6, physics);
		gemini.addStar(115*scaleX, 130*scaleY, 5, 4, physics);//Left foot
		gemini.addStar(140*scaleX, 120*scaleY, 5, 5, physics);
		gemini.addStar(158*scaleX, 125*scaleY, 5, 4, physics);
		gemini.addStar(175*scaleX, 125*scaleY, 5, 4, physics);
		
		
		
		gemini.addLine(0, 1);
		gemini.addLine(1, 2);
		gemini.addLine(1, 3);
		gemini.addLine(3, 4);
		gemini.addLine(4, 5);
		gemini.addLine(3, 6);
		gemini.addLine(6, 7);
		gemini.addLine(1, 8);
		gemini.addLine(8, 9);
		gemini.addLine(9, 10);
		gemini.addLine(9, 11);
		gemini.addLine(9, 12);
		gemini.addLine(12, 13);
		gemini.addLine(12, 14);
		gemini.addLine(14, 15);
		gemini.addLine(15, 16);
		
		for(Star star : gemini.getStars())
		{
//			star.addBehavior(force);
			int choice = (int)random(4);
			switch(choice)
			{
				case 0:
				{
					star.setStarImage(star1);
					break;
				}
				case 1:
				{
					star.setStarImage(star2);
					break;
				}
				case 2:
				{
					star.setStarImage(star3);
					break;
				}
				case 3:
				{
					star.setStarImage(star4);
					break;
				}
			}
		}
		
		constellations.add(gemini);
		
		//Leo
		constellationWidth = map(width*6/10, -1*width, 2 * width, 0, 1.5f * width);
		Constellation leo = new Constellation(this, (int)constellationWidth, height / 2*8/10);
		leo.addStar(0*scaleX, 0*scaleY, 5, 6, physics);
		leo.addStar(130*scaleX, -10*scaleY, 5, 3, physics);
		leo.addStar(120*scaleX, -100*scaleY, 5, 4, physics);
		leo.addStar(360*scaleX, -5*scaleY, 5, 8, physics);
		leo.addStar(350*scaleX, -65*scaleY, 5, 5, physics);
		leo.addStar(310*scaleX, -110*scaleY, 5, 6, physics);
		leo.addStar(305*scaleX, -160*scaleY, 5, 5, physics);
		leo.addStar(360*scaleX, -200*scaleY, 5, 3, physics);
		leo.addStar(380*scaleX, -180*scaleY, 5, 5, physics);
		
		leo.addLine(0, 1);
		leo.addLine(0, 2);
		leo.addLine(1, 3);
		leo.addLine(3, 4);
		leo.addLine(4, 5);
		leo.addLine(2, 5);
		leo.addLine(5, 6);
		leo.addLine(6, 7);
		leo.addLine(7, 8);
		
		for(Star star : leo.getStars())
		{
//			star.addBehavior(force);
			int choice = (int)random(4);
			switch(choice)
			{
				case 0:
				{
					star.setStarImage(star1);
					break;
				}
				case 1:
				{
					star.setStarImage(star2);
					break;
				}
				case 2:
				{
					star.setStarImage(star3);
					break;
				}
				case 3:
				{
					star.setStarImage(star4);
					break;
				}
			}
		}
		
		constellations.add(leo);
		
		//Libra
		constellationWidth = map(width*5/10, -1*width, 2 * width, 0, 1.5f * width);
		Constellation libra = new Constellation(this, (int)constellationWidth, height / 2*3/10);
		libra.addStar(0*scaleX, 0*scaleY, 5, 5, physics);
		libra.addStar(20*scaleX, -20*scaleY, 5, 5, physics);
		libra.addStar(-10*scaleX, -80*scaleY, 5, 5, physics);
		libra.addStar(45*scaleX, -130*scaleY, 5, 5, physics);
		libra.addStar(100*scaleX, -80*scaleY, 5, 5, physics);
		libra.addStar(105*scaleX, -15*scaleY, 5, 5, physics);
		libra.addStar(125*scaleX, 0*scaleY, 5, 5, physics);
		
		libra.addLine(0, 1);
		libra.addLine(1, 2);
		libra.addLine(2, 3);
		libra.addLine(2, 4);
		libra.addLine(3, 4);
		libra.addLine(4, 5);
		libra.addLine(5, 6);
		
		constellations.add(libra);
		
		for(Star star : libra.getStars())
		{
//			star.addBehavior(force);
			int choice = (int)random(4);
			switch(choice)
			{
				case 0:
				{
					star.setStarImage(star1);
					break;
				}
				case 1:
				{
					star.setStarImage(star2);
					break;
				}
				case 2:
				{
					star.setStarImage(star3);
					break;
				}
				case 3:
				{
					star.setStarImage(star4);
					break;
				}
			}
		}
		
		//Scorpio
		constellationWidth = map(width*5/20, -1*width, 2 * width, 0, 1.5f * width);
		Constellation scorpio = new Constellation(this, (int)constellationWidth, height / 2*12/20);
		scorpio.addStar(0*scaleX, 0*scaleY, 5, 5, physics);
		scorpio.addStar(40*scaleX, -10*scaleY, 5, 6, physics);
		scorpio.addStar(46*scaleX, 0*scaleY, 5, 5, physics);
		scorpio.addStar(20*scaleX, 24*scaleY, 5, 5, physics);
		scorpio.addStar(10*scaleX, 36*scaleY, 5, 5, physics);
		scorpio.addStar(40*scaleX, 70*scaleY, 5, 7, physics);
		scorpio.addStar(90*scaleX, 70*scaleY, 5, 5, physics);
		scorpio.addStar(120*scaleX, 60*scaleY, 5, 4, physics);
		scorpio.addStar(130*scaleX, 5*scaleY, 5, 5, physics);
		scorpio.addStar(135*scaleX, -20*scaleY, 5, 6, physics);
		scorpio.addStar(160*scaleX, -80*scaleY, 5, 5, physics);
		scorpio.addStar(170*scaleX, -90*scaleY, 5, 8, physics);
		scorpio.addStar(180*scaleX, -95*scaleY, 5, 5, physics);
		scorpio.addStar(210*scaleX, -115*scaleY, 5, 7, physics);
		scorpio.addStar(200*scaleX, -150*scaleY, 5, 7, physics);
		scorpio.addStar(180*scaleX, -160*scaleY, 5, 5, physics);
		scorpio.addStar(210*scaleX, -80*scaleY, 5, 5, physics);
		scorpio.addStar(212*scaleX, -60*scaleY, 5, 4, physics);
		
		scorpio.addLine(0, 1);
		scorpio.addLine(1, 2);
		scorpio.addLine(2, 3);
		scorpio.addLine(3, 4);
		scorpio.addLine(4, 5);
		scorpio.addLine(5, 6);
		scorpio.addLine(6, 7);
		scorpio.addLine(7, 8);
		scorpio.addLine(8, 9);
		scorpio.addLine(9, 10);
		scorpio.addLine(10, 11);
		scorpio.addLine(11,12);
		scorpio.addLine(12, 13);
		scorpio.addLine(13, 14);
		scorpio.addLine(14, 15);
		scorpio.addLine(13, 16);
		scorpio.addLine(16, 17);
		
		for(Star star : scorpio.getStars())
		{
//			star.addBehavior(force);
			int choice = (int)random(4);
			switch(choice)
			{
				case 0:
				{
					star.setStarImage(star1);
					break;
				}
				case 1:
				{
					star.setStarImage(star2);
					break;
				}
				case 2:
				{
					star.setStarImage(star3);
					break;
				}
				case 3:
				{
					star.setStarImage(star4);
					break;
				}
			}
		}
		
		constellations.add(scorpio);
		
		//Cancer
		constellationWidth = map(0-width*1/20, -1*width, 2 * width, 0, 1.5f * width);
		Constellation cancer = new Constellation(this, (int)constellationWidth, height / 2*10/20);
		cancer.addStar(0*scaleX, 0*scaleY, 5, 10, physics);
		cancer.addStar(90*scaleX, 150*scaleY, 5, 6, physics);
		cancer.addStar(120*scaleX, 230*scaleY, 5, 10, physics);
		cancer.addStar(130*scaleX, 380*scaleY, 5, 7, physics);
		cancer.addStar(370*scaleX, 340*scaleY, 5, 10, physics);
		
		cancer.addLine(0, 1);
		cancer.addLine(1, 2);
		cancer.addLine(2, 3);
		cancer.addLine(2, 4);
		
		for(Star star : cancer.getStars())
		{
//			star.addBehavior(force);
			int choice = (int)random(4);
			switch(choice)
			{
				case 0:
				{
					star.setStarImage(star1);
					break;
				}
				case 1:
				{
					star.setStarImage(star2);
					break;
				}
				case 2:
				{
					star.setStarImage(star3);
					break;
				}
				case 3:
				{
					star.setStarImage(star4);
					break;
				}
			}
		}
		
		constellations.add(cancer);
		
		//Aries
		constellationWidth = map(0 - width*5/20, -1*width, 2 * width, 0, 1.5f * width);
		Constellation aries = new Constellation(this, (int)constellationWidth, height / 2*3/20);
		aries.addStar(0*scaleX, 0*scaleY, 5, 5, physics);
		aries.addStar(100*scaleX, 30*scaleY, 5, 8, physics);
		aries.addStar(140*scaleX, 60*scaleY, 5, 6, physics);
		aries.addStar(150*scaleX, 90*scaleY, 5, 5, physics);
		
		aries.addLine(0, 1);
		aries.addLine(1, 2);
		aries.addLine(2, 3);
		
		for(Star star : aries.getStars())
		{
//			star.addBehavior(force);
			int choice = (int)random(4);
			switch(choice)
			{
				case 0:
				{
					star.setStarImage(star1);
					break;
				}
				case 1:
				{
					star.setStarImage(star2);
					break;
				}
				case 2:
				{
					star.setStarImage(star3);
					break;
				}
				case 3:
				{
					star.setStarImage(star4);
					break;
				}
			}
		}
		
		constellations.add(aries);
		
		//Sagittarius
		constellationWidth = map(0-width*9/20, -1*width, 2 * width, 0, 1.5f * width);
		Constellation sagi = new Constellation(this, (int)constellationWidth, height / 2*7/20);
		sagi.addStar(0*scaleX, 0*scaleY, 5, 6, physics);
		sagi.addStar(40*scaleX, 40*scaleY, 5, 8, physics);
		sagi.addStar(60*scaleX, 150*scaleY, 5, 6, physics);
		sagi.addStar(100*scaleX, 120*scaleY, 5, 5, physics);
		sagi.addStar(80*scaleX, 180*scaleY, 5, 8, physics);
		sagi.addStar(130*scaleX, 140*scaleY, 5, 7, physics);
		sagi.addStar(180*scaleX, 110*scaleY, 5, 7, physics);
		sagi.addStar(210*scaleX, 70*scaleY, 5, 5, physics);
		sagi.addStar(230*scaleX, 190*scaleY, 5, 6, physics);
		sagi.addStar(265*scaleX, 200*scaleY, 5, 6, physics);
		sagi.addStar(200*scaleX, 270*scaleY, 5, 8, physics);
		sagi.addStar(230*scaleX, 300*scaleY, 5, 5, physics);
		sagi.addStar(-25*scaleX, 85*scaleY, 5, 5, physics);
		sagi.addStar(-120*scaleX, 150*scaleY, 5, 5, physics);
		sagi.addStar(-110*scaleX, 270*scaleY, 5, 5, physics);
		sagi.addStar(-105*scaleX, 285*scaleY, 5, 5, physics);
		sagi.addStar(-90*scaleX, 390*scaleY, 5, 5, physics);
		sagi.addStar(0*scaleX, 380*scaleY, 5, 5, physics);
		sagi.addStar(10*scaleX, 410*scaleY, 5, 5, physics);
		
		sagi.addLine(0, 1);
		sagi.addLine(1, 2);
		sagi.addLine(2, 3);
		sagi.addLine(2, 4);
		sagi.addLine(3, 5);
		sagi.addLine(4, 5);
		sagi.addLine(5, 6);
		sagi.addLine(6, 7);
		sagi.addLine(6, 8);
		sagi.addLine(8, 9);
		sagi.addLine(8, 10);
		sagi.addLine(10, 11);
		sagi.addLine(2, 12);
		sagi.addLine(12, 13);
		sagi.addLine(13, 14);
		sagi.addLine(14, 15);
		sagi.addLine(15, 16);
		sagi.addLine(16, 17);
		sagi.addLine(16, 18);
		
		
		for(Star star : sagi.getStars())
		{
//			star.addBehavior(force);
			int choice = (int)random(4);
			switch(choice)
			{
				case 0:
				{
					star.setStarImage(star1);
					break;
				}
				case 1:
				{
					star.setStarImage(star2);
					break;
				}
				case 2:
				{
					star.setStarImage(star3);
					break;
				}
				case 3:
				{
					star.setStarImage(star4);
					break;
				}
			}
		}
		
		constellations.add(sagi);
		
		
		//Aquarius
		constellationWidth = map(0-width*16/20, -1*width, 2 * width, 0, 1.5f * width);
		Constellation aqua = new Constellation(this, (int)constellationWidth, height / 2* 6/20);
		aqua.addStar(0*scaleX, 0*scaleY, 5, 5, physics);
		aqua.addStar(40*scaleX, -80*scaleY, 5, 5, physics);
		aqua.addStar(35*scaleX, -140*scaleY, 5, 6, physics);
		aqua.addStar(-20*scaleX, -160*scaleY, 5, 8, physics);
		aqua.addStar(100*scaleX, -240*scaleY, 5, 6, physics);
		aqua.addStar(115*scaleX, -245*scaleY, 5, 6, physics);
		aqua.addStar(130*scaleX, -230*scaleY, 5, 6, physics);
		aqua.addStar(160*scaleX, -240*scaleY, 5, 8, physics);
		aqua.addStar(135*scaleX, -150*scaleY, 5, 7, physics);
		aqua.addStar(160*scaleX, -100*scaleY, 5, 6, physics);
		aqua.addStar(170*scaleX, -90*scaleY, 5, 6, physics);
		aqua.addStar(250*scaleX, -180*scaleY, 5, 8, physics);
		aqua.addStar(350*scaleX, -150*scaleY, 5, 8, physics);
		
		aqua.addLine(0, 1);
		aqua.addLine(1, 2);
		aqua.addLine(2, 3);
		aqua.addLine(3, 4);
		aqua.addLine(4, 5);
		aqua.addLine(5, 6);
		aqua.addLine(6, 7);
		aqua.addLine(7, 8);
		aqua.addLine(8, 9);
		aqua.addLine(9, 10);
		aqua.addLine(7, 11);
		aqua.addLine(11, 12);
		
		for(Star star : aqua.getStars())
		{
//			star.addBehavior(force);
			int choice = (int)random(4);
			switch(choice)
			{
				case 0:
				{
					star.setStarImage(star1);
					break;
				}
				case 1:
				{
					star.setStarImage(star2);
					break;
				}
				case 2:
				{
					star.setStarImage(star3);
					break;
				}
				case 3:
				{
					star.setStarImage(star4);
					break;
				}
			}
		}
		
		constellations.add(aqua);
		
		//Taurus
		constellationWidth = map(0-width*19/20, -1*width, 2 * width, 0, 1.5f * width);
		Constellation taurus = new Constellation(this, (int)constellationWidth, height / 2*14/20);
		taurus.addStar(0*scaleX, 0*scaleY, 5, 8, physics);
		taurus.addStar(100*scaleX, 60*scaleY, 5, 4, physics);
		taurus.addStar(110*scaleX, 70*scaleY, 5, 4, physics);
		taurus.addStar(120*scaleX, 85*scaleY, 5, 4, physics);
		taurus.addStar(105*scaleX, 86*scaleY, 5, 5, physics);
		taurus.addStar(90*scaleX, 82*scaleY, 5, 9, physics);
		taurus.addStar(-10*scaleX, 50*scaleY, 5, 6, physics);
		taurus.addStar(170*scaleX, 110*scaleY, 5, 5, physics);
		taurus.addStar(250*scaleX, 130*scaleY, 5, 5, physics);
		taurus.addStar(260*scaleX, 140*scaleY, 5, 5, physics);
		
		taurus.addLine(0, 1);
		taurus.addLine(1, 2);
		taurus.addLine(2, 3);
		taurus.addLine(3, 4);
		taurus.addLine(4, 5);
		taurus.addLine(5, 6);
		taurus.addLine(3, 7);
		taurus.addLine(7, 8);
		taurus.addLine(8, 9);
		
		for(Star star : taurus.getStars())
		{
//			star.addBehavior(force);
			int choice = (int)random(4);
			switch(choice)
			{
				case 0:
				{
					star.setStarImage(star1);
					break;
				}
				case 1:
				{
					star.setStarImage(star2);
					break;
				}
				case 2:
				{
					star.setStarImage(star3);
					break;
				}
				case 3:
				{
					star.setStarImage(star4);
					break;
				}
			}
		}
		
		constellations.add(taurus);
		
		
		//Capricorn
		constellationWidth = map(width-width*3/20, -1*width, 2 * width, 0, 1.5f * width);
		Constellation capri = new Constellation(this, (int)constellationWidth, height / 2*8/20);
		capri.addStar(0*scaleX, 0*scaleY, 5, 10, physics);
		capri.addStar(80*scaleX, 10*scaleY, 5, 7, physics);
		capri.addStar(160*scaleX, 30*scaleY, 5, 7, physics);
		capri.addStar(240*scaleX, 20*scaleY, 5, 7, physics);
		capri.addStar(320*scaleX, 18*scaleY, 5, 10, physics);
		capri.addStar(320*scaleX, 0*scaleY, 5, 7, physics);
		capri.addStar(350*scaleX, -230*scaleY, 5, 5, physics);
		capri.addStar(353*scaleX, -247*scaleY, 5, 10, physics);
		capri.addStar(170*scaleX, -110*scaleY, 5, 5, physics);
		capri.addStar(100*scaleX, -80*scaleY, 5, 8, physics);
		capri.addStar(20*scaleX, -20*scaleY, 5, 5, physics);
		
		capri.addLine(0, 1);
		capri.addLine(1, 2);
		capri.addLine(2, 3);
		capri.addLine(3, 4);
		capri.addLine(4, 5);
		capri.addLine(5, 6);
		capri.addLine(6, 7);
		capri.addLine(6, 8);
		capri.addLine(8,9);
		capri.addLine(9,10);
		capri.addLine(10, 0);
		
		for(Star star : capri.getStars())
		{
//			star.addBehavior(force);
			int choice = (int)random(4);
			switch(choice)
			{
				case 0:
				{
					star.setStarImage(star1);
					break;
				}
				case 1:
				{
					star.setStarImage(star2);
					break;
				}
				case 2:
				{
					star.setStarImage(star3);
					break;
				}
				case 3:
				{
					star.setStarImage(star4);
					break;
				}
			}
		}
		
		constellations.add(capri);
		
		//Virgo
		constellationWidth = map(width + width*4/20, -1*width, 2 * width, 0, 1.5f * width);
		Constellation virgo = new Constellation(this, (int)constellationWidth, height / 2*6/20);
		virgo.addStar(0*scaleX, 0*scaleY, 5, 12, physics);
		virgo.addStar(150*scaleX, -60*scaleY, 5, 5, physics);
		virgo.addStar(230*scaleX, -65*scaleY, 5, 5, physics);
		virgo.addStar(320*scaleX, -180*scaleY, 5, 10, physics);
		virgo.addStar(230*scaleX, -270*scaleY, 5, 8, physics);
		virgo.addStar(400*scaleX, -150*scaleY, 5, 8, physics);
		virgo.addStar(470*scaleX, -180*scaleY, 5, 5, physics);
		virgo.addStar(540*scaleX, -280*scaleY, 5, 5, physics);
		virgo.addStar(340*scaleX, -40*scaleY, 5, 6, physics);
		virgo.addStar(330*scaleX, 70*scaleY, 5, 10, physics);
		virgo.addStar(200*scaleX, 120*scaleY, 5, 7, physics);
		virgo.addStar(180*scaleX, 100*scaleY, 5, 7, physics);
		virgo.addStar(80*scaleX, 140*scaleY, 5, 10, physics);
		
		virgo.addLine(0, 1);
		virgo.addLine(1, 2);
		virgo.addLine(2, 3);
		virgo.addLine(3, 4);
		virgo.addLine(3,5);
		virgo.addLine(5, 6);
		virgo.addLine(6, 7);
		virgo.addLine(5, 8);
		virgo.addLine(8, 9);
		virgo.addLine(2,9);
		virgo.addLine(9, 10);
		virgo.addLine(10, 11);
		virgo.addLine(11, 12);
		
		
		for(Star star : virgo.getStars())
		{
//			star.addBehavior(force);
			int choice = (int)random(4);
			switch(choice)
			{
				case 0:
				{
					star.setStarImage(star1);
					break;
				}
				case 1:
				{
					star.setStarImage(star2);
					break;
				}
				case 2:
				{
					star.setStarImage(star3);
					break;
				}
				case 3:
				{
					star.setStarImage(star4);
					break;
				}
			}
		}
		
		constellations.add(virgo);
		
		//Pisces
		constellationWidth = map(width+width*12/20, -1*width, 2 * width, 0, 1.5f * width);
		Constellation pis = new Constellation(this, (int)constellationWidth, height / 2*15/20);
		pis.addStar(0*scaleX, 0*scaleY, 5, 10, physics);
		pis.addStar(80*scaleX, -20*scaleY, 5, 5, physics);
		pis.addStar(120*scaleX, -30*scaleY, 5, 5, physics);
		pis.addStar(220*scaleX, -50*scaleY, 5, 5, physics);
		pis.addStar(270*scaleX, -45*scaleY, 5, 7, physics);
		pis.addStar(400*scaleX, -40*scaleY, 5, 7, physics);
		pis.addStar(450*scaleX, -30*scaleY, 5, 5, physics);
		pis.addStar(445*scaleX, 5*scaleY, 5, 5, physics);
		pis.addStar(465*scaleX, 30*scaleY, 5, 5, physics);
		pis.addStar(520*scaleX, 35*scaleY, 5, 5, physics);
		pis.addStar(550*scaleX, 0*scaleY, 5, 5, physics);
		pis.addStar(530*scaleX, -30*scaleY, 5, 5, physics);
		pis.addStar(510*scaleX, -45*scaleY, 5, 5, physics);
		pis.addStar(50*scaleX, -100*scaleY, 5, 8, physics);
		pis.addStar(110*scaleX, -190*scaleY, 5, 6, physics);
		pis.addStar(220*scaleX,  -290*scaleY, 5, 5, physics);
		pis.addStar(160*scaleX, -370*scaleY, 5, 8, physics);
		pis.addStar(195*scaleX, -420*scaleY, 5, 8, physics);

		
		pis.addLine(0, 1);
		pis.addLine(1, 2);
		pis.addLine(2, 3);
		pis.addLine(3, 4);
		pis.addLine(4, 5);
		pis.addLine(5, 6);//
		pis.addLine(6, 7);
		pis.addLine(7, 8);
		pis.addLine(8, 9);
		pis.addLine(9, 10);
		pis.addLine(10, 11);
		pis.addLine(11, 12);
		pis.addLine(12, 6);
		pis.addLine(0, 13);
		pis.addLine(13, 14);
		pis.addLine(14,15);
		pis.addLine(15, 16);
		pis.addLine(16, 17);
		
		for(Star star : pis.getStars())
		{
//			star.addBehavior(force);
			int choice = (int)random(4);
			switch(choice)
			{
				case 0:
				{
					star.setStarImage(star1);
					break;
				}
				case 1:
				{
					star.setStarImage(star2);
					break;
				}
				case 2:
				{
					star.setStarImage(star3);
					break;
				}
				case 3:
				{
					star.setStarImage(star4);
					break;
				}
			}
		}
		
		constellations.add(pis);
		
		/*
		Constellation orion = new Constellation(this, width*1/20, height / 2*1/20);
		orion.addStar(0, 0, 5, 3, physics);
		orion.addStar(30, -5, 5, 3, physics);
		orion.addStar(10, 50, 5, 3, physics);
		orion.addStar(20, 46, 5, 3, physics);
		orion.addStar(30, 100, 5, 3, physics);
		orion.addStar(38, 115, 5, 8, physics);//Right Shoulder
		orion.addStar(85, 95, 5, 3, physics);//Head
		orion.addStar(110, 120, 5, 6, physics);//Left shoulder
		orion.addStar(95, 180, 5, 4, physics);
		orion.addStar(130, 240, 5, 8, physics);//Left foot
		orion.addStar(55, 250, 5, 4, physics);
		orion.addStar(75, 190, 5, 4, physics);
		orion.addStar(85, 185, 5, 6, physics);//Belt
		orion.addStar(200, 110, 5, 4, physics);//Left arm
		orion.addStar(198, 95, 5, 4, physics);
		orion.addStar(185, 85, 5, 4, physics);
		orion.addStar(198, 125, 5, 4, physics);
		orion.addStar(195, 155, 5, 4, physics);
		orion.addStar(185, 165, 5, 4, physics);

		orion.addLine(0, 2);
		orion.addLine(1, 3);
		orion.addLine(2, 3);
		orion.addLine(2, 4);
		orion.addLine(4, 5);
		orion.addLine(5, 6);
		orion.addLine(6, 7);
		orion.addLine(7, 8);
		orion.addLine(8, 9);
		orion.addLine(9, 10);
		orion.addLine(10, 11);
		orion.addLine(11, 5);
		orion.addLine(7, 13);
		orion.addLine(13, 14);
		orion.addLine(14, 15);
		orion.addLine(13, 16);
		orion.addLine(16, 17);
		orion.addLine(17, 18);
		
		for(Star star : orion.getStars())
		{
//			star.addBehavior(force);
			int choice = (int)random(4);
			switch(choice)
			{
				case 0:
				{
					star.setStarImage(star1);
					break;
				}
				case 1:
				{
					star.setStarImage(star2);
					break;
				}
				case 2:
				{
					star.setStarImage(star3);
					break;
				}
				case 3:
				{
					star.setStarImage(star4);
					break;
				}
			}
		}
		
		constellations.add(orion);
		
		

		//Ursa major
		Constellation ursa = new Constellation(this, width*7/10, height / 2*4/10);
		ursa.addStar(0, 0, 5, 6, physics);
		ursa.addStar(80, 80, 5, 6, physics);
		ursa.addStar(105, 200, 5, 6, physics);
		ursa.addStar(120, 300, 5, 6, physics);
		ursa.addStar(55, 360, 5, 6, physics);
		ursa.addStar(160, 490, 5, 6, physics);
		ursa.addStar(260, 440, 5, 6, physics);
		
		ursa.addLine(0, 1);
		ursa.addLine(1, 2);
		ursa.addLine(2, 3);
		ursa.addLine(3, 4);
		ursa.addLine(4, 5);
		ursa.addLine(5, 6);
		ursa.addLine(6, 3);
		
		for(Star star : ursa.getStars())
		{
//			star.addBehavior(force);
			int choice = (int)random(4);
			switch(choice)
			{
				case 0:
				{
					star.setStarImage(star1);
					break;
				}
				case 1:
				{
					star.setStarImage(star2);
					break;
				}
				case 2:
				{
					star.setStarImage(star3);
					break;
				}
				case 3:
				{
					star.setStarImage(star4);
					break;
				}
			}
		}
		
		//Cassiopeia
		Constellation cassio = new Constellation(this, width*8/10, height / 2*2/10);
		cassio.addStar(0, 0, 5, 5, physics);
		cassio.addStar(40, -20, 5, 5, physics);
		cassio.addStar(60, -60, 5, 5, physics);
		cassio.addStar(100, -50, 5, 5, physics);
		cassio.addStar(90, -95, 5, 5, physics);
		
		cassio.addLine(0, 1);
		cassio.addLine(1, 2);
		cassio.addLine(2, 3);
		cassio.addLine(3, 4);
		
		for(Star star : cassio.getStars())
		{
//			star.addBehavior(force);
			int choice = (int)random(4);
			switch(choice)
			{
				case 0:
				{
					star.setStarImage(star1);
					break;
				}
				case 1:
				{
					star.setStarImage(star2);
					break;
				}
				case 2:
				{
					star.setStarImage(star3);
					break;
				}
				case 3:
				{
					star.setStarImage(star4);
					break;
				}
			}
		}
		
		constellations.add(cassio);
		*/
		background(0);
	}

	public void draw()
	{
//		if(!stars.isEmpty() && stars.get(0).getDrawMethod() == DrawMethod.SPHERE)
//		{
//			lights();
//		}
		
		noStroke();
		fill(0, 0, 0, 60f);
		rect(0, 0, width, height);
		
//		For physics update
		physics.update();
		
		modulateForce();
		
		drawBackdrop();

//		Draw stars
		drawStars();
		
//		Draw constellation
		drawConstellations();

//		Draw shooting stars
		drawShootingStars();
		
//		Stream video to external applications
		streamVideo();
	}
	
	public void streamVideo()
	{
		if(isVideoStreaming)
		{
			if(!isWindows)
			{
				if(syphonServer == null)
				{
					syphonServer = new SyphonServer(this, "VAIFrontEnd#1");
				}
				syphonServer.sendScreen();
			}
		}
	}
	
	int backgroundXOrigin1 = 0;
	int backgroundYOrigin1 = 0;
	int backgroundXOrigin2 = (int)(width / 2f);
	int backgroundYOrigin2 = 0;
	
	int speed = 1;
	int backdropUpdateRate = 10;
	public void drawBackdrop()
	{
//		image(nebulousStars, backgroundXOrigin1, backgroundYOrigin1, (width / 2f), height / 2);
//		image(nebulousStars, backgroundXOrigin2, backgroundYOrigin2, (width / 2f), height / 2);
		
//		image(particleStars, backgroundXOrigin1, backgroundYOrigin1, (width / 2f), height / 2);
//		image(particleStars, backgroundXOrigin2, backgroundYOrigin2, (width / 2f), height / 2);
		
//		image(hebru1, backgroundXOrigin1, backgroundYOrigin1, width, height / 2);
//		image(hebru1, backgroundXOrigin2, backgroundYOrigin2, width, height / 2);
		
//		image(hebru2, backgroundXOrigin1, backgroundYOrigin1, width, height / 2);
//		image(hebru2, backgroundXOrigin2, backgroundYOrigin2, width, height / 2);
//		
//		image(hebru3, backgroundXOrigin1, backgroundYOrigin1, width, height / 2);
//		image(hebru3, backgroundXOrigin2, backgroundYOrigin2, width, height / 2);
		
//		image(hebru4, backgroundXOrigin1, backgroundYOrigin1, width, height / 2);
//		image(hebru4, backgroundXOrigin2, backgroundYOrigin2, width, height / 2);
		
//		image(starscape1, backgroundXOrigin1, backgroundYOrigin1, width, height / 2);
//		image(starscape1, backgroundXOrigin2, backgroundYOrigin2, width, height / 2);
		
//		image(starscape2, backgroundXOrigin1, backgroundYOrigin1, width, height / 2);
//		image(starscape2, backgroundXOrigin2, backgroundYOrigin2, width, height / 2);
		
		if(frameCount % backdropUpdateRate == 0)
		{
			backgroundXOrigin1 = backgroundXOrigin1 - speed;
			backgroundXOrigin2 = backgroundXOrigin2 - speed;
			
			if(backgroundXOrigin1 < -1 * width / 2f)
			{
				backgroundXOrigin1 = 0;
			}
			
			if(backgroundXOrigin2 < 0)
			{
				backgroundXOrigin2 = (int)(width / 2f);
			}
		}
		
	}
	
	public void modulateForce()
	{
		for(BehaviorInterface behavior : physics.behaviors)
		{
			if(behavior instanceof BConstantForce)
			{
				((BConstantForce)behavior).setForce(new Vec(width * 0.5f, 0f, 0f).normalizeTo(forceNormalization + 0.05f));
			}
		}
//		force.setForce(new Vec(width * 0.5f, 0f, 0f).normalizeTo(forceNormalization + 0.05f));
		
	}
	
	public void drawStars()
	{
//		int index = 1;
		for(Star star : stars)
		{

			star.draw();
			
//			if (star.x > 2*width)
//			{
//				star.setPreviousPosition(new Vec(-width, star.y));
//				star.setX(-width);
//			}
//			if (star.y > height / 2)
//			{
//				star.setPreviousPosition(new Vec(0f, star.y));
//				star.setY(0f);
//			}
//			System.out.println("Star " + index++ + " Position : (X : " + star.x + ", Y : " + star.y + ")");
		}

	}
	
	public void drawConstellations()
	{
		for(Constellation constellation : constellations)
		{
		
			constellation.draw();
		}

	}
	
	public void drawGalaxies()
	{
		int oldStroke = g.strokeColor;
		int oldFill = g.fillColor;
		
		
		
		fill(oldFill);
		stroke(oldStroke);
	}
	
	public void drawComets()
	{
		int oldStroke = g.strokeColor;
		int oldFill = g.fillColor;
		
		
		
		fill(oldFill);
		stroke(oldStroke);
	}
	
	public void drawShootingStars()
	{
		int oldStroke = g.strokeColor;
		int oldFill = g.fillColor;
		
		float rand = (int)random(100);
		if (rand > 98.5){

			float randomX = random(width - 100);
			float randomY = random(height / 2 -100);
			ShootingStar sStar = new ShootingStar(randomX, randomY, this);
			shootingStars.add(sStar);

			
		}
		
//		Iterator<ShootingStar> i = shootingStars.iterator();
//		while(i.hasNext()){
//			i.next().draw();
//		}
		
		//@Ju-Hwan - You can use the iterating for loop for this
		for(ShootingStar shootingStar : shootingStars)
		{
			shootingStar.draw();
		}

		
		fill(oldFill);
		stroke(oldStroke);
		
	}
	
	public void keyPressed()
	{
		if(key == 's' || key == 'S')
		{
			for(Star star : stars)
			{
				star.cycleDrawMethod();
			}
		}
	}

	public boolean isStarsAtTop()
	{
		return starsAtTop;
	}

	public void setStarsAtTop(boolean starsAtTop)
	{
		this.starsAtTop = starsAtTop;
	}

	public boolean isTwoStagePresent()
	{
		return twoStagePresent;
	}

	public void setTwoStagePresent(boolean twoStagePresent)
	{
		this.twoStagePresent = twoStagePresent;
	}
}
