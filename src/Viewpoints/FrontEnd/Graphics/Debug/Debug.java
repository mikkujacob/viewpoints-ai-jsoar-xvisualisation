/*
 * Text-to-screen functions including addGUIText and addDebugText
 * 
 * @author Sasi
 */
package Viewpoints.FrontEnd.Graphics.Debug;

import java.util.ArrayList;

import Viewpoints.FrontEnd.Application.ViewpointsFrontEnd;
import Viewpoints.FrontEnd.Controls.ControlsManager;
import Viewpoints.FrontEnd.Input.MotionInput.FileMotionInput;
import processing.core.PApplet;

public class Debug {
	
	private PApplet papplet;
	private ControlsManager controlsManager;
	private ArrayList<String> listGUIText = new ArrayList<String>();
	private ArrayList<Long> listGUIDuration = new ArrayList<Long>();
	
	public Debug(ControlsManager controlsManager, PApplet papplet) {
		this.controlsManager = controlsManager;
		this.papplet = papplet;
	}
	
	public Debug(PApplet papplet, ControlsManager listener) {
		this.papplet = papplet;
		this.controlsManager = listener;
	}
	
	/*
	 * Displays a string message on the frontend screen during displayGUI() for a long duration in milliseconds.
	 */
	public void addGUIText(String message, long duration) {
		for(int index = 0; index < listGUIText.size(); index++) {
			String testMessage = listGUIText.get(index);
			if(message.split(":")[0].equalsIgnoreCase(testMessage.split(":")[0])) {
				listGUIText.remove(index);
				listGUIDuration.remove(index);
				break;
			}
		}
		
		this.listGUIText.add(message.toUpperCase());
		this.listGUIDuration.add(System.currentTimeMillis() + duration);
	}
	
	/*
	 * Displays tHe items in listGUIText for the duration specified in addGUIText()
	 */
	public void displayGUI() {
		if(!controlsManager.isDEBUG_TEXT())
		{
			return;
		}
		papplet.pushMatrix();
		int oldFill = papplet.g.fillColor;
		papplet.fill(255);
		papplet.textAlign(PApplet.LEFT, PApplet.TOP);
		papplet.textSize(40);
		long timeNowMillis = System.currentTimeMillis();
		
		float xPosPercent = 4f / 1000f * (float)papplet.displayWidth;
		float yPosPercent = 3f / 1000f * (float)papplet.displayHeight;
//		if (controlsManager.isDEBUG()) {
			String timeString = "Time: ";
			if (!controlsManager.isPlaying()) {
				timeString += ((FileMotionInput) ((ViewpointsFrontEnd)papplet).getMotionInput()).getTime();
				papplet.text(timeString, xPosPercent, yPosPercent, 1.7f);
			} 
//		}
		
		yPosPercent += 5f / 100f * (float)papplet.displayHeight;
		for(int index = 0; index < listGUIText.size(); index++) {
			String message = listGUIText.get(index);
			long displayTime = listGUIDuration.get(index);
			papplet.text(message, xPosPercent, yPosPercent, 1.7f);
			yPosPercent += 5f / 100f * (float)papplet.displayHeight;
			if(displayTime < timeNowMillis) {
				listGUIText.remove(index);
				listGUIDuration.remove(index);
			}
//			System.out.println(message);
		}
		papplet.fill(oldFill);
		papplet.popMatrix();
	}
	
}
