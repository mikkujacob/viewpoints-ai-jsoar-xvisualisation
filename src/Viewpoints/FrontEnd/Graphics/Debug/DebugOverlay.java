package Viewpoints.FrontEnd.Graphics.Debug;

import Viewpoints.FrontEnd.Application.ViewpointsFrontEnd;
import Viewpoints.FrontEnd.Controls.ControlsManager;
import Viewpoints.FrontEnd.Input.MotionInput.FileMotionInput;
import controlP5.ControlP5;
import controlP5.Slider;
import controlP5.Toggle;
import processing.core.PApplet;

/**
 * Class that represents the clickable Debug GUI onscreen that uses
 * ControlP5 toggles and slider
 * 
 * @author sasi
 *
 */

public class DebugOverlay {
//	private Debug debug;
	private PApplet papplet;
	private ControlsManager controlsManager;
	
	private ControlP5 cp5; 
	private Slider debugSlider; 
	
	public DebugOverlay(Debug debug, ControlsManager controlsManager, PApplet vai) {
		
//		this.debug = debug;
		this.papplet = vai;
		this.controlsManager = controlsManager;
		
		//GUI object initialization
		cp5 = new ControlP5(vai);
		
		//initialize toggles inside ControlP5 object based on flags inside ControlsManager
		for(int i = 0; i < controlsManager.getFlags().length; i++) {
			cp5.addToggle(controlsManager.getFlags()[i])
			.setPosition(vai.displayWidth - (vai.displayWidth / 20), (vai.displayHeight / (controlsManager.getFlags().length * 1.5f)) * (i + 1))
			.setSize(50,20)
			.setValue(controlsManager.getBooleanFlags().get(controlsManager.getFlags()[i]))
			.setState(controlsManager.getBooleanFlags().get(controlsManager.getFlags()[i]))
			.setMode(ControlP5.SWITCH)
			.setVisible(controlsManager.isDEBUG());
		}
		
		// add a debug slider if not in Kinect Mode and using FileMotionInput
		if (!controlsManager.isUSE_KINECT() && (FileMotionInput)((ViewpointsFrontEnd)vai).getMotionInput()!= null) {
			debugSlider = cp5.addSlider("sliderValue")
				.setPosition(30, vai.displayHeight*.8f)
				.setSliderMode(Slider.FLEXIBLE)
				.setSize(450, 20)
				.setRange(0, ((FileMotionInput)((ViewpointsFrontEnd)vai).getMotionInput()).getDuration());
			if (!controlsManager.isDEBUG()) {
				debugSlider.hide()
					.lock();
			}
		}
		
	}
	
	/**
	 * Method that draws the toggles when the DEBUG flag is on
	 */
	public void drawDebugGUI()
	{
		//Toggle GUI
		for(int i = 0; i < controlsManager.getFlags().length; i++) {
			cp5.getController(controlsManager.getFlags()[i])
			.setVisible(controlsManager.isDEBUG());
		}
		
		if(controlsManager.isDEBUG()) {
			papplet.cursor();
		}else
		{
			papplet.noCursor();
		}
	}
	
	/*
	 * Converts a boolean to a float
	 */
	private float booleanToFloat(boolean bool) {
		if(bool) {
			return 1;
		}
		return 0;
	}
	
//	/*
//	 * Converts a float to a boolean, where anything below nonnegative and nonzero is true
//	 */
//	private boolean floatToBoolean(float f) {
//		if(f > 0) {
//			return true;
//		}
//		return false;
//	}
	
	//function attempt to update flags. does not work.
	public void updateFlags() {
		for(int i = 0; i < controlsManager.getFlags().length; i++) {
			if(((Toggle)cp5.getController(controlsManager.getFlags()[i])).getValue() !=  booleanToFloat(controlsManager.getBooleanFlags().get(controlsManager.getFlags()[i]))) {
//				controlsManager.universalSet(controlsManager.getFlags()[i], floatToBoolean(((Toggle)cp5.getController(controlsManager.getFlags()[i])).getValue()));
			}
		}
	}

	public ControlP5 getCP5() {
		return cp5;
	}
	
}
