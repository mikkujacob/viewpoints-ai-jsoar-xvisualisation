package Viewpoints.FrontEnd.Graphics.Field;

import java.util.Random;

import processing.core.*;

public interface Field {
	public FieldSample sample(PVector pos);
	public PVector teleportEndpoint(Random random);
}
