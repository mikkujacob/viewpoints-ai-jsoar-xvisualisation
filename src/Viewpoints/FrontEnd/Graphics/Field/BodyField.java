package Viewpoints.FrontEnd.Graphics.Field;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import Viewpoints.FrontEnd.Shared.*;
import processing.core.PVector;

public class BodyField implements Field {
	private HashMap<LIDX, EllipticalField> subfields = new HashMap<LIDX, EllipticalField>();
	private ComplexField bodyField;
	
	public BodyField(Body iBody)
	{
		PVector bodyOrientation = iBody.orientation();
		
		for (Map.Entry<LIDX, Pair<JIDX, JIDX>> limb : LIDX.LIMB_ENDS().entrySet()) {
			PVector end1 = iBody.get(limb.getValue().first);
			PVector end2 = iBody.get(limb.getValue().second);
			PVector delta = PVector.sub(end2, end1);
			PVector ort = PVecUtilities.ortonormalization(bodyOrientation, delta);
			
			EllipticalField limbField = new EllipticalField(end1, end2, ort);
			subfields.put(limb.getKey(), limbField);
		}
		
		Collection<EllipticalField> fieldCollection = subfields.values();
		Field[] fields = new Field[fieldCollection.size()];
		int cnt = 0;
		
		for (EllipticalField field : fieldCollection) {
			fields[cnt++] = field;
		}
		
		bodyField = new ComplexField(fields);
		bodyField.setCenter(iBody.get(JIDX.TORSO));
	}
	
	public void update(Body iBody)
	{
		PVector bodyOrientation = iBody.orientation();
		
		for (Map.Entry<LIDX, Pair<JIDX, JIDX>> limb : LIDX.LIMB_ENDS().entrySet()) {
			PVector end1 = iBody.get(limb.getValue().first);
			PVector end2 = iBody.get(limb.getValue().second);
			PVector delta = PVector.sub(end2, end1);
			PVector ort = PVecUtilities.ortonormalization(bodyOrientation, delta);
			
			subfields.get(limb.getKey()).morph(end1, end2, ort);
			bodyField.setCenter(iBody.get(JIDX.TORSO));
		}

		bodyField.resetBalancing();
	}

	@Override
	public FieldSample sample(PVector pos) {
		return bodyField.sample(pos);
	}
	
	@Override
	public PVector teleportEndpoint(Random random) {
		return bodyField.teleportEndpoint(random);
	}
}
