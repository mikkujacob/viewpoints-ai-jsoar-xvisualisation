package Viewpoints.FrontEnd.Graphics.Field;

import processing.core.*;

public class FieldSample {
	  public PVector dir;
	  public PVector disturbance;
	  public float power;
	  
	  public FieldSample(PVector iDir, PVector iDisturbance, float iPower)
	  {
	    dir = iDir;
	    disturbance = iDisturbance;
	    power = iPower;
	  }
}
