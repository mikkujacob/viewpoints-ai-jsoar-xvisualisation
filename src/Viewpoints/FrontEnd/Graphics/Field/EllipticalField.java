package Viewpoints.FrontEnd.Graphics.Field;

import java.util.Random;

import Viewpoints.FrontEnd.Shared.PVecUtilities;
import processing.core.*;

public class EllipticalField implements Field {
	private PVector focus1;
	private PVector focus2;
	private PVector ort;
	private PVector oldFocus1;
	private PVector[] transition = null;

	private float len0;

	public EllipticalField(PVector iFocus1, PVector iFocus2, PVector iOrt) {
		focus1 = iFocus1;
		focus2 = iFocus2;
		ort = iOrt;
		oldFocus1 = iFocus1;
		len0 = PVector.sub(focus1, focus2).mag();
	}

	public FieldSample sample(PVector pos) {
		PVector d1 = PVector.sub(focus1, pos);
		float len1 = d1.mag();
		if (len1 > 0) {
			d1.mult(1.0f / len1);
		}
		PVector d2 = PVector.sub(focus2, pos);
		float len2 = d2.mag();
		if (len2 > 0) {
			d2.mult(1.0f / len2);
		}
		PVector bisect = PVector.add(d1, d2);
		bisect.normalize();
		float exp = 1.0f;
		if (len0 > 0.0f) {
			float off = (len1 + len2) / len0;
			exp = PApplet.pow(128, -PApplet.pow(off - 1.0f, 2));
		}
		return new FieldSample(bisect, computeDisturbance(pos), exp);
	}

	public void morph(PVector iFocus1, PVector iFocus2, PVector iOrt1) {
		transition = new PVector[3];
		PVector[] oldBasis = basis(focus1, focus2, ort);
		PVector[] newBasis = basis(iFocus1, iFocus2, iOrt1);
		
		if (PVecUtilities.nullbasis(oldBasis) || PVecUtilities.nullbasis(newBasis)) {
			transition = PVecUtilities.STANDARD_BASIS;
			return;
		}

		for (int i = 0; i < 3; i++) {
			float[] dissolution = PVecUtilities.dissolve(PVecUtilities.STANDARD_BASIS[i], oldBasis);
			transition[i] = PVecUtilities.assemble(newBasis, dissolution);
		}

		oldFocus1 = focus1;
		focus1 = iFocus1;
		focus2 = iFocus2;
		ort = iOrt1;
	}

	public void reset() {
		transition = null;
	}

	private PVector[] basis(PVector iFocus1, PVector iFocus2, PVector iOrt) {
		PVector[] theBasis = new PVector[3];
		theBasis[0] = PVector.sub(iFocus2, iFocus1);
		theBasis[1] = iOrt;
		theBasis[2] = theBasis[0].cross(theBasis[1]);
		theBasis[2].normalize();
		return theBasis;
	}

	private PVector computeDisturbance(PVector pos) {
		PVector basic = PVector.sub(pos, oldFocus1);
		
		if (null != transition) {
			float p[] = { basic.x, basic.y, basic.z };
			PVector loc = PVecUtilities.assemble(transition, p);
			loc.add(focus1);
			return PVector.sub(loc, pos);
		} else {
			return PVecUtilities.ZERO;
		}
	}

	@Override
	public PVector teleportEndpoint(Random random) {
		PVector delta = PVector.sub(focus2, focus1);
		delta.mult(random.nextFloat());
		return PVector.add(focus1, delta);
	}
}
