package Viewpoints.FrontEnd.Graphics.Field;

import java.util.Arrays;
import java.util.Random;

import Viewpoints.FrontEnd.Shared.PVecUtilities;
import processing.core.*;

public class ComplexField implements Field {
	private Field[] subfields;
	private float[] rebalanceScores;
	private float[] portProbabilities;
	private boolean portProbabilitiesFresh = false;
	private PVector center = PVecUtilities.ZERO;

	public ComplexField(Field[] iFields) {
		subfields = iFields;
		rebalanceScores = new float[subfields.length];
		
		for (int i = 0; i < subfields.length; i++) {
			rebalanceScores[i] = 1.0f;
		}
		
		portProbabilities = new float[subfields.length];
	}
	
	public void setCenter(PVector iCenter) {
		center = iCenter;
	}

	public FieldSample sample(PVector pos) {
		PVector dir = new PVector(0.0f, 0.0f, 0.0f);
		PVector disturbance = new PVector(0.0f, 0.0f, 0.0f);
		float resultingPower = 0.0f;
		float powerSum = 0.0f;

		for (int i = 0; i < subfields.length; i++) {
			FieldSample theFieldSample = subfields[i].sample(pos);
			float fieldPower = theFieldSample.power;
			rebalanceScores[i] += fieldPower;
			theFieldSample.dir.mult(fieldPower);
			theFieldSample.disturbance.mult(fieldPower);
			powerSum += fieldPower;
			dir.add(theFieldSample.dir);
			disturbance.add(theFieldSample.disturbance);

			if (resultingPower < fieldPower) {
				resultingPower = fieldPower;
			}
		}

		if (0.001f < powerSum) {
			disturbance.mult(1.0f / powerSum);
		} else {
			disturbance = PVecUtilities.ZERO;
		}
		
		if (1.0f < resultingPower) {
			resultingPower = 1.0f;
		}
		
		float mag = dir.mag();
		
		if (mag < 0.01f) {
			dir = PVector.sub(center, pos);
			dir.normalize();
		} else {
			dir.mult(1.0f/mag);
		}

		return new FieldSample(dir, disturbance, resultingPower);
	}
	
	public void resetBalancing() {
		for (int i = 0; i < subfields.length; i++) {
			rebalanceScores[i] = 1.0f;
		}
		
		portProbabilitiesFresh = false;
	}

	@Override
	public PVector teleportEndpoint(Random random) {
		if (!portProbabilitiesFresh) {
			establishPortProbabilities();
			portProbabilitiesFresh = true;
		}
		
		Field selected = null;
		float selector = random.nextFloat();
		
		int selection = Arrays.binarySearch(portProbabilities, selector);
		
		if (selection < 0) {
			selection = -(selection + 1);
		}
		
		selected = subfields[selection];
		
		return selected.teleportEndpoint(random);
	}
	
	private void establishPortProbabilities() {
		float norm = 0.0f;
		
		for (int i = 0; i < subfields.length; i++) {
			norm += 1.0f / rebalanceScores[i];
		}
		
		norm = 1.0f/norm;
		
		for (int i = 0; i < subfields.length; i++) {
			portProbabilities[i] = norm / rebalanceScores[i];
		}
		
		float cumulativeProbability = 0.0f;
		
		for (int i = 0; i < subfields.length; i++) {
			cumulativeProbability += portProbabilities[i];
			portProbabilities[i] = cumulativeProbability;
		}
		
		portProbabilities[portProbabilities.length - 1] = 1.0f;
	}
}
