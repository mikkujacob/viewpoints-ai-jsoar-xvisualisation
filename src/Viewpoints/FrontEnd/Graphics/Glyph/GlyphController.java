package Viewpoints.FrontEnd.Graphics.Glyph;

import java.io.File;

import Viewpoints.FrontEnd.Application.ViewpointsFrontEnd;
import Viewpoints.FrontEnd.Controls.ControlsManager;
import Viewpoints.FrontEnd.Graphics.Embodiment.SkeletalEmbodiment.GraphicsUtilities;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.JIDX;
import processing.core.PConstants;
import processing.core.PImage;
import processing.core.PVector;

/*

 * Class for methods related to rendering glyphs
 * @author Sasi
 */

public class GlyphController {
	ControlsManager controlsManager;
	ViewpointsFrontEnd vai;
	
	
	public GlyphController(ControlsManager controlsManager, ViewpointsFrontEnd vai) {
		this.vai = vai;
		this.controlsManager = controlsManager;
	}
	
	//Draws glyphs for both user and vai
	public void drawGlyph(Body userBody, Body vaiBody)
	{	
		if(controlsManager.isDRAW_USER_GLYPH() && controlsManager.isSHOW_USER_GLYPH()) {
			if(ViewpointsFrontEnd.userGlyph == null) {
				ViewpointsFrontEnd.userGlyph = vai.loadImage(vai.getGLYPH_LIB() + File.separator + ViewpointsFrontEnd.getGlyphFiles().get(ViewpointsFrontEnd.userGlyphIndex));
				ViewpointsFrontEnd.userGlyph = ViewpointsFrontEnd.userGlyph.get(4, 4, ViewpointsFrontEnd.userGlyph.width - 8, ViewpointsFrontEnd.userGlyph.height - 8);
				PImage mask = vai.loadImage(vai.getGLYPH_LIB() + File.separator + ViewpointsFrontEnd.getGlyphFiles().get(ViewpointsFrontEnd.userGlyphIndex));
				mask = mask.get(4, 4, mask.width - 8, mask.height - 8);
				ViewpointsFrontEnd.userGlyph.mask(mask);
				ViewpointsFrontEnd.userGlyph.filter(PConstants.DILATE);
				ViewpointsFrontEnd.userGlyph.filter(PConstants.DILATE);
				ViewpointsFrontEnd.userGlyph.filter(PConstants.BLUR, 2);
			}
			
			PVector userNeck = PVector.mult(PVector.add(userBody.get(JIDX.LEFT_SHOULDER), userBody.get(JIDX.RIGHT_SHOULDER)), 0.5f);
			
			PVector screenUserGlyphPos = GraphicsUtilities.getScreenPos(userNeck, vai.g);
			
			Boolean isScreenLeft = screenUserGlyphPos.x < 0;
			
			//determines what side to draw user glyph on relative to user based on what side of screen user is on
			if(isScreenLeft) {
				ViewpointsFrontEnd.userGlyphPos = new PVector(
						screenUserGlyphPos.x - ViewpointsFrontEnd.radius * (float)Math.cos(ViewpointsFrontEnd.angle) - ViewpointsFrontEnd.userGlyph.width / 2, 
						screenUserGlyphPos.y - ViewpointsFrontEnd.radius * (float)Math.sin(ViewpointsFrontEnd.angle) - ViewpointsFrontEnd.userGlyph.height, 
						screenUserGlyphPos.z);
			} else {
				ViewpointsFrontEnd.userGlyphPos = new PVector(
						screenUserGlyphPos.x + ViewpointsFrontEnd.radius * (float)Math.cos(ViewpointsFrontEnd.angle) - ViewpointsFrontEnd.userGlyph.width / 2, 
						screenUserGlyphPos.y - ViewpointsFrontEnd.radius * (float)Math.sin(ViewpointsFrontEnd.angle) - ViewpointsFrontEnd.userGlyph.height, 
						screenUserGlyphPos.z);
			}
			
			//display the image
			vai.image(ViewpointsFrontEnd.userGlyph, ViewpointsFrontEnd.userGlyphPos.x, ViewpointsFrontEnd.userGlyphPos.y);
			
			//stop displaying user glyph after 5 seconds
			if(vai.getUserGlyphTimer().iniTimeMillis() > 5000) {
				controlsManager.setSHOW_USER_GLYPH(false);
				ViewpointsFrontEnd.userGlyph = null;
//				System.out.println("NO LONGER SHOWING USER GLYPH");
			}
		}
		
		
		if(controlsManager.isDRAW_VAI_GLYPH() && controlsManager.isSHOW_VAI_GLYPH()) {
			if(ViewpointsFrontEnd.vaiGlyph == null) {
				ViewpointsFrontEnd.vaiGlyph = vai.loadImage(vai.getGLYPH_LIB() + File.separator + ViewpointsFrontEnd.getGlyphFiles().get(ViewpointsFrontEnd.vaiGlyphIndex));
				ViewpointsFrontEnd.vaiGlyph = ViewpointsFrontEnd.vaiGlyph.get(4, 4, ViewpointsFrontEnd.vaiGlyph.width - 8, ViewpointsFrontEnd.vaiGlyph.height - 8);
				PImage mask = vai.loadImage(vai.getGLYPH_LIB() + File.separator + ViewpointsFrontEnd.getGlyphFiles().get(ViewpointsFrontEnd.vaiGlyphIndex));
				mask = mask.get(4, 4, mask.width - 8, mask.height - 8);
				ViewpointsFrontEnd.vaiGlyph.mask(mask);
				ViewpointsFrontEnd.vaiGlyph.filter(PConstants.DILATE);
				ViewpointsFrontEnd.vaiGlyph.filter(PConstants.DILATE);
				ViewpointsFrontEnd.vaiGlyph.filter(PConstants.BLUR, 2);
			}
			
			PVector vaiNeck = PVector.mult(PVector.add(vaiBody.get(JIDX.LEFT_SHOULDER), vaiBody.get(JIDX.RIGHT_SHOULDER)), 0.5f);
			
			PVector screenVAIGlyphPos = GraphicsUtilities.getScreenPos(vaiNeck, vai.g);
			
			Boolean isScreenLeft = screenVAIGlyphPos.x < 0;
			
			//determines what side to draw vai glyph on relative to vai based on what side of screen vai is on
			if(isScreenLeft) {
				ViewpointsFrontEnd.vaiGlyphPos = new PVector(
						screenVAIGlyphPos.x - ViewpointsFrontEnd.radius * (float)Math.cos(ViewpointsFrontEnd.angle) - ViewpointsFrontEnd.vaiGlyph.width / 2, 
						screenVAIGlyphPos.y - ViewpointsFrontEnd.radius * (float)Math.sin(ViewpointsFrontEnd.angle) - ViewpointsFrontEnd.vaiGlyph.height, 
						screenVAIGlyphPos.z);
			} else {
				ViewpointsFrontEnd.vaiGlyphPos = new PVector(
						screenVAIGlyphPos.x + ViewpointsFrontEnd.radius * (float)Math.cos(ViewpointsFrontEnd.angle) - ViewpointsFrontEnd.vaiGlyph.width / 2, 
						screenVAIGlyphPos.y - ViewpointsFrontEnd.radius * (float)Math.sin(ViewpointsFrontEnd.angle) - ViewpointsFrontEnd.vaiGlyph.height, 
						screenVAIGlyphPos.z);
			}
			//TODO: Get the coloring to work again
//			vai.tint(colorTable.getColor(RhythmVAI.getEffectsController().activeParameters().SPECTRE_PITCH, RhythmVAI.fireflies[0].intensity, vai.g));
			vai.image(ViewpointsFrontEnd.vaiGlyph, ViewpointsFrontEnd.vaiGlyphPos.x, ViewpointsFrontEnd.vaiGlyphPos.y);
			vai.noTint();
			
			//stop displaying vai glyph after 5 seconds
			if(vai.getUserGlyphTimer().iniTimeMillis() > 5000) {
				controlsManager.setSHOW_VAI_GLYPH(false);
				ViewpointsFrontEnd.vaiGlyph = null;
//				System.out.println("NO LONGER SHOWING VAI GLYPH");
			}
		}
	}
	
}
