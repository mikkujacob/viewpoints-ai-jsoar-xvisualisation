package Viewpoints.FrontEnd.Graphics.Embodiment.StencilEmbodiment;

import java.util.ArrayList;
import java.util.Random;

import Viewpoints.FrontEnd.Graphics.EnvironmentalEffects.Star;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;
import punktiert.math.Vec;
import punktiert.physics.BCollision;
import punktiert.physics.BSwarm;
import punktiert.physics.BWander;
import punktiert.physics.BWorldBox;
import punktiert.physics.VBoid;
import punktiert.physics.VParticle;
import punktiert.physics.VPhysics;

public class StencilStars
{
	private PGraphics graphics;
	private VPhysics physics;
	private ArrayList<Star> stars;
	private PImage star1;
	private PImage star2;
	private PImage star3;
	private PImage star4;
	private int amount = 1500;
	private BWander wander;
	private BCollision collision;
	private BWorldBox box;
	private Random random;
	
	public StencilStars(PGraphics graphics)
	{
		this.graphics = graphics;
		
		star1 = (new PApplet()).loadImage("data/images/star2.png");
		star2 = (new PApplet()).loadImage("data/images/star2.png");
		star3 = (new PApplet()).loadImage("data/images/star3.png");
		star4 = (new PApplet()).loadImage("data/images/star4.png");
		
		box = new BWorldBox(0, 0, 0, graphics.width, graphics.height, 0);
		box.setWrapSpace(true);
//		box.setBounceSpace(true);
		
		physics = new VPhysics();
		
		physics.addBehavior(box);
		collision = new BCollision();
		wander = new BWander(20, 10, 20);
		
		random = new Random();
		
		stars = new ArrayList<Star>();
		Star star;
		for(int i = 0; i < amount; i++)
		{
			float radius = random.nextInt(30);
			float weight = random.nextInt(10);
			int tintColor = graphics.color(255);
			
			if(radius > 25)
			{
				tintColor = graphics.color(random.nextInt(256), random.nextInt(256), random.nextInt(256));
			}
//			star = new Star(new VParticle(new Vec(random.nextInt(this.graphics.width), random.nextInt(this.graphics.height), random.nextInt(20)), random.nextInt(20), random.nextInt(10)), tintColor, this.graphics);
			star = new Star(new VParticle(new Vec(random.nextInt(this.graphics.width), random.nextInt(this.graphics.height), 0), weight, radius), tintColor, this.graphics);
			
//			VBoid p = new VBoid(new Vec(random.nextInt(this.graphics.width), random.nextInt(this.graphics.height)));
//			float rad = random.nextInt(10);
//		    
//			p.swarm.setSeperationScale(rad * .7f);
//		    p.setRadius(rad);
		    
//		    star = new Star(p, graphics);
			int choice = random.nextInt(4);
			switch(choice)
			{
				case 0:
				{
					star.setStarImage(star1);
					break;
				}
				case 1:
				{
					star.setStarImage(star2);
					break;
				}
				case 2:
				{
					star.setStarImage(star3);
					break;
				}
				case 3:
				{
					star.setStarImage(star4);
					break;
				}
			}
//			star.addBehavior(collision);
			star.addBehavior(wander);
			physics.addParticle(star);
			stars.add(star);
		}
	}
	
	public void draw(boolean isNotStreaming)
	{
		if(isNotStreaming)
		{
			graphics.beginDraw();
		}
		
		graphics.background(graphics.color(0));
		
		physics.update();
		
		for(Star star : stars)
		{
			star.draw();
		}
		
		if(isNotStreaming)
		{
			graphics.endDraw();
		}
	}
}
