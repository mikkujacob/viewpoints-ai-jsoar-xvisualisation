package Viewpoints.FrontEnd.Graphics.Embodiment.StencilEmbodiment;

import Viewpoints.FrontEnd.Graphics.Embodiment.StencilEmbodiment.StencilEmbodiment.StencilDrawMethod;

public class StencilParameters
{
	public final float DEFAULT_FADE = 60.0f;
	public float FADE = DEFAULT_FADE;
	public int OUTLINE_WIDTH = 20;
	public double SHADOW_SCALE = 0.75;
	public int SHADOW_PIXEL = 2;
	public float MIN_FADE = 10f;
	public float MAX_FADE = 128;
	public StencilDrawMethod drawMethod = StencilDrawMethod.SHADOW;

	public void setFade(float exposure) {
		FADE = exposure;
		
		if (FADE < 0.0f) {
			FADE = 0.0f;
		} else if (FADE > 255.0f) {
			FADE = 255.0f;
		}
	}

	public void setOutlineWidth(int width) {
		OUTLINE_WIDTH = width;
		
		if (OUTLINE_WIDTH < 0) {
			OUTLINE_WIDTH = 0;
		} else if (OUTLINE_WIDTH > 255) {
			OUTLINE_WIDTH = 255;
		}
	}

	public void setShadowScale(double scale) {
		SHADOW_SCALE = scale;
		
		if (SHADOW_SCALE < 0.0) {
			SHADOW_SCALE = 0.0;
		} else if (SHADOW_SCALE > 255.0) {
			SHADOW_SCALE = 255.0;
		}
	}

	public void setShadowPixel(int pixel) {
		SHADOW_PIXEL = pixel;
		
		if (SHADOW_PIXEL < 0) {
			SHADOW_PIXEL = 0;
		} else if (SHADOW_PIXEL > 255) {
			SHADOW_PIXEL = 255;
		}
	}

	public float getFade()
	{
		return FADE;
	}

	public int getOutlineWidth()
	{
		return OUTLINE_WIDTH;
	}

	public double getShadowScale()
	{
		return SHADOW_SCALE;
	}

	public int getShadowPixel()
	{
		return SHADOW_PIXEL;
	}
}
