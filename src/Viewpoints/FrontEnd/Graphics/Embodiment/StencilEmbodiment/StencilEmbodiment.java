package Viewpoints.FrontEnd.Graphics.Embodiment.StencilEmbodiment;

import Viewpoints.FrontEnd.Communication.Video.Syphon.SyphonClient;
import Viewpoints.FrontEnd.Graphics.EffectsController.EffectsController;
import Viewpoints.FrontEnd.Graphics.EffectsController.EmbodimentEffects.StencilEffects;
import Viewpoints.FrontEnd.Graphics.Embodiment.Embodiment;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;

public class StencilEmbodiment extends Embodiment
{
	public static enum StencilDrawMethod
	{
		SHADOW,
		RGB,
		HEBRU_IMAGE,
		STARS_IMAGE,
		INPUT_IMAGE
	}
	
	private boolean isWindows;
	private boolean isVideoStreaming = false;
	
	private PImage starsImageFrame;
	private PGraphics starsGraphicsFrame;
	private PImage rgbImageFrame;
	private PGraphics rgbGraphicsFrame;
	private PImage hebruImageFrame;
	private PGraphics hebruGraphicsFrame;
	private PImage inputImageFrame;
	private PGraphics inputGraphicsFrame;
	private StencilStars stencilStars;
	private SyphonClient syphonClient;
	private PImage savedMaskFrame;

	public StencilEmbodiment(PGraphics graphics, EffectsController effectsController)
	{
		super(graphics, effectsController);
		
		starsImageFrame = (new PApplet()).createImage(graphics.width, graphics.height, PApplet.ARGB);
		rgbImageFrame = (new PApplet()).createImage(graphics.width, graphics.height, PApplet.ARGB);
		hebruImageFrame = (new PApplet()).loadImage("data/images/hebru-stencil/hebru.png");
		savedMaskFrame = (new PApplet()).createImage(graphics.width, graphics.height, PApplet.ARGB);
		starsGraphicsFrame = graphics;
		rgbGraphicsFrame = graphics;
		hebruGraphicsFrame = graphics;
		inputGraphicsFrame = graphics;
		
		hebruGraphicsFrame.beginDraw();
		hebruGraphicsFrame.clear();
		hebruGraphicsFrame.image(hebruImageFrame, 0, 0, hebruGraphicsFrame.width, hebruGraphicsFrame.height);
		hebruGraphicsFrame.endDraw();
		hebruImageFrame = hebruGraphicsFrame.get();
		
		if (System.getProperty("os.name").toLowerCase().indexOf("windows") > -1)  
		{
			isWindows = true;
		}
		else
		{
			isWindows = false;
		}
		
		if(isVideoStreaming && !isWindows)
		{
			syphonClient = new SyphonClient(this.graphics.parent, "StencilStarSketch");
			System.out.println("Syphon client loaded");
		}
		else
		{
			stencilStars = new StencilStars(graphics);
		}
		
		this.graphics.beginDraw();
		this.graphics.fill(0, 0, 0, 255);
		this.graphics.rect(0, 0, this.graphics.width, this.graphics.height);
		this.graphics.endDraw();
	}

	@Override
	public PImage draw()
	{
		clearGraphics();
		
		if(((StencilEffects)effectsController).activeParameters().drawMethod == StencilDrawMethod.RGB)
		{
			updateRGBImage();
			
			clearGraphics();
		}
		
		if(((StencilEffects)effectsController).activeParameters().drawMethod == StencilDrawMethod.STARS_IMAGE)
		{
			updateStars();
			
			clearGraphics();
		}
		
		if(((StencilEffects)effectsController).activeParameters().drawMethod == StencilDrawMethod.INPUT_IMAGE)
		{
			updateInputImage();
			
			clearGraphics();
		}

		if(((StencilEffects)effectsController).getUserPixels() == null || ((StencilEffects)effectsController).getUserPixels().getUserPixels() == null || ((StencilEffects)effectsController).getUserPixels().getUserPixels().length == 0)
		{
			return graphics.get();
		}
		
		graphics.beginDraw();
		
		graphics.image(savedMaskFrame, 0, 0, graphics.width, graphics.height);

		graphics.strokeWeight(((StencilEffects)effectsController).activeParameters().OUTLINE_WIDTH);
		graphics.stroke(255, 255, 255, 255.0f /(((StencilEffects)effectsController).activeParameters().OUTLINE_WIDTH/(((StencilEffects)effectsController).activeParameters().SHADOW_PIXEL/1.5f)));

		//draw user outline
		graphics.pushMatrix();
		graphics.translate((float)((1-((StencilEffects)effectsController).activeParameters().SHADOW_SCALE)*graphics.width)/2, (float)((1-((StencilEffects)effectsController).activeParameters().SHADOW_SCALE)*graphics.height)/2);
		
		int [] userMap = ((StencilEffects)effectsController).getUserPixels().getUserPixels();
		
		// this breaks our array down into rows  
		for(int y = 1; y < 480; y++)
		{
			// this breaks our array down into specific pixels in each row
			for(int x = 1; x < 640; x++)
			{  
				int i = x + y * 640;
				int curUser = userMap[i];
				
				//If there is any user at all, aka any non zero user pixel (or if nearest user pixel is non zero)
				if (((!((StencilEffects)effectsController).getControlsManager().isDrawNearestUserEmbodiment()) || (userMap[i] == ((StencilEffects)effectsController).getUserPixels().getUserNumber())) && (curUser != 0))
				{
					int scaledX = (int)(((double)x/640)*(graphics.width*((StencilEffects)effectsController).activeParameters().SHADOW_SCALE));
					int scaledY = (int)(((double)y/480)*(graphics.height*((StencilEffects)effectsController).activeParameters().SHADOW_SCALE));
					//Draw the point on stencil
					graphics.stroke(255, 255);
					graphics.point(scaledX, scaledY);
				}
			}
		}
		
//		graphics.loadPixels();
//		graphics.translate((float)((1-((StencilEffects)effectsController).activeParameters().SHADOW_SCALE)*graphics.width)/2, (float)((1-((StencilEffects)effectsController).activeParameters().SHADOW_SCALE)*graphics.height)/2);
//		for(int[] point : ((StencilEffects)effectsController).getUserMask())
//		{
//			int scaledX = (int)(((double)point[0]/640)*(graphics.width*((StencilEffects)effectsController).activeParameters().SHADOW_SCALE));
//			int scaledY = (int)(((double)point[1]/480)*(graphics.height*((StencilEffects)effectsController).activeParameters().SHADOW_SCALE));
//			
////			graphics.pixels[scaledX + scaledY * graphics.width] = graphics.color(255, 255);
//			
////			graphics.stroke(point[3], 255.0f /(((StencilEffects)effectsController).activeParameters().OUTLINE_WIDTH/(((StencilEffects)effectsController).activeParameters().SHADOW_PIXEL/1.5f)));
////			graphics.stroke(point[3], 255.0f);
//			graphics.stroke(255, 255);
//			graphics.point(scaledX, scaledY);
//		}
//		graphics.updatePixels();
		
		graphics.popMatrix();
		graphics.endDraw();
		
		savedMaskFrame = graphics.get();
		
		PImage userEmbodimentFrame = graphics.get();
//		userEmbodimentFrame.save("data/user" + System.currentTimeMillis() + ".png");
//		userEmbodimentFrame.mask(userEmbodimentFrame);
//		userEmbodimentFrame.save("data/userMasked" + System.currentTimeMillis() + ".png");
		
		return drawStencil(userEmbodimentFrame);
	}
	
	private void updateInputImage()
	{
		if(inputImageFrame == null)
		{
			return;
		}
		inputGraphicsFrame.beginDraw();
		inputGraphicsFrame.clear();
		inputGraphicsFrame.image(inputImageFrame, 0, 0, inputGraphicsFrame.width, inputGraphicsFrame.height);
		inputGraphicsFrame.endDraw();
		inputImageFrame = inputGraphicsFrame.get();
	}
	
	private void updateStars()
	{		
		if(isVideoStreaming && !isWindows)
		{
			if(syphonClient.available())
			{
				starsGraphicsFrame.beginDraw();
				starsGraphicsFrame.background(0);
				starsGraphicsFrame = syphonClient.getGraphics(starsGraphicsFrame);
				starsGraphicsFrame.image(starsGraphicsFrame, 0, 0, starsGraphicsFrame.width, starsGraphicsFrame.height);
				starsGraphicsFrame.endDraw();
				starsImageFrame = starsGraphicsFrame.get();
			}
		}
		else
		{
			stencilStars.draw(!(isVideoStreaming && !isWindows));
			starsImageFrame = graphics.get();
		}
	}
	
	private void updateRGBImage()
	{
		rgbImageFrame = ((StencilEffects)effectsController).getRgbImage();
		rgbGraphicsFrame.beginDraw();
		rgbGraphicsFrame.clear();
		if(rgbImageFrame != null)
		{
			rgbGraphicsFrame.image(rgbImageFrame, 0, 0, rgbGraphicsFrame.width, rgbGraphicsFrame.height);
		}
		rgbGraphicsFrame.endDraw();
		rgbImageFrame = rgbGraphicsFrame.get();
	}
	
	private void clearGraphics()
	{
		graphics.beginDraw();
		graphics.lights();
		if(effectsController.getControlsManager().isBlendedGraphics())
		{
			graphics.noStroke();
			graphics.clear();
		}
		graphics.endDraw();
	}
	
	private PImage drawStencil(PImage stencil)
	{
//		System.out.println("Stencil: " + stencil.width + " x " + stencil.height);
		switch(((StencilEffects)effectsController).activeParameters().drawMethod)
		{
			case SHADOW:
			{
				return stencil;
			}
			case RGB:
			{
				PImage rgbImageCloneFrame = (new PApplet()).createImage(rgbImageFrame.width, rgbImageFrame.height, rgbImageFrame.format);
				rgbImageCloneFrame.copy(rgbImageFrame, 0, 0, rgbImageFrame.width, rgbImageFrame.height, 0, 0, rgbImageCloneFrame.width, rgbImageCloneFrame.height);
				rgbImageCloneFrame.mask(stencil);
				return rgbImageCloneFrame;
			}
			case STARS_IMAGE:
			{
				PImage starsImageCloneFrame = (new PApplet()).createImage(starsImageFrame.width, starsImageFrame.height, starsImageFrame.format);
				starsImageCloneFrame.copy(starsImageFrame, 0, 0, starsImageFrame.width, starsImageFrame.height, 0, 0, starsImageCloneFrame.width, starsImageCloneFrame.height);
				starsImageCloneFrame.mask(stencil);
				return starsImageCloneFrame;
			}
			case HEBRU_IMAGE:
			{
				PImage hebruImageCloneFrame = (new PApplet()).createImage(hebruImageFrame.width, hebruImageFrame.height, hebruImageFrame.format);
				hebruImageCloneFrame.copy(hebruImageFrame, 0, 0, hebruImageFrame.width, hebruImageFrame.height, 0, 0, hebruImageCloneFrame.width, hebruImageCloneFrame.height);
				hebruImageCloneFrame.mask(stencil);
				return hebruImageCloneFrame;
			}
			case INPUT_IMAGE:
			{
				if(inputImageFrame == null)
				{
					return stencil;
				}
				PImage inputImageCloneFrame = (new PApplet()).createImage(inputImageFrame.width, inputImageFrame.height, inputImageFrame.format);
				inputImageCloneFrame.copy(inputImageFrame, 0, 0, inputImageFrame.width, inputImageFrame.height, 0, 0, inputImageCloneFrame.width, inputImageCloneFrame.height);
				inputImageCloneFrame.mask(stencil);
				return inputImageCloneFrame;
			}
			default:
			{
				return stencil;
			}
		}
	}

	@Override
	public void initialize()
	{
		graphics.beginDraw();
		graphics.fill(0, 0, 0, 255);
		graphics.rect(0, 0, graphics.width, graphics.height);
		graphics.endDraw();
	}

	@Override
	public EffectsController getEffectsController()
	{
		return this.effectsController;
	}

	public void setInputImageFrame(PImage inputImageFrame)
	{
		this.inputImageFrame = inputImageFrame;
	}

}
