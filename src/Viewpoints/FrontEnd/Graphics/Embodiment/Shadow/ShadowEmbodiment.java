/*

 * Class for methods related to rendering the user
 * @author Sasi
 */

package Viewpoints.FrontEnd.Graphics.Embodiment.Shadow;

import Viewpoints.FrontEnd.Graphics.EffectsController.EffectsController;
import Viewpoints.FrontEnd.Graphics.EffectsController.EmbodimentEffects.ShadowEffects;
import Viewpoints.FrontEnd.Graphics.Embodiment.Embodiment;
import processing.core.PGraphics;
import processing.core.PImage;

public class ShadowEmbodiment extends Embodiment {
	public ShadowEmbodiment(PGraphics graphics, EffectsController effectsController)
	{
		super(graphics, effectsController);
		
		graphics.beginDraw();
		graphics.fill(0, 0, 0, 255);
		graphics.rect(0, 0, graphics.width, graphics.height);
		graphics.endDraw();
	}

	// Draws the user's shadow outline
	@Override
	public PImage draw()
	{
		graphics.beginDraw();
		graphics.lights();
		if(effectsController.getControlsManager().isBlendedGraphics())
		{
			graphics.noStroke();
			graphics.fill(0, 0, 0, ((ShadowEffects)effectsController).activeParameters().FADE);
			graphics.rect(0, 0, graphics.width, graphics.height);
		}
		
		if(((ShadowEffects)effectsController).getUserPixels() == null || ((ShadowEffects)effectsController).getUserPixels().getUserPixels() == null || ((ShadowEffects)effectsController).getUserPixels().getUserPixels().length == 0)
		{
//			System.out.println("No Outline");
			graphics.endDraw();
			return graphics.get();
		}

		graphics.strokeWeight(((ShadowEffects)effectsController).activeParameters().OUTLINE_WIDTH);
		graphics.stroke(255, 255, 255, 255.0f /(((ShadowEffects)effectsController).activeParameters().OUTLINE_WIDTH/(((ShadowEffects)effectsController).activeParameters().SHADOW_PIXEL/1.5f)));

		//draw user outline
		graphics.pushMatrix();
		
		int [] userMap = ((ShadowEffects)effectsController).getUserPixels().getUserPixels();
		graphics.translate((float)((1-((ShadowEffects)effectsController).activeParameters().SHADOW_SCALE)*graphics.width)/2, (float)((1-((ShadowEffects)effectsController).activeParameters().SHADOW_SCALE)*graphics.height)/2);
//		System.out.println("UserMap: " + userMap.length);
		// this breaks our array down into rows  
		for(int y = 1; y < 480; y++)
		{
			// this breaks our array down into specific pixels in each row  
			int leftUser = userMap[y * 640];
			for(int x = 1; x < 640; x++)
			{
				int i = x + y * 640;
				int curUser = userMap[i];
				int upUser = userMap[x + (y-1) * 640];
				//This next line is true at a boundary between values, example user1 and user2 or no user and user 1, etc.
				//If it is a boundary then add to outline point array
				if (((!((ShadowEffects)effectsController).getControlsManager().isDrawNearestUserEmbodiment()) || (userMap[i] == ((ShadowEffects)effectsController).getUserPixels().getUserNumber() || upUser == ((ShadowEffects)effectsController).getUserPixels().getUserNumber() || leftUser == ((ShadowEffects)effectsController).getUserPixels().getUserNumber())) && (curUser != leftUser || curUser != upUser))
				{
					int scaledX = (int)(((double)x/640)*(graphics.width*((ShadowEffects)effectsController).activeParameters().SHADOW_SCALE));
					int scaledY = (int)(((double)y/480)*(graphics.height*((ShadowEffects)effectsController).activeParameters().SHADOW_SCALE));
					graphics.point(scaledX, scaledY);
//					System.out.println("Shadow point: " + scaledX + ", " + scaledY);
				}
				leftUser = curUser;
			}
		}
		
//		graphics.translate((float)((1-((ShadowEffects)effectsController).activeParameters().SHADOW_SCALE)*graphics.width)/2, (float)((1-((ShadowEffects)effectsController).activeParameters().SHADOW_SCALE)*graphics.height)/2);
//		for(int[] point : ((ShadowEffects)effectsController).getUserOutline()){
//			int scaledX = (int)(((double)point[0]/640)*(graphics.width*((ShadowEffects)effectsController).activeParameters().SHADOW_SCALE));
//			int scaledY = (int)(((double)point[1]/480)*(graphics.height*((ShadowEffects)effectsController).activeParameters().SHADOW_SCALE));
//			graphics.point(scaledX, scaledY);
//		}
		
		graphics.popMatrix();
		graphics.endDraw();
		
		PImage userEmbodimentFrame = graphics.get();
//		userEmbodimentFrame.save("data/user" + System.currentTimeMillis() + ".png");
		userEmbodimentFrame.mask(userEmbodimentFrame);
//		userEmbodimentFrame.save("data/userMasked" + System.currentTimeMillis() + ".png");
		
		return userEmbodimentFrame;
	}
	
	public void drawToBuffer()
	{
		
	}

	@Override
	public void initialize()
	{
		graphics.beginDraw();
		graphics.fill(0, 0, 0, 255);
		graphics.rect(0, 0, graphics.width, graphics.height);
		graphics.endDraw();
	}

	@Override
	public EffectsController getEffectsController()
	{
		return this.effectsController;
	}
}
