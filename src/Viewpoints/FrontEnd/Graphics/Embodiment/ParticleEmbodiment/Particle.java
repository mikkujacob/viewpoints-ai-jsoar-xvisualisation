package Viewpoints.FrontEnd.Graphics.Embodiment.ParticleEmbodiment;

import Viewpoints.FrontEnd.Graphics.EffectsController.EffectsController;
import processing.core.PGraphics;
import processing.core.PVector;
import processing.opengl.PGraphics3D;

/**
 * An object that represents one single particle that makes up a particle embodiment.
 * @author Lauren and Sasi
 *
 */
public abstract class Particle {
	protected PGraphics graphics;
	public PVector position;
	public PVector lastPosition;
	
	public Particle(PGraphics graphics) {
		this.graphics = graphics;
	}
	
	public abstract void draw(EffectsController effectsController);
	
	public abstract void update();
	
}
