package Viewpoints.FrontEnd.Graphics.Embodiment.ParticleEmbodiment;

import java.util.Random;

import processing.core.PGraphics;
import processing.core.PImage;
import processing.opengl.PGraphics3D;
import Viewpoints.FrontEnd.Graphics.EffectsController.EffectsController;
import Viewpoints.FrontEnd.Graphics.EffectsController.EmbodimentEffects.FireflyEffects;
import Viewpoints.FrontEnd.Graphics.Embodiment.Embodiment;

/**
 * Type of embodiment based off a particle system.
 * @author Lauren and Sasi
 *
 */
public abstract class ParticleEmbodiment extends Embodiment {
	
	public ParticleEmbodiment(PGraphics graphics,
			EffectsController effectsController) {
		super(graphics, effectsController);
		if(this.effectsController instanceof FireflyEffects)
		{
			particles = new Particle[((FireflyEffects)this.effectsController).activeParameters().fireflyCount];
		}
		else
		{
			particles = new Particle[1500];
		}
	}

	protected Random random = new Random();
	protected Particle[] particles;
	
	//Initializes particles to whatever particle may be being used.
	public abstract void initialize();
	
	public PImage draw() {
		for (int i = 0; i < particles.length; i++) {
			particles[i].update();
			particles[i].draw(effectsController);
		}
		return graphics.get();
	}
}
