package Viewpoints.FrontEnd.Graphics.Embodiment.ParticleEmbodiment.Fireflies;

import processing.core.PGraphics;
import processing.core.PImage;
import processing.core.PVector;
import Viewpoints.FrontEnd.Graphics.EffectsController.EffectsController;
import Viewpoints.FrontEnd.Graphics.EffectsController.EmbodimentEffects.FireflyEffects;
import Viewpoints.FrontEnd.Graphics.Embodiment.ParticleEmbodiment.ParticleEmbodiment;
import Viewpoints.FrontEnd.Shared.PVecUtilities;

public class FireflyEmbodiment extends ParticleEmbodiment {
	public FireflyEmbodiment(PGraphics graphics, FireflyEffects effectsController) {
		super(graphics, effectsController);
		graphics.beginDraw();
		graphics.fill(0, 0, 0, 255);
		graphics.rect(0, 0, graphics.width, graphics.height);
		graphics.endDraw();
	}

	@Override
	public void initialize() {
//		System.out.println("Init w. size of " + particles.length);
		for (int i = 0; i < particles.length; i++) {
			PVector position = PVector.add(FireflyEffects.CLOUD_CENTER, 
					PVector.mult(PVecUtilities.randomDirection(random), ((FireflyEffects) effectsController).getCharSize()));
			PVector heading = PVecUtilities.randomDirection(random);
			particles[i] = new Firefly(position, heading, random, graphics, this);
		}
	}
	
	@Override
	public PImage draw() {
		graphics.beginDraw();
		graphics.lights();
		if(effectsController.getControlsManager().isBlendedGraphics())
		{
			graphics.fill(0, 0, 0, ((FireflyEffects)effectsController).activeParameters().FADE);
			graphics.rect(0, 0, graphics.width, graphics.height);
		}
		graphics.translate(graphics.width/2f, graphics.height/2f);
		if (!((FireflyEffects)effectsController).isCloudMode()) {
			super.draw();
			for (int i = 0; i < particles.length / 40; i++) {
				int idx = (int)(particles.length*random.nextFloat());
				//TODO: (Lauren) Does "teleport" need to be an abstract particle method?
				((Firefly)particles[idx]).teleport(((FireflyEffects)effectsController).getControlField().teleportEndpoint(random));
			}
		} else {
			for (int i = 0; i < particles.length; i++) {
				particles[i].draw(effectsController);
			}
		}
		graphics.endDraw();
		
		PImage vaiEmbodimentFrame = graphics.get();
//		PImage mask = graphics.get();
//		vaiEmbodimentFrame.save("data/user" + System.currentTimeMillis() + ".png");
//		mask.filter(PGraphics.GRAY);
//		mask.filter(PGraphics.THRESHOLD, 0.3f);
//		mask.save("data/userMask" + System.currentTimeMillis() + ".png");
//		vaiEmbodimentFrame.mask(mask);
//		vaiEmbodimentFrame.mask(vaiEmbodimentFrame);
//		vaiEmbodimentFrame.filter(PGraphics.OPAQUE);
//		vaiEmbodimentFrame.save("data/userMasked" + System.currentTimeMillis() + ".png");
		
//		return graphics.get();
		return vaiEmbodimentFrame;
	}

	@Override
	public EffectsController getEffectsController() {
		// TODO Auto-generated method stub
		return this.effectsController;
	}
}
