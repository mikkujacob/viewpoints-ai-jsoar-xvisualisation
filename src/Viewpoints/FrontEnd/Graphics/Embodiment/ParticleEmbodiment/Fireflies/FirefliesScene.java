package Viewpoints.FrontEnd.Graphics.Embodiment.ParticleEmbodiment.Fireflies;

import java.util.Random;

import Viewpoints.FrontEnd.Controls.ControlsManager;
import Viewpoints.FrontEnd.Graphics.EffectsController.EmbodimentEffects.FireflyEffects;
import Viewpoints.FrontEnd.Graphics.Field.*;
import Viewpoints.FrontEnd.Shared.PVecUtilities;
import processing.core.*;
import polymonkey.time.*;

public class FirefliesScene extends PApplet
{
	/**
	 * Generated serial number
	 */
	private static final long serialVersionUID = -4479403047809897301L;
	Random random = new Random();
	Time time = new Time(this);
	private FireflyParameters fflyParams = new FireflyParameters(200.0f, 40.0f);
	private float incline1 = PI/4;
//	private float incline2 = PI/2;
	private EllipticalField field1 = new EllipticalField(new PVector(200.0f*sin(incline1), 
																	 200.0f*cos(incline1),
																	 0.0f), 
														 new PVector(-200.0f*sin(incline1), 
																     -200.0f*cos(incline1), 
																     0.0f), 
														 new PVector(cos(incline1), 
																     -sin(incline1), 
																     0.0f));
	//private EllipticalField field2 = new EllipticalField(new PVector(200.0f, 0.0f, 0.0f), new PVector(-200.0f, 0.0f, 0.0f), PVecUtilities.UPVEC);
	private Field[] fields = {field1};
	private ComplexField field = new ComplexField(fields);
	private Firefly[] fireflies;
	private static float INCLINE_SPEED = 3.5f;
	private static float MOVEMENT_SPEED = 100.0f;
	private static float OPACITY_FACTOR = 255.0f/1000.0f;
	private PVector eCenter = new PVector(0.0f, 0.0f, 0.0f);
	private ControlsManager controlsManager;

	public void setup() {
		size(700, 700);
		background(0, 0, 48);
		// noLoop();
		fireflies = new Firefly[1000];
		frameRate(30);
		fflyParams.setFlightSpeed(10.0f);
		controlsManager = new ControlsManager(this);
		for (int i = 0; i < fireflies.length; i++) {
			PVector position = PVector.mult(
					PVecUtilities.randomDirection(random),
					random(3.0f * fflyParams.TURN_RADIUS,
						   10.0f * fflyParams.TURN_RADIUS));
			PVector heading = PVecUtilities.randomDirection(random);
			fireflies[i] = new Firefly(position, heading, random, this.g, new FireflyEffects(controlsManager));
		}
	}

	public void draw() {
		fill(0, 0, 48, 64);
		rect(0, 0, width, height);
		translate(width / 2, height / 2);
		noStroke();
		float dt = Time.getDeltaTime();

		boolean moved1 = false;
		//boolean moved2 = false;

		if (keyPressed && CODED == key) {
			if (LEFT == keyCode) {
				incline1 += INCLINE_SPEED * dt;
				moved1 = true;
			} else if (RIGHT == keyCode) {
				incline1 -= INCLINE_SPEED * dt;
				moved1 = true;
			}
			
			if (UP == keyCode) {
				eCenter.y += MOVEMENT_SPEED * dt;
				moved1 = true;
			} else if (DOWN == keyCode) {
				eCenter.y -= MOVEMENT_SPEED * dt;
				moved1 = true;
			}

			/*if (UP == keyCode) {
				incline2 += INCLINE_SPEED * dt;
				moved2 = true;
			} else if (DOWN == keyCode) {
				incline2 -= INCLINE_SPEED * dt;
				moved2 = true;
			}*/
		}

		if (moved1) {
			// print(incline + "\n");
			PVector focus1 = new PVector(eCenter.x + 200.0f * sin(incline1),
					eCenter.y + 200.0f * cos(incline1), 0.0f);
			PVector focus2 = new PVector(eCenter.x -200.0f * sin(incline1),
					eCenter.y - 200.0f * cos(incline1), 0.0f);
			PVector ort = new PVector(cos(incline1), -sin(incline1), 0.0f);
			field1.morph(focus1, focus2, ort);
		} else {
			field1.reset();
		}

		/* if (moved2) {
			// print(incline + "\n");
			PVector focus1 = new PVector(200.0f * sin(incline2),
					200.0f * cos(incline2), 0.0f);
			PVector focus2 = new PVector(-200.0f * sin(incline2),
					-200.0f * cos(incline2), 0.0f);
			field2.morph(focus1, focus2, PVecUtilities.UPVEC);
		} else {
			field2.reset();
		} */

		for (int i = 0; i < fireflies.length; i++) {
			fireflies[i].fly(field, fflyParams, dt);
			drawFirefly(fireflies[i].position);
		}
	}

	void drawFirefly(PVector iPosition) {
		float d = 600.0f - iPosition.z;
		int opacity = 350 - (int) (OPACITY_FACTOR * d);
		float scale = 600.0f / d;
		fill(255, 201, 14, opacity);
		ellipse(scale * iPosition.x, scale * iPosition.y, scale * 15,
				scale * 15);
	}
}
