package Viewpoints.FrontEnd.Graphics.Embodiment.ParticleEmbodiment.Fireflies;

import Viewpoints.FrontEnd.Graphics.Embodiment.ParticleEmbodiment.Fireflies.Firefly.FireFlyDrawMethod;

public class FireflyParameters {
	public float LAG_FACTOR = 0.9f;
	public float ANXIETY = 5.0f;
	public float FLIGHT_SPEED = 0.0f;
	public float BASIC_SPEED = 0.0f;
	public float TURN_RADIUS = 0.0f;
	public float BASIC_RADIUS = 0.0f;
	public float ANGSPEED_FACTOR = 0.0f;
	public float PITCH_FACTOR = 0.0f;
	public float PITCH_CUTOFF = 0.0f;
	public float DEVIATION_FACTOR = 0.0f;
	public float DEVIATION_CUTOFF = 0.0f;
	public float INTENCITY_FLUCTUATION_CUTOFF = 0.05f;
	public float ELECTRIZATION = 0.4f;
	public float SPARKLENESS = 0.7f;
	public float DEFAULT_FADE = 180.0f;
	public float FADE = DEFAULT_FADE;
	public float SPECTRE_PITCH = 0.5f;
	public FireFlyDrawMethod drawMethod = FireFlyDrawMethod.VARIABLE_LINE;
	public int fireflyCount = 1500;
	public float scaleFactor = 0.5f;
	
	public FireflyParameters() {
		
	}
	
	public FireflyParameters(float flightSpeed, float turnRadius) {
		adjustToScale(flightSpeed, turnRadius);
	}
	
	public void adjustToScale(float flightSpeed, float turnRadius) {
		FLIGHT_SPEED = flightSpeed;
		BASIC_SPEED = flightSpeed;
		TURN_RADIUS = turnRadius;
		BASIC_RADIUS = turnRadius;
		ANGSPEED_FACTOR = FLIGHT_SPEED/TURN_RADIUS;
		PITCH_FACTOR = ANGSPEED_FACTOR * 5.0f;
		PITCH_CUTOFF = 3 * ANGSPEED_FACTOR;
		DEVIATION_FACTOR = ANGSPEED_FACTOR * 1.0f;
		DEVIATION_CUTOFF = ANGSPEED_FACTOR / 2;
	}
	
	public void setFlightSpeed(float flightSpd) {
		ANXIETY *= FLIGHT_SPEED/flightSpd;
		FLIGHT_SPEED = flightSpd;
		ANGSPEED_FACTOR = FLIGHT_SPEED/TURN_RADIUS;
	}
	
	public void setTurnRadius(float turnR) {
		ANXIETY *= TURN_RADIUS/turnR;
		TURN_RADIUS = turnR;
		ANGSPEED_FACTOR = FLIGHT_SPEED/TURN_RADIUS;
	}
	
	public void setAnxiety(float anxiety) {
		ANXIETY = anxiety;
	}
	
	public void setElectrization(float electrization) {
		ELECTRIZATION = electrization;
		
		if (ELECTRIZATION < 0.0f) {
			ELECTRIZATION = 0.0f;
		} else if (ELECTRIZATION > 1.0f) {
			ELECTRIZATION = 1.0f;
		}
	}
	
	public void setSparkleness(float sparkleness) {
		SPARKLENESS = sparkleness;
		
		if (SPARKLENESS < 0.0f) {
			SPARKLENESS = 0.0f;
		} else if (SPARKLENESS > 1.0f) {
			SPARKLENESS = 1.0f;
		}
	}
	
	public void setFade(float exposure) {
		FADE = exposure;
		
		if (FADE < 0.0f) {
			FADE = 0.0f;
		} else if (FADE > 255.0f) {
			FADE = 255.0f;
		}
	}
	
	public void setSpectrePitch(float spectrePitch) {
		SPECTRE_PITCH = spectrePitch;
		
		if (SPECTRE_PITCH < 0.0f) {
			SPECTRE_PITCH = 0.0f;
		} else if (SPECTRE_PITCH > 1.0f) {
			SPECTRE_PITCH = 1.0f;
		}
	}
	
	public void setLag(float lagFactor) {
		LAG_FACTOR = lagFactor;
	}
	
	/*public Object clone() {
		FireflyParameters clone = new FireflyParameters();
		clone.LAG_FACTOR = LAG_FACTOR;
		clone.ANXIETY = ANXIETY;
		clone.FLIGHT_SPEED = FLIGHT_SPEED;
		clone.BASIC_SPEED = BASIC_SPEED;
		clone.TURN_RADIUS = TURN_RADIUS;
		clone.BASIC_RADIUS = BASIC_RADIUS;
		clone.ANGSPEED_FACTOR = ANGSPEED_FACTOR;
		clone.PITCH_FACTOR = PITCH_FACTOR;
		clone.PITCH_CUTOFF = PITCH_CUTOFF;
		clone.DEVIATION_FACTOR = DEVIATION_FACTOR;
		clone.DEVIATION_CUTOFF = DEVIATION_CUTOFF;
		clone.INTENCITY_FLUCTUATION_CUTOFF = INTENCITY_FLUCTUATION_CUTOFF;
		clone.ELECTRIZATION = ELECTRIZATION;
		clone.SPARKLENESS = SPARKLENESS;
		clone.DEFAULT_FADE = DEFAULT_FADE;
		clone.FADE = FADE;
		clone.SPECTRE_PITCH = SPECTRE_PITCH;
		return clone;
	}
	
	public void driftTowards(FireflyParameters target, float deltaTime) {
		LAG_FACTOR = driftParameter(LAG_FACTOR, target.LAG_FACTOR, deltaTime);
		ANXIETY = driftParameter(ANXIETY, target.ANXIETY, deltaTime);
		FLIGHT_SPEED = driftParameter(FLIGHT_SPEED, target.FLIGHT_SPEED, deltaTime);
		BASIC_SPEED = driftParameter(BASIC_SPEED, target.BASIC_SPEED, deltaTime);
		TURN_RADIUS = driftParameter(TURN_RADIUS, target.TURN_RADIUS, deltaTime);
		BASIC_RADIUS = driftParameter(BASIC_RADIUS, target.BASIC_RADIUS, deltaTime);
		ANGSPEED_FACTOR = driftParameter(ANGSPEED_FACTOR, target.ANGSPEED_FACTOR, deltaTime);
		PITCH_FACTOR = driftParameter(PITCH_FACTOR, target.PITCH_FACTOR, deltaTime);
		PITCH_CUTOFF = driftParameter(PITCH_CUTOFF, target.PITCH_CUTOFF, deltaTime);
		DEVIATION_FACTOR = driftParameter(DEVIATION_FACTOR, target.DEVIATION_FACTOR, deltaTime);
		DEVIATION_CUTOFF = driftParameter(DEVIATION_CUTOFF, target.DEVIATION_CUTOFF, deltaTime);
		INTENCITY_FLUCTUATION_CUTOFF = driftParameter(INTENCITY_FLUCTUATION_CUTOFF, target.INTENCITY_FLUCTUATION_CUTOFF, deltaTime);
		ELECTRIZATION = driftParameter(ELECTRIZATION, target.ELECTRIZATION, deltaTime);
		SPARKLENESS = driftParameter(SPARKLENESS, target.SPARKLENESS, deltaTime);
		DEFAULT_FADE = driftParameter(DEFAULT_FADE, target.DEFAULT_FADE, deltaTime);
		FADE = driftParameter(FADE, target.FADE, deltaTime);
		SPECTRE_PITCH = driftParameter(SPECTRE_PITCH, target.SPECTRE_PITCH, deltaTime);
	}
	
	private float driftParameter(float parameter, float target, float deltaTime) {
		float d = target - parameter;
		d *= Math.pow(2.0, -deltaTime / DRIFT_CHARACTERISTIC_TIME);
		return target - d;
	}*/
}
