package Viewpoints.FrontEnd.Graphics.Embodiment.ParticleEmbodiment.Fireflies;

import java.util.Random;

//import Viewpoints.FrontEnd.Application.ViewpointsFrontEnd;
import Viewpoints.FrontEnd.Clock.ClockManager;
import Viewpoints.FrontEnd.Graphics.EffectsController.EffectsController;
import Viewpoints.FrontEnd.Graphics.EffectsController.EmbodimentEffects.FireflyEffects;
import Viewpoints.FrontEnd.Graphics.Embodiment.ParticleEmbodiment.Particle;
import Viewpoints.FrontEnd.Graphics.Embodiment.ParticleEmbodiment.ParticleEmbodiment;
import Viewpoints.FrontEnd.Graphics.Embodiment.ParticleEmbodiment.Fireflies.FireflyParameters;
import Viewpoints.FrontEnd.Graphics.Embodiment.SkeletalEmbodiment.GraphicsUtilities;
import Viewpoints.FrontEnd.Graphics.Field.Field;
import Viewpoints.FrontEnd.Graphics.Field.FieldSample;
import processing.core.*;
//import processing.opengl.PGraphics3D;

public class Firefly extends Particle {
	private final static float CUTOFF = 200.0f;
	private FireflyColorTable colorTable = FireflyColorTable.Standard(graphics);
	public PVector position;
	public PVector lastPosition;
	private PVector heading;
	private float pitchSpd = 0.0f;
	private float deviationSpd = 0.0f;
	private Random random;
	public float intensity = 0.0f;
	private PImage starImage;
	private PImage hebruImage;
	private static PImage starImage1 = (new PApplet()).loadImage("data/images/star1.png");
	private static PImage starImage2 = (new PApplet()).loadImage("data/images/star2.png");
	private static PImage starImage3 = (new PApplet()).loadImage("data/images/star3.png");
	private static PImage starImage4 = (new PApplet()).loadImage("data/images/star4.png");
	private static PImage hebruImage1 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru1.png");
	private static PImage hebruImage2 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru2.png");
	private static PImage hebruImage3 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru3.png");
	private static PImage hebruImage4 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru4.png");
	private static PImage hebruImage5 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru5.png");
	private static PImage hebruImage6 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru6.png");
	private static PImage hebruImage7 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru7.png");
	private static PImage hebruImage8 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru8.png");
	private static PImage hebruImage9 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru9.png");
	private static PImage hebruImage10 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru10.png");
	private static PImage hebruImage11 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru11.png");
	private static PImage hebruImage12 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru12.png");
	private static PImage hebruImage13 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru13.png");
	private static PImage hebruImage14 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru14.png");
	private static PImage hebruImage15 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru15.png");
	private static PImage hebruImage16 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru16.png");
	private static PImage hebruImage17 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru17.png");
	private static PImage hebruImage18 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru18.png");
	private static PImage hebruImage19 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru19.png");
	private static PImage hebruImage20 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru20.png");
	private static PImage hebruImage21 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru21.png");
	private static PImage hebruImage22 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru22.png");
	private static PImage hebruImage23 = (new PApplet()).loadImage("data/images/hebru-images/cropped/hebru23.png");
	

	public static enum FireFlyDrawMethod
	{
		VARIABLE_LINE,
		FIXED_ELLIPSE,
		VARIABLE_ELLIPSE,
//		FIXED_SPHERE,
		STAR_IMAGE,
		HEBRU_IMAGE2
	}
	
	////// Constructors //////
	
	public Firefly(PVector iniPosition, PVector iniHeading, Random random, PGraphics graphics, ParticleEmbodiment embodiment) {
		super(graphics);
		position = iniPosition;
		lastPosition = position;
		heading = iniHeading;
		this.random = random;
		int choice = random.nextInt(4);
		switch (choice)
		{
			case 0:
			{
				starImage = starImage1;
				break;
			}
			case 1:
			{
				starImage = starImage2;
				break;
			}
			case 2:
			{
				starImage = starImage3;
				break;
			}
			case 3:
			default:
			{
				starImage = starImage4;
				break;
			}
		}
		
		choice = random.nextInt(23);
		switch (choice)
		{
			case 0:
			{
				hebruImage = hebruImage1;
				break;
			}
			case 1:
			{
				hebruImage = hebruImage2;
				break;
			}
			case 2:
			{
				hebruImage = hebruImage3;
				break;
			}
			case 3:
			{
				hebruImage = hebruImage4;
				break;
			}
			case 4:
			{
				hebruImage = hebruImage5;
				break;
			}
			case 5:
			{
				hebruImage = hebruImage6;
				break;
			}
			case 6:
			{
				hebruImage = hebruImage7;
				break;
			}
			case 7:
			{
				hebruImage = hebruImage8;
				break;
			}
			case 8:
			{
				hebruImage = hebruImage9;
				break;
			}
			case 9:
			{
				hebruImage = hebruImage10;
				break;
			}
			case 10:
			{
				hebruImage = hebruImage11;
				break;
			}
			case 11:
			{
				hebruImage = hebruImage12;
				break;
			}
			case 12:
			{
				hebruImage = hebruImage13;
				break;
			}
			case 13:
			{
				hebruImage = hebruImage14;
				break;
			}
			case 14:
			{
				hebruImage = hebruImage15;
				break;
			}
			case 15:
			{
				hebruImage = hebruImage16;
				break;
			}
			case 16:
			{
				hebruImage = hebruImage17;
				break;
			}
			case 17:
			{
				hebruImage = hebruImage18;
				break;
			}
			case 18:
			{
				hebruImage = hebruImage19;
				break;
			}
			case 19:
			{
				hebruImage = hebruImage20;
				break;
			}
			case 20:
			{
				hebruImage = hebruImage21;
				break;
			}
			case 21:
			{
				hebruImage = hebruImage22;
				break;
			}
			case 22:
			{
				hebruImage = hebruImage23;
				break;
			}
			default:
			{
				hebruImage = hebruImage5;
			}
		}
	}
	
//	public Firefly(PVector iniPosition, PVector iniHeading, Random random) {
//		this(iniPosition, iniHeading, random, new PGraphics(), new FireflyEffects(null));
//	}


	public Firefly(PVector iniPosition, PVector iniHeading, Random random2, PGraphics graphics, FireflyEffects fireflyEffects) {
		super(graphics);
		position = iniPosition;
		lastPosition = position;
		heading = iniHeading;
	}

	
	////// Methods //////
	
	public void fly(Field controlField, FireflyParameters params, float deltaTime) {
		pitchSpd = deviate(pitchSpd, params.PITCH_FACTOR, params.PITCH_CUTOFF, deltaTime);
		deviationSpd = deviate(deviationSpd, params.DEVIATION_FACTOR,
				params.DEVIATION_CUTOFF, deltaTime);
		FieldSample sample = controlField.sample(position);
		updatePosition(sample, params, deltaTime);
		updateHeading(sample, params, deltaTime);
		updateIntensity(sample, params, deltaTime);
	}
	
	private void updateIntensity(FieldSample sample, FireflyParameters params, float deltaTime) {
		float disturbanceSpd = 0.0f;
		if (deltaTime > 0.0f) {
			disturbanceSpd = sample.disturbance.mag()/deltaTime;
		}
		float ignition = 0.05f * disturbanceSpd / params.TURN_RADIUS;
		
		if (ignition > 1.0f)
			ignition = 1.0f;
		
		ignition = PApplet.pow(ignition, 5);
		
		float nintensity = 0.9f*sample.power; //intensityFluctuation;
		nintensity = (1.0f - ignition)*nintensity + (1.0f + params.SPARKLENESS)*ignition;
		intensity = intensity + params.ELECTRIZATION*(nintensity - intensity);
	}

	private void updatePosition(FieldSample sample, FireflyParameters params, float deltaTime) {
		lastPosition = position;
		float speed = params.FLIGHT_SPEED * (1.0f + params.ANXIETY * (1.0f - sample.power));
//		float speed = params.FLIGHT_SPEED * (1.0f + params.ANXIETY * (1.0f - sample.power));
		position = PVector.add(position, PVector.mult(heading, speed * deltaTime));
		position.add(PVector.mult(sample.disturbance, params.LAG_FACTOR * sample.power));
	}

	private float deviate(float var, float factor, float cutoff, float deltaTime) {
		float randomFloat = random.nextFloat();
		var += 2 * factor * (randomFloat - 0.5) * deltaTime;

		if (var > cutoff)
			var = cutoff;

		if (var < -cutoff)
			var = -cutoff;

		return var;
	}

	private void updateHeading(FieldSample sample, FireflyParameters params, float deltaTime) {
		PVector dir = sample.dir;
		float turnImpetus = 1.0f - dir.dot(heading);
		float turn = ((1.0f + params.ANXIETY * (1.0f - sample.power)) * params.ANGSPEED_FACTOR * turnImpetus + sample.power * deviationSpd) * deltaTime;
		PVector axis = heading.cross(dir);
		PVector ort = axis.cross(heading);
		float pitch = sample.power * pitchSpd * deltaTime;
		heading.mult(1.0f - turn);
		ort.mult(turn);
		heading.add(ort);
		heading.mult(1.0f - pitch);
		axis.mult(pitch);
		heading.add(axis);
		heading.normalize();
	}
	
	public void teleport(PVector iPosition) {
		position = iPosition;
		lastPosition = iPosition;
	}
	
	public void printPosition() {
		System.out.println(position);
	}

	@Override
	public void draw(EffectsController effectsController) {
		this.fly(((FireflyEffects)effectsController).getControlField(), ((FireflyEffects)effectsController).activeParameters(), ClockManager.getMainClock().deltatime());
		if (position.z < CUTOFF)
			return;
		int color = colorTable.getColor(((FireflyEffects)effectsController).activeParameters().SPECTRE_PITCH, intensity, graphics);
		color = addHeartColor(this, color, effectsController);
//		color = gradient(this, color, effectsController);
		int oldStroke = graphics.strokeColor;
		graphics.stroke(color);
		PVector screenPos = GraphicsUtilities.getScreenPos(position, graphics);
		PVector lastScreenPos = GraphicsUtilities.getScreenPos(lastPosition, graphics);
		int sz = (int)(5.0f*screenPos.z + 2.0f*intensity);
		if (sz < 0)
			sz = 0;
		graphics.strokeWeight(sz);
		int oldFill = graphics.fillColor;
		graphics.fill(color);
		
		if(effectsController.getControlsManager().isDRAW_EMBODIMENT())
		{
			switch(((FireflyEffects)effectsController).activeParameters().drawMethod)
			{
//				//Fixed Sphere takes WAYYY too much processing power so recommend only between 50 - 150 spheres.
//				case FIXED_SPHERE:
//					graphics.translate(screenPos.x, screenPos.y, 0);
//					graphics.sphere(sz/8f);
//					graphics.translate(-1 * screenPos.x, -1 * screenPos.y, 0);
//					break;
				//Fixed Ellipse has ellipses of constant size sz for the current frame.
				case FIXED_ELLIPSE:
					graphics.ellipse(screenPos.x, screenPos.y, sz/8f, sz/8f);
					break;
				//Variable Ellipse has different sized ellipses based on delta between current and last screen pos.
				case VARIABLE_ELLIPSE:
					int oldEllipseMode = graphics.ellipseMode;
					graphics.ellipseMode(PApplet.CORNERS);
					graphics.ellipse(lastScreenPos.x, lastScreenPos.y, screenPos.x, screenPos.y);
					graphics.ellipseMode(oldEllipseMode);
					break;
				//Experimental image with stars instead of fireflies
				case STAR_IMAGE:
					float value = (sz);
					float scaled = scaleRange(value, 0.0f, 100f, 20f, 100f);
					graphics.tint(255);
					graphics.fill(255);
//					graphics.tint(color);
					graphics.image(starImage, screenPos.x, screenPos.y, scaled, scaled);
					break;
				//Experimental images as pieces of Hebru's art instead of fireflies
				case HEBRU_IMAGE2:
					float valueH = (sz);
					float scaledH = scaleRange(valueH, 0.0f, 100f, 20f, 100f);
					graphics.tint(255);
					graphics.fill(255);
//						graphics.tint(color);
					graphics.image(hebruImage, screenPos.x, screenPos.y, ((FireflyEffects)effectsController).activeParameters().scaleFactor * scaledH, ((FireflyEffects)effectsController).activeParameters().scaleFactor * scaledH);
					break;				
				//Variable Line is the standard / original particle drawing mode. Also fastest and cheapest with ~10000 particles if sz if reduced to look nice.
				case VARIABLE_LINE:
				default:
//					graphics.strokeWeight(sz/2f);
					graphics.line(lastScreenPos.x, lastScreenPos.y, screenPos.x, screenPos.y);
			}
		}
		
		graphics.fill(oldFill);
		graphics.stroke(oldStroke);
	}
	
	public float scaleRange(float oldValue, float oldMin, float oldMax, float newMin, float newMax)
	{
		return (((oldValue - oldMin) * (newMax - newMin)) / (oldMax - oldMin)) + newMin;
	}
	
	/*
	 * Changes the firefly’s color to a slightly different color based on where it is 
	 * and whether there is a heartbeat color effect ongoing due to the presence of a rhythm.
	 */
	private int addHeartColor(Firefly firefly, int originalColor, EffectsController effectsController) {
		if (((FireflyEffects)effectsController).isCloudMode() || ((FireflyEffects)effectsController).getHeartIgnition() < 0.1f) {
			return originalColor;
		} else {
			PVector delta = PVector.sub(((FireflyEffects)effectsController).getHeartPosition(), firefly.position);
			float heartSize = (0.03f + 0.08f * ((FireflyEffects)effectsController).getCardio()) * ((FireflyEffects)effectsController).getCharSize();
			float intensityModulator = ((FireflyEffects)effectsController).getHeartIgnition() * (0.7f + 0.3f * ((FireflyEffects)effectsController).getCardio()) * Math.min(1.0f, heartSize * heartSize / delta.magSq());
			return graphics.lerpColor(originalColor, ((FireflyEffects)effectsController).getHeartColor(), intensityModulator);
		}
	}
	
//	private float count = 0;
//	private int gradient(Firefly firefly, int originalColor, EffectsController effectsController) {
//
//	    count += 0.02;
//		return graphics.lerpColor(originalColor, 255, PApplet.sin(count));
//	}
//	
//	private int monochromeGradient(Firefly firefly, int originalColor, EffectsController effectsController) {
//	    count += 0.02;
//		return graphics.color(originalColor/2 + (originalColor/2)*PApplet.sin(count));
//	}
//	
//	private int accidentalPartyMode(Firefly firefly, int originalColor, EffectsController effectsController) {
//		count++;
//		return graphics.lerpColor(originalColor, 255, count);
//	}

	@Override
	public void update() {
	}
}
