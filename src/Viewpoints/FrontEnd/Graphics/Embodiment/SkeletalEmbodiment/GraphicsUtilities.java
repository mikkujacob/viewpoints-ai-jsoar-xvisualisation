package Viewpoints.FrontEnd.Graphics.Embodiment.SkeletalEmbodiment;


import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Clock.Clock;
import Viewpoints.FrontEnd.Clock.ClockManager;
import Viewpoints.FrontEnd.Controls.ControlsManager;
import Viewpoints.FrontEnd.Shared.JIDX;
import Viewpoints.FrontEnd.Shared.LIDX;
import Viewpoints.FrontEnd.Shared.MathUtilities;
import Viewpoints.FrontEnd.Shared.PVecUtilities;
import Viewpoints.FrontEnd.Shared.Pair;
import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PVector;

/**
 * Utility class that contains useful functions for rendering onscreen such as converting between
 * coordinate systems and drawing bodies
 * @author Sasi
 */

public class GraphicsUtilities {
	// Instance Variables
	static float prevEMA = -1;
	static float smoothingFactor = 1.0f;
	static int iterations = 0;
	static float frames = 10f; //change this as desired
	static boolean offsetting = false;
	static PVector offsetDir = null;
	static PVector previousPosition = null;
	static float screenFloat = 1.25f;
	static Clock emaClock = new Clock();
	static Clock negClock = new Clock();
	static Clock posClock = new Clock();
	static float xSign = 1; // represents sign of vai's x location
	static float updateInterval = 1500; // duration of time(ms) system should wait before moving vai when user hovers near center
	
	//Converts from kinect's position to screen coordinate position on the PApplet
	public static PVector getScreenPos(PVector pos, PGraphics graphics) {
		PVector screenPos = PVector.mult(pos, 0.3f); //new PVector();
		screenPos.x =  screenPos.x;
		screenPos.y =  -screenPos.y;
		screenPos.z = (graphics.height/900.0f) * 800.0f / screenPos.z;
		screenPos.x *= (screenPos.z);
		screenPos.y *= screenPos.z;
		return screenPos;
	}
	
	//Draws a skeletal outline of the given body inside the passed in PApplet window
	public static void drawBody(Body body, PGraphics graphics) 
	{
		graphics.stroke(255);
		graphics.strokeWeight(1);
		
		for (Pair<JIDX, JIDX> limbEnds : LIDX.LIMB_ENDS().values()) {
			PVector pos1 = GraphicsUtilities.getScreenPos(body.get(limbEnds.first), graphics);
			PVector pos2 = GraphicsUtilities.getScreenPos(body.get(limbEnds.second), graphics);
			graphics.line(pos1.x, pos1.y, pos2.x, pos2.y);
			
		}
	}
	
	public static void debugLines(Body userBody, PGraphics graphics, float vaiPosition, float newPosition, float personalSpace) {
		
		//graph lines
		for(int i = 0; i < graphics.width / 2; i++) {
			graphics.line(i, graphics.height, i, graphics.height - 100);
		}
		
		//personal space between user and vai (red)
		graphics.stroke(255, 0, 0);
		graphics.strokeWeight(5);
		graphics.line(userBody.center().x - personalSpace, -200, userBody.center().x - personalSpace, 100);
		graphics.line(userBody.center().x + personalSpace, -200, userBody.center().x + personalSpace, 100);
		graphics.stroke(255);
		
		// where vai was (blue)
		graphics.stroke(0, 0, 255);
		graphics.strokeWeight(10);
		graphics.line(vaiPosition, -200, vaiPosition, 100);
		graphics.stroke(255);
		
		// where vai is (green)
		graphics.stroke(0, 255, 0);
		graphics.strokeWeight(3);
		graphics.line(newPosition, -200, newPosition, 100);
		graphics.stroke(255);
		
		graphics.noStroke();
	}
	
	/*
	 *  Offsets vai to avoid collision with user during interaction
	 *  @param userBody		the user's Body instance 
	 *  @param vaiBody		the system's/vai's Body instance
	 *  @param graphics		the instance of vai's graphics object
	 *  @param offsetClock	the offset clock in Vai
	 */
	public static Body offsetVai(ControlsManager controlsManager, Body userBody, Body vaiBody, PGraphics graphics) {
		
		// Initialize positions
		PVector oldPosition = vaiBody.center();
	    Body newBody = vaiBody;
	    PVector newPosition = vaiBody.center();
	    
		// Calculate reflected center
		PVector reflectedCenter = vaiBody.center();
		reflectedCenter = new PVector(vaiBody.center().x * -1, vaiBody.center().y, vaiBody.center().z);
		
		// Check for reflection and reflect as needed
		if(controlsManager.isREFLECT_VAI_MOVEMENTS()) {
			newBody = vaiBody.transformed(PVecUtilities.STANDARD_REFLECTED_BASIS, newBody.localBasis(), newBody.center());
		} else {
			newBody = vaiBody.transformed(PVecUtilities.STANDARD_BASIS, newBody.localBasis(), newBody.center());
		}
		
	    // Offset-related settings that should be adjusted on an as-needed basis
	    PVector offsetDistance = new PVector(graphics.width * 0.4f, 0, 0); //min offset distance
	    PVector personalSpace = new PVector(graphics.width * 0.15f, 0, 0); // amount of space between user and vai
        PVector screenVaiPos = getScreenPos(newBody.center(), graphics); // vai's adjusted position on screen
        PVector screenUserPos = getScreenPos(userBody.center(), graphics); // user's adjusted position on screen
        PVector screenLimit = new PVector(graphics.width * .6f, 0, 0); // when vai should switch sides in offset mode 
        
        float posDifference;
        
        // logic for center reflection
		if(controlsManager.isREFLECT_VAI_CENTER()) {
			newBody = newBody.translated(reflectedCenter);
			
			
			float currEMA = screenVaiPos.x;
				
			// calculate curr EMA
	        if(prevEMA == -1) {
	        	currEMA = oldPosition.x;
	        } else {
		        currEMA = MathUtilities.calculateEMA(prevEMA, newBody.center().x, smoothingFactor);
	        }	
		    
	        // calculate position difference and update previous EMA
	        posDifference = currEMA - screenUserPos.x;
			prevEMA = currEMA;	
	        
			
	        if(Math.abs(posDifference) <= personalSpace.x) { //check for closeness to user
	        	
	    		if(userBody.center().x > 0   && userBody.center().x < personalSpace.x) { // case when vai is too close to user
	    			
	    			// only change position when vai has been on the same side for longer than updateInterval
	    			if(negClock.iniTimeMillis() - posClock.iniTimeMillis() >= updateInterval) {
	    				// change the position to predetermined position on positive x axis and update xSign
		    			newPosition.x = -offsetDistance.x;	
		    			xSign = newPosition.x / Math.abs(newPosition.x);
	    			} else {
	    				// stay on same side of screen as before
	    				newPosition.x = offsetDistance.x * xSign;
	    			}
	    			
	    			// reset the clock that denotes when vai was last on positive side of the screen
	    			posClock.start();
	    			
	    		} else if(userBody.center().x < 0 && userBody.center().x > -personalSpace.x){
	    			
	    			// only change position when vai has been on the same side for longer than updateInterval
	    			if(posClock.iniTimeMillis() - negClock.iniTimeMillis() >= updateInterval) {
	    				// change position to pre-determined location on positive x axis and update x sign
		    			newPosition.x = offsetDistance.x;	
		    			xSign = newPosition.x / Math.abs(newPosition.x);
	    			} else {
	    				// draw vai on same side of screen as before
	    				newPosition.x = offsetDistance.x * xSign;
	    			}
	    			
	    			// reset the clock that denotes when last time vai was on negative side of screen
	    			negClock.start();
	    			
	    		}else {
	    			// reflect vai center when on the same side for longer than the update interval
	    			if(posClock.iniTimeMillis() >= updateInterval && negClock.iniTimeMillis() >= updateInterval) {
			        	// reflect center when user is far enough from both vai and the center 
	    				newPosition = reflectedCenter;	
	    			} else {
	    				// draw vai on same side of screen as before
	    				newPosition.x = offsetDistance.x * xSign;
	    			}
	    			
	    		}
	        	
	        } else {
	        	newPosition = reflectedCenter;
				xSign = newPosition.x / Math.abs(newPosition.x);
	        }
	        
		// offset-mode logic
		} else {
			
			// keep vai a set distance away from the user
			newPosition.x = newBody.center().x + xSign * offsetDistance.x * 1.5f;
			
			// if offscreen, switch the side vai is drawn on
			if(Math.abs(newPosition.x) >= screenLimit.x) {
				xSign = -xSign;
			}
			
		}
	    
		// translate body to new position
	    newBody = newBody.translated(newPosition);
	    
	    return newBody;
	}
	
}