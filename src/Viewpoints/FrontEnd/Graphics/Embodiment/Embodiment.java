package Viewpoints.FrontEnd.Graphics.Embodiment;

import Viewpoints.FrontEnd.Graphics.EffectsController.EffectsController;
import processing.core.PGraphics;
import processing.core.PImage;

public abstract class Embodiment
{
	protected EffectsController effectsController;
	protected PGraphics graphics;
	
	public Embodiment(PGraphics graphics, EffectsController effectsController) {
		this.graphics = graphics;
		this.effectsController = effectsController;
	}
	
	public abstract PImage draw();
	
	public abstract void initialize();
	
	public abstract EffectsController getEffectsController();
	
}
