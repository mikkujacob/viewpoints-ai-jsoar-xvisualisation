package Viewpoints.FrontEnd.Model;

import java.util.*;

import Viewpoints.FrontEnd.Gesture.JointSpaceGesture.JointSpaceGesture;
import Viewpoints.FrontEnd.Shared.*;
import processing.core.*;

public class SegmentNormalization {
	public static JointSpaceGesture normalize(JointSpaceGesture inputSegment) {
		ArrayList<Body> segmentContent = inputSegment.getGestureFramesList();
		ArrayList<Body> normalized = normalizeOrientation(segmentContent);
		normalizeTiming(normalized);
		matchEnds(normalized);
		return new JointSpaceGesture(normalized);
	}
	
	private static ArrayList<Body> normalizeOrientation(ArrayList<Body> segmentContent) {
		ArrayList<Body> normalized = new ArrayList<Body>();
		PVector standardCenter = PVecUtilities.ZERO;
		for (Body pose : segmentContent) {
			float charsize = pose.characteristicSize();
			PVector[] targetBasis = new PVector[3];
			for (int i = 0; i < 3; i++) {
				targetBasis[i] = PVector.mult(PVecUtilities.STANDARD_BASIS[i], 1.0f / charsize);
			}
			normalized.add(pose.transformed(pose.localBasis(), targetBasis, standardCenter));
		}
		return normalized;
	}
	
	private static void normalizeTiming(ArrayList<Body> segmentContent) {
		Body startPose = segmentContent.get(0);
		Body endPose = segmentContent.get(segmentContent.size() - 1);
		float startTime = startPose.getTimestamp();
		float deltaTime = endPose.getTimestamp() - startTime;
		for (Body pose : segmentContent) {
			pose.setTimestamp((pose.getTimestamp() - startTime)/deltaTime);
		}
	}
	
	private static void matchEnds(ArrayList<Body> segmentContent) {
		Body start = segmentContent.get(0);
		Body end = segmentContent.get(segmentContent.size() - 1);
		HashMap<JIDX, PVector> correction = new HashMap<JIDX, PVector>();
		for (JIDX jidx : JIDX.ALL_JIDX) {
			correction.put(jidx, PVector.sub(end.get(jidx), start.get(jidx)));
		}
		for (Body pose : segmentContent) {
			for (JIDX jidx : JIDX.ALL_JIDX) {
				pose.set(jidx, PVector.add(pose.get(jidx), PVector.mult(correction.get(jidx), pose.getTimestamp())));
			}
		}
	}
}
