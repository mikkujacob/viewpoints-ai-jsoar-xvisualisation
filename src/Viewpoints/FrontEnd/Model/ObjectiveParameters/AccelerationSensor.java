package Viewpoints.FrontEnd.Model.ObjectiveParameters;

import java.util.ArrayList;
import java.util.HashMap;

import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.BodyDerivative;
import Viewpoints.FrontEnd.Shared.JIDX;
import Viewpoints.FrontEnd.Shared.Measure;
import processing.core.PVector;

public class AccelerationSensor implements ParameterSensor {
	private static final float SPEED_CUTOUT_THRESHOLD = 0.001f;
	
	@Override
	public HashMap<ParameterKey, ArrayList<Measure>> sense(	ArrayList<Body> frames,
															ArrayList<BodyDerivative> firstDerivative,
															ArrayList<BodyDerivative> secondDerivative,
															TargetKeyset targetTypes) {
		
		HashMap<ParameterKey, ArrayList<Measure>> measures = new HashMap<ParameterKey, ArrayList<Measure>>();
		
		if (!targetTypes.hasTargetType(ParameterType.NORMAL_ACCELERATION) && 
				!targetTypes.hasTargetType(ParameterType.TANGENT_ACCELERATION) 
			|| 0 == secondDerivative.size())
			return measures;
		
		for (JIDX jidx : ProminentJointsSets.LIMB_JIDX) {
			ArrayList<Measure> tangentialAccelerations = new ArrayList<Measure>();
			ArrayList<Measure> normalAccelerations = new ArrayList<Measure>();
			
			for (int i = 0; i < secondDerivative.size(); i++) {
				PVector movdir = PVector.add(firstDerivative.get(i).get(jidx), firstDerivative.get(i+1).get(jidx));
				movdir.div(2);
				float mdmag = movdir.mag();
				if (mdmag > SPEED_CUTOUT_THRESHOLD)
					movdir.div(movdir.mag());
				BodyDerivative secDerivativeAtI = secondDerivative.get(i);
				PVector accvec = secDerivativeAtI.get(jidx);
				float tangAcc;
				float normAcc;
				if (mdmag > SPEED_CUTOUT_THRESHOLD) {
					tangAcc = Math.abs(accvec.dot(movdir));
					float normAccSqr = accvec.magSq() - tangAcc * tangAcc;
					if (normAccSqr > 0.0f)
						normAcc = (float)Math.sqrt(normAccSqr);
					else
						normAcc = 0.0f;
				} else {
					tangAcc = accvec.mag();
					normAcc = 0.0f;
				}
				tangentialAccelerations.add(new Measure(secDerivativeAtI.getTimestamp(), tangAcc));
				normalAccelerations.add(new Measure(secDerivativeAtI.getTimestamp(), normAcc));
			}
			
			if (targetTypes.hasTargetType(ParameterType.NORMAL_ACCELERATION)) {
				measures.put(new ParameterKey(ParameterType.NORMAL_ACCELERATION, jidx), normalAccelerations);
			}
			if (targetTypes.hasTargetType(ParameterType.TANGENT_ACCELERATION)) {
				measures.put(new ParameterKey(ParameterType.TANGENT_ACCELERATION, jidx), tangentialAccelerations);
			}
		}
		
		return measures;
	}

}
