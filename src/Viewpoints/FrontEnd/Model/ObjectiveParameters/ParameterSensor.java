package Viewpoints.FrontEnd.Model.ObjectiveParameters;

import java.util.*;

import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.BodyDerivative;
import Viewpoints.FrontEnd.Shared.Measure;

public interface ParameterSensor {
	public HashMap<ParameterKey, ArrayList<Measure>> sense(	ArrayList<Body> frames,
															ArrayList<BodyDerivative> firstDerivative,
															ArrayList<BodyDerivative> secondDerivative,
															TargetKeyset targetTypes);
}
