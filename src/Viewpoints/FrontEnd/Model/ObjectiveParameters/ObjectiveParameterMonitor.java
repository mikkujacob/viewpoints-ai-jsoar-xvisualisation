package Viewpoints.FrontEnd.Model.ObjectiveParameters;

import java.util.*;

import Viewpoints.FrontEnd.Shared.AveragingCore;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.BodyDerivative;
import Viewpoints.FrontEnd.Shared.Measure;

public class ObjectiveParameterMonitor {
	private SpeedSensor speedSensor = new SpeedSensor();
	private AccelerationSensor accelerationSensor = new AccelerationSensor();
	private BoxSensor boxSensor = new BoxSensor();
	private BendingSensor bendingSensor = new BendingSensor();
	private ParameterSensor[] sensors = {speedSensor, accelerationSensor, boxSensor, bendingSensor};
	private AveragingCore averagingCore = new AveragingCore(0.5f);
	private TargetKeyset targetKeyset;
	
	public ObjectiveParameterMonitor(TargetKeyset targetKeyset) {
		this.targetKeyset = targetKeyset;
	}
	
	public HashMap<ParameterKey, Float> getParameters(ArrayList<Body> frames) 
	{
		if (frames.size() < 4) {
			return null;
		}
		HashMap<ParameterKey, ArrayList<Measure>> measures = new HashMap<ParameterKey, ArrayList<Measure>>();
		frames = scaleToStandardSize(frames);
		ArrayList<BodyDerivative> firstDerivative = BodyDerivative.derivative(frames);
		ArrayList<BodyDerivative> secondDerivative = BodyDerivative.derivative(firstDerivative);
		for (ParameterSensor sensor : sensors) {
			measures.putAll(sensor.sense(frames, firstDerivative, secondDerivative, targetKeyset));
		}
		return symmetrize(aggregate(derive(measures)));
	}
	
	private ArrayList<Body> scaleToStandardSize(ArrayList<Body> frames) {
		ArrayList<Body> scaled = new ArrayList<Body>();
		for (Body frame: frames) {
			scaled.add(frame.scale(1.0f / frame.characteristicSize()));
		}
		return scaled;
	}
	
	private HashMap<ParameterKey, ArrayList<Measure>> derive(HashMap<ParameterKey, ArrayList<Measure>> measures) {
		HashMap<ParameterKey, ArrayList<Measure>> derivedMeasures = new HashMap<ParameterKey, ArrayList<Measure>>();
		for (Map.Entry<ParameterKey, ArrayList<Measure>> keyedMeasures : measures.entrySet()) {
			ParameterKey defaultKey = keyedMeasures.getKey();
			ParameterKey basicKey = defaultKey.deriveModifiedData(DataDerivationType.BASIC);
			ArrayList<Measure> basicMeasures = keyedMeasures.getValue();
			if (targetKeyset.hasTargetFor(basicKey)) derivedMeasures.put(basicKey, basicMeasures);
			if (ParameterType.isSigned(basicKey.getParamType())) {
				ParameterKey absKey = basicKey.deriveModifiedData(DataDerivationType.ABS);
				if (targetKeyset.hasTargetFor(absKey)) {
					ArrayList<Measure> absMeasures = new ArrayList<Measure>();
					for (Measure basicMeasure : basicMeasures) {
						absMeasures.add(new Measure(basicMeasure.getTimestamp(), Math.abs(basicMeasure.getValue())));
					}
					derivedMeasures.put(absKey, absMeasures);
				}
			}
		}
		return derivedMeasures;
	}
	
	private HashMap<ParameterKey, Float> aggregate(HashMap<ParameterKey, ArrayList<Measure>> measures) {
		HashMap<ParameterKey, Float> aggregations = new HashMap<ParameterKey, Float>();
		for (Map.Entry<ParameterKey, ArrayList<Measure>> keyedMeasures : measures.entrySet()) {
			ArrayList<Measure> theMeasures = keyedMeasures.getValue();
			ArrayList<Measure> smoothedMeasures = averagingCore.apply(keyedMeasures.getValue());
			ParameterKey basicKey = keyedMeasures.getKey();
			ParameterKey avgKey = basicKey.deriveAggregation(AggregationType.AVE);
			if (targetKeyset.hasTargetFor(avgKey)) {
				float ave = 0.0f;
				for (Measure measure : theMeasures) {
					ave += measure.getValue();
				}
				ave /= measures.size();
				aggregations.put(avgKey, ave);
			}
			float max = -Float.MAX_VALUE;
			ParameterKey maxKey = basicKey.deriveAggregation(AggregationType.MAX);
			ParameterKey rangeKey = basicKey.deriveAggregation(AggregationType.RANGE);
			if (targetKeyset.hasTargetFor(maxKey) || targetKeyset.hasTargetFor(rangeKey)) {
				for (Measure measure : smoothedMeasures) {
					if (max < measure.getValue()) {
						max = measure.getValue();
					}
				}
				if (targetKeyset.hasTargetFor(maxKey)) aggregations.put(maxKey, max);
			}
			if (ParameterType.isSigned(basicKey.getParamType()) && 
					DataDerivationType.ABS != basicKey.getDerivationType()) {
				ParameterKey minKey = basicKey.deriveAggregation(AggregationType.MIN);
				if (targetKeyset.hasTargetFor(minKey) || targetKeyset.hasTargetFor(rangeKey)) {
					float min = Float.MAX_VALUE;
					for (Measure measure : smoothedMeasures) {
						if (min > measure.getValue()) {
							min = measure.getValue();
						}
					}
					if (targetKeyset.hasTargetFor(minKey)) aggregations.put(minKey, min);
					if (targetKeyset.hasTargetFor(rangeKey)) aggregations.put(rangeKey, max - min);
				}
			}
			ParameterKey momentumKey = basicKey.deriveAggregation(AggregationType.MOMENTUM);
			ParameterKey reverseMomentumKey = basicKey.deriveAggregation(AggregationType.REVERSE_MOMENTUM);
			if (targetKeyset.hasTargetFor(momentumKey) || targetKeyset.hasTargetFor(reverseMomentumKey)) {
				float momentum = 0.0f;
				float reverseMomentum = 0.0f;
				float startTime = theMeasures.get(0).getTimestamp();
				float duration = theMeasures.get(theMeasures.size() - 1).getTimestamp() - startTime;
				for (Measure measure : theMeasures) {
					float relativeTime = (measure.getTimestamp() - startTime) / duration;
					momentum += relativeTime * measure.getValue();
					reverseMomentum += (1.0 - relativeTime) * measure.getValue();
				}
				if (targetKeyset.hasTargetFor(momentumKey)) aggregations.put(momentumKey, momentum);
				if (targetKeyset.hasTargetFor(reverseMomentumKey)) aggregations.put(reverseMomentumKey, reverseMomentum);
			}
		}
		return aggregations;
	}
	
	private HashMap<ParameterKey, Float> symmetrize(HashMap<ParameterKey, Float> parameters) {
		HashMap<ParameterKey, Float> symmetrization = new HashMap<ParameterKey, Float>();
		for (Map.Entry<ParameterKey, Float> parameter : parameters.entrySet()) {
			ParameterKey key = parameter.getKey();
			Float paramValue = parameter.getValue();
			if (!"LEFT".equals(key.getJidx().toString().substring(0, 4))) {
				if (!"RIGH".equals(key.getJidx().toString().substring(0, 4))) {
					symmetrization.put(key, paramValue);
				}
				continue;
			}
			ParameterKey symKey = key.symKey();
			Float symParamValue = parameters.get(symKey);
			ParameterKey midKey = key.deriveSymmetryAggregation(SymmetryAggregationType.MID);
			if (targetKeyset.hasTargetFor(midKey)) {
				symmetrization.put(midKey, (paramValue + symParamValue)/2);
			}
			ParameterKey deltaKey = key.deriveSymmetryAggregation(SymmetryAggregationType.DELTA);
			if (targetKeyset.hasTargetFor(deltaKey)) {
				symmetrization.put(deltaKey, Math.abs(symParamValue - paramValue));
			}
			ParameterKey leadKey = key.deriveSymmetryAggregation(SymmetryAggregationType.LEAD);
			if (targetKeyset.hasTargetFor(leadKey)) {
				symmetrization.put(leadKey, Math.max(paramValue, symParamValue));
			}
			ParameterKey trailKey = key.deriveSymmetryAggregation(SymmetryAggregationType.TRAIL);
			if (targetKeyset.hasTargetFor(trailKey)) {
				symmetrization.put(trailKey, Math.min(paramValue, symParamValue));
			}
		}
		return symmetrization;
	}
}
