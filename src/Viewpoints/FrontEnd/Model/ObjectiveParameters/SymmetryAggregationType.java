package Viewpoints.FrontEnd.Model.ObjectiveParameters;

public enum SymmetryAggregationType {
	NONE,
	MID,
	DELTA,
	LEAD,
	TRAIL
}
