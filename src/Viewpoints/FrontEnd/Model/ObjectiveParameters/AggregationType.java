package Viewpoints.FrontEnd.Model.ObjectiveParameters;

public enum AggregationType {
	NONE,
	AVE,
	MIN,
	MAX,
	RANGE,
	MOMENTUM,
	REVERSE_MOMENTUM
}
