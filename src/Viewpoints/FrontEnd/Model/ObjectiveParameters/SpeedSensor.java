package Viewpoints.FrontEnd.Model.ObjectiveParameters;

import java.util.*;
import processing.core.PVector;
import Viewpoints.FrontEnd.Model.ObjectiveParameters.ParameterType;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.BodyDerivative;
import Viewpoints.FrontEnd.Shared.JIDX;
import Viewpoints.FrontEnd.Shared.Measure;

public class SpeedSensor implements ParameterSensor
{
	@Override
	public HashMap<ParameterKey, ArrayList<Measure>> sense(	ArrayList<Body> frames,
															ArrayList<BodyDerivative> firstDerivative,
															ArrayList<BodyDerivative> secondDerivative,
															TargetKeyset targetTypes) {
		HashMap<ParameterKey, ArrayList<Measure>> measures = new HashMap<ParameterKey, ArrayList<Measure>>();
		
		if (!targetTypes.hasTargetType(ParameterType.SPEED)) return measures;
		
		if (0 == firstDerivative.size())
			return measures;

		for (JIDX jidx : ProminentJointsSets.LIMB_JIDX) {
			ArrayList<Measure> speeds = new ArrayList<Measure>();
			
			for (BodyDerivative bodyDerivative : firstDerivative) {
				PVector speedvec = bodyDerivative.get(jidx);
				float speed = speedvec.mag();
				speeds.add(new Measure(bodyDerivative.getTimestamp(), speed));
			}
			
			measures.put(new ParameterKey(ParameterType.SPEED, jidx), speeds);
		}
		
		return measures;
	}
	
}
