package Viewpoints.FrontEnd.Model.ObjectiveParameters;

import java.io.File;
import java.util.HashMap;

import Viewpoints.FrontEnd.Communication.File.FileUtilities;
import Viewpoints.FrontEnd.Gesture.JointSpaceGesture.JointSpaceGesture;
import Viewpoints.FrontEnd.Model.SegmentNormalization;

public class ParametrizeGestureLibrary {
/*	public static final String GESTURE_DIR = "training-gestures/nonsad-golden";
	public static final String PARAMETERS_DIR = "testParams/nonsad-golden";*/
	public static final String GESTURE_DIR = "recorded-input-sessions/boundary";
	public static final String PARAMETERS_DIR = "testParams/boundary";
	
	public static void main(String[] args) {
		try {
			//Quickfix to allow this code be called from outside (eg.: python script)
			String gesture_dir = GESTURE_DIR;
			String parameters_dir = PARAMETERS_DIR;
			if(args.length == 2) {
				gesture_dir = args[0];
				parameters_dir = args[1];
			}
			
			File gestureDir = new File(gesture_dir);
			ObjectiveParameterMonitor objectiveParameterMonitor = new ObjectiveParameterMonitor(TargetKeyset.getFullKeyset());
			for (String gestureFileName : gestureDir.list()) {
				JointSpaceGesture gesture = FileUtilities.deserializeJointsGesture(gesture_dir + "/" + gestureFileName);
				gesture = SegmentNormalization.normalize(gesture);
				HashMap<ParameterKey, Float> parameters = objectiveParameterMonitor.getParameters(gesture.getGestureFramesList());
				String clearName = gestureFileName.replaceFirst("[.][^.]+$", "");
				IO.out(parameters_dir + "/" + clearName + ".param", parameters);
			}
		} catch (Exception e) {
			System.out.println(e);
			e.printStackTrace();
		}
	}
};
