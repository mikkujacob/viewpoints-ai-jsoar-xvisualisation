package Viewpoints.FrontEnd.Model.ObjectiveParameters;

import java.io.*;
import java.util.*;

import Viewpoints.FrontEnd.Communication.File.FileUtilities;
import Viewpoints.FrontEnd.Gesture.JointSpaceGesture.JointSpaceGesture;
import Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture.PredicateSpaceGesture;
import Viewpoints.FrontEnd.Shared.*;

public class ObjectiveParametersMetric implements Classifier {
	private static final String DEFAULT_GESTURE_DIR = "gestures";
	private static final String DEFAULT_GESTURE_PARAM_DIR = "gesture_parameters";
	private static final String DEFAULT_RUNTIME_GESTURE_DIR = "runtime_gestures";
	private static final String DEFAULT_RUNTIME_GESTURE_PARAM_DIR = "runtime_gesture_parameters";
	private static final float MIN_PARAMETER_HALFRANGE = 0.1f;
	private static final double ATTENTION_RANGE = Math.log(0.5);
	private static final double WHIM_RANGE = Math.log(0.01);
	
	private String gestureDir = DEFAULT_GESTURE_DIR;
	private String gestureParamDir = DEFAULT_GESTURE_PARAM_DIR;
	private String runtimeGestureDir = DEFAULT_RUNTIME_GESTURE_DIR;
	private String runtimeGestureParamDir = DEFAULT_RUNTIME_GESTURE_PARAM_DIR;
	
	private ArrayList<Pair<String, HashMap<ParameterKey, Float>>> gestureParametersCatalogue = new ArrayList<Pair<String,HashMap<ParameterKey,Float>>>();
	private HashMap<ParameterKey, Pair<Float, Float>> parameterRange = new HashMap<ParameterKey, Pair<Float, Float>>();
	private Random random = new Random(System.currentTimeMillis());
	private ObjectiveParameterMonitor objectiveParameterMonitor;
	private JointSpaceGesture lastGesture = null;
	private HashMap<ParameterKey, Float> lastParameters = null;
	private ArrayList<String> labels = new ArrayList<String>();
	private HashSet<ParameterKey> relevantParameters = new HashSet<ParameterKey>();
	
	public ObjectiveParametersMetric(TargetKeyset targetKeyset) {
		objectiveParameterMonitor = new ObjectiveParameterMonitor(targetKeyset);
	};
	
	public ObjectiveParametersMetric(TargetKeyset targetKeyset, 
									String gestureDir, 
									String gestureParamDir,
									String runtimeGestureDir,
									String runtimeGestureParamDir) {
		objectiveParameterMonitor = new ObjectiveParameterMonitor(targetKeyset);
		this.gestureDir = gestureDir;
		this.gestureParamDir = gestureParamDir;
		this.runtimeGestureDir = runtimeGestureDir;
		this.runtimeGestureParamDir = runtimeGestureParamDir;
	}
	
	public void init(HashMap<String, Object> parameters) {
		//loadRelevantParameters((String)parameters.get("relevant_parameters_file"));
	}
	
	public int size() {
		return gestureParametersCatalogue.size();
	}
	
	public void loadRelevantParameters(String filename) {
		File gestureParamFile = new File(filename);
		Scanner paramFileScanner = null;
		try {
			paramFileScanner = new Scanner(gestureParamFile);
			while (true) {
				String line = paramFileScanner.nextLine();
				relevantParameters.add(new ParameterKey(line));
			}
		} catch (NoSuchElementException e) {
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {paramFileScanner.close();} catch (Exception e) {}
		}
	}
	
	public void load() {
		String[] gestureDirs = {gestureDir, runtimeGestureDir};
		String[] parametersDirs = {gestureParamDir, runtimeGestureParamDir};
		try {
			for (int i = 0; i < gestureDirs.length; i++) {
				loadDir(gestureDirs[i], parametersDirs[i]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void loadDir(String gestureDir, String parametersDir) {
		File gestureDirFile = new File(gestureDir);
		File parametersDirFile = new File(parametersDir);
		HashSet<String> existingParameterFiles = new HashSet<String>();
		for (String paramFile : parametersDirFile.list()) {
			existingParameterFiles.add(paramFile);
		}
		for (String gestureFileName : gestureDirFile.list()) {
			if (!isJsg(gestureFileName)) 
				continue;
			String paramName = gestureFileName.replaceAll(".jsg", ".param");
			String fullname = gestureDir+"/"+gestureFileName;
			String fullParamName = parametersDir+"/"+paramName;
			HashMap<ParameterKey, Float> parameters = null;
			if (existingParameterFiles.contains(paramName)) {
				parameters = IO.in(fullParamName);
			} else {
				JointSpaceGesture gesture = FileUtilities.deserializeJointsGesture(fullname);
				parameters = objectiveParameterMonitor.getParameters(gesture.getGestureFramesList());
				IO.out(fullParamName, parameters);
			}
			memorizeGestureParameters(fullname, parameters);
		}
	}
	
	private boolean isJsg(String gestureFileName) {
		return gestureFileName.length() > 4 && gestureFileName.substring(gestureFileName.length() - 4, gestureFileName.length()).equals(".jsg");
	}
	
	public void trainOnSample(String fullname, String label) {
		HashMap<ParameterKey, Float> parameters = IO.in(fullname);
		memorizeGestureParameters(fullname, parameters);
		labels.add(label);
	}
	
	//Debug code
//	public void loadWithUUIDs() {
//		String[] gestureDirs = {GESTURE_DIR/*, RUNTIME_GESTURE_DIR*/};
//		try {
//			for (String gestureDir : gestureDirs) {
//				File gestureDirFile = new File(gestureDir);
//				for (String gestureFileName : gestureDirFile.list()) {
//					if (gestureFileName.length() < 4 || !gestureFileName.substring(gestureFileName.length() - 4, gestureFileName.length()).equals(".jsg")) 
//						continue;
//					String fullname = gestureDir+"/"+gestureFileName;
//					JointSpaceGesture gesture = readGesture(fullname);
//					HashMap<ParameterKey, Float> parameters = objectiveParameterMonitor.getParameters(gesture.getGestureFramesList());
//					memorizeGestureParameters(fullname, parameters);
//					
//					gesture.setGestureUUID(UUID.randomUUID());
//					String fileName = RUNTIME_GESTURE_DIR + "/" + gesture.getGestureUUID().toString() + ".jsg";
//					try {
//						FileOutputStream ofstream = new FileOutputStream(fileName);
//						ObjectOutputStream objstream = new ObjectOutputStream(ofstream);
//						objstream.writeObject(gesture);
//						objstream.flush();
//						objstream.close();
//						ofstream.flush();
//						ofstream.close();
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
	
	public void memorizeLastGesture() {
		if (null == lastGesture)
			return;
		String fileName = runtimeGestureDir + "/" + (new Date()).toString().replace(':','-') + ".jsg";
		try {
			FileOutputStream ofstream = new FileOutputStream(fileName);
			ObjectOutputStream objstream = new ObjectOutputStream(ofstream);
			objstream.writeObject(lastGesture);
			objstream.flush();
			objstream.close();
			ofstream.flush();
			ofstream.close();
			memorizeGestureParameters(fileName, lastParameters);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String memorizeLastGesture(UUID gestureUUID) {
		if (null == lastGesture)
			return null;
		lastGesture.setGestureUUID(gestureUUID);
		String fileName = runtimeGestureDir + "/" + gestureUUID.toString() + "-" + (new Date()).toString().replace(':','-') + ".jsg";
		try {
			FileOutputStream ofstream = new FileOutputStream(fileName);
			ObjectOutputStream objstream = new ObjectOutputStream(ofstream);
			objstream.writeObject(lastGesture);
			objstream.flush();
			objstream.close();
			ofstream.flush();
			ofstream.close();
			memorizeGestureParameters(fileName, lastParameters);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileName;
	}
	
	public String memorizeLastGesture(UUID gestureUUID, PredicateSpaceGesture predicateGesture) {
		if (null == lastGesture)
			return null;
		lastGesture.setGestureUUID(gestureUUID);
		String fileName = runtimeGestureDir + "/" + gestureUUID.toString() + "-" + (new Date()).toString().replace(':','-');
		try {
			FileOutputStream ofstream = new FileOutputStream(fileName + ".jsg");
			ObjectOutputStream objstream = new ObjectOutputStream(ofstream);
			objstream.writeObject(lastGesture);
			objstream.flush();
			objstream.close();
			ofstream.flush();
			ofstream.close();
			memorizeGestureParameters(fileName + ".jsg", lastParameters);
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			FileOutputStream ofstream = new FileOutputStream(fileName + ".psg");
			ObjectOutputStream objstream = new ObjectOutputStream(ofstream);
			objstream.writeObject(predicateGesture);
			objstream.flush();
			objstream.close();
			ofstream.flush();
			ofstream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return fileName;
	}
	
	private void memorizeGestureParameters(String gestureFileName, HashMap<ParameterKey, Float> parameters) {
		gestureParametersCatalogue.add(Pair.of(gestureFileName, parameters));
		for (Map.Entry<ParameterKey, Float> parameter : parameters.entrySet()) {
			if (!parameterRange.containsKey(parameter.getKey())) {
				parameterRange.put(parameter.getKey(), Pair.of(	parameter.getValue()-MIN_PARAMETER_HALFRANGE, 
																parameter.getValue()+MIN_PARAMETER_HALFRANGE));
			} else {
				Pair<Float, Float> range = parameterRange.get(parameter.getKey());
				if (range.first > parameter.getValue()) {
					parameterRange.put(parameter.getKey(), Pair.of(parameter.getValue(), range.second));
				} else if (range.second < parameter.getValue()) {
					parameterRange.put(parameter.getKey(), Pair.of(range.first, parameter.getValue()));
				}
			}
		}
	}
	

	public JointSpaceGesture selectClosest(JointSpaceGesture gesture) {
		return selectClosest(gesture, -Double.MAX_VALUE);
	}
	
	public JointSpaceGesture selectClosest(JointSpaceGesture gesture, double loglikelihoodThreshold) {
		lastGesture = gesture;
		lastParameters = objectiveParameterMonitor.getParameters(gesture.getGestureFramesList());
		if(null == lastParameters || gestureParametersCatalogue.size() == 0)
		{
			return null;
		}
		ArrayList<ArrayList<Double>> parameterScores = getParameterScores(lastParameters);
		String gestureFile = whimPhase(likelihoodPhase(parameterScores, attentionFocusPhase(parameterScores), loglikelihoodThreshold));
		if (null == gestureFile)
			return null;
		System.out.println(gestureFile);
		return FileUtilities.deserializeJointsGesture(gestureFile);
	}
	
	public String predict(HashMap<ParameterKey, Float> parameters) {
		return predict(parameters, -750.0);
	}
	
	public String predict(HashMap<ParameterKey, Float> parameters, double loglikelihoodThreshold) {
		ArrayList<ArrayList<Double>> parameterScores = getParameterScores(parameters);
		ArrayList<Integer> attentionGestures = attentionFocusPhase(parameterScores);
		ArrayList<Double>  loglikelihoodBuffer = new ArrayList<Double>();
		ArrayList<Integer> neighbours = likelihoodPhase(parameterScores, attentionGestures, loglikelihoodThreshold, loglikelihoodBuffer);
		if (neighbours.isEmpty()) {
			return "neg";
		}
		ArrayList<Double> weights = lognormalize(loglikelihoodBuffer);
		for (int i = 0; i < weights.size(); i++) {
			weights.set(i, Math.exp(weights.get(i)));
		}
		HashMap<String, Double> votes = new HashMap<String, Double>();
		for (int i = 0; i < attentionGestures.size(); i++) {
			int index = attentionGestures.get(i);
			String label = labels.get(index);
			if (!votes.containsKey(label)) {
				votes.put(label, 0.0);
			}
			votes.put(label, votes.get(label) + 1.0);//weights.get(i));
		}
		String topLabel = null;
		double topWeight = -Double.MAX_VALUE;
		for (Map.Entry<String, Double> voteEntry : votes.entrySet()) {
			if (voteEntry.getValue() > topWeight) {
				topLabel = voteEntry.getKey();
				topWeight = voteEntry.getValue();
			}
		}
		return topLabel;
	}
	
	private ArrayList<ArrayList<Double>> getParameterScores(HashMap<ParameterKey, Float> iGestureParameters) {
		ArrayList<ArrayList<Double>> scores = new ArrayList<ArrayList<Double>>(iGestureParameters.size());
		for (Map.Entry<ParameterKey, Float> parameter : iGestureParameters.entrySet()) {
			ArrayList<Double> parameterScores = new ArrayList<Double>(gestureParametersCatalogue.size());
			scores.add(parameterScores);
			Pair<Float, Float> range = parameterRange.get(parameter.getKey());
			double stdDev = (range.second - range.first) / Math.sqrt(gestureParametersCatalogue.size());
			double normDsnDenominator = 2.0 * stdDev * stdDev;
			double actualValue = parameter.getValue();
			for (int i = 0; i < gestureParametersCatalogue.size(); i++) {
				Pair<String, HashMap<ParameterKey, Float>> catalogueParameters = gestureParametersCatalogue.get(i);
//				double sampleValue = catalogueParameters.second.get(parameter.getKey());
//				double score = -Math.pow(sampleValue - actualValue, 2)/normDsnDenominator - 0.5*Math.log(normDsnDenominator * Math.PI);
//				parameterScores.add(score);
				if(catalogueParameters.second.containsKey(parameter.getKey()))
				{
					double sampleValue = catalogueParameters.second.get(parameter.getKey());
					double score = -Math.pow(sampleValue - actualValue, 2)/normDsnDenominator - 0.5*Math.log(normDsnDenominator * Math.PI);
					parameterScores.add(score);
					
//					System.out.println("Parameter found and added: " + parameter.getKey());
				}
				else
				{
//					System.out.println("Parameter not found: " + parameter.getKey());
				}
			}
		}
		return scores;
	}
	
	private double logsum(double logA, double logB) {
		return logA + Math.log(1.0 + Math.exp(logB - logA));
	}
	
	private ArrayList<Double> lognormalize(ArrayList<Double> logvalues) {
		ArrayList<Double> lognormalized = new ArrayList<Double>();
		if (logvalues.isEmpty()) {
			return lognormalized;
		}
		if (1 == logvalues.size()) {
			lognormalized.add(0.0);
			return lognormalized;
		}
		double lognormalizer = logsum(logvalues.get(0), logvalues.get(1));
		for (int i = 2; i < logvalues.size(); i++) {
			lognormalizer = logsum(lognormalizer, logvalues.get(i));
		}
		for (Double logvalue : logvalues) {
			lognormalized.add(logvalue - lognormalizer);
		}
		return lognormalized;
	}
	
	private ArrayList<Integer> attentionFocusPhase(ArrayList<ArrayList<Double>> scoreMatrix) {
		HashSet<Integer> selectedColumns = new HashSet<Integer>();
		double scoreMatrixMax = -Double.MAX_VALUE;
		ArrayList<ArrayList<Double>> normalizedScoreMatrix = new ArrayList<ArrayList<Double>>();
		for (ArrayList<Double> scoreRow : scoreMatrix) {
			ArrayList<Double> normalizedRow = lognormalize(scoreRow);
			normalizedScoreMatrix.add(normalizedRow);
			for (Double normscore : normalizedRow) {
				if (normscore > scoreMatrixMax) {
					scoreMatrixMax = normscore;
				}
			}
		}
		double attentionThreshold = scoreMatrixMax + ATTENTION_RANGE;
		for (ArrayList<Double> normalizedRow : normalizedScoreMatrix ) {
			for (int j = 0; j < normalizedRow.size(); j++) {
				if (normalizedRow.get(j) > attentionThreshold) {
					selectedColumns.add(j);
				}
			}
		}
		return new ArrayList<Integer>(selectedColumns);
	}
	
	private ArrayList<Integer> likelihoodPhase(ArrayList<ArrayList<Double>> scoreMatrix, ArrayList<Integer> preselection, double loglikelihoodThreshold) {
		return likelihoodPhase(scoreMatrix, preselection, loglikelihoodThreshold, null);
	}
	
	private ArrayList<Integer> likelihoodPhase(	ArrayList<ArrayList<Double>> scoreMatrix, 
												ArrayList<Integer> preselection, 
												double loglikelihoodThreshold, 
												ArrayList<Double> oLoglikelihoods) {
		ArrayList<Integer> likelihoodSelection = new ArrayList<Integer>();
		ArrayList<ArrayList<Double>> loglikelihoodMatrix = new ArrayList<ArrayList<Double>>(preselection.size());
		for (Integer sel : preselection) {
			ArrayList<Double> logLikelihoodsRow = new ArrayList<Double>(scoreMatrix.size());
			for (int j = 0; j < scoreMatrix.size(); j++) {
				logLikelihoodsRow.add(scoreMatrix.get(j).get(sel));
			}
			loglikelihoodMatrix.add(logLikelihoodsRow);
		}
		ArrayList<Double> loglikelihoods = new ArrayList<Double>(preselection.size());
		for (ArrayList<Double> loglikelihoodRow : loglikelihoodMatrix) {
			double loglikelihood = 0.0f;
			for (Double loglikelihoodCell : loglikelihoodRow) {
				loglikelihood += loglikelihoodCell;
			}
			loglikelihoods.add(loglikelihood);
		}
		double maxLoglikelihood = -Double.MAX_VALUE;
		for (Double loglikelihood : loglikelihoods) {
			if (loglikelihood > maxLoglikelihood) {
				maxLoglikelihood = loglikelihood;
			}
		}
		double whimThreshold = maxLoglikelihood + WHIM_RANGE;
		if (loglikelihoodThreshold > whimThreshold)
			whimThreshold = loglikelihoodThreshold;
		for (int i = 0; i < preselection.size(); i++) {
			if (loglikelihoods.get(i) > whimThreshold) {
				likelihoodSelection.add(preselection.get(i));
				if (null != oLoglikelihoods) {
					oLoglikelihoods.add(loglikelihoods.get(i));
				}
			}
		}
		
		return likelihoodSelection;
	}
	
	private String whimPhase(ArrayList<Integer> preselection) {
		if (preselection.isEmpty())
			return null;
		int randSelector = random.nextInt(preselection.size());
		int selectedIndex = preselection.get(randSelector);
		return gestureParametersCatalogue.get(selectedIndex).first;
	}
	
	public static void main(String[] args) {
		ObjectiveParametersMetric metric = new ObjectiveParametersMetric(TargetKeyset.getBasicSimilarityKeyset());
		metric.load();
		try {
			File gestureDirFile = new File(DEFAULT_GESTURE_DIR);
			for (File gestureFile : gestureDirFile.listFiles()) {
				FileInputStream ifstream = new FileInputStream(gestureFile);
				//FileInputStream ifstream = new FileInputStream("gestures/MonApr0822-48-14GMT-04-002013.jsg");
				ObjectInputStream objstream = new ObjectInputStream(ifstream);
				JointSpaceGesture gesture = (JointSpaceGesture) objstream.readObject();
				gesture.autotiming();
				ifstream.close();
				System.out.print(gestureFile.getName() + ": ");
				metric.selectClosest(gesture);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
