package Viewpoints.FrontEnd.Model.ObjectiveParameters;

import java.util.*;

import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.BodyDerivative;
import Viewpoints.FrontEnd.Shared.JIDX;
import Viewpoints.FrontEnd.Shared.Measure;
import processing.core.PVector;

public class BoxSensor implements ParameterSensor {

	@Override
	public HashMap<ParameterKey, ArrayList<Measure>> sense(	ArrayList<Body> frames,
															ArrayList<BodyDerivative> firstDerivative,
															ArrayList<BodyDerivative> secondDerivative,
															TargetKeyset targetTypes) {
		HashMap<ParameterKey, ArrayList<Measure>> measures = new HashMap<ParameterKey, ArrayList<Measure>>();
		
		if (!targetTypes.hasTargetType(ParameterType.FORWARDNESS) &&
				!targetTypes.hasTargetType(ParameterType.UPWARDNESS) &&
				!targetTypes.hasTargetType(ParameterType.SIDEWARDNESS) &&
				!targetTypes.hasTargetType(ParameterType.FORWARD_SPEED) &&
				!targetTypes.hasTargetType(ParameterType.UPWARD_SPEED) &&
				!targetTypes.hasTargetType(ParameterType.SIDEWARD_SPEED)
			|| 0 == frames.size())
			return measures;
		
		JIDX relevantJointSets[][] = {	{JIDX.LEFT_ELBOW, JIDX.LEFT_HAND},
										{JIDX.RIGHT_ELBOW, JIDX.RIGHT_HAND},
										{JIDX.LEFT_KNEE, JIDX.LEFT_FOOT},
										{JIDX.RIGHT_KNEE, JIDX.RIGHT_FOOT} };//,
										//{JIDX.HEAD},
										//{JIDX.NECK}};
		
		JIDX references[] = {JIDX.LEFT_SHOULDER, JIDX.RIGHT_SHOULDER, JIDX.LEFT_HIP, JIDX.RIGHT_HIP}; //, JIDX.NECK, JIDX.TORSO};
		
		ParameterType measuredTypes[] = {ParameterType.FORWARDNESS, ParameterType.UPWARDNESS, ParameterType.SIDEWARDNESS,
										 ParameterType.FORWARD_SPEED, ParameterType.UPWARD_SPEED, ParameterType.SIDEWARD_SPEED};
										 //ParameterType.FORWARD_SPEED_RELATIVE, ParameterType.UPWARD_SPEED_RELATIVE, ParameterType.SIDEWARD_SPEED_RELATIVE};
		
		ParameterKey variKey = new ParameterKey(ParameterType.FORWARDNESS, JIDX.HEAD);
		
		for (ParameterType paramType : measuredTypes) {
			variKey.setParamType(paramType);
			for (JIDX[] relevantJointSet : relevantJointSets) {
				for (JIDX jidx : relevantJointSet) {
					variKey.setJidx(jidx);
					measures.put(variKey.clone(), new ArrayList<Measure>());
				}
			}		
		}
		
		for (int bodyI = 0; bodyI < frames.size(); bodyI++) {
			Body body = frames.get(bodyI);
			BodyDerivative firstDerivativeFrame = null;
			if (bodyI < firstDerivative.size())
				firstDerivativeFrame = firstDerivative.get(bodyI);
			PVector forwardVec = body.orientation();
			PVector upperUpvec = body.upperUpvec();
			PVector lowerUpvec = body.lowerUpvec();
			PVector upperLeftSidevec = forwardVec.cross(upperUpvec);
			PVector upperRightSidevec = PVector.mult(upperLeftSidevec, -1);
			PVector lowerLeftSidevec = forwardVec.cross(lowerUpvec);
			PVector lowerRightSidevec = PVector.mult(lowerLeftSidevec, -1);
			
			PVector refSystems[][] = {	{forwardVec, upperUpvec, upperLeftSidevec},
										{forwardVec, upperUpvec, upperRightSidevec},
										{forwardVec, lowerUpvec, lowerLeftSidevec},
										{forwardVec, lowerUpvec, lowerRightSidevec} };
			
			for (int i = 0; i < relevantJointSets.length; i++) {
				JIDX jointSet[] = relevantJointSets[i];
				PVector reference = body.get(references[i]);
				PVector refSystem[] = refSystems[i];
				
				for (JIDX jidx : jointSet) {
					variKey.setJidx(jidx);
					
					PVector jposition = PVector.sub(body.get(jidx), reference);
					if (targetTypes.hasTargetType(ParameterType.FORWARDNESS)) {
						float forwardness = refSystem[0].dot(jposition);
						variKey.setParamType(ParameterType.FORWARDNESS);
						measures.get(variKey).add(new Measure(body.getTimestamp(), forwardness));
					}
					if (targetTypes.hasTargetType(ParameterType.UPWARDNESS)) {
						float upwardness = refSystem[1].dot(jposition);
						variKey.setParamType(ParameterType.UPWARDNESS);
						measures.get(variKey).add(new Measure(body.getTimestamp(), upwardness));
					}
					if (targetTypes.hasTargetType(ParameterType.SIDEWARDNESS)) {
						float sidewardness = refSystem[2].dot(jposition);
						variKey.setParamType(ParameterType.SIDEWARDNESS);
						measures.get(variKey).add(new Measure(body.getTimestamp(), sidewardness));
					}
					
					if (null != firstDerivativeFrame) {
						PVector jspeed = firstDerivativeFrame.get(jidx);
						PVector refspeed = firstDerivativeFrame.get(references[i]);
						PVector relspeed = PVector.sub(jspeed, refspeed);
						if (targetTypes.hasTargetType(ParameterType.FORWARD_SPEED)) {
							float forwardSpeed = refSystem[0].dot(relspeed);
							variKey.setParamType(ParameterType.FORWARD_SPEED);
							measures.get(variKey).add(new Measure(body.getTimestamp(), forwardSpeed));
						}
						if (targetTypes.hasTargetType(ParameterType.UPWARD_SPEED)) {
							float upwardSpeed = refSystem[1].dot(relspeed);
							variKey.setParamType(ParameterType.UPWARD_SPEED);
							measures.get(variKey).add(new Measure(body.getTimestamp(), upwardSpeed));
						}
						if (targetTypes.hasTargetType(ParameterType.SIDEWARD_SPEED)) {
							float sidewardSpeed = refSystem[2].dot(relspeed);
							variKey.setParamType(ParameterType.SIDEWARD_SPEED);
							measures.get(variKey).add(new Measure(body.getTimestamp(), sidewardSpeed));
						}
					}
				}
			}
		}
		
		return measures;
	}

}
