package Viewpoints.FrontEnd.Model.ObjectiveParameters;

import java.util.HashSet;

public enum ParameterType {
	NONE, // used for specifying filtering goal in target keyset
	SPEED,
	TANGENT_ACCELERATION,
	NORMAL_ACCELERATION,
	UPWARDNESS,
	FORWARDNESS,
	SIDEWARDNESS,
	FORWARD_SPEED,
	SIDEWARD_SPEED,
	UPWARD_SPEED,
	BENDING,
	BENDING_SPEED;
	
	private static HashSet<ParameterType> _SIGNED_PARAMETERS = null;
	
	public static boolean isSigned(ParameterType pType) {
		if (null == _SIGNED_PARAMETERS) {
			_SIGNED_PARAMETERS = new HashSet<ParameterType>();
			_SIGNED_PARAMETERS.add(UPWARDNESS);
			_SIGNED_PARAMETERS.add(FORWARDNESS);
			_SIGNED_PARAMETERS.add(SIDEWARDNESS);
			_SIGNED_PARAMETERS.add(FORWARD_SPEED);
			_SIGNED_PARAMETERS.add(UPWARD_SPEED);
			_SIGNED_PARAMETERS.add(SIDEWARD_SPEED);
			_SIGNED_PARAMETERS.add(BENDING);
			_SIGNED_PARAMETERS.add(BENDING_SPEED);
		} 
		return _SIGNED_PARAMETERS.contains(pType);
	}
}