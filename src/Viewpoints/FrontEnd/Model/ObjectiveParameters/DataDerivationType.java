package Viewpoints.FrontEnd.Model.ObjectiveParameters;

public enum DataDerivationType {
	NONE,
	BASIC,
	ABS
}
