package Viewpoints.FrontEnd.Model.ObjectiveParameters;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;

public class IO {
	public static void out(String filename, HashMap<ParameterKey, Float> parameters) {
		try {
			Locale.setDefault(Locale.US);
			FileOutputStream ofstream = new FileOutputStream(filename);
			PrintStream printstream = new PrintStream(ofstream);
			outToStream(printstream, parameters);
			ofstream.flush();
			ofstream.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public static void outToStream(PrintStream printstream, HashMap<ParameterKey, Float> parameters) {
		for (Map.Entry<ParameterKey, Float> parameter : parameters.entrySet()) {
			printstream.printf("%s:%f\n", parameter.getKey().toString(), parameter.getValue());
		}
		printstream.println();
		printstream.flush();
	}
	
	public static HashMap<ParameterKey, Float> in(String filename) {
		Locale.setDefault(Locale.US);
		HashMap<ParameterKey, Float> parameters = new HashMap<ParameterKey, Float>();
		try {
			FileInputStream ifstream = new FileInputStream(filename);
			Scanner ifscanner = new Scanner(ifstream);
			while (ifscanner.hasNext()) {
				String nextln = ifscanner.nextLine();
				if (nextln.isEmpty()) 
					break;
				String[] nextParts = nextln.split(":");
				parameters.put(new ParameterKey(nextParts[0]), new Float(nextParts[1]));
			}
			ifscanner.close();
			ifstream.close();
		} catch (Exception e) {
			System.out.println(e);
		}
		return parameters;
	}
}
