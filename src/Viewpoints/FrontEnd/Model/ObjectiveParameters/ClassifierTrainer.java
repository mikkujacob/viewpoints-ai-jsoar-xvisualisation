package Viewpoints.FrontEnd.Model.ObjectiveParameters;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import Viewpoints.FrontEnd.Shared.Pair;

import java.lang.Class;

public class ClassifierTrainer {
	private static final String EMOTION_SAMPLES = "emotion_samples";
	
	private static boolean isParam(String gestureFileName) {
		return gestureFileName.length() > 6 || !gestureFileName.substring(gestureFileName.length() - 6, gestureFileName.length()).equals(".param");
	}
	
	private static ArrayList<Pair<String, String>> getLabelTable() {
		ArrayList<Pair<String, String>> labelTable = new ArrayList<Pair<String, String>>();
		File emotionSamplesDir = new File(EMOTION_SAMPLES);
		for (File emotionCategoryDir : emotionSamplesDir.listFiles()) {
			if (!emotionCategoryDir.isDirectory())
				continue;
			for (String gestureFileName : emotionCategoryDir.list()) {
				if (!isParam(gestureFileName)) 
					continue;
				String fullname = EMOTION_SAMPLES+"/"+emotionCategoryDir.getName()+"/"+gestureFileName;
				labelTable.add(Pair.of(fullname, emotionCategoryDir.getName()));
			}
		}
		return labelTable;
	}
	
	public static void trainClassifier(Classifier classifier) {
		for (Pair<String, String> labelTableRecord : getLabelTable()) {
			classifier.trainOnSample(labelTableRecord.first, labelTableRecord.second);
		}
	}
	
	private static ArrayList<String> getTrueLabels(ArrayList<Pair<String, String>> labelTable) {
		ArrayList<String> trueLabels = new ArrayList<String>();
		for (Pair<String, String> labelTableRecord : labelTable) {
			trueLabels.add(labelTableRecord.second);
		}
		return trueLabels;
	}
	
	private static void analyzeResponses(ArrayList<String> trueLabels, ArrayList<String> responses) {
		Set<String> allLabels = new HashSet<String>(trueLabels);
		int correctGuesses = 0;
		for (int i = 0; i < trueLabels.size(); i++) {
			if (trueLabels.get(i).equals(responses.get(i))) {
				correctGuesses++;
			}
		}
		Double accuracy = (double)correctGuesses / (double)trueLabels.size();
		System.out.println("Accuracy: "+accuracy.toString());
		for (String label : allLabels) {
			int truePositives = 0;
			int falsePositives = 0;
			int falseNegatives = 0;
			for (int i = 0; i < trueLabels.size(); i++) {
				if (label.equals(responses.get(i))) {
					if (label.equals(trueLabels.get(i))) {
						truePositives++;
					} else {
						falsePositives++;
					}
				} else if (label.equals(trueLabels.get(i))) {
					falseNegatives++;
				}
			}
			Double precision = (double)truePositives/(double)(truePositives + falsePositives);
			Double recall    = (double)truePositives/(double)(truePositives + falseNegatives);
			Double fmeasure  = 2 * precision * recall / (precision + recall);
			System.out.println(label + " -> Precision: " + precision.toString() + "; Recall: " + recall.toString() + "; F-Measure: " + fmeasure.toString());
		}
	}
	
	public static void printClassifierCV(Class<? extends Classifier> classifierClass, HashMap<String, Object> classifierParameters) {
		ArrayList<Pair<String, String>> labelTable = getLabelTable();
		ArrayList<String> responses = new ArrayList<String>();
		for (int i = 0; i < labelTable.size(); i++) {
			Classifier classifier = null;
			try {
				classifier = classifierClass.newInstance();
			} catch (Exception e) {
				e.printStackTrace();
				return;
			}
			classifier.init(classifierParameters);
			ArrayList<Pair<String, String>> trainData = new ArrayList<Pair<String, String>>();
			for (int j = 0; j < labelTable.size(); j++) {
				if (j != i) {
					trainData.add(labelTable.get(j));
				}
			}
			for (Pair<String, String> trainDataRecord : trainData) {
				classifier.trainOnSample(trainDataRecord.first, trainDataRecord.second);
			}
			HashMap<ParameterKey, Float> parameters = IO.in(labelTable.get(i).first);
			responses.add(classifier.predict(parameters));
		}
		analyzeResponses(getTrueLabels(labelTable), responses);
	}
	
	public static void main(String[] args) {
		HashMap<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("relevant_parameters_file", "emotion_samples/relevant_parameters.txt");
		printClassifierCV(ObjectiveParametersMetric.class, parameters);
	}
}
