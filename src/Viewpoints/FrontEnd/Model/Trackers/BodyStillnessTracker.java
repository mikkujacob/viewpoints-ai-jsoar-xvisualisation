package Viewpoints.FrontEnd.Model.Trackers;

import java.util.HashMap;
import java.util.Map;

import Viewpoints.FrontEnd.Clock.Clock;
import Viewpoints.FrontEnd.Shared.*;


public class BodyStillnessTracker {
	
	private static final JIDX[] RESTRICTED_JSET = JIDX.ALL_JIDX;
	private Map[] trackers = {new HashMap(), new HashMap(), new HashMap()};
	private static final float MIN_DELTA = 15.0f;
	private static final float MAX_STILLNESS = 10f;
	private boolean isStill;
	private boolean forceStill = false;
	private Clock time;
	private int delay;

	
	private class RhythmTracker {
		private int movementSign = 0;
		private float stillnessStart = -1;
		private float prevX = Integer.MAX_VALUE;
		private float stillTime = -1;
		private float moveTime = -1;

		public boolean stillnessCheck(float x, float time) {
			if (Integer.MAX_VALUE == prevX) {
				prevX = x;
				return false;
			}
			float delta = x - prevX;
			prevX = x;
			if (Math.abs(delta) < MIN_DELTA) {
				if (stillnessStart == -1) {
					//System.out.println("Stillness start");
					stillnessStart = time;
				} else if (time - stillnessStart > MAX_STILLNESS) {
					//System.out.println("Stillness overtime");
					movementSign = 0;
					stillnessStart = time;
				}
				stillTime = time - stillnessStart;
				return true;
			}
			stillnessStart = -1;
			int currentMovementSign = (int)Math.signum(delta);
			if (0 == movementSign) {
				//System.out.println("Movement start");
				movementSign = currentMovementSign;
			}
			return false;
		}
		
		public float getStillTime(){
			return stillTime;
		}
	}

	
	public BodyStillnessTracker(){
		for (JIDX jidx : RESTRICTED_JSET) { //JIDX.ALL_JIDX) {
			for (int i = 0; i < 3; i++) {
				trackers[i].put(jidx, new RhythmTracker());
			}
		}
		isStill = true;
		time = new Clock();
		time.iniTime();
		delay = 0;
	}
	
	public void update(Body body) {
		if(forceStill){
			forceStill = false;
			if(isStill){
				delay--;
			}else{
				delay += 2;
			}
			if(!isStill && delay > 20){
				time = new Clock();
				time.iniTime();
				isStill = true;
			}
			time.check();
		}else{
			int stillJoints = 0;
			for (JIDX jidx : RESTRICTED_JSET) { //JIDX.ALL_JIDX) {
				float[] pos = body.get(jidx).array();
				int xyzCounter = 0;
				for (int i = 0; i < 3; i++) {
					if(((RhythmTracker)trackers[i].get(jidx)).stillnessCheck(pos[i], body.getTimestamp())){
						xyzCounter++;
					}
				}
				if(xyzCounter == 3){
					stillJoints++;
				}
			}
			if((double)(stillJoints) / RESTRICTED_JSET.length > .95){
				if(isStill){
					delay--;
				}else{
					delay += 2;
				}
				if(!isStill && delay > 20){
					time = new Clock();
					time.iniTime();
					isStill = true;
				}
				time.check();
			}else{
				if(isStill){
					delay += 2;
				}else{
					delay--;
				}
				if(isStill && delay > 15){
					time = new Clock();
					time.iniTime();
					isStill = false;
				}
				time.check();
			}
		}
		if(delay > 20 || delay < -10)
			delay = 0;
	}
	
	public boolean isStill(){
		return isStill;
	}
	
	public void setStill(){
		forceStill = true;
	}
	
	public void reset(){
		delay = 0;
		time = new Clock();
		time.iniTime();
		isStill = false;
	}
	
	public float getTime(){
		time.check();
		return time.time();
	}
}
