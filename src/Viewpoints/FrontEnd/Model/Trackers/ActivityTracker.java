package Viewpoints.FrontEnd.Model.Trackers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import Viewpoints.FrontEnd.Clock.Clock;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.JIDX;
import processing.core.PVector;

/**
 * This class manages the information related to the activity of a body, including velocity, momentum, and force.
 * @author Lauren
 *
 */

public class ActivityTracker {
	private ArrayList<Body> history = new ArrayList<Body>();
	private Clock time;
	
	private float velocitySum;
	private float accelSum;
	private float kineticEnergySum;
	private float momentumSum;
	private float forceSum;
	//Is true if takePose exited early. For use in segmenters.
	private boolean isExit;

	private final float M = 55;  // weight of user in kg

	private ArrayList<HashMap<JIDX, Float>> vHist = new ArrayList<HashMap<JIDX, Float>>();
	private ArrayList<HashMap<JIDX, Float>> aHist = new ArrayList<HashMap<JIDX, Float>>();
	private ArrayList<Float> forceHist = new ArrayList<Float>();
	private ArrayList<LinkedHashMap<JIDX, float[]>> activityHist = new ArrayList<LinkedHashMap<JIDX, float[]>>();  // 0 - force, 1 - kinetic energy, 2 - momentum
	
	public ActivityTracker() {
		aHist.add(null);
		activityHist.add(null);
		forceHist.add(0f);
	}
	
	public void takePose(Body pose) {
		isExit = false;
		history.add(pose);
		if(history.size() == 1){
			time = new Clock();
			isExit = true;
			return;
		}

		time.check();

		Body curBody = history.get(history.size()-1);
		Body prevBody = history.get(history.size()-2);

		resetSumData();
		HashMap<JIDX, Float> velocity = createVelocityMap(curBody, prevBody, time.deltatime());
		vHist.add(velocity);
		
		if(vHist.size() > 1){
			HashMap<JIDX, Float> accel = createAccelMap(vHist.get(vHist.size()-1), vHist.get(vHist.size()-2), time.deltatime());
			aHist.add(accel);
			activityHist.add(createActivityMap(velocity, accel));
			
			float force = 0;
			for (float[] activity : activityHist.get(activityHist.size()-1).values()) {
				force += activity[0];
			}
			forceHist.add(force * 3);
		}
	}
	
	private void resetSumData() {
		velocitySum = 0;
		kineticEnergySum = 0;
		momentumSum = 0;
		forceSum = 0;
		accelSum = 0;
	}
	
	public boolean isExit() {
		return isExit;
	}

	public ArrayList<HashMap<JIDX, Float>> getVelocityHistory() {
		return vHist;
	}

	public ArrayList<HashMap<JIDX, Float>> getAccelerationHistory() {
		return aHist;
	}
	
	public ArrayList<LinkedHashMap<JIDX, float[]>> getActivityHistory() {
		return activityHist;
	}
	
	public ArrayList<Body> getBodyHistory() {
		return history;
	}
	
	public ArrayList<Float> getForceHistory() {
		return forceHist;
	}

	/**
	 * @return The total velocity sum from all joints.
	 */
	public float getVelocity() {
		return velocitySum;
	}
	
	/**
	 * @param joint The joint at which the velocity is being evaluated.
	 * @return The velocity at a current joint.
	 */
	public float getVelocity(JIDX joint) {
		if(vHist.size() == 0 || vHist.size() == 1)
		{
			return 0f;
		}
		HashMap<JIDX, Float> prevMap = vHist.get(vHist.size() - 1);
		return prevMap.get(joint);
	}
	
	/**
	 * @return The total acceleration sum from all joints.
	 */
	public float getAcceleration() {
		return accelSum;
	}
	
	/**
	 * @param joint The joint at which the acceleration is being evaluated.
	 * @return The acceleration at a current joint.
	 */
	public float getAcceleration(JIDX joint) {
		if(aHist.size() == 0 || aHist.size() == 1)
		{
			return 0f;
		}
		HashMap<JIDX, Float> prevMap = aHist.get(aHist.size() - 1);
		return prevMap.get(joint);
	}
	
	/**
	 * @return The kinetic energy sum across all joints.
	 */
	public float getKineticEnergy() {
		return kineticEnergySum;
	}
	
	/**
	 * @param joint The joint at which the kinetic energy is being evaluated.
	 * @return The kinetic energy from a given joint.
	 */
	public float getKineticEnergy(JIDX joint) {
		if(activityHist.size() == 0 || activityHist.size() == 1)
		{
			return 0f;
		}
		HashMap<JIDX, float[]> prevMap = activityHist.get(activityHist.size() - 1);
		return prevMap.get(joint)[1];
	}

	/**
	 * @return The momentum sum across all joints.
	 */
	public float getMomentum() {
		return momentumSum;
	}

	/**
	 * @param joint The joint at which the momentum is being evaluated.
	 * @return The momentum energy from a given joint.
	 */
	public float getMomentum(JIDX joint) {
		if(activityHist.size() == 0 || activityHist.size() == 1)
		{
			return 0f;
		}
		HashMap<JIDX, float[]> prevMap = activityHist.get(activityHist.size() - 1);
		return prevMap.get(joint)[2];
	}
	
	/**
	 * @return The force sum across all joints.
	 */
	public float getForce() {
		return forceSum;
	}
	
	/**
	 * @param joint The joint at which the force is being evaluated.
	 * @return The force from a given joint.
	 */
	public float getForce(JIDX joint) {
		if(activityHist.size() == 0 || activityHist.size() == 1)
		{
			return 0f;
		}
		HashMap<JIDX, float[]> prevMap = activityHist.get(activityHist.size() - 1);
		return prevMap.get(joint)[0];
	}
	
	/*
	 * Returns a hashmap of each joint mapped to the velocity of that joint. The velocity is calculated using its current position and its previous position.
	 */
	private HashMap<JIDX, Float> createVelocityMap(HashMap<JIDX, PVector> curBody, HashMap<JIDX, PVector> prevBody, float deltaTime) {
		if (curBody.size() != prevBody.size()) {
			throw new Error("trying to take difference of different length skeletons!");
		}
		
		HashMap<JIDX, Float> velocity = new HashMap<JIDX, Float>();
		for (JIDX ji : curBody.keySet()) {
			PVector v = PVector.div(PVector.sub(curBody.get(ji), prevBody.get(ji)), deltaTime);
			velocity.put(ji, (float)(v.mag() / 100));
			velocitySum += v.mag() / 100;
		}
		return velocity;
	}
	
	/*
	 * Returns a hashmap of each joint mapped to the acceleration of that joint. The acceleration is calculated using its current velocity and its previous velocity.
	 */
	private HashMap<JIDX, Float> createAccelMap(HashMap<JIDX, Float> curV, HashMap<JIDX, Float> prevV, float deltaTime) {
		if (curV.size() != prevV.size()) {
			throw new Error("trying to take difference of different length skeletons!");
		}
		
		HashMap<JIDX, Float> accel = new HashMap<JIDX, Float>();
		for (JIDX ji : curV.keySet()) {
			float a = Math.abs((curV.get(ji) - prevV.get(ji)) / deltaTime);
			accel.put(ji, a);
			accelSum += a;
		}
		return accel;
	}
	
	private LinkedHashMap<JIDX, float[]> createActivityMap(HashMap<JIDX, Float> v, HashMap<JIDX, Float> a){
		if (v.size() != a.size()) {
			throw new Error("trying to take difference of different length skeletons!");
		}
		LinkedHashMap<JIDX, float[]> activities = new LinkedHashMap<JIDX, float[]>();
		for (JIDX ji : JIDX.ALL_JIDX) {
			JIDX segment = null;
			float mass = 0;
			float velocity, accel;
			switch(ji){
				case HEAD:
					mass = 0.0397f * M + 2.4f;
					segment = JIDX.HEAD;
					velocity = v.get(JIDX.HEAD);
					accel = a.get(JIDX.HEAD);
					activities.put(segment, getActivityArray(mass, velocity, accel));
					break;
				case NECK: //neck and torso
					mass = 0.75f * (0.5640f * M - 4.66f);  
					segment = JIDX.NECK;
					velocity = (v.get(JIDX.NECK) + v.get(JIDX.TORSO)) / 2.0f;
					accel = (a.get(JIDX.NECK) + a.get(JIDX.TORSO)) / 2.0f;
					activities.put(segment, getActivityArray(mass, velocity, accel));
					break;
				case TORSO: //waist 
					mass = 0.25f * (0.5640f * M - 4.66f); 
					segment = JIDX.TORSO;
					velocity = (v.get(JIDX.RIGHT_HIP) + v.get(JIDX.LEFT_HIP) + v.get(JIDX.TORSO)) / 3.0f;
					accel = (a.get(JIDX.RIGHT_HIP) + a.get(JIDX.LEFT_HIP) + a.get(JIDX.TORSO)) / 3.0f;
					activities.put(segment, getActivityArray(mass, velocity, accel));
					break;
				case LEFT_SHOULDER: //upper arm 
					mass = 0.0274f * M - 0.01f; 
					segment = JIDX.LEFT_SHOULDER;
					velocity = (v.get(JIDX.LEFT_SHOULDER) + v.get(JIDX.LEFT_ELBOW)) / 2.0f;
					accel = (a.get(JIDX.LEFT_SHOULDER) + a.get(JIDX.LEFT_ELBOW)) / 2.0f;
					activities.put(segment, getActivityArray(mass, velocity, accel));
					break;
				case LEFT_ELBOW: //lower arm and hand
					mass = 0.85f * (0.0233f * M - 0.01f); 
					segment = JIDX.LEFT_ELBOW;
					velocity = (v.get(JIDX.LEFT_ELBOW) + v.get(JIDX.LEFT_FINGERTIP) + v.get(JIDX.LEFT_HAND)) / 3.0f;
					accel = (a.get(JIDX.LEFT_ELBOW) + a.get(JIDX.LEFT_FINGERTIP) + a.get(JIDX.LEFT_HAND)) / 3.0f;
					activities.put(segment, getActivityArray(mass, velocity, accel));
					break;
				case LEFT_HAND: //hand  
					mass = 0.15f * (0.0233f * M - 0.01f);   
					segment = JIDX.LEFT_HAND;
					velocity = (v.get(JIDX.LEFT_HAND) + v.get(JIDX.LEFT_FINGERTIP)) / 2.0f;
					accel = (a.get(JIDX.LEFT_HAND) + a.get(JIDX.LEFT_FINGERTIP)) / 2.0f;
					activities.put(segment, getActivityArray(mass, velocity, accel));
					break;
				case RIGHT_SHOULDER: //upper arm 
					mass = 0.0274f * M - 0.01f;  
					segment = JIDX.RIGHT_SHOULDER;
					velocity = (v.get(JIDX.RIGHT_SHOULDER) + v.get(JIDX.RIGHT_ELBOW)) / 2.0f;
					accel = (a.get(JIDX.RIGHT_SHOULDER) + a.get(JIDX.RIGHT_ELBOW)) / 2.0f;
					activities.put(segment, getActivityArray(mass, velocity, accel));
					break;
				case RIGHT_ELBOW: //lower arm and hand
					mass = 0.85f * (0.0233f * M - 0.01f);     
					segment = JIDX.RIGHT_ELBOW;
					velocity = (v.get(JIDX.RIGHT_ELBOW) + v.get(JIDX.RIGHT_HAND) + v.get(JIDX.RIGHT_FINGERTIP)) / 3.0f;
					accel = (a.get(JIDX.RIGHT_ELBOW) + a.get(JIDX.RIGHT_HAND) + a.get(JIDX.RIGHT_FINGERTIP)) / 3.0f;
					activities.put(segment, getActivityArray(mass, velocity, accel));
					break;
				case RIGHT_HAND: //hand
					mass = 0.15f * (0.0233f * M - 0.01f);   
					segment = JIDX.RIGHT_HAND;
					velocity = (v.get(JIDX.RIGHT_HAND) + v.get(JIDX.RIGHT_FINGERTIP)) / 2.0f;
					accel = (a.get(JIDX.RIGHT_HAND) + a.get(JIDX.RIGHT_FINGERTIP)) / 2.0f;
					activities.put(segment, getActivityArray(mass, velocity, accel));
					break;
				case LEFT_HIP: //thigh  
					mass = 0.1159f * M - 1.02f;
					segment = JIDX.LEFT_HIP;  
					velocity = (v.get(JIDX.LEFT_HIP) + v.get(JIDX.LEFT_KNEE)) / 2.0f;
					accel = (a.get(JIDX.LEFT_HIP) + a.get(JIDX.LEFT_KNEE)) / 2.0f;
					activities.put(segment, getActivityArray(mass, velocity, accel));
					break;
				case LEFT_KNEE: //shank   
					mass =  0.0452f * M + 0.82f;
					segment = JIDX.LEFT_KNEE;  
					velocity = (v.get(JIDX.LEFT_KNEE) + v.get(JIDX.LEFT_FOOT)) / 2.0f;
					accel = (a.get(JIDX.LEFT_KNEE) + a.get(JIDX.LEFT_FOOT)) / 2.0f;
					activities.put(segment, getActivityArray(mass, velocity, accel));
					break;
				case LEFT_FOOT: //foot  
					mass = 0.0069f * M + 0.47f;
					segment = JIDX.LEFT_FOOT;
					velocity = v.get(JIDX.LEFT_FOOT);
					accel = a.get(JIDX.LEFT_FOOT);
					activities.put(segment, getActivityArray(mass, velocity, accel));
					break;
				case RIGHT_HIP: //thigh
					mass = 0.1159f * M - 1.02f;
					segment = JIDX.RIGHT_HIP;  
					velocity = (v.get(JIDX.RIGHT_HIP) + v.get(JIDX.RIGHT_KNEE)) / 2.0f;
					accel = (a.get(JIDX.RIGHT_HIP) + a.get(JIDX.RIGHT_KNEE)) / 2.0f;
					activities.put(segment, getActivityArray(mass, velocity, accel));
					break;
				case RIGHT_KNEE:  //shank   
					mass = 0.0452f * M + 0.82f;
					segment = JIDX.RIGHT_KNEE;  
					velocity = (v.get(JIDX.RIGHT_KNEE) + v.get(JIDX.RIGHT_FOOT)) / 2.0f;
					accel = (a.get(JIDX.RIGHT_KNEE) + a.get(JIDX.RIGHT_FOOT)) / 2.0f;
					activities.put(segment, getActivityArray(mass, velocity, accel));
					break;
				case RIGHT_FOOT: //foot
					mass = 0.0069f * M + 0.47f;
					segment = JIDX.RIGHT_FOOT;
					velocity = v.get(JIDX.RIGHT_FOOT);
					accel = a.get(JIDX.RIGHT_FOOT);
					activities.put(segment, getActivityArray(mass, velocity, accel));
					break;
			default:
				break;
			}
		}
		return activities;
	}
	
	private float[] getActivityArray(float mass, float velocity, float acceleration) {
		float activity[] = {0, 0, 0};  //0 - force, 1 - kinetic energy, 2 - momentum
		activity[0] = mass * acceleration;
		activity[1] = 0.5f * mass * (velocity * velocity);
		activity[2] = mass * velocity;
		forceSum += activity[0];
		kineticEnergySum += activity[1];
		momentumSum += activity[2];
		return activity;
	}
	
	public void reset() {
		history.clear();
		activityHist.clear();
		vHist.clear();
		aHist.clear();
		time = null;
		forceHist.clear();
		aHist.add(null);
		activityHist.add(null);
		forceHist.add(null);
	}
}
