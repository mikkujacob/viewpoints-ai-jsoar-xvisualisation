package Viewpoints.FrontEnd.Model.Trackers;

import java.util.ArrayList;

import Viewpoints.FrontEnd.Shared.Body;

/**
 * Interface for classes that track aggregate statistics / properties.
 * @author mikhail.jacob
 *
 */
public interface AggregateTracker {
	void takePose(Body pose);
	void takePoses(ArrayList<Body> poses);
	void reset();
	void requestClassification();
	boolean isTracking();
	Object getValue(String key);
}
