package Viewpoints.FrontEnd.Model.Trackers;

//import java.io.*;
import java.util.*;

import Viewpoints.FrontEnd.Model.ObjectiveParameters.ObjectiveParameterMonitor;
import Viewpoints.FrontEnd.Model.ObjectiveParameters.ParameterKey;
import Viewpoints.FrontEnd.Model.ObjectiveParameters.TargetKeyset;
import Viewpoints.FrontEnd.Model.VAIClassifier.Algorithm;
import Viewpoints.FrontEnd.Model.VAIClassifier.Data;
import Viewpoints.FrontEnd.Model.VAIClassifier.VAILearner;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.MathUtilities;

public class ClassifierViewpointsTracker implements ViewpointsTracker {
//	private static final String MAC_ANACONDA_PATH = System.getProperty("user.home") + File.separator + "anaconda" + File.separator + "bin" + File.separator;
//	private static final String VIEWPOINTS_EXPERT_PATH = "/emotion_expert/ViewpointsExpert.py";
	private static final float CHECK_PERIOD = 0.5f;
	private static final float HISTORY_LENGTH = 1.5f;
	private static final float CHARACTERISTIC_UPDATE_TIME = 0.5f;
	
	private ArrayList<Body> history = new ArrayList<Body>();
	private float lastCheckTime = -1.0f;
	private float energyValue = 0.5f;
	private float smoothnessValue = 0.5f;
	
	private ClassifierCommThread classifierCommThread = null;

	private static List<VAILearner> classifiers;
	
	public ClassifierViewpointsTracker() {
//	
		classifiers = new ArrayList<VAILearner>();
		
		for(Data type : Data.values()) {
			try {
//				classifiers.add(VAILearner.create(type, Algorithm.RANDOM_FOREST));
//				classifiers.add(VAIClassifier.create(type, Algorithm.KMEANS));
				if(type == Data.BOUNDARY){
					continue;
				}
				if(type == Data.ENERGY){
					classifiers.add(VAILearner.create(type, Algorithm.RANDOM_FOREST));
				}
				else
					classifiers.add(VAILearner.create(type, Algorithm.EM));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		try {
			classifierCommThread = new ClassifierCommThread();
			classifierCommThread.start();
		} catch (Exception e) { 
			e.printStackTrace();
		}
	}

	@Override
	public void takePose(Body pose) {
		history.add(pose);
		
		if (-1.0f == lastCheckTime) {
			lastCheckTime = pose.getTimestamp();
		}
		
		if (pose.getTimestamp() - lastCheckTime >= CHECK_PERIOD) {
			lastCheckTime = pose.getTimestamp();
			truncateHistory();
			classifierCommThread.requestClassificationOf(new ArrayList<Body>(history));
		}
		
		updateViewpoints();
	}
	
	private void truncateHistory() {
		float farEndTime = lastCheckTime - HISTORY_LENGTH;
		int cutIndex = 0;
		for (; cutIndex < history.size(); cutIndex++) {
			if (history.get(cutIndex).getTimestamp() > farEndTime) break;
		}
		history = new ArrayList<Body>(history.subList(cutIndex, history.size()));
	}
	
	private void updateViewpoints() {
		if (history.size() < 2) {
//			System.out.println("Energy classifier warning: short history!");
			return;
		}
		
		Body last = history.get(history.size() - 1);
		Body prelast = history.get(history.size() - 2);
		
		float deltatime = last.getTimestamp() - prelast.getTimestamp();
		energyValue = MathUtilities.exponentialDrift(energyValue, 
													 classifierCommThread.targetEnergy(), 
													 deltatime,
													 CHARACTERISTIC_UPDATE_TIME);
		smoothnessValue = MathUtilities.exponentialDrift(smoothnessValue, 
				 										 classifierCommThread.targetSmoothness(), 
				 										 deltatime,
				 										 CHARACTERISTIC_UPDATE_TIME);
//		System.out.println(energyValue);
//		System.out.println(smoothnessValue);
//		System.out.println(classifierCommThread.targetEnergy());
//		System.out.println(classifierCommThread.targetSmoothness());
	}

	@Override
	public void reset() {
		energyValue = 0.5f;
		classifierCommThread.reset();
	}

	@Override
	public boolean isTracking() {
		return true;
	}

	@Override
	public float getEnergy() {
		return energyValue;
	}

	@Override
	public float getSmoothness() {
		return smoothnessValue;
	}
	
	private static class ClassifierCommThread extends Thread {
		private ObjectiveParameterMonitor objectiveParametersMonitor = new ObjectiveParameterMonitor(TargetKeyset.getFullKeyset());
		
//		private Process viewpointsExpert = null;
//		private PrintStream viewpointsExpertInput = null;
//		private Scanner viewpointsExpertOutput = null;
		//private Scanner viewpointsExpertErr = null;
		
		private ArrayList<Body> request = null;
		private float targetEnergy = 0.5f;
		private float targetSmoothness = 0.5f;
		
		private boolean isReset = false;
		
		public ClassifierCommThread() throws Exception {
//			String viewpointsExpertCommand = null;
//			
//			String PROJECT_HOME = System.getProperty("user.dir");
//			
//			if (System.getProperty("os.name").toLowerCase().indexOf("windows") == -1)  
//			{
//				viewpointsExpertCommand = MAC_ANACONDA_PATH + "python " + PROJECT_HOME + VIEWPOINTS_EXPERT_PATH;
//			}
//			else
//			{
//				viewpointsExpertCommand = "python " + PROJECT_HOME + VIEWPOINTS_EXPERT_PATH;
//			}
			
//			System.out.println(viewpointsExpertCommand);
			
//			viewpointsExpert = Runtime.getRuntime().exec(viewpointsExpertCommand);
//			
//			viewpointsExpertInput = new PrintStream(viewpointsExpert.getOutputStream());
//			viewpointsExpertOutput = new Scanner(viewpointsExpert.getInputStream());
			//viewpointsExpertErr = new Scanner(viewpointsExpert.getErrorStream());
		}

		public void run() {
			while (true) {
				synchronized(this) {
					while (null == request) {
						try {wait();} catch(InterruptedException e) {}
					}
				}
				
				List<Float> received = classifyViewpoints();
				
				synchronized(this) {
					request = null;
					if (!isReset && null != received && received.size() >= 6) {
						//System.out.println("target: " + Float.toString(targetEnergy));
						targetEnergy = received.get(4);
						targetSmoothness = received.get(5);
					} else {
						targetEnergy = 0.5f;
						targetSmoothness = 0.5f;
					}
				}
			}
		}
		
		private List<Float> classifyViewpoints() {
			HashMap<ParameterKey, Float> parameters = objectiveParametersMonitor.getParameters(request);

			if (null != parameters) {
				List<Float> responseValues = new ArrayList<Float>();
				//TODO: Adding new classifier here
				List<Float> parameters_values = new ArrayList<Float>(parameters.values());
				parameters_values.add(.0f);
//				StringBuilder result = new StringBuilder();
				int count = 0;
				for(VAILearner classifier : classifiers) {
					try {
						
//						System.out.println(Arrays.toString(classifier.classifyWithFilter(parameters_values.toArray(new Float[0]))));
						if(count == 4){
							String[] response = classifier.getDistWithString(parameters_values.toArray(new Float[0]));
//							String[] subparts = response.split(" ");
							float val = 0f * Float.parseFloat(response[5]) + .5f * Float.parseFloat(response[3]) + 1f * Float.parseFloat(response[1]);
							//System.out.println(val);
							/*for(String str : response)
								System.out.println(str + " ");
							System.out.println();*/
							/*switch(subparts[1]){
								case "normal-energy-golden":
									val = .5f;
									break;
								case "low-energy-golden":
									val = 0f;
									break;
								case "high-energy-golden":
									val = 1f;
									break;
							}*/
							responseValues.add(val);
							//System.out.println(val);
							/*double[] a = classifier.classifyWithFilter(parameters_values.toArray(new Float[0]));
							for(double i : a)
								System.out.print("         " + i );
							System.out.println();*/
						}else{
							String response = "";
							String[] subparts = null;
							
							if(classifier != null && parameters_values != null)
							{
								response = classifier.classifyWithIndexToString(parameters_values.toArray(new Float[0]));
								if(response != null)
								{
//									System.out.println("Response: " + response.toString());
									subparts = response.split(" ");
									if(subparts != null && subparts.length >= 2)
									{
//										System.out.println("Subparts: [" + subparts[0].toString() + ", " + subparts[1].toString() + "]");
										responseValues.add(Float.valueOf(subparts[1]) / 2.0f);
									}
								}
							}
						}
						count++;
//						response = classifier.classifyWithConfidenceToString(parameters_values.toArray(new Float[0]));
//						result.append(response + ",");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
							
//				String[] filter = new String[]{"angry", "fear", "joy", "sad"};
//				String preprocessedResult = new String(result.deleteCharAt(result.length() - 1));
//				System.out.println(VAIClassifier.postProcessEmotions(preprocessedResult, filter));
//				System.out.println(responseValues);
				return responseValues;
			} else {
				return null;
			}
		
			
//			if (null != parameters) {
////				System.out.println("Viewpoints Parameters:");
////				IO.outToStream(System.out, parameters);
//				IO.outToStream(viewpointsExpertInput, parameters);
//				try {
//					viewpointsExpert.getOutputStream().flush();
//				} catch (IOException e1) {
//					e1.printStackTrace();
//				}
//				String response = viewpointsExpertOutput.nextLine();
//				response = response.trim();
////				System.out.println(response);
//				String[] responseParts = response.split(",");
//				for (String responsePart : responseParts) {
//					responsePart = responsePart.trim();
//					String[] subparts = responsePart.split(" ");
//					responseValues.add(Float.valueOf(subparts[1]) / 2.0f);
//				}
//				return responseValues;
//			} else {
//				return null;
//			}
		}
		
		public void requestClassificationOf(ArrayList<Body> history) {
			synchronized(this) {
				isReset = false;
				if (null != request) return;
				request = history;
				notifyAll();
			}
		}
		
		public float targetEnergy() {
			synchronized(this) {
				return targetEnergy;
			}
		}
		
		public float targetSmoothness() {
			synchronized(this) {
				return targetSmoothness;
			}
		}
		
		public void reset() {
			synchronized(this) {
				isReset = true;
				targetEnergy = 0.5f;
				targetSmoothness = 0.5f;
			}
		}
	}
}
