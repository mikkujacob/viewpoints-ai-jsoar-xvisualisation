package Viewpoints.FrontEnd.Model.Trackers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.JIDX;

/**
 * This class tracks the rhythm of a body.
 * 
 * @author Lauren
 */

public class RhythmTracker {

	private static final float MIN_SEGMENT_DURATION = 0.5f;
	private static final JIDX[] RESTRICTED_JSET = JIDX.ALL_JIDX;
	private static final float MIN_DEVIATION = 150.0f;
	private static final float MIN_DELTA = 20.0f;
	private static final float MAX_STILLNESS = 0.5f;
	private static final float PERIOD_PRECISION = 1.75f;
	private static final float PERIOD_CONSERVATISM = 0.8f;
	private static final int MIN_HALFPERIODS = 4;
	private float phase = 0.0f;
	private float period = Float.MAX_VALUE;
	private float lastTime = -1.0f;

	private boolean hadRhythm = false;
	private boolean havingRhythm = false;
	
	private Map[] trackers = {new HashMap(), new HashMap(), new HashMap()};
	
	public RhythmTracker() {
		for (JIDX jidx : RESTRICTED_JSET) { //JIDX.ALL_JIDX) {
			for (int i = 0; i < 3; i++) {
				trackers[i].put(jidx, new JointRhythmTracker());
			}
		}
	}
	
	private void updateJointTrackers(Body body) {
		for (JIDX jidx : RESTRICTED_JSET) { //JIDX.ALL_JIDX) {
			float[] pos = body.get(jidx).array();
			for (int i = 0; i < 3; i++) {
				((JointRhythmTracker)trackers[i].get(jidx)).takePosition(pos[i], body.getTimestamp());
			}
		}
	}
	
	public void takePose(Body body) {
		updateJointTrackers(body);
		checkRhythm();
		updatePhase(body.getTimestamp());
		lastTime = body.getTimestamp();
	}
	
	private void checkRhythm() {
		hadRhythm = havingRhythm;
		havingRhythm = false;
		period = Float.MAX_VALUE;
		
		for (int i = 0; i < 3; i++) {
			Map<JIDX, JointRhythmTracker> dimensionTrackers = (Map<JIDX, JointRhythmTracker>)trackers[i];
			for (JointRhythmTracker tracker : dimensionTrackers.values()) {
				if (tracker.isPeriodic()) {
					havingRhythm = true;
					float localPeriod = tracker.getPeriod();
					if (localPeriod < period) {
						period = localPeriod;
					}
				}
			}
		}
	}
	
	private void updatePhase(float time) {
		if (lastTime > 0 && havingRhythm) {
			float deltatime = time - lastTime;
			phase += 2 * Math.PI * deltatime / period;
		}
	}
	
	public void collectActiveBeats(ArrayList<Float> oActiveBeats) {
		for (int i = 0; i < 3; i++) {
			Map<JIDX, JointRhythmTracker> dimensionTrackers = (Map<JIDX, JointRhythmTracker>)trackers[i];
			for (JointRhythmTracker rhythmTracker : dimensionTrackers.values()) {
				if (rhythmTracker.isBeatMoment()) {
					if (rhythmTracker.getPeriod() > MIN_SEGMENT_DURATION) {
						oActiveBeats.add(rhythmTracker.getPeriod());
					}
				}
			}
		}
	}
	
	public boolean hasRhythm() {
		return havingRhythm;
	}
	
	public boolean rhythmJustStarted() {
		return !hadRhythm && havingRhythm;
	}
	
	public boolean rhythmJustEnded() {
		return !havingRhythm && hadRhythm;
	}
	
	public ArrayList<String> getPeriodicityLocations() {
		ArrayList<String> locations = new ArrayList<String>();
		for (int i = 0; i < 3; i++) {
			Map<JIDX, JointRhythmTracker> dimensionTrackers = (Map<JIDX, JointRhythmTracker>)trackers[i];
			for (Map.Entry<JIDX, JointRhythmTracker> entry : dimensionTrackers.entrySet()) {
				if (entry.getValue().isPeriodic()) {
					locations.add(entry.getKey().toString() + " " + Integer.toString(i));
				}
			}
		}
		return locations;
	}
	
	public float getPeriod() {
		return period;
	}
	
	public float getPhase() {
		return phase;
	}
	
	public String getSampleStatus() {
		JointRhythmTracker sampleTracker = (JointRhythmTracker)(trackers[2].get(JIDX.LEFT_HAND));
		return sampleTracker.getStatus();
	}
	
	/**
	 * Tracks the rhythm of an individual joint.
	 */
	private class JointRhythmTracker {
		private int movementSign = 0;
		private float period = Float.MAX_VALUE;
		private int halfperiods = 0;
		private float turnTime = 0;
		private float turnX = 0;
		private float beatTime = 0;
		private float stillnessStart = -1;
		private float prevX = Integer.MAX_VALUE;
		private float dbgdelta = 0;
		private float dbgdev = 0;
		private boolean loud = false;
		private boolean beatMoment = false;
		
//		public void setLoud(boolean loud) {
//			this.loud = loud;
//		}
		
		private void resetCycle(float time, float x) {
			turnX = x;
			halfperiods = 0;
			beatTime = time;
			turnTime = time;
			period = Float.MAX_VALUE;
		}
		
		public void takePosition(float x, float time) {
			beatMoment = false;
			if (Integer.MAX_VALUE == prevX) {
				prevX = x;
				return;
			}
			float delta = x - prevX;
			prevX = x;
			dbgdev = x - turnX;
			dbgdelta = delta;
			if (0 != movementSign &&
				time - turnTime > PERIOD_PRECISION * period) {
				if (loud) System.out.println("Halfperiod overtime");
				resetCycle(turnTime, x);
			}
			if (Math.abs(delta) < MIN_DELTA) {
				if (stillnessStart == -1) {
					//System.out.println("Stillness start");
					stillnessStart = time;
				} else if (time - stillnessStart > MAX_STILLNESS) {
					//System.out.println("Stillness overtime");
					resetCycle(time, x);
					movementSign = 0;
					stillnessStart = time;
				}
				return;
			}
			stillnessStart = -1;
			int currentMovementSign = (int)Math.signum(delta);
			if (0 == movementSign) {
				//System.out.println("Movement start");
				movementSign = currentMovementSign;
				resetCycle(time, x);
				return;
			}
			if (currentMovementSign != movementSign) {
				if (Math.abs(x - turnX) < MIN_DEVIATION && halfperiods < MIN_HALFPERIODS) {
					if (loud) System.out.println("Underdeviation: " + Math.abs(x - turnX));
					resetCycle(time, x);
					return;
				}
				turnX = x;
				turnTime = time;
				halfperiods++;
				movementSign = currentMovementSign;
				if (halfperiods % 2 == 0) {
					if (2 == halfperiods) {
						//System.out.println("Period establised");
						period = time - beatTime;
					} else {
						if (time - beatTime < period / PERIOD_PRECISION && halfperiods < MIN_HALFPERIODS) {
							if (loud) System.out.println("Halfperiod undertime");
							resetCycle(time, x);
						} else {
							//System.out.println("Period confirmed");
							period = PERIOD_CONSERVATISM * period + (1 - PERIOD_CONSERVATISM) * (time - beatTime);
						}
					}
					beatMoment = true;
					beatTime = time;
				}
			}
		}
		
		public boolean isBeatMoment() {
			return beatMoment;
		}
		
		public float getPeriod() {
			return period;
		}

		public boolean isPeriodic() {
			return halfperiods >= MIN_HALFPERIODS;
		}
		
		public String getStatus() {
			return String.format("MovementSign: %d\nDeviation: %.2f\nHalfperiods: %d\nDelta: %.2f", movementSign, dbgdev, halfperiods, dbgdelta);
		}
		
	}
}
