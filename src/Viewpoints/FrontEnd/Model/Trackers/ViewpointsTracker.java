package Viewpoints.FrontEnd.Model.Trackers;

import Viewpoints.FrontEnd.Shared.Body;

public interface ViewpointsTracker {
	void takePose(Body pose);
	void reset();
	boolean isTracking();
	float getEnergy();
	float getSmoothness();
}
