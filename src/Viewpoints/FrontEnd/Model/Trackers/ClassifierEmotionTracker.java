package Viewpoints.FrontEnd.Model.Trackers;

//import java.io.*;
import java.util.*;

import Viewpoints.FrontEnd.Model.ObjectiveParameters.ObjectiveParameterMonitor;
import Viewpoints.FrontEnd.Model.ObjectiveParameters.ParameterKey;
import Viewpoints.FrontEnd.Model.ObjectiveParameters.TargetKeyset;
import Viewpoints.FrontEnd.Model.VAIClassifier.Algorithm;
import Viewpoints.FrontEnd.Model.VAIClassifier.Data;
import Viewpoints.FrontEnd.Model.VAIClassifier.VAILearner;
import Viewpoints.FrontEnd.Shared.Body;

public class ClassifierEmotionTracker implements AggregateTracker
{
//	private static final String MAC_ANACONDA_PATH = System.getProperty("user.home") + File.separator + "anaconda" + File.separator + "bin" + File.separator;
//	private static final String EMOTIONS_EXPERT_PATH = "/emotion_expert_twp/EmotionExpert.py";

	private ArrayList<Body> history = new ArrayList<Body>();
	private String emotionValue = "none";

	private ClassifierCommThread classifierCommThread = null;

	private static List<VAILearner> classifiers;
	
	public ClassifierEmotionTracker()
	{
		classifiers = new ArrayList<VAILearner>();
		
		for(Data type : new Data[]{Data.ANGRY, Data.FEAR, Data.JOY, Data.SAD}) {
			try {
				classifiers.add(VAILearner.create(type, Algorithm.RANDOM_FOREST));
				if(type == Data.BOUNDARY){
					continue;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		try
		{
			classifierCommThread = new ClassifierCommThread();
			classifierCommThread.start();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void takePose(Body pose)
	{
		history.add(pose);
	}
	
	@Override
	public void takePoses(ArrayList<Body> poses)
	{
		history.addAll(poses);
	}

	private void truncateHistory()
	{
		history.clear();
	}

	@Override
	public void reset()
	{
		emotionValue = "none";
		classifierCommThread.reset();
	}

	@Override
	public void requestClassification()
	{
		classifierCommThread.requestClassificationOf(new ArrayList<Body>(history));
		truncateHistory();
		emotionValue = classifierCommThread.targetEmotion();
	}

	@Override
	public boolean isTracking()
	{
		return true;
	}

	@Override
	public String getValue(String key)
	{
		if(key == null)
		{
			return "";
		}
		else if(key.equalsIgnoreCase("emotion"))
		{
			return emotionValue;
		}
		return "";
	}

	private static class ClassifierCommThread extends Thread
	{
		private ObjectiveParameterMonitor objectiveParametersMonitor = new ObjectiveParameterMonitor(TargetKeyset.getFullKeyset());

//		private Process emotionsExpert = null;
//		private PrintStream emotionsExpertInput = null;
//		private Scanner emotionsExpertOutput = null;
//		private Scanner emotionsExpertErr = null;

		private ArrayList<Body> request = null;
		private String targetEmotion = "none";

		private boolean isReset = false;

		private static final String[] trackedEmotionsFilter = new String[]{"angry", "fear", "joy", "sad"};

		public ClassifierCommThread() throws Exception
		{
//			String emotionsExpertCommand = null;
//
//			String PROJECT_HOME = System.getProperty("user.dir");
//
//			if (System.getProperty("os.name").toLowerCase().indexOf("windows") == -1)
//			{
//				emotionsExpertCommand = MAC_ANACONDA_PATH + "python " + PROJECT_HOME + EMOTIONS_EXPERT_PATH;
//			}
//			else
//			{
//				emotionsExpertCommand = "python " + PROJECT_HOME + EMOTIONS_EXPERT_PATH;
//			}
//
//			System.out.println(emotionsExpertCommand);
//
//			emotionsExpert = Runtime.getRuntime().exec(emotionsExpertCommand);
//
//			emotionsExpertInput = new PrintStream(emotionsExpert.getOutputStream());
//			emotionsExpertOutput = new Scanner(emotionsExpert.getInputStream());
//			emotionsExpertErr = new Scanner(emotionsExpert.getErrorStream());
		}

		public void run()
		{
			while (true)
			{
				synchronized (this)
				{
					while (null == request)
					{
						try
						{
							wait();
						}
						catch (InterruptedException e)
						{
						}
					}
				}

				List<String> received = classifyEmotions();

				synchronized (this)
				{
					request = null;
					if (!isReset && null != received)
					{
						targetEmotion = received.get(0);
					}
					else
					{
						targetEmotion = "none";
					}
				}
			}
		}

		private List<String> classifyEmotions()
		{
			HashMap<ParameterKey, Float> parameters = objectiveParametersMonitor.getParameters(request);
			if (null != parameters)
			{
				//TODO: Adding new classifier here
				List<Float> parameters_values = new ArrayList<Float>(parameters.values());
				parameters_values.add(.0f);
				StringBuilder result = new StringBuilder();
				for(VAILearner classifier : classifiers) {
					try {
		//				System.out.println(Arrays.toString(classifier.classifyWithFilter(parameters_values.toArray(new Float[0]))));
//						String response = classifier.classifyWithIndexToString(parameters_values.toArray(new Float[0]));
//						String[] subparts = response.split(" ");
						String  response = classifier.classifyWithConfidenceToString(parameters_values.toArray(new Float[0]));
						result.append(response + ",");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				
				String preprocessedResult = new String(result.deleteCharAt(result.length() - 1));
				String processedResult = VAILearner.postProcessOutput(preprocessedResult, trackedEmotionsFilter).split(" ")[0];

				List<String> responseValues = new ArrayList<String>();
				if(processedResult.contains("No_emotion_detected"))  
					responseValues.add("none");
				else 
					responseValues.add(processedResult);

//				System.out.println(responseValues);
				return responseValues;
				} else {
					return null;
				}
				
////				System.out.println("Emotions Parameters:");
////				IO.outToStream(System.out, parameters);
//				IO.outToStream(emotionsExpertInput, parameters);
//				String response = "";
//				try
//				{
//					emotionsExpert.getOutputStream().flush();
//				}
//				catch (IOException e1)
//				{
//					e1.printStackTrace();
//				}
//				
//				try
//				{
////					System.out.println("Waiting on response");
//					if(emotionsExpertOutput.hasNextLine())
//					{
//						response = emotionsExpertOutput.nextLine();
////						System.out.println("Response: " + response);
//					}
//				}
//				catch (Exception e)
//				{
//					e.printStackTrace();
//				}
//				
//				response = response.trim();
////				System.out.println(response);
//				String[] responseParts = response.split(" ");
//				List<String> responseValues = new ArrayList<String>();
//				for (String responsePart : responseParts)
//				{
//					responsePart = responsePart.trim();
//					responseValues.add(responsePart);
//				}
//				return responseValues;
//			}
//			else
//			{
//				return null;
//			}
		}

		public void requestClassificationOf(ArrayList<Body> history)
		{
			synchronized (this)
			{
				isReset = false;
				if (null != request)
					return;
				request = history;
				notifyAll();
			}
		}

		public String targetEmotion()
		{
			synchronized (this)
			{
				return targetEmotion;
			}
		}

		public void reset()
		{
			synchronized (this)
			{
				isReset = true;
				targetEmotion = "none";
			}
		}
	}
}