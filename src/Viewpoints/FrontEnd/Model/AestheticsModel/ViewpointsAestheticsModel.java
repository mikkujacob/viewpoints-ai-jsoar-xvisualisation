package Viewpoints.FrontEnd.Model.AestheticsModel;

import Viewpoints.FrontEnd.Model.Trackers.ActivityTracker;
import Viewpoints.FrontEnd.Model.Trackers.AggregateTracker;
import Viewpoints.FrontEnd.Model.Trackers.BodyStillnessTracker;
import Viewpoints.FrontEnd.Model.Trackers.ClassifierEmotionTracker;
import Viewpoints.FrontEnd.Model.Trackers.ClassifierViewpointsPostSegmentationTracker;
import Viewpoints.FrontEnd.Model.Trackers.ClassifierViewpointsTracker;
import Viewpoints.FrontEnd.Model.Trackers.RhythmTracker;
import Viewpoints.FrontEnd.Model.Trackers.ViewpointsTracker;
import Viewpoints.FrontEnd.Shared.Body;
/**
 * Class should maintain all of the various trackers used by Viewpoints.
 * @author Lauren
 *
 */

public class ViewpointsAestheticsModel extends AestheticsModel {
	private static ViewpointsTracker viewpointsTracker = new ClassifierViewpointsTracker(); 
	private static AggregateTracker emotionTracker = new ClassifierEmotionTracker();
	private static ClassifierViewpointsPostSegmentationTracker postSegmentationTracker = new ClassifierViewpointsPostSegmentationTracker();
	private static RhythmTracker rhythmTracker = new RhythmTracker();
	private static ActivityTracker activityTracker = new ActivityTracker();
	private static BodyStillnessTracker humanStillness = new BodyStillnessTracker();
	private static BodyStillnessTracker agentStillness = new BodyStillnessTracker();
	
	private static boolean IS_INIT = false;
	
	public static ViewpointsTracker getViewpointsTracker() {
		return viewpointsTracker;
	}
	
	public static AggregateTracker getEmotionTracker() {
		return emotionTracker;
	}
	
	public static ClassifierViewpointsPostSegmentationTracker getPostSegmentationTracker() {
		return postSegmentationTracker;
	}
	
	public static RhythmTracker getRhythmTracker() {
		return rhythmTracker;
	}
	
	public static ActivityTracker getActivityTracker() {
		return activityTracker;
	}
	
	public static BodyStillnessTracker getHumanStillness() {
		return humanStillness;
	}
	
	public static BodyStillnessTracker getAgentStillness() {
		return agentStillness;
	}
	
	public static void init() {
		if (!IS_INIT) {
			IS_INIT = true;
		}
	}

	/**
	 * Updates all of the trackers before segmentation occurs.
	 * @param body The new body to update with.
	 */
	public static void preSegmentationUpdate(Body user, Body stillnessBody) {
		rhythmTracker.takePose(user);
		activityTracker.takePose(user);
		
		if (stillnessBody != null) {
			humanStillness.update(stillnessBody);
		}
		
	}

	/**
	 * Updates all of the trackers after segmentation occurs.
	 * @param body The new body to update with.
	 */
	public static void postSegmentationUpdate() {
	}
}
