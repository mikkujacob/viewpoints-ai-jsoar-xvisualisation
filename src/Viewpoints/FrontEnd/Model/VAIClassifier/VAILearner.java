package Viewpoints.FrontEnd.Model.VAIClassifier;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import weka.attributeSelection.AttributeTransformer;
import weka.attributeSelection.PrincipalComponents;
import weka.core.Instance;
import weka.core.Instances;

/**
 * 
 * @author adamlab (Luis)
 *
 */
public class VAILearner {
	private Instances				data;
	private GenericLearner			learner;
	private	AttributeTransformer	filter;

	private static final String DATASET = "./new_models/data/TYPE.arff".replace("/", File.separator);
	private static final String LEARNER = "./new_models/learners/ALG_TYPE_50.model".replace("/", File.separator);
	private static final String FILTER = "./new_models/pca/pca_TYPE.model".replace("/", File.separator);
	
	private boolean					isUsingClusterer;
	public VAILearner(String datasetFilename, String modelFilename, String filterModelFilename) throws Exception {
		this.setData(new Instances(readDataFile(datasetFilename)));
		this.getData().setClassIndex(getData().numAttributes() - 1);
        
//		this.learner = new SerializedClassifier();
//		this.learner.setModelFile(new File(modelFilename));
		
		isUsingClusterer = modelFilename.contains("clusterer");
		this.learner = (isUsingClusterer ? ClustererLearner.load(modelFilename) : ClassifierLearner.load(modelFilename));
		if(!modelFilename.contains("boundary"))
			this.filter = (PrincipalComponents) weka.core.SerializationHelper.read(filterModelFilename);
	}

	public static BufferedReader readDataFile(String filename) {
        BufferedReader inputReader = null;
        
        try {
            inputReader = new BufferedReader(new FileReader(filename));
        } catch (FileNotFoundException ex) {
            System.err.println("File not found: " + filename);
        }
        
        return inputReader;
    }
	
	public static VAILearner create(String datasetFilename, String modelFilename, String filterModelFilename) throws Exception {
		
		return new VAILearner(datasetFilename, modelFilename, filterModelFilename);
	}

	public static VAILearner create(Data type, Algorithm algorithm) throws Exception {
		String resultData = getStringDataType(type);

		String resultClassifier = getStringAlgorithm(algorithm);
		
		String datasetFilename = DATASET.replace("TYPE",resultData);
		String classifierModelFilename = LEARNER.replace("ALG", resultClassifier).replace("TYPE",resultData);
		if(type == Data.BOUNDARY)
			classifierModelFilename = "new_models" + File.separator +"learners" + File.separator + "trees_randomForests_smoothedBoundary_50.model";
//			classifierModelFilename = "new_models" + File.separator +"learners" + File.separator + "trees_randomForests_boundary4_50.model";
		String filterModelFilename = FILTER.replace("TYPE",resultData);

		return new VAILearner(datasetFilename, classifierModelFilename, filterModelFilename);
	}

	private static String getStringAlgorithm(Algorithm algorithm) {
		String result = "";
		switch(algorithm){
		case DECISITON_TREE:
			result = "trees_J48";
			break;
		case SMO:
			result = "functions_SMO";
			break;
		case LOGISTIC:
			result = "functions_logistic";
			break;
		case RANDOM_FOREST:
			result = "trees_randomForests";
			break;
		case KMEANS:
			result = "clusterer_kmeans";
			break;
		case EM:
			result = "clusterer_em";
			break;
			
		default:
			break;
		}
		
		return result;
	}

	private static String getStringDataType(Data type) {
		String result = "";
		
		switch(type) {
		case ANGRY:
			result = "Angry";
			break;
		case ENERGY:
			result = "Energy";
			break;
		case FEAR:
			result = "Fear";
			break;
		case JOY:
			result = "Joy";
			break;
		case SAD:
			result = "Sad";
			break;
		case SMOOTH:
			result = "Smooth";
			break;
		case BOUNDARY:
			result = "smoothedBoundary";
		//	result = "boundary4";
			break;
		default:
			break;
		}
		return result;
	}

	public double[] classify(double[] instanceData) throws Exception {
		Instance instance = new Instance(1, instanceData);
		instance.setDataset(getData());
		return learner.distributionForInstance(instance);
	}
	
	public String[] classifyToStringNoFilter(double[] sample) throws Exception{
		Instance instance = new Instance(1, sample);
		instance.setDataset(getData());
		double[] dist = learner.distributionForInstance(instance);
		String[] res = new String[data.numClasses() * 2];
		for(int i = 0; i < dist.length; i++) {
			res[i*2] = data.classAttribute().value(i);
			res[i*2 + 1] = dist[i] + "";
        }	
		return res;
	}

	public Instances getData() {
		return data;
	}

	public void setData(Instances data) {
		this.data = data;
	}
	

	public double[] transformWithFilter(Instance instance) throws Exception {
		return this.filter.convertInstance(instance).toDoubleArray();
	}

	private double[] arrayFloatToDouble(Float[] floatData) {
		double[]  doubleData = new double[floatData.length];
		
		for (int i = 0 ; i < floatData.length; i++)
		{
			doubleData[i] = (double) floatData[i];
		}
		return doubleData;
	}


	public double[] classifyWithFilter(double[] instanceData) throws Exception {
		Instance instance = new Instance(1, instanceData);
		instance.setDataset(getData());
		instance = this.filter.convertInstance(instance);
		if(!isUsingClusterer) instance.setDataset(getData());
		
		return learner.distributionForInstance(instance);
	}

	public double[] classifyWithFilter(Float[] floatData) throws Exception {
		double[] doubleData = arrayFloatToDouble(floatData);
		return classifyWithFilter(doubleData);
	}
	
	public String classifyToString(double[] sample) throws Exception {
		double[] dist = classifyWithFilter(sample);
		double max = 0;
		String maxLabel = "";
		
		for(int i=0; i < data.numClasses(); i++) {
			if (dist[i] > max) {
				dist[i] = max;
				maxLabel = data.classAttribute().value(i);
			}
//        	System.out.println(data.classAttribute().value(i) + " probability : " + dist[i]);
        }
		return maxLabel;
	}
	
	public String classifyToString(Float[] floatData) throws Exception {
		double[] doubleData = arrayFloatToDouble(floatData);
		return classifyToString(doubleData);
	}
	
	public String classifyWithIndexToString(double[] sample) throws Exception {
		double[] dist = classifyWithFilter(sample);
		double max = 0;
		String maxLabel = "";
		for(int i=0; i < data.numClasses(); i++) {
			if (dist[i] > max) {
				max = dist[i];
				//dist[i] = max;
				maxLabel =  data.classAttribute().value(i)+ " " + i;
			}
        	//System.out.print(data.classAttribute().value(i) + " probability : " + dist[i]);
        }	
		return maxLabel;
	}
	
	public String[] getDistWithString(Float[] floatData) throws Exception{
		double[] doubleData = arrayFloatToDouble(floatData);
		return getDistWithString(doubleData);
	}
	
	public String[] getDistWithString(double[] sample) throws Exception {
		double[] dist = classifyWithFilter(sample);
		String[] res = new String[data.numClasses() * 2];
		for(int i = 0; i < data.numClasses(); i++) {
			res[i*2] = data.classAttribute().value(i);
			res[i*2 + 1] = dist[i] + "";
        }	
		return res;
	}
	
	public String classifyWithIndexToString(Float[] floatData) throws Exception {
		double[] doubleData = arrayFloatToDouble(floatData);
		return classifyWithIndexToString(doubleData);
	}

	private String classifyWithConfidenceToString(double[] sample) throws Exception {
		double[] dist = classifyWithFilter(sample);
		double max = 0;
		String maxLabel = "";
		for(int i=0; i < data.numClasses(); i++) {
			if (dist[i] > max) {
				max = dist[i];
				maxLabel =  data.classAttribute().value(i)+ " " + dist[i];
			}
//        	System.out.println(data.classAttribute().value(i) + " probability : " + dist[i]);
        }
		
		return maxLabel;
	}
	
	public String classifyWithConfidenceToString(Float[] floatData) throws Exception {
		double[] doubleData = arrayFloatToDouble(floatData);
		return classifyWithConfidenceToString(doubleData);
	}
	
	public static String postProcessOutput(String preprocessedResult, String[] filter) {
		String result = "No_emotion_detected";
		double confidence = 1.0;
		double maxConfidence = 0;
		String[] subresults = preprocessedResult.split(",");
		StringBuilder processedData = new StringBuilder();
		
		for(String subresult : subresults) {
			String[] split = subresult.split(" ");
			String classification = split[0];
			float dist = Float.parseFloat(split[1]);
			
			boolean found = false;
			for(String word : filter)
				if(classification.contains(word))
					found = true;
					
			if(!found){
				processedData.append(subresult + ", ");
				continue;
			}
			
			if(classification.contains("non")) {
				if(maxConfidence == 0) confidence *= dist;
			}
			else if(maxConfidence < dist) {
				result = classification;
				maxConfidence = confidence = dist;
			}
		}
		
		processedData.append(result + " " + confidence);
		
//		System.out.println(preprocessedResult + Arrays.toString(filter));
		return new String(processedData);
	}
}
