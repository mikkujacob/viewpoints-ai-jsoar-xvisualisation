package Viewpoints.FrontEnd.Model.VAIClassifier;

public enum Algorithm {
	DECISITON_TREE,
	LOGISTIC,
	RANDOM_FOREST, 
	SMO,
	NB,
	
	KMEANS,
	EM
}
