package Viewpoints.FrontEnd.Model.VAIClassifier;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import weka.attributeSelection.PrincipalComponents;
import weka.core.Instances;

public class PCAModelGenerator {
    public static BufferedReader readDataFile(String filename) {
        BufferedReader inputReader = null;
        
        try {
            inputReader = new BufferedReader(new FileReader(filename));
        } catch (FileNotFoundException ex) {
            System.err.println("File not found: " + filename);
        }
        
        return inputReader;
    }

	public static void main(String[] args) throws Exception {
    	
    	String data = "smoothedBoundary";
    	
    	//Instances trainingData = new Instances(readDataFile("./models/data/DATA.arff".replace("DATA", data)));
    	Instances trainingData = new Instances(readDataFile("./new_models/data/DATA.arff".replace("DATA", data + "_PCA_50")));
    	trainingData.setClassIndex(trainingData.numAttributes() - 1);

    	PrincipalComponents pca = new PrincipalComponents();
    	pca.buildEvaluator(trainingData);
//    	weka.core.SerializationHelper.write("./models/pca/pca_DATA.model".replace("DATA", data), pca);
    	weka.core.SerializationHelper.write("./new_models/pca/pca_DATA.model".replace("DATA", data), pca);
    	System.out.println("Model for " + data + " was created!");
    	
	}

}
