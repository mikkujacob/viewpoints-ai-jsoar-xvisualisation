package Viewpoints.FrontEnd.Model.VAIClassifier;

public enum Data {
	ANGRY,
	FEAR,
	JOY,
	SAD,
	
	ENERGY,
	SMOOTH,
	
	BOUNDARY
}
