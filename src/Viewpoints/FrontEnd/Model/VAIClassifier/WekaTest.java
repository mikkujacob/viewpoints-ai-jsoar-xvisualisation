package Viewpoints.FrontEnd.Model.VAIClassifier;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.evaluation.NominalPrediction;
import weka.core.FastVector;
import weka.core.Instances;
import weka.attributeSelection.PrincipalComponents;

/**
 * WekaTest -
 * This is an experimental class. Use it whenever you need to try samples of code.
 * @author adamlab (Luis)
 *
 */
public class WekaTest {
    public static BufferedReader readDataFile(String filename) {
        BufferedReader inputReader = null;
        
        try {
            inputReader = new BufferedReader(new FileReader(filename));
        } catch (FileNotFoundException ex) {
            System.err.println("File not found: " + filename);
        }
        
        return inputReader;
    }
    
    public static Evaluation simpleClassify(Classifier model, Instances trainingSet, Instances testingSet) throws Exception {
        Evaluation validation = new Evaluation(trainingSet);
        
        model.buildClassifier(trainingSet);
        validation.evaluateModel(model, testingSet);
        
        return validation;
    }
    
    public static double calculateAccuracy(FastVector predictions) {
        double correct = 0;
        
        for (int i = 0; i < predictions.size(); i++) {
            NominalPrediction np = (NominalPrediction) predictions.elementAt(i);
            if (np.predicted() == np.actual()) {
                correct++;
            }
        }
        
        return 100 * correct / predictions.size();
    }
    
    public static Instances[][] crossValidationSplit(Instances data, int numberOfFolds) {
        Instances[][] split = new Instances[2][numberOfFolds];
        
        for (int i = 0; i < numberOfFolds; i++) {
            split[0][i] = data.trainCV(numberOfFolds, i);
            split[1][i] = data.testCV(numberOfFolds, i);
        }
        
        return split;
    }
    
    public static void main(String[] args) throws Exception {
//        // I've commented the code as best I can, at the moment.
//        // Comments are denoted by "//" at the beginning of the line.
//
////        BufferedReader datafile = readDataFile("C:\\Program Files (x86)\\Weka-3-6\\data\\iris.arff");
//        BufferedReader datafile = readDataFile("C:\\Users\\adamlab\\Desktop\\Luis\\Angry_PCA_50.arff");
//        
//        Instances data = new Instances(datafile);
//        data.setClassIndex(data.numAttributes() - 1);
//           
//        // Choose a type of validation split
//        Instances[][] split = crossValidationSplit(data, 10);
//        
//        // Separate split into training and testing arrays
//        Instances[] trainingSplits = split[0];
//        Instances[] testingSplits  = split[1];
//        
//        // Choose a set of classifiers
//        Classifier[] models = {     new J48(),
//                					new NNge(),
//                					new Logistic(),
//                					new RandomForest(),
//                					new PART(),
//                                    new DecisionTable(),
//                                    new OneR(),
//                                    new DecisionStump(),
//                                    new NaiveBayes() };
//        
//        // Run for each classifier model
//        for(int j = 0; j < models.length; j++) {
//
//            // Collect every group of predictions for current model in a FastVector
//            FastVector predictions = new FastVector();
//            
//            // For each training-testing split pair, train and test the classifier
//            for(int i = 0; i < trainingSplits.length; i++) {
//                Evaluation validation = simpleClassify(models[j], trainingSplits[i], testingSplits[i]);
//                predictions.appendElements(validation.predictions());
//                
//                // Uncomment to see the summary for each training-testing pair.
//                // System.out.println(models[j].toString());
//            }
//            
//            // Calculate overall accuracy of current classifier on all splits
//            double accuracy = calculateAccuracy(predictions);
//            
//            // Print current classifier's name and accuracy in a complicated, but nice-looking way.
//            System.out.println(models[j].getClass().getSimpleName() + ": " + String.format("%.2f%%", accuracy) + "\n=====================");
//                        
//        }
//
////        Classifier classifier = models[0];
////        Classifier classifier = new J48();
////        classifier.setModelFile(new File("C:\\Users\\adamlab\\Desktop\\Luis\\trees_J48_angry_50.model"));
        
//    	Instances data = new Instances(readDataFile("C:\\Users\\adamlab\\Desktop\\Luis\\test.arff"));
//        data.setClassIndex(data.numAttributes() - 1);
//        
////        Enumeration attrs = data.enumerateAttributes();
////        
////        while(attrs.hasMoreElements()) {
////        	System.out.println(attrs.nextElement() + ", " + attrs.nextElement().getClass());
////        }
//        
//        SerializedClassifier classifier = new SerializedClassifier();
//        classifier.setModelFile(new File("C:\\Users\\adamlab\\Desktop\\Luis\\trees_J48_angry_50.model"));
//        
//        System.out.println("Testing a angry instance:");
//        double[] instDataPositive = {5.676569,9.585465,6.72791,-6.928949,7.188235,9.089702,2.602825,-4.630073,-1.173977,-0.179361,-2.95537,0.499157,-2.888744,-0.328793,1.480159,1.072232,0.078396,2.754455,-1.933911,-1.024681,-1.643192,-2.849768,-4.219525,-0.289382,1.404408,2.553121,0.228537,0.880637,2.24332,-5.177461,3.494552,1.111509,4.150372,3.814256,-0.316989,-3.566091,-1.933393,0.924613,-1.87107,-0.358888,-3.297894,-2.341285,0.387361,-0.822734,-1.410895,-0.937999,-0.326359,-2.234479,-0.982696};
//              
//        Instance iUse = new Instance(1,instDataPositive);
//        iUse.setDataset(data);
//
//        double[] dists = classifier.distributionForInstance(iUse);
//        
//        for(int i=0; i < 2; i++)
//        	System.out.println(data.classAttribute().value(i) + " probability : " + dists[i]);
//        
//        System.out.println("--------------------------\n");
//
//        
//        System.out.println("Testing a NON angry instance:");
//        double[] instDataNegative = {-3.655268,-3.735978,-5.766927,4.021172,4.277226,-2.684276,-2.897433,-7.800543,-0.001796,0.776147,1.183994,3.489571,1.301572,-4.052759,-1.984282,1.174926,-3.067184,-2.383461,-0.972229,-1.857608,0.660036,-0.132282,-0.615497,-0.274463,0.457032,2.401367,-0.998283,-2.484133,-0.65297,-0.670665,-3.521927,-0.1686,-0.74406,1.446728,1.054517,0.200795,-0.744741,2.159368,0.370889,0.009061,1.131275,1.495701,0.110287,-0.820832,-0.284125,0.739543,0.231293,0.000839,-1.340106};
//        
//        iUse = new Instance(1,instDataNegative);
//        iUse.setDataset(data);
//        
//        dists = classifier.distributionForInstance(iUse);
//        
//        for(int i=0; i < 2; i++)
//        	System.out.println(data.classAttribute().value(i) + " probability : " + dists[i]);
    	
    	String data = "Sad";
    	
    	Instances trainingData = new Instances(readDataFile("./models/data/DATA.arff".replace("DATA", data)));
    	trainingData.setClassIndex(trainingData.numAttributes() - 1);

    	PrincipalComponents pca = new PrincipalComponents();
    	pca.buildEvaluator(trainingData);
    	weka.core.SerializationHelper.write("./models/pca/pca_DATA.model".replace("DATA", data), pca);
    	
//    	Instances data = new Instances(readDataFile("C:\\Users\\adamlab\\Desktop\\Luis\\Energy_PCA_50.arff"));
//    	data.setClassIndex(data.numAttributes() - 1);
//    	
//    	EM em =  (EM) weka.core.SerializationHelper.read("C:/Users/adamlab/Desktop/Luis/clusterer_em_Energy_50.model");
//    	System.out.println(em.clusterInstance(data.firstInstance()));
//    	System.out.println(Arrays.toString(em.distributionForInstance(data.firstInstance())));
//    	
//    	SimpleKMeans km = (SimpleKMeans) weka.core.SerializationHelper.read("C:/Users/adamlab/Desktop/Luis/clusterer_kmeans_Energy_50.model");
//    	System.out.println(km.clusterInstance(data.firstInstance()));
//    	System.out.println(Arrays.toString(km.distributionForInstance(data.firstInstance())));
//    	
//    	AbstractClusterer clusterer = (AbstractClusterer) weka.core.SerializationHelper.read("C:/Users/adamlab/Desktop/Luis/clusterer_kmeans_Energy_50.model");
//    	System.out.println(clusterer.clusterInstance(data.firstInstance()));
//    	System.out.println(Arrays.toString(clusterer.distributionForInstance(data.firstInstance())));
    	
    	            	
//    	Instance test = trainingData.firstInstance();
//		test.setDataset(trainingData);
//		PrincipalComponents filter = (PrincipalComponents) weka.core.SerializationHelper.read("C:\\Users\\adamlab\\Desktop\\Luis\\pca_Angry.model");
//    	
//    	double[] expected = {5.676569346202178, 9.585464556100815, 6.727909839417079, -6.928948680288817, 7.188234948432649, 9.089702117659652, 2.6028252622616455, -4.6300726080629335, -1.1739765423889301, -0.17936110669345526, -2.955369974110703, 0.49915731531229884, -2.888743583789669, -0.3287926497638693, 1.4801591705353045, 1.0722315365936084, 0.07839617440620841, 2.7544548210116915, -1.9339106845406877, -1.0246806639481059, -1.6431923251821594, -2.849768012888993, -4.21952546480131, -0.2893822520934113, 1.4044080531499123, 2.5531213150797756, 0.2285369931984065, 0.88063658515852, 2.243319644730483, -5.177460904826192, 3.4945516523730547, 1.1115088475805739, 4.1503717011983605, 3.8142556899575, -0.31698854368165513, -3.5660914328946984, -1.933392630223015, 0.924612534135326, -1.871070394379789, -0.3588883319741235, -3.2978939604787345, -2.341284979485897, 0.3873606642955612, -0.8227337400131433, -1.4108954107759162, -0.9379989151385606, -0.3263594794780992, -2.234479317515491, -0.9826960149103502, 0.0};
//        double[] result = filter.convertInstance(test).toDoubleArray();
//    	System.out.println(Arrays.equals(result, expected));
    	
    	
    	
    }
}
