package Viewpoints.FrontEnd.Model.VAIClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

public class WekaDemo {
  public static void main(String[] args) throws Exception {
	  //Step 1: Exoress the problem with features
	  Attribute attr1 = new Attribute("firstNumeric");
	  Attribute attr2 = new Attribute("secondNumeric");
	  
//	  FastVector fvNominalVal = new FastVector(3);
//	  fvNominalVal.addElement("r");
//	  fvNominalVal.addElement("g");
//	  fvNominalVal.addElement("b");
//	  Attribute attr3 = new Attribute ("aNominal",fvNominalVal);
	  
	  FastVector fvClassVal = new FastVector(2);
	  fvClassVal.addElement("positive");
	  fvClassVal.addElement("negative");
	  Attribute classAttr = new Attribute("theClass",fvClassVal);
	  
	  FastVector fvWekaAttrs = new FastVector(3);
	  fvWekaAttrs.addElement(attr1);
	  fvWekaAttrs.addElement(attr2);
//	  fvWekaAttrs.addElement(attr3);
	  fvWekaAttrs.addElement(classAttr);
	  
	  
	  //Step 2: Train a classifier
	  Instances isTrainingSet =  new Instances("Rel", fvWekaAttrs, 10);
	  isTrainingSet.setClassIndex(2);
	  
	  Instance iExample = new Instance(3);
	  iExample.setValue((Attribute) fvWekaAttrs.elementAt(0), 1.0);
	  iExample.setValue((Attribute) fvWekaAttrs.elementAt(1), 0.5);
//	  iExample.setValue((Attribute) fvWekaAttrs.elementAt(2), "r");
	  iExample.setValue((Attribute) fvWekaAttrs.elementAt(2), "positive");

	  isTrainingSet.add(iExample);
	  
	  System.out.println(isTrainingSet);
	  
	  Classifier cModel = (Classifier) new NaiveBayes();
	  
	  
	  //Step 3: Test the classifiers
	  Evaluation eTest = new Evaluation(isTrainingSet);
	  eTest.evaluateModel(cModel, isTrainingSet);
	  
	  String strSummary = eTest.toSummaryString();
	  System.out.println(strSummary);
	  
	  double[][] cmMatrix = eTest.confusionMatrix();
	  
	  
	  //Step 4: Use the Classifier
	  Instance iUse = new Instance(3);
	  iUse.setValue((Attribute)fvWekaAttrs.elementAt(0), 2.0);      
	  iUse.setValue((Attribute)fvWekaAttrs.elementAt(1), 2.5);      
//	  iUse.setValue((Attribute)fvWekaAttrs.elementAt(2), "g");
	  iUse.setValue((Attribute)fvWekaAttrs.elementAt(2), "negative");
	  iUse.setDataset(isTrainingSet);
	  
	  // Get the likelihood of each classes 
	  // fDistribution[0] is the probability of being �positive� 
	  // fDistribution[1] is the probability of being �negative� 
	  double[] fDistribution = cModel.distributionForInstance(iUse);
	  
	  System.out.println(fDistribution);
  }
}
