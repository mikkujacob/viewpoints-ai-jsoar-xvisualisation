package Viewpoints.FrontEnd.Model.VAIClassifier;

import weka.core.Instance;

public interface GenericLearner {
	public double[] distributionForInstance(Instance instance) throws Exception ;
}
