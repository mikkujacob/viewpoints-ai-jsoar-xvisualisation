package Viewpoints.FrontEnd.Model.VAIClassifier;

import java.io.File;

import weka.classifiers.misc.SerializedClassifier;
import weka.core.Instance;

public class ClassifierLearner implements GenericLearner {

	protected SerializedClassifier	learner;
	
	public ClassifierLearner(String filename) {
		this.learner = new SerializedClassifier();
		this.learner.setModelFile(new File(filename));	
	}

	public static GenericLearner load(String filename) {
		return new ClassifierLearner(filename);
	}

	@Override
	public double[] distributionForInstance(Instance instance) throws Exception {
		return this.learner.distributionForInstance(instance);
	}

}
