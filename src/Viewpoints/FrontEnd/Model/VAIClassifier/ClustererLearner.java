package Viewpoints.FrontEnd.Model.VAIClassifier;

import weka.clusterers.AbstractClusterer;
import weka.core.Instance;

public class ClustererLearner implements GenericLearner {

	protected AbstractClusterer learner;
	
	public ClustererLearner(String filename) {
		try {
			learner = (AbstractClusterer) weka.core.SerializationHelper.read(filename);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	public static GenericLearner load(String filename) {
		return new ClustererLearner(filename);
	}

	@Override
	public double[] distributionForInstance(Instance instance) throws Exception {
		return this.learner.distributionForInstance(instance);
	}

}
