package Viewpoints.FrontEnd.Application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

import Viewpoints.FrontEnd.Model.AestheticsModel.ViewpointsAestheticsModel;
import Viewpoints.FrontEnd.Shared.JIDX;
import controlP5.Chart;
import controlP5.ControlP5;
import processing.core.PApplet;
/**
 * 
 * Class handles drawing graphs of values of force, smoothed force, kinetic energy, velocity, and momentum using the activity tracker.
 * It requires use of a recorded gesture input file. The default jsg file @playbackVideoName specified in RhythmVAI will be used.
 */

public class ActivityGrapher extends PApplet {
	/**
	 * Generated serialization UUID.
	 */
	private static final long serialVersionUID = -3718552278182045031L;

	public static void main(String[] args) {
		//TODO: (Lauren) Update this
		//VAIFrontEnd.USE_KINECT = false;
		//VAIFrontEnd.DEBUG = true;
		PApplet.main("Viewpoints.FrontEnd.Application.ViewpointsFrontEnd", new String[]{"--full-screen", "--display=1"});
		PApplet.main("Viewpoints.FrontEnd.Application.ActivityGrapher", new String[]{"--full-screen", "--display=1"});
	}

	private ControlP5 cp5;

	private static Chart chart;
	
	private static boolean hasActiveInstance = false;
	private static String keStr = "kinetic energy";
	private static String vStr = "velocity";
	private static String mStr = "momentum";
	private static String fStr = "force";
	private static String fSmooth = "fSmooth";

	private int keColor = color(0, 128, 0);
	private int vColor = color(0, 255, 255);
	private int mColor = color(255, 255, 0);
	private int fColor = color(255, 0, 0);
	private int fSmoothColor = color(255, 192, 192);

	private int white = color(255, 255, 255, 254);
	private int black = color(0, 0, 0);

	private int annotatedStartColor = color(192, 0, 192);
	private int annotatedEndColor = color(128, 0, 128);
	private int currentStartColor = color(0, 128, 192);
	private int currentEndColor = color(0, 0, 192);

	private int x = 0;

	/*
	 * Controls the width of the graph. A value of 1 means that the entire graph will be displayed
	 * on the screen at once. Higher values reduce compression and allow use of the arrow keys to 
	 * view different segments of the graphs at a time.
	 * 
	 */
	private int widthScale = 15; 
	private ArrayList<Float> annotatedStarts = new ArrayList<Float>();
	private ArrayList<Float> annotatedEnds = new ArrayList<Float>();
	private static ArrayList<Float> currentStarts = new ArrayList<Float>();
	private static ArrayList<Float> currentEnds = new ArrayList<Float>();
	private static ArrayList<Float> annotatedTimes = new ArrayList<Float>();
	private static HashMap<Float, Integer> timeIndexMapping = new HashMap<Float, Integer>();
	
	private static ArrayList<Float> timeStamps;
	
	private int max = 4000;
	private final float defaultChartStrokeWeight = 5f;

	public void setup() {
		frame.setResizable(true);
		size(displayWidth, displayHeight);
		smooth();
		cp5 = new ControlP5(this);
		
		timeStamps=new ArrayList<Float>();
		
		chart = cp5.addChart("Graphs")
	               .setPosition(0, 0)
	               .setSize(displayWidth * widthScale, (int)(displayHeight / 1.2))
	               .setRange(0, max)
	               .setView(Chart.LINE)
	               ;
	  
		chart.getColor().setBackground(color(0, 100));
		chart.setStrokeWeight(10f);

		cp5.setAutoDraw(false);
		chart.addDataSet(keStr);
		chart.addDataSet(vStr);
		chart.addDataSet(mStr);
		chart.addDataSet(fSmooth);
		chart.addDataSet(fStr);

		chart.getDataSet(keStr).setColors(keColor);
		chart.getDataSet(keStr).setStrokeWeight(defaultChartStrokeWeight);

		chart.getDataSet(vStr).setColors(vColor);
		chart.getDataSet(vStr).setStrokeWeight(defaultChartStrokeWeight);

		chart.getDataSet(mStr).setColors(mColor);
		chart.getDataSet(mStr).setStrokeWeight(defaultChartStrokeWeight);

		chart.setColors(fStr, fColor);
		chart.getDataSet(fStr).setStrokeWeight(defaultChartStrokeWeight);

		chart.setColors(fSmooth, fSmoothColor);
		chart.getDataSet("fSmooth").setStrokeWeight(defaultChartStrokeWeight);
		hasActiveInstance = true;
		setAnnotations();
		textSize(12);
	}
	
	/*
	 * Finds the csv file corresponding to the currently used file playback and creates
	 * an ArrayList of those annotations.
	 */
	public void setAnnotations(){
		try {
			File file = new File("gesture-timestamps/Annotated/" + ViewpointsFrontEnd.getPlaybackName() + "Annotate" + ".csv");
			String gesture = "";
			if (file.exists()) {
				BufferedReader reader = new BufferedReader(new FileReader(file));
				reader.readLine();
				while ((gesture = reader.readLine()) != null) {
					String[] split = gesture.split(",");
					annotatedStarts.add(Float.parseFloat(split[0]));
					annotatedEnds.add(Float.parseFloat(split[1]));
					annotatedTimes.add(Float.parseFloat(split[0]));
					annotatedTimes.add(Float.parseFloat(split[1]));
				}
				reader.close();
			}
		} catch (Exception e) {
			return;
		}
	}

	/*
	 * Labels the previously annotated boundaries on the graph.
	 */
	public void drawBoundaries(ArrayList<Float> bound, int color) {
		strokeWeight(4);
		stroke(color);
		fill(color);
		int size = timeStamps.size();
		int lastIndex = getIndexAt(displayWidth);
		int firstIndex = getIndexAt(0);
		if (lastIndex < size && firstIndex < size) {
			float last = timeStamps.get(lastIndex);
			float first = timeStamps.get(firstIndex);
			for (int i = 0; i < bound.size(); i++) {
				float time = bound.get(i);
				if (time <= last && time >= first) {
					int index;
					if (!timeIndexMapping.containsKey(time)) {
						index = timeStamps.indexOf(time);
					} else {
						index = timeIndexMapping.get(time);
					}
					stroke(color);
					fill(color);
					line(getLocAt(index), 0, getLocAt(index), (int)(displayHeight/1.2) + 10);
					tick(getLocAt(index), (int)(displayHeight/1.2) + 10, "" + time);
				}
			}
		}
	}

	public void draw() {
		background(0);
		boolean displayLegend = true;
		
		//Draw the annotation boundaries.
		drawBoundaries(annotatedStarts, annotatedStartColor);
		drawBoundaries(annotatedEnds, annotatedEndColor);
		
		//Draw the boundaries detected by this current instance of the application.
		drawBoundaries(currentStarts, currentStartColor);
		drawBoundaries(currentEnds, currentEndColor);
		cp5.draw();
		
		int width = (int)(displayWidth/1.15);
		int height = (int)(displayHeight/1.13);
		int size = timeStamps.size();
		
		//Display a white vertical line to get information for a specific time stamp on a graph.
		if (chart.isMouseOver()) {
			stroke(white);
			fill(white);
			line(mouseX, 0, mouseX, displayHeight);
			int index = (mouseX - x) * size / (displayWidth * widthScale);
			if (index < size) {
				String text = "kinetic energy: " + chart.getDataSet(keStr).get(index).getValue() + "\n";
				text+= "velocity: " + chart.getDataSet(vStr).get(index).getValue() + "\n";
				text+= "momentum: " + chart.getDataSet(mStr).get(index).getValue() + "\n";
				text+= "force: " + chart.getDataSet(fStr).get(index).getValue();
			//	tickMouse(mouseX, mouseY, "" + timeStamps.get(index) + ": " + chart.getDataSet(fSmooth).get(index).getValue(), text);
				if (mouseX > (int)(width/1.13)) {
					displayLegend = false;
				}
			}
		}

		stroke(white);
		fill(white);
		line(0, (int)(displayHeight/1.2) + 10, displayWidth, (int)(displayHeight/1.2) + 10);
		
		//Draws the legend.
		if (displayLegend) {
			fill(white);
			rect(width, height, displayWidth, displayHeight);

			int offset = 15;
			fill(keColor);
			ellipse(width + 10, height + offset, 15, 15);
			fill(black);
			text("Kinetic Energy", width + 25, height + offset);
		
			offset += 15;
			fill(vColor);
			ellipse(width + 10, height + offset, 15, 15);
			fill(black);
			text("Velocity", width + 25, height + offset);

			offset += 15;
			fill(mColor);
			ellipse(width + 10, height + offset, 15, 15);
			fill(black);
			text("Momentum", width + 25, height + offset);

			offset += 15;
			fill(fColor);
			ellipse(width + 10, height + offset, 15, 15);
			fill(black);
			text("Force", width + 25, height + offset);
		}
	}
	
	public void keyPressed() {

		//Move the display of the graph to the left or the right.
		if (key == CODED && keyCode == LEFT) {
			x += 10;
			chart.setPosition(x, 0);
		}
		if (key == CODED && keyCode == RIGHT) {
			x -= 10;
			chart.setPosition(x, 0);
		}
		
		//Toggle between which graphs are visible.
		if (key == '1') {
			if (chart.getDataSet(keStr).getStrokeWeight() == 0) { 
				chart.getDataSet(keStr).setStrokeWeight(defaultChartStrokeWeight);
			} else {
				chart.getDataSet(keStr).setStrokeWeight(0);
			}
		}
		if (key == '2') {
			if (chart.getDataSet(vStr).getStrokeWeight() == 0) { 
				chart.getDataSet(vStr).setStrokeWeight(defaultChartStrokeWeight);
			} else {
				chart.getDataSet(vStr).setStrokeWeight(0);
			}
		}
		if (key == '3') {
			if (chart.getDataSet(mStr).getStrokeWeight() == 0) { 
				chart.getDataSet(mStr).setStrokeWeight(defaultChartStrokeWeight);
			} else {
				chart.getDataSet(mStr).setStrokeWeight(0);
			}
		}
		if (key == '4') {
			if (chart.getDataSet(fStr).getStrokeWeight() == 0) { 
				chart.getDataSet(fStr).setStrokeWeight(defaultChartStrokeWeight);
			} else {
				chart.getDataSet(fStr).setStrokeWeight(0);
			}
		}
		if (key == '5') {
			if (chart.getDataSet(fSmooth).getStrokeWeight() == 0) { 
				chart.getDataSet(fSmooth).setStrokeWeight(defaultChartStrokeWeight);
			} else {
				chart.getDataSet(fSmooth).setStrokeWeight(0);
			}
		}

		//Toggle all charts to visible.
		if (key == 'a' || key == 'A') {
			chart.getDataSet(keStr).setStrokeWeight(defaultChartStrokeWeight);
			chart.getDataSet(vStr).setStrokeWeight(defaultChartStrokeWeight);
			chart.getDataSet(mStr).setStrokeWeight(defaultChartStrokeWeight);
			chart.getDataSet(fSmooth).setStrokeWeight(defaultChartStrokeWeight);
			chart.getDataSet(fStr).setStrokeWeight(defaultChartStrokeWeight);
		}
	}

	public int getIndexAt(int loc) {
		int size = timeStamps.size();
		return (loc - x) * size / (displayWidth * widthScale);
	}

	public int getLocAt(int index) {
		int size = timeStamps.size();
		return ((index * (displayWidth * widthScale)) / size) + x;
	}

	public void tick(int x, int y, String text) {
		if (text.length() > 5) {
			text = text.substring(0, 5);
		}
		stroke(white);
		fill(white);
		ellipse(x, y, 10, 10);
		text(text, x-25, y + 25);
	}

	public void tickMouse(int x, int y, String text, String extendedText) {
		stroke(white);
		fill(white);

		ellipse(x, y, 3, 3);
		text(text, x + 15, y + 15);
		text(extendedText, x + 15, (int)(displayHeight/1.14) + 10);
	}
	
	/*TODO: (Lauren) of this format
	 * activityTracker = ViewpointsAestheticsModel.getActivityTracker();
	 * 								activityTracker.getKineticEnergy(),
									(activityTracker).getVelocity(),
									(activityTracker).getMomentum(),
									(activityTracker).getForce(),
									((FileMotionInput)currentMotionInput).time(RECORD_FILE),
									(activityTracker).getStarted(),
									(activityTracker).getEnded(),
									(activityTracker).getSmoothedForce()
	 */
	/**
	 * 
	 * @param time The time that these values occur at
	 * @param start If the start of a gesture was detected at this time.
	 * @param end If the end of a gesture was detected at this time.
	 */
	public static void update(float time, boolean start, boolean end) {
		//float ke = ViewpointsAestheticsModel.getActivityTracker().getKineticEnergy();
		//float v = ViewpointsAestheticsModel.getActivityTracker().getVelocity();
		//float m = ViewpointsAestheticsModel.getActivityTracker().getMomentum();
		//float f = ViewpointsAestheticsModel.getActivityTracker().getForce();
		
		float ke = ViewpointsAestheticsModel.getActivityTracker().getAcceleration(JIDX.LEFT_HAND);
		float v = ViewpointsAestheticsModel.getActivityTracker().getAcceleration(JIDX.RIGHT_HAND);
		float m = ViewpointsAestheticsModel.getViewpointsTracker().getEnergy();
		float f = ViewpointsAestheticsModel.getViewpointsTracker().getSmoothness();
		chart.addData(keStr, ke);
		chart.addData(vStr, v);
		chart.addData(mStr, m);
		chart.addData(fStr, f);

		//The smoothed force data is constantly changing so the data needs to be flushed for the chart 
		//to display an accurate visualization.
		if (chart.getDataSet(fSmooth) != null) {
			chart.getDataSet(fSmooth).clear();
		}

		/*for (int i = 0; i < smoothedForce.size();i++) {
			if (smoothedForce.get(i) != null) {
				chart.addData(fSmooth, smoothedForce.get(i));
			} else {
				chart.addData(fSmooth, 0);
			}
		}*/
		
		timeStamps.add(time);
		if (start) {
			currentStarts.add(time);
		}
		
		if (end) {
			currentEnds.add(time);
		}
		
		//Map the annotated times to the nearest incoming time.
		if (annotatedTimes.size() > 0) {
			float comp = annotatedTimes.get(0);
		
			if (!timeIndexMapping.containsKey(comp)) {
				timeIndexMapping.put(comp, timeStamps.size() - 1);
			} else {
				float diff = Math.abs(timeStamps.get(timeIndexMapping.get(comp)) - comp);
				if (Math.abs(time - comp) < diff) {
					timeIndexMapping.put(comp, timeStamps.size() - 1);
				} else {
					annotatedTimes.remove(0);
					if (annotatedTimes.size() > 0) {
						comp = annotatedTimes.get(0);
						timeIndexMapping.put(comp, timeStamps.size() - 1);
					}
				}
			}
		}
		
	}
	
	public static boolean active() {
		return hasActiveInstance;
	}
	
	public void stop() {
		hasActiveInstance = false;
		super.stop();
	}
}
