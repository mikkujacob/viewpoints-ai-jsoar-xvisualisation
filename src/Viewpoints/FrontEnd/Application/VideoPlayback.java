package Viewpoints.FrontEnd.Application;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PVector;
import Viewpoints.FrontEnd.Audio.AudioPlayer.MusicControl;
import Viewpoints.FrontEnd.Clock.Clock;
import Viewpoints.FrontEnd.Clock.ClockManager;
import Viewpoints.FrontEnd.Communication.File.FileUtilities;
import Viewpoints.FrontEnd.Controls.ControlsManager;
import Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture.PredicateSpaceGesture;
import Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture.TransformPredicates;
import Viewpoints.FrontEnd.Graphics.EffectsController.EmbodimentEffects.FireflyEffects;
import Viewpoints.FrontEnd.Graphics.Embodiment.Embodiment;
import Viewpoints.FrontEnd.Graphics.Embodiment.ParticleEmbodiment.Fireflies.FireflyEmbodiment;
import Viewpoints.FrontEnd.Graphics.Embodiment.SkeletalEmbodiment.GraphicsUtilities;
import Viewpoints.FrontEnd.Input.MotionInput.FileMotionInput;
import Viewpoints.FrontEnd.Model.AestheticsModel.ViewpointsAestheticsModel;
import Viewpoints.FrontEnd.ResponseLogic.RandomResponseLogic;
import Viewpoints.FrontEnd.ResponseLogic.ResponseLogic;
import Viewpoints.FrontEnd.ResponseLogic.SimilarityResponseLogic;
import Viewpoints.FrontEnd.ResponseLogic.SoarResponseLogic;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.JointSpaceGestureInterpolator;
import Viewpoints.FrontEnd.Shared.PVecUtilities;

/**
 * Looped playback of previously captured jsg files.
 * @author Lauren
 *
 */
public class VideoPlayback extends PApplet{
	/*
	 * These variables affect which subdirectory the gestures are pulled from.
	 * Change these to reference the correct set of gestures.
	 */
	private static String playbackVideoName = "RepeatedShort";
	private String segmentMethod = "Activity";
	
	private static String REHEARSAL_GESTURE_LIB = "gesture-segments" + File.separator;
	public static String PROJECT_HOME;
	
	/**
	 * Used for quickly renaming files for data analysis.
	 */
	private static boolean LABEL_MODE = false;
	
	/**
	 * The label to be used for the participant whose gestures
	 * are being replayed.
	 */
	private final String participantLabel = "001";
	
	private final static String[] gestureLabels = {
		"popAndLock", "lawnmower", "chrisTucker", "duff",
		"saturdayNightFever", "twist", "windowWipe", "souljaboy",
		"cancan", "beatIt", "whipNaeNae", "shoppingCart","skip",
		"badRomance", "cabbagePatch", "californiaGirls",
		"chickenDance","chickenWing","gangnamStyle","hammertime",
		"hipBounce","hoedown","johnTravolta","macarena", "moonwalk",
		"singleLadies","spongebob","sprinnkler","swim","timeWarp",
		"wiggle","ymca","freestyle",
	};
	
	private static int gestureLabelIndex = 0;
	private int[] gestureLabelCounts = new int[gestureLabels.length];
	
	private static int shadowPixel; 	//we need this for drawing the user shadow
	
	private static final long serialVersionUID = 1L;
	protected FireflyEffects effectsController = null;
	protected static ResponseLogic responseLogic;
	protected static ResponseLogic backupSoarResponseLogic;
	protected static ResponseLogic backupRandomResponseLogic;
	protected Clock timer, sameResponseTimer, randomResponseTimer, newResponseTimer;
	protected static MusicControl audioController;
	protected ArrayList<String> listGUIText = new ArrayList<String>();
	protected ArrayList<Long> listGUIDuration = new ArrayList<Long>();
	protected VAIState currentState = VAIState.CLOUD;
	protected int randomCounter = 0;
	protected float maxZ = 5000;
	protected float minZ = 500;
	protected boolean repeat = true;
	
	protected Embodiment embodiment;
	
	private ControlsManager controlsManager;
	
	//Gesture directory logic.
	public final String gestureVideoName = "gestures";
	//public final String GESTURE_DIR = "gesture-captured/" + playbackVideoName + "/" + segmentMethod + "/";
	public final String GESTURE_DIR = "gestures/" + "001/";
//	public final String GESTURE_DIR = "motion-capture/" + "jsg/";
	private static File[] filenames; 
	public static int fileIndex = 0;
	private static String[] filerenames;

	//timers
	protected final float randomDurationScaleTimeout = 3;
	protected final float humanStillnessToRandom = 4;
	protected final float agentStillnessToRandom = 7;
	protected static float humanMovementTimeout;
	protected float humanStillnessTimeout;
	protected float agentStillnessTimeout;
	protected static Clock vaiGlyphTimer;
	protected static Clock userGlyphTimer;
	
	//MotionInputs 
	protected static FileMotionInput currentMotionInput;

	protected JointSpaceGestureInterpolator response = null;
	protected int col = color(255);
	
	public void setup() {
		try{
			PROJECT_HOME = System.getProperty("user.dir");
			if(PROJECT_HOME.equalsIgnoreCase(null)) {
				try {
					PROJECT_HOME = new java.io.File( "." ).getCanonicalPath();
				}
				catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			
			//Creates directory for gesture file to toggle between.
			File dir = new File(GESTURE_DIR);
			setFilenames(dir.listFiles());
			filerenames = new String[filenames.length];

			noCursor();
			ClockManager.setMainClock(new Clock());
			vaiGlyphTimer = new Clock();
			userGlyphTimer = new Clock();
			shadowPixel = 2;

			try { 
				currentMotionInput = new FileMotionInput(filenames[fileIndex].getPath());
				System.out.println("Filenames: " + filenames[fileIndex].getPath());
			} catch (NullPointerException e)	{
				addGUIText("Invalid gesture directory", 5000);
			}
			
			controlsManager = new ControlsManager(this);
			controlsManager.setDRAW_EMBODIMENT(false);
		//	controlsManager.setDRAW_USER(true);
			
			effectsController = new FireflyEffects(controlsManager);

			if (currentMotionInput.init()) {
				
				//set response logic
				responseLogic = new SimilarityResponseLogic();
				backupSoarResponseLogic = new SoarResponseLogic();
				backupRandomResponseLogic = new RandomResponseLogic(getREHEARSAL_GESTURE_LIB());
				getBackupSoarResponseLogic().start();
				backupRandomResponseLogic.start();
			} else {
				background(255, 0, 0);
			}

			ClockManager.getMainClock().start();
			timer = new Clock();
			timer.start();
			
			audioController = new MusicControl(this);
			effectsController = new FireflyEffects(controlsManager);
			embodiment = new FireflyEmbodiment(this.g, effectsController);
			embodiment.initialize();
		}catch(Exception e){
			e.printStackTrace();
			restart();
		}
		size(displayWidth, displayHeight);
	}

	public void draw() {
		try{
			
			if (!currentMotionInput.isInit()) {
				return;
			} else {
				ClockManager.getMainClock().check();
				noStroke();
				fill(0, 0, 0, effectsController.activeParameters().FADE);
				rect(0, 0, displayWidth, displayHeight);
				Body userPose = currentMotionInput.next(ClockManager.getMainClock());
				Body vaiPose = null;
				drawing(userPose, vaiPose);
			}
		}catch(Exception e){
			e.printStackTrace();
			restart();
		}
		
		addGUIText("Gesture " + (fileIndex + 1) + ": " + filenames[fileIndex].getName(), 100);
		if (LABEL_MODE) {
			if (filerenames[fileIndex] == null) {
				addGUIText("Label as: " + gestureLabels[gestureLabelIndex] + "?", 100);
			} else {
				addGUIText("Rename to: " + filerenames[fileIndex], 100);
				addGUIText("Change to: " + gestureLabels[gestureLabelIndex] + "?", 100);
			}
		}
		/*fill(color(255,255,255));
		stroke(color(255,255,255));
		text( "x: " + mouseX + " y: " + mouseY, mouseX, mouseY );*/
	}
	
	@Override
	public boolean sketchFullScreen() {
		  return true;
	}
	
	public void addGUIText(String message, long duration) {
		for(int index = 0; index < listGUIText.size(); index++) {
			String testMessage = listGUIText.get(index);
			if(message.split(":")[0].equalsIgnoreCase(testMessage.split(":")[0])) {
				listGUIText.remove(index);
				listGUIDuration.remove(index);
				break;
			}
		}
		
		this.listGUIText.add(message.toUpperCase());
		this.listGUIDuration.add(System.currentTimeMillis() + duration);
	}
	
	public void displayGUI() {
		long timeNowMillis = System.currentTimeMillis();
		textSize(32);
		fill(255);
		stroke(255);
		float xPosPercent = 2f / 100f * (float)displayWidth;
		float yPosPercent = 3f / 100f * (float)displayHeight;
		yPosPercent += 5f / 100f * (float)displayHeight;
		for(int index = 0; index < listGUIText.size(); index++) {
			String message = listGUIText.get(index);
			long displayTime = listGUIDuration.get(index);
			text(message, xPosPercent, yPosPercent, 75f);
			yPosPercent += 5f / 100f * (float)displayHeight;
			if(displayTime < timeNowMillis) {
				listGUIText.remove(index);
				listGUIDuration.remove(index);
			}
		}
	}
	
	public void mousePressed() {
	}
	
	public void keyPressed() {
		try{
			if(keyCode == UP) {
				updateIndex(1);
				currentMotionInput = new FileMotionInput(filenames[fileIndex].getPath());
				System.out.println("switching to: " + filenames[fileIndex].getPath());
			}
			
			if (keyCode == DOWN) {
				updateIndex(-1);
				currentMotionInput = new FileMotionInput(filenames[fileIndex].getPath());
			}
			if(keyCode == RIGHT) {
				updateLabelIndex(1);
				return;
			}
			
			if (keyCode == LEFT) {
				updateLabelIndex(-1);
				return;
			}
			
			if (key == ' ') {
				repeat = (!currentMotionInput.getRepeat());
			}

			if (keyCode == ENTER) {
				repeat = (!currentMotionInput.getRepeat());
				addGUIText("Repeat: " + repeat, 5000);
			}

			if (key == 'r' || key == 'R') { 
				currentMotionInput.replayAt(0);
			}
			
			if (keyCode == ENTER && LABEL_MODE) {
				String name = gestureLabels[gestureLabelIndex];
				gestureLabelCounts[gestureLabelIndex] += 1;
				int count = gestureLabelCounts[gestureLabelIndex];
				filerenames[fileIndex] = participantLabel + name + count;
				addGUIText("Updating File Name", 5000);
				return;
			}

			//addGUIText("Gesture " + (fileIndex + 1) + ": " + filenames[fileIndex].getName(), 5000);
			changeMotionInput();
			
		}catch(Exception e){
			e.printStackTrace();
			restart();
		}
	}
	
	protected void restart(){
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(PROJECT_HOME + File.separator + "file_communications" + File.separator + "restart_commands.txt", true));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		out.println("rf");
		out.close();
	}

	protected void changeMotionInput(){
		if (currentMotionInput.init()) {
			
			effectsController = new FireflyEffects(controlsManager);
			
			embodiment = new FireflyEmbodiment(this.g, effectsController);
			embodiment.initialize();
			currentMotionInput.repeat(repeat);
			
			if(responseLogic == null) {
				setResponseLogic(new SimilarityResponseLogic());
				setBackupSoarResponseLogic(new SoarResponseLogic());
				backupRandomResponseLogic = new RandomResponseLogic(getREHEARSAL_GESTURE_LIB());
				getBackupSoarResponseLogic().start();
				backupRandomResponseLogic.start();
			}
		} else {
			background(255, 0, 0);
		}
	}
	
	protected void drawing(Body userPose, Body vaiPose){
		if (null == userPose) {
			effectsController.update(null, null, null, null, null);
		} else {
			//Logic for segmenting gestures
			ViewpointsAestheticsModel.getHumanStillness().update(userPose);

			if(ViewpointsAestheticsModel.getHumanStillness().isStill()){
				humanStillnessTimeout = ViewpointsAestheticsModel.getHumanStillness().getTime();
				humanMovementTimeout = -1;
			} else {
				humanStillnessTimeout = -1;
				humanMovementTimeout = ViewpointsAestheticsModel.getHumanStillness().getTime();
			}

			//LeaderRecord response = null;
			if(sameResponseTimer != null){
				sameResponseTimer.check();
			}
			if(newResponseTimer != null){
				newResponseTimer.check();
			}

			if(response != null && newResponseTimer != null && newResponseTimer.time() > response.getBasicDuration() * randomDurationScaleTimeout){
				newResponseTimer.check();
				newResponseTimer = null;
			}
			if(response != null && randomResponseTimer != null && randomResponseTimer.time() >  response.getBasicDuration() * randomDurationScaleTimeout){
				randomResponseTimer.check();
				response = null;
				randomResponseTimer = null;
			}

			//Random response. Occurs when the user has been doing same movement for a repeated amount of time or the user / agent stillness passes a certain threshold.
			if(currentState == VAIState.INTERACTION &&
					((response != null && randomResponseTimer != null && randomResponseTimer.time() < response.getBasicDuration() * randomDurationScaleTimeout)
					|| ((ViewpointsAestheticsModel.getHumanStillness().isStill() && humanStillnessTimeout > humanStillnessToRandom) || 
					(ViewpointsAestheticsModel.getAgentStillness().isStill() && agentStillnessTimeout > agentStillnessToRandom)/* || (sameResponse != null && sameResponse.time() > 8)*/))){				
				if(randomResponseTimer == null){
					PrintWriter out = null;
					try {
						out = new PrintWriter(new FileWriter(PROJECT_HOME + File.separator + "file_communications" + File.separator + "requestRandomResponse.txt", true));
					} catch (IOException e1) {
						e1.printStackTrace();
					}

					out.println("r");
					out.close();
					sameResponseTimer = null;

					if (responseLogic instanceof SoarResponseLogic) {
						response = ((SoarResponseLogic)responseLogic).requestRandomResponse();
					}

					if(response != null){
						if(randomCounter == 3){
							randomCounter = 0;
							addGUIText("Try moving in rhythm! ;)", 3000);
						}
						randomCounter++;
						randomResponseTimer = new Clock();
						randomResponseTimer.check();
					}
				}
				if(randomResponseTimer != null)
					randomResponseTimer.check();

				//Human stillness needs to be reset to prevent continuously random responses with every frame.
				ViewpointsAestheticsModel.getHumanStillness().reset();
				ViewpointsAestheticsModel.getAgentStillness().reset();
			}

			// This handles if there is a directed response or if there is a random response.
			if (response != null && (newResponseTimer != null || randomResponseTimer != null)) { //directed response
				//addGUIText("Directed Response", 1000);
				if(randomResponseTimer == null)
					randomCounter = 0;
				//addGUIText("RESPONSE", 100);
				
				if(responseLogic.getClass().getSimpleName().equalsIgnoreCase("SoarResponseLogic"))
				{
					PredicateSpaceGesture psGesture =  ((SoarResponseLogic)responseLogic).checkPSG();
					
					if(psGesture != null) {
						for(int i = 0; i < psGesture.transforms.size(); i++) {
							if(psGesture.transforms.get(i) instanceof TransformPredicates.RESPONSE_MODE) {
								vaiGlyphTimer.start();
								((SoarResponseLogic)responseLogic).setPSG(null);
							}
						}
					}
				}
				
				vaiPose = response.replayFrame(ClockManager.getMainClock().deltatime());				
				
				PVector[] targetBasis;
				float characteristicSize;
				//reflect userPose here
				PVector newCenter = new PVector(userPose.center().x * -1, userPose.center().y, userPose.center().z);
				userPose = userPose.translated(newCenter);
				//proceed as usual
				targetBasis = userPose.localBasis();
				characteristicSize = userPose.characteristicSize();
				for (PVector basisVector : targetBasis) {
					basisVector.mult(characteristicSize);
				}
					vaiPose = vaiPose.transformed(PVecUtilities.STANDARD_BASIS, targetBasis, userPose.center());
			} else { //cloning
				//addGUIText("Cloning", 1000);
				ViewpointsAestheticsModel.getAgentStillness().setStill();
				//reflect user pose here
				PVector newCenter = new PVector(userPose.center().x * -1, userPose.center().y, userPose.center().z);
				userPose = userPose.translated(newCenter);
				vaiPose = (Body) userPose.clone();
			}

			//Update agent stillness.
			if(currentState == VAIState.INTERACTION){
				ViewpointsAestheticsModel.getAgentStillness().update(vaiPose);
				if(ViewpointsAestheticsModel.getAgentStillness().isStill()){
					agentStillnessTimeout = ViewpointsAestheticsModel.getAgentStillness().getTime();
				}else{
					agentStillnessTimeout = -1;
				}
			}else{
				ViewpointsAestheticsModel.getAgentStillness().reset();
			}

			vaiPose.setTimestamp(ClockManager.getMainClock().time());
			effectsController.update(userPose, vaiPose, null, null, null);
		}
		fill(255);
		textAlign(LEFT, TOP);
		textSize(40);

		pushMatrix();
		translate(width/2, height/2);
		
//		addGUIText("DRAW USER: " + DRAW_USER, 5000);
		if (null != userPose) {
			GraphicsUtilities.drawBody(userPose, this.g);
		}
		
		if (null != vaiPose) {
			GraphicsUtilities.drawBody(vaiPose, this.g);
		}
		
		embodiment.draw();
		
		popMatrix();
		//Draw GUI Text
		displayGUI();
		
	//	syphonServer.sendScreen();
	}
	
	
	private enum VAIState{
		CLOUD{
			@Override
			public VAIState nextState(VAIState input){
				stateTimer = new Clock();
				stateTimer.start();
				//always greetwelcome
				return input;
			}
		},
		GREETWELCOME{
			public VAIState nextState(VAIState input){
				//user left, to goodbye
				if(input == GREETGOODBYE){
					stateTimer = new Clock();
					stateTimer.start();
					return input;
				}
				//to interaction
				if(humanMovementTimeout > greetTimer && input != VAIState.CLOUD){
					return input;
				}else
					return this;
			}
		},
		INTERACTION{
			public VAIState nextState(VAIState input){
				stateTimer = new Clock();
				stateTimer.start();
				//always greetgoodbye
				return input;
			}
		},
		GREETGOODBYE{
			public VAIState nextState(VAIState input){
				if(input == GREETWELCOME){
					stateTimer = new Clock();
					stateTimer.start();
					return input;
				}
				stateTimer.check();
				if(stateTimer.time() > byeTimer){
					return input;
				}
				else
					return this;
			}
		};
		public abstract VAIState nextState(VAIState input);
		public static Clock stateTimer;
		public float greetTimer = 1f;
		public float byeTimer = 5f;
	}
	
	public static String getPlaybackName() {
		return playbackVideoName;
	}

	public static void main(String[] args) {
		PApplet.main("Viewpoints.FrontEnd.Application.VideoPlayback", new String[]{"--full-screen", "--display=1"});
	}

	//Updates the file index after toggling.
	public static void updateIndex(int delta) {
		fileIndex = (fileIndex + delta) % getFilenames().length;
		if (fileIndex < 0) {
			fileIndex = 0;
		}
		
		if (fileIndex >= filenames.length) {
			fileIndex = filenames.length - 1;
		}
	}
	
	//Updates the gesture label index after toggling.
		public static void updateLabelIndex(int delta) {
			gestureLabelIndex = (gestureLabelIndex + delta) % gestureLabels.length;
			if (gestureLabelIndex < 0) {
				gestureLabelIndex = 0;
			}
			
			if (gestureLabelIndex >= gestureLabels.length) {
				gestureLabelIndex = gestureLabels.length - 1;
			}
		}

	@Override
	public void exit() {
		FileUtilities.flush(PROJECT_HOME);
		super.exit();
		if (filenames != null) {
			for (int i = 0; i < filenames.length; i++) {
				System.out.println(filenames[i].getName() + "-> " + filerenames[i]);
				if (filerenames[i] != null) { 
					File rename = new File(GESTURE_DIR + "/" + filerenames[i] + ".jsg");
					filenames[i].renameTo(rename);
				}
			}
		}
	}

	///// Getters and Setters /////

	public FileMotionInput getMotionInput() {
		return currentMotionInput;
	}

	public void setShadowPixel(int i) {
		shadowPixel = i;
	}

	public MusicControl getAudioController() {
		return audioController;
	}

	public int getShadowPixel() {
		return shadowPixel;
	}

	public float getMinZ() {
		return minZ;
	}

	public float getMaxZ() {
		return maxZ;
	}

	public static String getREHEARSAL_GESTURE_LIB() {
		return REHEARSAL_GESTURE_LIB;
	}

	public static void setREHEARSAL_GESTURE_LIB(String rEHEARSAL_GESTURE_LIB) {
		REHEARSAL_GESTURE_LIB = rEHEARSAL_GESTURE_LIB;
	}

	public static File[] getFilenames() {
		return filenames;
	}

	public static void setFilenames(File[] filenames) {
		VideoPlayback.filenames = filenames;
	}

	public ResponseLogic getResponseLogic() {
		return responseLogic;
	}

	public void setResponseLogic(ResponseLogic responseLogic) {
		VideoPlayback.responseLogic = responseLogic;
	}

	public ResponseLogic getBackupSoarResponseLogic() {
		return backupSoarResponseLogic;
	}

	public void setBackupSoarResponseLogic(ResponseLogic backupSoarResponseLogic) {
		VideoPlayback.backupSoarResponseLogic = backupSoarResponseLogic;
	}

	public void setMaxZ(float maxZ) {
		this.maxZ = maxZ;
	}

	public void setMinZ(float minZ) {
		this.minZ = minZ;
	}

	public void setMotionInput(FileMotionInput newMotionInput) {
		currentMotionInput = newMotionInput;
	}

	public Clock getUserGlyphTimer() {
		return userGlyphTimer;
	}
}