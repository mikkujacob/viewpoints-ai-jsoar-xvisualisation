package Viewpoints.FrontEnd.Application;
import java.io.File;

import Viewpoints.FrontEnd.Input.MotionInput.FileMotionInput;
import processing.core.PApplet;

/**
 * Tool for simple playback of the gestures that were captured during segmentation.
 * 
 * @author Lauren Winston
 *
 */
public class GesturePlayback extends PApplet{
	
	/**
	 * Generated serial number
	 */
	private static final long serialVersionUID = -9043877489756368561L;
	public static String playbackVideoName = "gestures";
	public static String GESTURE_DIR = "gesture-captured/" + playbackVideoName + "/";
	private FileMotionInput motionInput;
	private File[] filenames; 
	private int fileIndex = 0;
	
	public void setup() {
		frame.setResizable(true);
		noCursor();
		size(displayWidth, displayHeight);
		File dir = new File(GESTURE_DIR);
		filenames = dir.listFiles();

		motionInput = new FileMotionInput(filenames[0].toString());
		motionInput.init();
	}

	public void keyPressed() {
		if (keyCode == UP) {
			updateIndex(1);
			motionInput.changeFile(filenames[fileIndex].toString());
		}
		
		if (keyCode == DOWN) {
			updateIndex(-1);
			motionInput.changeFile(filenames[fileIndex].toString());
		}
	}
	
	public void updateIndex(int delta) {
		fileIndex = (fileIndex + delta) % filenames.length;
	}
	
	public static void main(String[] args) {
		PApplet.main("Viewpoints.Synchronous.GesturePlayback", new String[]{"--full-screen", "--display=1"});
	}
}
