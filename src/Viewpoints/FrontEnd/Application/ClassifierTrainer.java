package Viewpoints.FrontEnd.Application;


import java.nio.file.Files;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.*;

import Viewpoints.FrontEnd.Clock.Clock;
import Viewpoints.FrontEnd.Clock.ClockManager;
import Viewpoints.FrontEnd.Communication.File.FileUtilities;
import Viewpoints.FrontEnd.Controls.ControlsManager;
import Viewpoints.FrontEnd.Graphics.EffectsController.EmbodimentEffects.FireflyEffects;
import Viewpoints.FrontEnd.Graphics.Embodiment.Embodiment;
import Viewpoints.FrontEnd.Graphics.Embodiment.ParticleEmbodiment.Fireflies.FireflyEmbodiment;
import Viewpoints.FrontEnd.Input.MotionInput.FileMotionInput;
import Viewpoints.FrontEnd.ResponseLogic.ResponseLogic;
import Viewpoints.FrontEnd.ResponseLogic.SimilarityResponseLogic;
import Viewpoints.FrontEnd.ResponseLogic.SoarResponseLogic;
import Viewpoints.FrontEnd.Segmentation.ActivitySegmenter;
import Viewpoints.FrontEnd.Shared.*;
import processing.core.*;


/**
 * Application that initiates retraining the classifier for the Activity-based segmentation method.
 * @author Lauren
 *
 */
public class ClassifierTrainer extends PApplet{
	public static String PROJECT_HOME;
	
	
	/*
	 * The directory that all of the annotated jsg files reside in.
	 */
	private String gestureDir = "retraining-gestures";

	/*
	 * One of the jsg video files that has been annotated. If there are additional files, 
	 * they must be in the same directory, gestureDir.
	 */
	private String dataFile = "MonApr0620-59-01EDT2015.jsg";
	
	/*
	 * The text file that contains the start and end gesture boundary annotations for all
	 * of the videos in gestureDir. This file can be generated using the regular annotation
	 * system.
	 */
	File logs = new File(gestureDir + File.separator + "orig_retraining_time_log.txt");
	
	/*
	 * This file is necessary to switch between to get accurate timings for the generated boundaries.
	 * Annotations for this file should also be present in logs 
	 */
	private static String playbackFilePrefix = "recorded-input-sessions/";
	private static String playbackVideoName = "Capture";
	private final static String playbackFile = playbackFilePrefix + playbackVideoName + ".jsg";

	Scanner scan;
	float boundary = 0;
	boolean swap = false;
	private float inbetween;
	private String tempData;
	
	private Embodiment embodiment;
	private FireflyEffects effectsController = null;
	private static ResponseLogic responseLogic;
	private static ResponseLogic backupSoarResponseLogic;
	private Clock timer;
	private ArrayList<String> listGUIText = new ArrayList<String>();
	private ArrayList<Long> listGUIDuration = new ArrayList<Long>();
	private final float UPDATE_TIMER = 5; // in seconds
	private ControlsManager controlsManager;
	
	//MotionInput 
	private static FileMotionInput currentMotionInput;
	
	/*
	 * The segmenter that is used to train against the start and end boundary data and interact with the necessary files.
	 */
	private ActivitySegmenter activityTracker = new ActivitySegmenter(true); 

	private static final long serialVersionUID = 1L;
	
	public void setup() {
		try{
			PROJECT_HOME = System.getProperty("user.dir");
			if(PROJECT_HOME.equalsIgnoreCase(null)) {
				try {
					PROJECT_HOME = new java.io.File( "." ).getCanonicalPath();
				}
				catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			
			frame.setResizable(true);
			noCursor();
			ClockManager.setMainClock(new Clock());
			size(displayWidth, displayHeight);

			// Creating data for retraining the classifier.
			currentMotionInput = new FileMotionInput(gestureDir + File.separator + dataFile);
			scan = new Scanner(logs);
			
			controlsManager = new ControlsManager(this);
			
			effectsController = new FireflyEffects(controlsManager);

			if (currentMotionInput.init()) {
				currentMotionInput.repeat(false);

				responseLogic = new SimilarityResponseLogic();
				backupSoarResponseLogic = new SoarResponseLogic();
				backupSoarResponseLogic.start();
			} else {
				background(255, 0, 0);
			}

			ClockManager.getMainClock().start();
			timer = new Clock();
			timer.start();
			
			effectsController = new FireflyEffects(controlsManager);
			embodiment = new FireflyEmbodiment(this.g, effectsController);
			embodiment.initialize();
		}catch(Exception e){
			e.printStackTrace();
			restart();
		}
	}

	public void draw() {
		try{
			if(timer.iniTime() > UPDATE_TIMER){
				readInput();
				timer = new Clock();
				timer.start();
			}
			
			if (!currentMotionInput.isInit()) {
				return;
			} else {

				ClockManager.getMainClock().check();
				noStroke();
				fill(0, 0, 0, effectsController.activeParameters().FADE);
				rect(0, 0, displayWidth, displayHeight);
				Body userPose = currentMotionInput.next(ClockManager.getMainClock());
				Body vaiPose = null;
				if (null == userPose) {
					effectsController.update(null, null, null, null, null);
				}else{
					vaiPose = (Body) userPose.clone();
					PVector newCenter = vaiPose.center();
					newCenter = new PVector(vaiPose.center().x * -1, vaiPose.center().y, vaiPose.center().z);
					vaiPose = vaiPose.translated(newCenter);
					vaiPose = vaiPose.transformed(PVecUtilities.STANDARD_REFLECTED_BASIS, vaiPose.localBasis(), newCenter);
					vaiPose.setTimestamp(ClockManager.getMainClock().time());
						
					activityTracker.takePose(userPose);

					float time = currentMotionInput.getTime();
					float duration = currentMotionInput.getDuration();
						
					//System.out.println("boundary: "+ boundary);
					//System.out.println("time: "+time);
					//System.out.println(duration);
						
					if(!scan.hasNext())
						System.exit(0);
					while(time > duration && scan.hasNext()){
						if(swap){
							dataFile = tempData;
							currentMotionInput = new FileMotionInput(gestureDir + File.separator + dataFile);
							currentMotionInput.init();
							boundary = 0;
							swap = false;
							break;
						}
						String line = scan.nextLine();
						//if(line.contains(dataFile))
						String[] str = line.split(" ");
						if(str[0].trim().equals("File:") && !dataFile.equals(str[1].trim())){
							dataFile = str[1].trim();
							currentMotionInput = new FileMotionInput(gestureDir + File.separator + dataFile);
							currentMotionInput.init();
							boundary = 0;
							swap = false;
							//restartTracker();
							break;
						}
					}
						
					while(!swap && (boundary == 0 || boundary < time - .15f) && scan.hasNext()){
						String line = scan.nextLine();
						String[] str = line.split(" ");
						//inbetween = new Clock();
						if(str[0].trim().equals("File:") && !dataFile.equals(str[1].trim())){
							swap = true;
							tempData = str[1].trim();
							break;
						}
						if(str[0].trim().equals("Time:")){
							boundary = Float.parseFloat(str[1].trim());
							inbetween = boundary - time;
							break;
						}
					}	

					if(inbetween > 2f && ( ((boundary-time) < inbetween*(2.0f/5) + .1f && (boundary-time) > inbetween*(2.0f/5) - .1f) || ((boundary-time) < inbetween*(3.0f/5) + .1f && (boundary-time) > inbetween*(3.0f/5) - .1f) ) ){
						markFrames(0);
					}
					if(boundary < time + .15 && boundary > time - .15){
						markFrames(1);
					}
						
					effectsController.update(userPose, vaiPose, null, null, null);
				}
				
				fill(255);
				textAlign(LEFT, TOP);
				textSize(40);
				pushMatrix();
				translate(width/2, height/2);
				embodiment.draw();
				popMatrix();
				//Draw GUI Text
				displayGUI();
			}
		}catch(Exception e){
			e.printStackTrace();
			restart();
		}
	}
	
	@Override
	public boolean sketchFullScreen() {
		  return true;
	}
	
	public void addGUIText(String message, long duration) {
		for(int index = 0; index < listGUIText.size(); index++) {
			String testMessage = listGUIText.get(index);
			if(message.split(":")[0].equalsIgnoreCase(testMessage.split(":")[0])) {
				listGUIText.remove(index);
				listGUIDuration.remove(index);
				break;
			}
		}
		
		this.listGUIText.add(message.toUpperCase());
		this.listGUIDuration.add(System.currentTimeMillis() + duration);
	}
	
	public void displayGUI() {
		long timeNowMillis = System.currentTimeMillis();
		textSize(32);
		float xPosPercent = 2f / 100f * (float)displayWidth;
		float yPosPercent = 3f / 100f * (float)displayHeight;
		yPosPercent += 5f / 100f * (float)displayHeight;
		for(int index = 0; index < listGUIText.size(); index++) {
			String message = listGUIText.get(index);
			long displayTime = listGUIDuration.get(index);
			text(message, xPosPercent, yPosPercent, 75f);
			yPosPercent += 5f / 100f * (float)displayHeight;
			if(displayTime < timeNowMillis) {
				listGUIText.remove(index);
				listGUIDuration.remove(index);
			}
		}
	}
	
	private void readInput(){
		List<String> lines = null;
		try {
			lines = Files.readAllLines(Paths.get(PROJECT_HOME + File.separator + "file_communications" + File.separator + "commands.txt"),
					Charset.defaultCharset());
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}	    
		for (String line : lines) {
			if(line.contains("r")){
				try {
					currentMotionInput = new FileMotionInput(playbackFile);
					changeMotionInput();
					
				} catch (Exception e) {
				        e.printStackTrace();
				} finally {
					FileUtilities.clearText(PROJECT_HOME + File.separator + "file_communications" + File.separator + "commands.txt");
				}
			}
        }
	}
	
	private void restart(){
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(PROJECT_HOME + File.separator + "file_communications" + File.separator + "restart_commands.txt", true));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		out.println("rf");
		out.close();
	}
	
	private void changeMotionInput(){
		if (currentMotionInput.init()) {
				currentMotionInput.repeat(false);
			
			effectsController = new FireflyEffects(controlsManager);
			
			embodiment = new FireflyEmbodiment(this.g, effectsController);
			embodiment.initialize();

			if(responseLogic == null) {
				responseLogic = new SimilarityResponseLogic();
				backupSoarResponseLogic = new SoarResponseLogic();
				backupSoarResponseLogic.start();
			}
		} else {
			background(255, 0, 0);
		}
	}
	
	public void markFrames(int bound){
		activityTracker.mark(bound, -1);
	}
	
	public static String getPlaybackName() {
		return playbackVideoName;
	}

	public FileWriter annotate(FileWriter writer, float time, boolean start, String type) {
		try {
			if (writer == null) {
				File file =  new File("gesture-timestamps/" + playbackVideoName + type + ".csv");
				if (file.exists()) {
					writer = new FileWriter(file, true);
					writer.write('\n');
					writer.write('\n');
				} else {
					writer = new FileWriter(file, true);
					writer.write("Start");
					writer.write(',');
					writer.write("End");
					writer.write('\n');
				}
			}
			
			writer.append(time + "");
			
			if (start) {
				writer.append(',');
			} else {
				writer.append('\n');
			}

			writer.flush();
			
			return writer;
			
		} catch(Exception e) {
			e.printStackTrace();
			return null;

		}
		
	}
	
	@Override
	public void exit() {
		flush();
		super.exit();
	}
	
	public static void main(String[] args) {
		PApplet.main("Viewpoints.FrontEnd.Application.ClassifierTrainer", new String[]{"--full-screen", "--display=1"});
	}

	/*
	 * Clear all text files used.
	 */
	public void flush() {
		FileUtilities.clearText(PROJECT_HOME + File.separator + "file_communications" + File.separator + "requestRandomResponse.txt");
	}
}
