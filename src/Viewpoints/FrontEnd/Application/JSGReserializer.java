package Viewpoints.FrontEnd.Application;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;

import Viewpoints.FrontEnd.Gesture.JointSpaceGesture.JointSpaceGesture;

/**
 * Use this class to reserialize gestures in the event of a package changes that affect the JSG files.
 * @author Lauren
 *
 */

public class JSGReserializer {

	public static void main(String[] args) {
		//List the directories to reserialize here.
		ArrayList<File> directories = new ArrayList<File>();
		directories.add(new File("example_gestures/gestures"));
		directories.add(new File("example_gestures/gesture-segments"));
		directories.add(new File("gesture-captured"));
		directories.add(new File("gesture-segments"));
		directories.add(new File("greetings"));
		directories.add(new File("gesture-segments"));
		directories.add(new File("retraining-gestures"));
		directories.add(new File("recorded-input-sessions"));
		directories.add(new File("runtime-gesture-segments"));
		directories.add(new File("training-gestures"));
		directories.add(new File("gesture-captured/oneShot"));
		
		ArrayList<File> fileNames = new ArrayList<File>();
		ListIterator<File> iterator = directories.listIterator();
		while (iterator.hasNext()) {
			File directory = iterator.next();
			fileNames.addAll(Arrays.asList(directory.listFiles()));
		}
		
		int gestureCount = 0;
		ListIterator<File> fileIterator = fileNames.listIterator();
		while (fileIterator.hasNext()) {
			File file = fileIterator.next();
			String path= file.getPath();
			if (file.isDirectory()) {
				File[] list = file.listFiles();
				for (int i = 0; i < list.length; i++) {
					fileIterator.add(list[i]);
				}
			} else {
				if (path.contains("jsg")) { 
					System.out.println(path);
					JointSpaceGesture jsg = JSGReserializer.deserializeJointsGesture(path);
					boolean success =JSGReserializer.serializeJointsGesture(path, jsg);
					if (success) {
						gestureCount++;
					}
				}
			}
		}

		System.out.println("Reserialized " + gestureCount + " gestures");
	
	}
	
	/**
	 * Serialize Joint Space Gesture
	 * 
	 * @param path
	 * @param outputGestureJoints
	 * @return
	 */
	public static Boolean serializeJointsGesture(String path,
			JointSpaceGesture outputGestureJoints) {
		FileOutputStream fileoutstream = null;
		ObjectOutputStream objectoutstream = null;
		try {
			fileoutstream = new FileOutputStream(path);
			objectoutstream = new ObjectOutputStream(fileoutstream);
			objectoutstream.writeObject(outputGestureJoints);
			objectoutstream.flush();
			objectoutstream.close();
			fileoutstream.flush();
			fileoutstream.close();
		} catch (IOException e) {
			return false;
		} finally
		{ try {
			if (null != objectoutstream) {
					objectoutstream.close();
				} if (null != fileoutstream) {
					fileoutstream.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

		return true;
	}

	/**
	 * Deserialize Joint Space Gesture
	 * 
	 * @param path
	 * @return
	 */
	public static JointSpaceGesture deserializeJointsGesture(String path)
	{
		JointSpaceGesture readGesture = new JointSpaceGesture();
		FileInputStream fileinstream = null;
		HackedObjectInputStream objectinstream = null;
		try {
			File infile = new File(path);
			fileinstream = new FileInputStream(infile);

			if (fileinstream.available() == 0) {
				fileinstream.close();
				return null;
			}

			objectinstream = new HackedObjectInputStream(fileinstream);

			readGesture = (JointSpaceGesture) objectinstream.readObject();
			objectinstream.close();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		} finally { 
			try {
				if (null != objectinstream) {
					objectinstream.close();
				} if (null != fileinstream) {
					fileinstream.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

		if(readGesture == null)
			return null;
		readGesture.autotiming();
		return readGesture;
	}

	
	//Class for fixing refactoring issues with serialization. Leaving in for now.
		private static class HackedObjectInputStream extends ObjectInputStream {
			/**
		     * Migration table. Holds old to new classes representation.
		     */
		    private static final Map<String, Class<?>> SERIALIZATION_MAP = new HashMap<String, Class<?>>();

		    static
		    {
		        SERIALIZATION_MAP.put("Viewpoints.Shared.JointSpaceGesture", Viewpoints.FrontEnd.Gesture.JointSpaceGesture.JointSpaceGesture.class);
		        SERIALIZATION_MAP.put("Viewpoints.Shared.Body", Viewpoints.FrontEnd.Shared.Body.class);
		        SERIALIZATION_MAP.put("Viewpoints.Shared.SkeletalData", Viewpoints.FrontEnd.Shared.SkeletalData.class);
		        SERIALIZATION_MAP.put("Viewpoints.Shared.JIDX", Viewpoints.FrontEnd.Shared.JIDX.class);
		    }

		    public HackedObjectInputStream(final InputStream in) throws IOException {
		        super(in);
		    }

		    @Override
		    protected ObjectStreamClass readClassDescriptor() throws IOException, ClassNotFoundException {
		        ObjectStreamClass resultClassDescriptor = super.readClassDescriptor();
		        
		        for (final String oldName : SERIALIZATION_MAP.keySet())
		        {
		            if (resultClassDescriptor.getName().equals(oldName))
		            {
		                String replacement = SERIALIZATION_MAP.get(oldName).getName();

		                try
		                {
		                    Field f = resultClassDescriptor.getClass().getDeclaredField("name");
		                    f.setAccessible(true);
		                    f.set(resultClassDescriptor, replacement);
		                }
		                catch (Exception e)
		                {
		                	e.printStackTrace();
		                }

		            }
		        }
		        return resultClassDescriptor;
		    }
		}
}
