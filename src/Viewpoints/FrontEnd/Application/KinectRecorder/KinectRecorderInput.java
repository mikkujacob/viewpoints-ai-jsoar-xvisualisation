package Viewpoints.FrontEnd.Application.KinectRecorder;

/**
 * Class to record and play back kinect joint space input
 * @author mjacob6
 *
 */
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import polymonkey.time.Time;
import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;
import SimpleOpenNI.*;
import Viewpoints.FrontEnd.Application.ViewpointsFrontEnd;
import Viewpoints.FrontEnd.Communication.File.FileUtilities;
import Viewpoints.FrontEnd.Gesture.JointSpaceGesture.JointSpaceGesture;
import Viewpoints.FrontEnd.Input.MotionInput.FileMotionInput;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Shared.JIDX;
import Viewpoints.FrontEnd.Shared.LIDX;
import Viewpoints.FrontEnd.Shared.Pair;

public class KinectRecorderInput extends PApplet {

	/**
	 * Generated serial id
	 */
	private static final long serialVersionUID = -6048102105138254150L;
	
	public SimpleOpenNI  context;
	GestureController gc;
	
	private ArrayList<String> listGUIText = new ArrayList<String>();
	private ArrayList<Long> listGUIDuration = new ArrayList<Long>();

	public ArrayList<Body> jointFrameList = new ArrayList<Body>();
	public ArrayList<Body> playBackJointFrameList = new ArrayList<Body>();
	public JointSpaceGesture recordedGesture = new JointSpaceGesture();
	public JointSpaceGesture readGesture = new JointSpaceGesture();
	public int playBackIndex = 0;

	public Boolean isStarted = false;
	public Boolean isRecordMode = true;
	public Boolean isPlayBackMode = false;
	public Boolean isGesture = false;
	public Boolean isKinectEnabled = true;
	public Boolean isSegmenterMode = false;
	public Boolean isImageVisible = false;
	
	public String PROJECT_HOME;
	public ArrayList<String> gestureFileNameList = new ArrayList<String>();
	public String gestureSubDir ="studies/004";
	public String gestureLibraryDir = "gestures" + "/" + gestureSubDir;//-segments";
	public String segmenterModeGestureLibraryDir = "gestures/segmenter_mode";
	public int gestureFileIndex = 0;
	
	public String kinectPredicatesOutPath = "file_communications/KinectPredicatesToJSOAR.txt";
	public String kinectJointsOutPath = "file_communications/KinectJointsToJSOAR.txt";

	public Time time;
	
	public void setup()
	{
		size(displayWidth, displayHeight);
		PROJECT_HOME = System.getProperty("user.dir");
		if(PROJECT_HOME.equalsIgnoreCase(null))
		{
			try
			{
				PROJECT_HOME = new java.io.File( "." ).getCanonicalPath();
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
			
			if(PROJECT_HOME.equalsIgnoreCase(null))
			{
				System.out.println("ERROR! PROJECT_HOME IS NULL");
			}
		}
		
		if(PROJECT_HOME.endsWith(File.separator + "bin"))
		{
			PROJECT_HOME = PROJECT_HOME.substring(0, PROJECT_HOME.length() - 4);
		}
		
		gestureLibraryDir = PROJECT_HOME + File.separator + gestureLibraryDir;
		segmenterModeGestureLibraryDir = PROJECT_HOME + File.separator + segmenterModeGestureLibraryDir;
		kinectPredicatesOutPath = PROJECT_HOME + File.separator + kinectPredicatesOutPath;
		kinectJointsOutPath = PROJECT_HOME + File.separator + kinectJointsOutPath;
		
		System.out.println("Gesture Library Path: " + gestureLibraryDir);
		
		gc = new GestureController();
		
		// context = new SimpleOpenNI(this);
		context = new SimpleOpenNI(this,SimpleOpenNI.RUN_MODE_MULTI_THREADED);
//		context = new SimpleOpenNI(this);

		// disable mirror
		context.setMirror(true); // from Kinect_Interface code
		
		// enable depthMap generation 
		if (!context.isInit())
		{
			println("Can't open the depthMap, maybe the camera is not connected!");
			println("Cannot use RecordMode");
			isKinectEnabled = false;
			toggleRecordPlayBack();
		}
		else
		{
			//enable depth map
			println("Enabled Color Image: " + context.enableRGB());
			
			//enable depth map
			println("Enabled DepthMap: " + context.enableDepth());

			// enable skeleton generation for all joints
			println("Enabled User Tracking: " + context.enableUser());
			
			//enable IR map.
//			println("Enabled IRMap: " + context.enableIR());
		}
		
		background(200,0,0);

		stroke(255,255,255);
		strokeWeight(10);
		smooth();

//		if(isKinectEnabled)
//		{
////			size(context.depthWidth(), context.depthHeight());
//			size(displayWidth, displayHeight);
//		}
//		else
//		{
//			size(displayWidth, displayHeight);
//		}
		
		jointFrameList.clear();
		playBackJointFrameList.clear();
		
		//Import Gesture Library File Names
		String gestureFile;
		File gestureLibraryFile = new File(gestureLibraryDir);
		if (!gestureLibraryFile.exists()) {
			gestureLibraryFile.mkdir();
		}

		File[] gestureFileList = gestureLibraryFile.listFiles(); 
		int readGestureCount = 0;
		
		if(gestureFileList != null)
		{
			for (int i = 0; i < gestureFileList.length; i++) 
			{
				if (gestureFileList[i].isFile()) 
				{
					gestureFile = gestureFileList[i].getName();
					if (gestureFile.toLowerCase().endsWith(".jsg"))
					{
						System.out.println("Gesture File: " + gestureLibraryDir + File.separator + gestureFile);
						
						gestureFileNameList.add(gestureFile);
						
						readGestureCount++;
					}
				}
			}
		}

		if(readGestureCount == 0) {
			System.out.println("No Gesture Files In Folder: " + gestureLibraryDir);
		}
//		
//		//Increase FrameRate Of Processing
		//TODO: Debug this with different values and see whether anything needs to be changed.
		//Currently set at 15 because some gestures seem to be recorded at that frame rate.
		this.frameRate(30);
//		this.frameRate(15);
//		frameRate(5);
		
		time = new Time(this);
	}

	public void draw()
	{
		//noStroke();
		noStroke();
        fill(0);//fill(0, 0, 0, 32); //fill(2, 2, 32, 255);
		rect(0, 0, width, height);
		
		if(isKinectEnabled)
		{
			// update the cam
			context.update();
		}
		
		// draw depthImageMap
		if(isKinectEnabled && isImageVisible)
		{
			drawDepthImage();
		}
	
		addGUIText("STARTED: " + isStarted, 100);
		
		displayGUI();
		
		translate(width/2, height/2);
		
		Body body = null;

		if(isKinectEnabled)
		{
			int[] userList = context.getUsers();
			
			// draw the skeleton if it's available
			for(int i = 0; i < userList.length; i++)
			{
				int userId = userList[i];
				if(context.isTrackingSkeleton(userId) && isRecordMode) 
				{
					body = new Body();
					body.initialize(context, userId, Time.getCountTime());
					drawBody(body);

					jointFrameList.add(body);
					
					break;
				}
			}
		}
		
		if(isStarted && isPlayBackMode)
		{
			body = jointFrameList.get(playBackIndex);
			
			if (0.0 == body.getTimestamp()) { //probably not initialized
				body.setTimestamp((float)playBackIndex / 30.0f);
			}
			
			playBackJointFrameList.add(body);
			
			if(isKinectEnabled)
			{
				drawBody(body);
			}
			
			if(playBackIndex == jointFrameList.size() - 1)
			{
				toggleStartStop();
				
				if(!isGesture)
				{
					gc.detectedGestureCleanup();
					onGesture();
					recordedGesture = new JointSpaceGesture();
				}
				else
				{
					gc.gcGestureDetectionReset();
					
					isGesture = false;
				}
				playBackJointFrameList.clear();
			} else {
				playBackIndex++;
			}
		}
		
		if (null != body)
//			gc.update(body);  
		
		if(gc.hasGesture())
		{
			onGesture();
			isGesture = true;
		}
	}
	
	/*
	 * Displays a string message on the frontend screen during displayGUI() for a long duration in milliseconds.
	 */
	public void addGUIText(String message, long duration) {
		for(int index = 0; index < listGUIText.size(); index++) {
			String testMessage = listGUIText.get(index);
			if(message.split(":")[0].equalsIgnoreCase(testMessage.split(":")[0])) {
				listGUIText.remove(index);
				listGUIDuration.remove(index);
				break;
			}
		}
		
		this.listGUIText.add(message.toUpperCase());
		this.listGUIDuration.add(System.currentTimeMillis() + duration);
	}
	
	/*
	 * Displays tHe items in listGUIText for the duration specified in addGUIText()
	 */
	public void displayGUI() {
		pushMatrix();
		int oldFill = g.fillColor;
		fill(255);
		textAlign(LEFT, TOP);
		textSize(40);
		long timeNowMillis = System.currentTimeMillis();
		
		float xPosPercent = 4f / 1000f * (float)displayWidth;
		float yPosPercent = 3f / 1000f * (float)displayHeight;
		yPosPercent += 5f / 100f * (float)displayHeight;
		for(int index = 0; index < listGUIText.size(); index++) {
			String message = listGUIText.get(index);
			long displayTime = listGUIDuration.get(index);
			text(message, xPosPercent, yPosPercent, 1.7f);
			yPosPercent += 5f / 100f * (float)displayHeight;
			if(displayTime < timeNowMillis) {
				listGUIText.remove(index);
				listGUIDuration.remove(index);
			}
//			System.out.println(message);
		}
		fill(oldFill);
		popMatrix();
	}
	
	private void drawDepthImage()
	{
		PImage depthImage = context.depthImage();
//		depthImage.resize(displayWidth, displayHeight);
//		PImage userMaskImage = context.userImage();
//		userMaskImage.resize(displayWidth, displayHeight);
		
//		depthImage.mask(userMaskImage);
		
		PImage rgbImage = context.rgbImage();
		
//		PImage depthImage = context.irImage();
		
		image(depthImage, 0, 0, displayWidth, displayHeight);
		image(rgbImage, depthImage.width, 0, displayWidth, displayHeight);
	}
	
	private void onGesture() {
//		FileUtilities.writeToText(kinectPredicatesOutPath, gc.yieldCurrentGesture());
		Boolean isGestureWritten = FileUtilities.serializeGesture(kinectPredicatesOutPath, gc.yieldCurrentGesture());
		recordedGesture = gc.yieldJSGesture();
		isGestureWritten = isGestureWritten && FileUtilities.serializeJointsGesture
				(kinectJointsOutPath, recordedGesture);
		
		if(isSegmenterMode)
		{
			Boolean isSegmenterWritten = FileUtilities.serializeJointsGesture
					(segmenterModeGestureLibraryDir + File.separator + UUID.randomUUID().toString() + ".jsg", recordedGesture);
			
			if(!isSegmenterWritten)
			{
				System.out.println("Error: Did not write Segmenter Mode Gesture!");
			}
		}
		
		if(!isGestureWritten)
		{
			System.out.println("Error: Did not write Segmented Gesture");
		}
	}

	// -----------------------------------------------------------------
	// SimpleOpenNI events

	public void onNewUser(SimpleOpenNI context, int userId)
	{
		println("onNewUser - userId: " + userId);
		
		context.startTrackingSkeleton(userId);
	}

	public void onLostUser(SimpleOpenNI context, int userId)
	{
		println("onLostUser - userId: " + userId);
		
		if(isRecordMode && isStarted)
		{
//			toggleStartStop();
		}
	}
	
	public void onVisibleUser(SimpleOpenNI context, int userId)
	{
//	  println("onVisibleUser - userId: " + userId);
	}
	
	public void keyPressed()
	{
		if(key == ' ')
		{
			//Stop / Start whatever mode
			toggleStartStop();
		}
		else if(key == 'r' || key == 'R')
		{	
			//Record Mode && Kinect is Connected
			if(!isRecordMode && isKinectEnabled)
			{
				toggleRecordPlayBack();
			}
		}
		else if(key == 'p' || key == 'P')
		{
			//Play Back Mode
			if(!isPlayBackMode)
			{
				toggleRecordPlayBack();
			}
		}
		else if(key == 's' || key == 'S')
		{
			//Toggle Segmenter Mode
			toggleSegmenterMode();
		}
		else if (key == DELETE) {
			if (!gestureFileNameList.isEmpty()) {
				File theFile = new File(gestureLibraryDir + "/" + gestureFileNameList.get(gestureFileIndex));
				theFile.delete();
				gestureFileNameList.remove(gestureFileIndex);
				if (gestureFileNameList.size() == gestureFileIndex) {
					gestureFileIndex--;
				}
			}
		}
		else if(key == 'i' || key == 'I')
		{
			isImageVisible = !isImageVisible;
		}
		else if(key == CODED)
		{
			//Non ASCII Character
			if(keyCode == UP)
			{
				//Select Next Gesture In Library
				if(gestureFileIndex < gestureFileNameList.size() - 1)
				{
					gestureFileIndex++;
					System.out.println("Gesture Play Back Index Incremented = " + gestureFileIndex);
				}
			}
			else if(keyCode == DOWN)
			{
				//Select Previous Gesture In Library
				if(gestureFileIndex > 0)
				{
					gestureFileIndex--;
					System.out.println("Gesture Play Back Index Decremented = " + gestureFileIndex);
				}
			}
		}
	}
	
	public void toggleStartStop()
	{
		if(isStarted)
		{
			//Started To Stopped
			if(isRecordMode)
			{
				//Stop Recording
				//TODO if needed
				
				System.out.println("Stopping Recording");
				
				if(jointFrameList.size() > 0)
				{
					recordedGesture = new JointSpaceGesture(jointFrameList);
					String fileName = (new Date()).toString().replace(':','-') + ".jsg";
					fileName = fileName.replaceAll(" ", "");
					Boolean isGestureWritten = FileUtilities.serializeJointsGesture
							(gestureLibraryDir + "/" + fileName, recordedGesture);
					if(isGestureWritten)
					{
						gestureFileNameList.add(fileName);
					}
					else
					{
						System.out.println("Error: Did not write recordedGesture");
					}
					
					recordedGesture = new JointSpaceGesture();
					jointFrameList.clear();
				}
			}
			else if(isPlayBackMode)
			{
				//Stop Playing Back Gestures
				//TODO if needed
				
				System.out.println("Stopping Play Back");
				
				playBackIndex = 0;
			}
			
			isStarted = false;
		}
		else
		{
			//Stopped To Started
			if(isRecordMode)
			{
				//Start Record Mode
				//TODO if needed
				jointFrameList.clear();
				this.gc.gcGestureDetectionReset();
				
				System.out.println("Starting Recording");
			}
			else if(isPlayBackMode)
			{
				//Start Play Back Mode
				//TODO if needed
				
				System.out.println("Starting Play Back: " + gestureFileNameList.get(gestureFileIndex));
				
				readGesture = new JointSpaceGesture(FileUtilities.deserializeJointsGesture(gestureLibraryDir + "/" + gestureFileNameList.get(gestureFileIndex)));
				if(readGesture == null)
				{
					System.out.println("Error: Did not read readGesture");
					System.out.println("Error: Play Back Failed");
					return;
				}
				
				jointFrameList = new ArrayList<Body>(readGesture.getGestureFramesList());
				readGesture = new JointSpaceGesture();
			}
			
			isStarted = true;
		}
	}
	
	public void toggleRecordPlayBack()
	{
		if(isStarted)
		{
			toggleStartStop();
		}
		
		if(isRecordMode && !isPlayBackMode)
		{
			System.out.println("Record Mode: OFF\tPlay Back Mode: ON");
			
			size(displayWidth, displayHeight);
			
			isRecordMode = false;
			isPlayBackMode = true;
		}
		else if(isPlayBackMode && !isRecordMode)
		{
			System.out.println("Record Mode: ON\tPlay Back Mode: OFF");
			
//			size(context.depthWidth(), context.depthHeight());
			size(displayWidth, displayHeight);
			
			isPlayBackMode = false;
			isRecordMode = true;
		}
	}
	
	public void toggleSegmenterMode()
	{
		//Place into Segmenter Mode
		isSegmenterMode = !isSegmenterMode;
		System.out.println("Segmenter Mode: " + isSegmenterMode);
	}
	
	public static void main(String[] args) {
		main("Viewpoints.FrontEnd.Application.KinectRecorder.KinectRecorderInput", new String[]{});
	}
	
	public PVector getScreenPos(PVector pos) {
		PVector screenPos = PVector.mult(pos, 0.3f); //new PVector();
		screenPos.y = - screenPos.y;
		screenPos.z = 800.0f / screenPos.z;
		screenPos.x *= screenPos.z;
		screenPos.y *= screenPos.z;
		return screenPos;
	}
	
	public void drawBody(Body body) 
	{
		stroke(255,255,255);
		strokeWeight(1);
		
		for (Map.Entry<LIDX, Pair<JIDX, JIDX>> limbEnds : LIDX.LIMB_ENDS().entrySet()) {
			PVector pos1 = getScreenPos(body.get(limbEnds.getValue().first));
			PVector pos2 = getScreenPos(body.get(limbEnds.getValue().second));
			line(pos1.x, pos1.y, pos2.x, pos2.y);
		}
	}
	
	public void drawVector(PVector origin, PVector vec) {
		strokeWeight(1);
		PVector pos1 = getScreenPos(origin);
		PVector pos2 = getScreenPos(PVector.add(origin, vec));
		line(pos1.x, pos1.y, pos2.x, pos2.y);
	}
}

