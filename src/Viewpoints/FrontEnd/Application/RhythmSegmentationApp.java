package Viewpoints.FrontEnd.Application;

import java.io.File;
import java.util.*;

import Viewpoints.FrontEnd.Communication.File.FileUtilities;
import Viewpoints.FrontEnd.Gesture.JointSpaceGesture.JointSpaceGesture;
import Viewpoints.FrontEnd.Model.SegmentNormalization;
import Viewpoints.FrontEnd.Segmentation.*;
import Viewpoints.FrontEnd.Shared.Body;
import Viewpoints.FrontEnd.Model.AestheticsModel.*;

public class RhythmSegmentationApp {
	
	private RhythmSegmenter rhythmSegmenter = new RhythmSegmenter();

	public ArrayList<JointSpaceGesture> segmentMotion(ArrayList<Body> motion) {
		ArrayList<JointSpaceGesture> output = new ArrayList<JointSpaceGesture>();
		for (Body body : motion) {
			ViewpointsAestheticsModel.getRhythmTracker().takePose(body);
			ArrayList<Body> segment = rhythmSegmenter.getSegment();
			if (null != segment) {
				output.add(new JointSpaceGesture(segment));
			}
		}
		return output;
	}
	
	public void segmentLibrary(String infolder, String outfolder) {
		File infolderfile = new File(infolder);
		for (String gesturefile : infolderfile.list()) {
			String[] dotSeparated = gesturefile.split("\\.");
			if (!"jsg".equals(dotSeparated[dotSeparated.length - 1]))
				continue;
			JointSpaceGesture gesture = FileUtilities.deserializeJointsGesture(infolder + "/" + gesturefile);
			ArrayList<JointSpaceGesture> segments = segmentMotion(gesture.getGestureFramesList());
			int index = 0;
			for (JointSpaceGesture segment : segments) {
				index += 1;
				segment = SegmentNormalization.normalize(segment);
				String outfile = gesturefile.replaceFirst("[.][^.]+$", "") + "-" + Integer.toString(index) + ".jsg";
				FileUtilities.serializeJointsGesture(outfolder + "/" + outfile, segment);
			}
		}
	}
	
	public static void main(String[] args) {
		RhythmSegmentationApp segmenter = new RhythmSegmentationApp();
		String targetDirs[] = {"training-gestures/noemotion-verified",
							   "training-gestures/angry-golden",
							   "training-gestures/joy-golden",
							   "training-gestures/fear-golden",
							   "training-gestures/sad-golden",
							   "greetings",
							   "runtime_gestures"};
		for (String targetDir : targetDirs) {
			segmenter.segmentLibrary(targetDir, "gesture-segments");
		}
	}
}
