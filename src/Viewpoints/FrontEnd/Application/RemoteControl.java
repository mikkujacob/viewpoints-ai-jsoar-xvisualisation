package Viewpoints.FrontEnd.Application;

import java.util.ArrayList;

import Viewpoints.FrontEnd.Communication.Socket.RemoteControlSocketManager;
import Viewpoints.FrontEnd.Communication.Socket.RemoteControl.RemoteControlData.RemoteControlPreset;
import processing.core.PApplet;

public class RemoteControl extends PApplet
{

	/**
	 * Serialization version ID.
	 */
	private static final long serialVersionUID = 3592649626423591013L;

	/**
	 * 
	 */
	private ArrayList<String> listGUIText = new ArrayList<String>();

	/**
	 * 
	 */
	private ArrayList<Long> listGUIDuration = new ArrayList<Long>();

	private RemoteControlPreset currentPreset;
	
	private RemoteControlSocketManager socketManager;
	
	public static void main(String[] args)
	{
		// PApplet.main("Viewpoints.FrontEnd.Application.RemoteControl", new
		// String[]{"--full-screen", "--display=1"});
		PApplet.main("Viewpoints.FrontEnd.Application.RemoteControl", new String[] { "" });
	}

	public void setup()
	{
		size(displayWidth, displayHeight);
		currentPreset = RemoteControlPreset.NONE;
		socketManager = new RemoteControlSocketManager();
	}
	
	public void draw()
	{
		background(0);
		addGUIText("CURRENT PRESET: " + currentPreset.toString(), 50);
		displayGUI();
	}

	public void keyPressed()
	{
		if (key == '1')
		{
			System.out.println("ONE");
			currentPreset = RemoteControlPreset.ONE;
			socketManager.togglePreset(currentPreset);
		}

		if (key == '2')
		{
			System.out.println("TWO");
			currentPreset = RemoteControlPreset.TWO;
			socketManager.togglePreset(currentPreset);
		}

		if (key == '3')
		{
			System.out.println("THREE");
			currentPreset = RemoteControlPreset.THREE;
			socketManager.togglePreset(currentPreset);
		}

		if (key == '4')
		{
			System.out.println("FOUR");
			currentPreset = RemoteControlPreset.FOUR;
			socketManager.togglePreset(currentPreset);
		}

		if (key == '5')
		{
			System.out.println("FIVE");
			currentPreset = RemoteControlPreset.FIVE;
			socketManager.togglePreset(currentPreset);
		}

		if (key == '6')
		{
			System.out.println("SIX");
			currentPreset = RemoteControlPreset.SIX;
			socketManager.togglePreset(currentPreset);
		}

		if (key == '7')
		{
			System.out.println("SEVEN");
			currentPreset = RemoteControlPreset.SEVEN;
			socketManager.togglePreset(currentPreset);
		}

		if (key == '8')
		{
			System.out.println("EIGHT");
			currentPreset = RemoteControlPreset.EIGHT;
			socketManager.togglePreset(currentPreset);
		}

		if (key == '9')
		{
			System.out.println("NINE");
			currentPreset = RemoteControlPreset.NINE;
			socketManager.togglePreset(currentPreset);
		}

		if (key == '0')
		{
			System.out.println("ZERO");
			currentPreset = RemoteControlPreset.ZERO;
			socketManager.togglePreset(currentPreset);
		}
		
		if (key == 'a' || key == 'A')
		{
			System.out.println("A");
			currentPreset = RemoteControlPreset.A;
			socketManager.togglePreset(currentPreset);
		}

		if (key == 's' || key == 'S')
		{
			System.out.println("S");
			currentPreset = RemoteControlPreset.S;
			socketManager.togglePreset(currentPreset);
		}

		if (key == 'd' || key == 'D')
		{
			System.out.println("D");
			currentPreset = RemoteControlPreset.D;
			socketManager.togglePreset(currentPreset);
		}

		if (key == 'f' || key == 'F')
		{
			System.out.println("F");
			currentPreset = RemoteControlPreset.F;
			socketManager.togglePreset(currentPreset);
		}

		if (key == 'g' || key == 'G')
		{
			System.out.println("G");
			currentPreset = RemoteControlPreset.G;
			socketManager.togglePreset(currentPreset);
		}

		if (key == 'h' || key == 'H')
		{
			System.out.println("H");
			currentPreset = RemoteControlPreset.H;
			socketManager.togglePreset(currentPreset);
		}

		if (key == 'j' || key == 'J')
		{
			System.out.println("J");
			currentPreset = RemoteControlPreset.J;
			socketManager.togglePreset(currentPreset);
		}

		if (key == 'k' || key == 'K')
		{
			System.out.println("K");
			currentPreset = RemoteControlPreset.K;
			socketManager.togglePreset(currentPreset);
		}

		if (key == 'l' || key == 'L')
		{
			System.out.println("L");
			currentPreset = RemoteControlPreset.L;
			socketManager.togglePreset(currentPreset);
		}
	}

	/*
	 * Displays a string message on the frontend screen during displayGUI() for
	 * a long duration in milliseconds.
	 */
	public void addGUIText(String message, long duration)
	{
		for (int index = 0; index < listGUIText.size(); index++)
		{
			String testMessage = listGUIText.get(index);
			if (message.split(":")[0].equalsIgnoreCase(testMessage.split(":")[0]))
			{
				listGUIText.remove(index);
				listGUIDuration.remove(index);
				break;
			}
		}

		this.listGUIText.add(message.toUpperCase());
		this.listGUIDuration.add(System.currentTimeMillis() + duration);
	}

	/*
	 * Displays tHe items in listGUIText for the duration specified in
	 * addGUIText()
	 */
	public void displayGUI()
	{
		pushMatrix();
		int oldFill = g.fillColor;
		fill(255);
		textAlign(LEFT, TOP);
		textSize(40);
		long timeNowMillis = System.currentTimeMillis();

		float xPosPercent = 4f / 1000f * (float) displayWidth;
		float yPosPercent = 3f / 1000f * (float) displayHeight;

		yPosPercent += 5f / 100f * (float) displayHeight;
		for (int index = 0; index < listGUIText.size(); index++)
		{
			String message = listGUIText.get(index);
			long displayTime = listGUIDuration.get(index);
			text(message, xPosPercent, yPosPercent, 1.7f);
			yPosPercent += 5f / 100f * (float) displayHeight;
			if (displayTime < timeNowMillis)
			{
				listGUIText.remove(index);
				listGUIDuration.remove(index);
			}
			// System.out.println(message);
		}
		fill(oldFill);
		popMatrix();
	}
}
