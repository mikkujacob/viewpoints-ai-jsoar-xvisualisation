package Viewpoints.FrontEnd.Application;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import Viewpoints.FrontEnd.Audio.AudioPlayer.MusicControl;
import Viewpoints.FrontEnd.Clock.Clock;
import Viewpoints.FrontEnd.Clock.ClockManager;
import Viewpoints.FrontEnd.Communication.File.FileUtilities;
import Viewpoints.FrontEnd.Controls.ControlsManager;
import Viewpoints.FrontEnd.Graphics.EffectsController.EmbodimentEffects.FireflyEffects;
import Viewpoints.FrontEnd.Graphics.Embodiment.ParticleEmbodiment.Fireflies.FireflyEmbodiment;
import Viewpoints.FrontEnd.Input.MotionInput.FileMotionInput;
import Viewpoints.FrontEnd.ResponseLogic.RandomResponseLogic;
import Viewpoints.FrontEnd.ResponseLogic.SimilarityResponseLogic;
import Viewpoints.FrontEnd.ResponseLogic.SoarResponseLogic;
import Viewpoints.FrontEnd.Shared.Body;
import controlP5.ControlP5;
import controlP5.Slider;
import processing.core.PApplet;

/**
 * Applet used to annotate the start and ends of a gesture boundary. This would only be used with a video playback.
 * @author Lauren
 *
 */
public class VideoAnnotater extends VideoPlayback{
	
	/**
	 * Set to true to generate an output file time_log.txt
	 * that can be used in @ClassifierTrainer 
	 */
	public final static boolean RETRAIN_CLASSIFIER = true;
	
	/**
	 * Adjust this number to set how quickly the arrow keys scrub through
	 * the video frames.
	 */
	private static float GRANULARITY = .05f;
	
	/**
	 * Use this to use to enable toggling through files in a directory instead of
	 * looking at a single file. 
	 */
	private final static boolean DIRECTORY_MODE = false;

	private static final long serialVersionUID = 1L;
	
	private boolean isPlaying = false;
	
	private static String playbackFilePrefix = "gestures/studies/004/";
	private static String playbackVideoName = "annotated";
	private final static String playbackFile = playbackFilePrefix + playbackVideoName + ".jsg";
	
	private String subDir = "studies/004";
	private String writeDirectory = "study-annotations" + "/" + subDir;

	private static File[] filenames; 
	public static int fileIndex = 0;
	private static FileWriter annotateWriter;

	private ControlP5 cp5; 
	private Slider slider; 
	
	private ControlsManager controlsManager;
	
	
	/**
	 * Boolean to keep track of whether a start or end has been detected.
	 */
	private boolean started = false;
	
	public void setup() {
		try{
			PROJECT_HOME = System.getProperty("user.dir");
			if(PROJECT_HOME.equalsIgnoreCase(null)) {
				try {
					PROJECT_HOME = new java.io.File( "." ).getCanonicalPath();
				}
				catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			
			//Creates directory for gesture file to toggle between.
			File dir = new File(playbackFilePrefix);
			setFilenames(dir.listFiles());

			noCursor();
			ClockManager.setMainClock(new Clock());
			vaiGlyphTimer = new Clock();
			userGlyphTimer = new Clock();

			if (DIRECTORY_MODE) {
				if(filenames.length > 0)
				{
					currentMotionInput = new FileMotionInput(filenames[fileIndex].getPath());
				}
//				System.out.println("Path: " + filenames[fileIndex].getPath());
			} else {
				currentMotionInput = new FileMotionInput(playbackFile);
//				System.out.println("Path: " + playbackFile);
			}
			
			controlsManager = new ControlsManager(this);
			controlsManager.setDRAW_EMBODIMENT(false);
			controlsManager.setIsBlendedGraphics(true);
			controlsManager.setDEBUG(true);

			effectsController = new FireflyEffects(controlsManager);

			if (currentMotionInput.init()) {
				
				//set response logic
				responseLogic = new SimilarityResponseLogic();
				backupSoarResponseLogic = new SoarResponseLogic();
				backupRandomResponseLogic = new RandomResponseLogic(getREHEARSAL_GESTURE_LIB());
				getBackupSoarResponseLogic().start();
				backupRandomResponseLogic.start();
			} else {
				background(255, 0, 0);
			}

			ClockManager.getMainClock().start();
			timer = new Clock();
			timer.start();
			

			audioController = new MusicControl(this);
			effectsController = new FireflyEffects(controlsManager);
			embodiment = new FireflyEmbodiment(this.g, effectsController);
			embodiment.initialize();
		}catch(Exception e){
			e.printStackTrace();
			restart();
		}
		size(displayWidth, displayHeight);

		cp5 = new ControlP5(this);
		slider = cp5.addSlider("sliderValue")
		.setPosition(30, displayHeight*.8f)
		.setSliderMode(Slider.FLEXIBLE)
		.setSize(450, 20)
		.setRange(0, getMotionInput().getDuration());
		
		slider.setVisible(true);
		slider.setColorForeground(0);
	}

	protected void changeMotionInput(){
		if (currentMotionInput.init()) {
			
			effectsController = new FireflyEffects(controlsManager);
			
			embodiment = new FireflyEmbodiment(this.g, effectsController);
			embodiment.initialize();
			currentMotionInput.repeat(repeat);
			
			if(responseLogic == null) {
				setResponseLogic(new SimilarityResponseLogic());
				setBackupSoarResponseLogic(new SoarResponseLogic());
				backupRandomResponseLogic = new RandomResponseLogic(getREHEARSAL_GESTURE_LIB());
				getBackupSoarResponseLogic().start();
				backupRandomResponseLogic.start();
			}
		} else {
			background(255, 0, 0);
		}
	}
	
	public void draw() {
		try{

			if (!currentMotionInput.isInit()) {
				return;
			} else {
				if (isPlaying) {
					slider.changeValue(currentMotionInput.getTime());
				}
				DecimalFormat df = new DecimalFormat("#.000");
				addGUIText("Time: " + df.format(currentMotionInput.getTime()), 1000);

				ClockManager.getMainClock().check();
				noStroke();
				fill(0, 0, 0, effectsController.activeParameters().FADE);
				rect(0, 0, displayWidth, displayHeight);
				Body userPose = null;
				if (!isPlaying) {
					userPose = ((FileMotionInput)currentMotionInput).next(slider.getValue());
				} else {
					userPose = currentMotionInput.next(ClockManager.getMainClock());
				}
				Body vaiPose = null;
				drawing(userPose, vaiPose);
			}
		}catch(Exception e){
			e.printStackTrace();
			restart();
		}
		
		if (DIRECTORY_MODE) {
			addGUIText("Gesture " + (fileIndex + 1) + ": " + filenames[fileIndex].getName(), 100);
		}
		cp5.draw();
	}
	
	public static String getPlaybackName() {
		return playbackVideoName;
	}

	///// Getters and Setters /////

	public void keyPressed() {
		if (key == 's' || key == 'S') {
			isPlaying = !isPlaying;
		}
		
		if (key == 'r' || key =='R') {
			isPlaying = false;
			slider.setValue(0);
			getMotionInput().replayAt(0);
			changeMotionInput();
		}
		
		if (DIRECTORY_MODE && keyCode == ENTER) {
			addGUIText("New video", 5000);
			updateIndex(1);
			slider.setValue(0);
			started = false;
			annotateWriter = null;
			currentMotionInput = new FileMotionInput(filenames[fileIndex].getPath());
			changeMotionInput();
			
		}
		
		if (!isPlaying && keyCode == LEFT && slider.getValue() > 0) {
			slider.setValue(slider.getValue() - GRANULARITY);
			getMotionInput().replayAt(-1 * GRANULARITY);
		}

		if (!isPlaying && keyCode == RIGHT && slider.getValue() < getMotionInput().getDuration()) {
			slider.setValue(slider.getValue() + GRANULARITY);
			getMotionInput().replayAt(GRANULARITY);
		}
		
		//Annotate boundary start.
		if(key == 'o' || key == 'o' || (!started && key == ' ')) {
			started = true;
			float time;
			if (RETRAIN_CLASSIFIER) {
				if (DIRECTORY_MODE) {
					time = getMotionInput().getTimeAndLog(filenames[fileIndex].getPath());
				} else {
					time = getMotionInput().getTimeAndLog(playbackFile);
				}
			} else {
				time = getMotionInput().getTime();
			}
			System.out.println("Starting: " + time);
			addGUIText("START TIME RECORDED: " + time, 5000);
			String date = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss").format(new Date());
			if (DIRECTORY_MODE) {
				annotateWriter = FileUtilities.annotate(annotateWriter, writeDirectory, filenames[fileIndex].getName(), time, true, "Annotate" + date);
			} else {
				annotateWriter = FileUtilities.annotate(annotateWriter, writeDirectory, playbackVideoName, time, true, "Annotate" + date);
			}
		} else if(key == 'p' || key == 'P' || (started && key == ' ')) { //Annotated boundary end.
			started = false;
			float time;
			if (RETRAIN_CLASSIFIER) {
				if (DIRECTORY_MODE) {
					time = getMotionInput().getTimeAndLog(filenames[fileIndex].getPath());
				} else {
					time = getMotionInput().getTimeAndLog(playbackFile);
				}
			} else {
				time = getMotionInput().getTime();
			}
			System.out.println("Ending: " + time);
			addGUIText("END TIME RECORDED: " + time, 5000);
			String date = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss").format(new Date());
			if (DIRECTORY_MODE) {
				annotateWriter = FileUtilities.annotate(annotateWriter, writeDirectory, filenames[fileIndex].getName(), time, false, "Annotate" + date);
			} else {
				annotateWriter = FileUtilities.annotate(annotateWriter, writeDirectory, playbackVideoName, time, false, "Annotate" + date);
			}
		}
	}
	
	//Updates the file index after toggling.
		public static void updateIndex(int delta) {
			fileIndex = (fileIndex + delta) % filenames.length;
			if (fileIndex < 0) {
				fileIndex = 0;
			}
			
			if (fileIndex >= filenames.length) {
				fileIndex = filenames.length - 1;
			}
		}

	public static void setFilenames(File[] filenames) {
		ArrayList<File> files = new ArrayList<File>(Arrays.asList(filenames));
		for(int index = 0; index < files.size(); index++)
		{
			if(!files.get(index).getName().endsWith("jsg"))
			{
				files.remove(index);
			}
		}
		filenames = files.toArray(filenames);
		VideoAnnotater.filenames = filenames;
	}
	
	public static void main(String[] args) {
		PApplet.main("Viewpoints.FrontEnd.Application.VideoAnnotater", new String[]{"--full-screen", "--display=1"});
	}
}
