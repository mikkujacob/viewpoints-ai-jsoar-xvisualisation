/**
 * 
 */
package Viewpoints.FrontEnd.Application;

import Viewpoints.FrontEnd.Communication.Video.Syphon.SyphonServer;
import Viewpoints.FrontEnd.Graphics.Embodiment.StencilEmbodiment.StencilStars;
import processing.core.PApplet;

/**
 * @author mikhailjacob
 *
 */
public class StencilStarSketch extends PApplet
{
	/**
	 * Generated serialization ID
	 */
	private static final long serialVersionUID = 3053217777336760015L;

	private StencilStars stencilStars;
	
	private SyphonServer syphonServer;
	
	private boolean isWindows;
	
	private boolean isVideoStreaming = true;
	
	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		PApplet.main("Viewpoints.FrontEnd.Application.StencilStarSketch", new String[]{"--full-screen", "--display=1"});
	}
	
	@Override
	public boolean sketchFullScreen()
	{
		return true;
	}
	
	public void setup()
	{
		size(displayWidth, displayHeight, PApplet.P3D);
		stencilStars = new StencilStars(this.g);
		background(color(0));
		
		if (System.getProperty("os.name").toLowerCase().indexOf("windows") > -1)  
		{
			isWindows = true;
		}
		else
		{
			isWindows = false;
		}
		
		if(isVideoStreaming)
		{
			if(!isWindows)
			{
				syphonServer = new SyphonServer(this, "StencilStarSketch");
				System.out.println("Syphon server loaded");
			}
		}
	}
	
	public void draw()
	{
		stencilStars.draw(!(isVideoStreaming && !isWindows));
		
		streamVideo();
	}
	
	public void streamVideo()
	{
		if(isVideoStreaming)
		{
			if(!isWindows)
			{
				if(syphonServer == null)
				{
					syphonServer = new SyphonServer(this, "StencilStarSketch");
					System.out.println("Syphon server loaded");
				}
				syphonServer.sendScreen();
			}
		}
	}
}
