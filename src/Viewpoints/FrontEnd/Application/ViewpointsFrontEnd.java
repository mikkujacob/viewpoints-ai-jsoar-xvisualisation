package Viewpoints.FrontEnd.Application;

import controlP5.*;

//Sending Joint Data
import oscP5.*;
import netP5.*;
import processing.net.*;

import java.lang.Boolean;
import java.nio.file.Files;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.util.*;

import SimpleOpenNI.SimpleOpenNI;
//import Viewpoints.FrontEnd.Audio.AudioPlayer.MusicControl;
import Viewpoints.FrontEnd.Clock.Clock;
import Viewpoints.FrontEnd.Clock.ClockManager;
import Viewpoints.FrontEnd.Communication.File.FileUtilities;
//import Viewpoints.FrontEnd.Communication.Socket.ViewpointsRemoteControlSocketManager;
import Viewpoints.FrontEnd.Communication.Socket.ViewpointsSocketManager;
//import Viewpoints.FrontEnd.Communication.Socket.RemoteControl.RemoteControlData.RemoteControlPreset;
import Viewpoints.FrontEnd.Communication.Video.Syphon.SyphonServer;
import Viewpoints.FrontEnd.Controls.ControlsManager;
import Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture.PredicateSpaceGesture;
import Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture.TransformPredicates;
import Viewpoints.FrontEnd.Gesture.PredicateSpaceGesture.TransformPredicates.RESPONSE_MODE;
import Viewpoints.FrontEnd.Graphics.Debug.Debug;
import Viewpoints.FrontEnd.Graphics.Debug.DebugOverlay;
import Viewpoints.FrontEnd.Graphics.EffectsController.EmbodimentEffects.FireflyEffects;
import Viewpoints.FrontEnd.Graphics.EffectsController.EmbodimentEffects.ShadowEffects;
//import Viewpoints.FrontEnd.Graphics.EffectsController.EmbodimentEffects.StencilEffects;
import Viewpoints.FrontEnd.Graphics.Embodiment.Embodiment;
import Viewpoints.FrontEnd.Graphics.Embodiment.ParticleEmbodiment.Fireflies.FireflyEmbodiment;
import Viewpoints.FrontEnd.Graphics.Embodiment.Shadow.ShadowEmbodiment;
//import Viewpoints.FrontEnd.Graphics.Embodiment.Shadow.ShadowParameters.MultiShadowType;
import Viewpoints.FrontEnd.Graphics.Embodiment.SkeletalEmbodiment.GraphicsUtilities;
//import Viewpoints.FrontEnd.Graphics.Embodiment.StencilEmbodiment.StencilEmbodiment;
//import Viewpoints.FrontEnd.Graphics.Embodiment.StencilEmbodiment.StencilEmbodiment.StencilDrawMethod;
import Viewpoints.FrontEnd.Graphics.Glyph.GlyphController;
//import Viewpoints.FrontEnd.Input.MotionInput.FileCueingMotionInput;
import Viewpoints.FrontEnd.Input.MotionInput.FileMotionInput;
import Viewpoints.FrontEnd.Input.MotionInput.FileRehearsalMotionInput;
import Viewpoints.FrontEnd.Input.MotionInput.KinectMotionInput;
import Viewpoints.FrontEnd.Input.MotionInput.MotionInput;
import Viewpoints.FrontEnd.Input.MotionInput.UnityMotionInput;
import Viewpoints.FrontEnd.Model.AestheticsModel.ViewpointsAestheticsModel;
import Viewpoints.FrontEnd.ResponseLogic.RandomResponseLogic;
import Viewpoints.FrontEnd.ResponseLogic.ResponseLogic;
import Viewpoints.FrontEnd.ResponseLogic.SimilarityResponseLogic;
import Viewpoints.FrontEnd.ResponseLogic.SoarResponseLogic;
import Viewpoints.FrontEnd.Segmentation.SegmentMethods;
import Viewpoints.FrontEnd.Segmentation.SegmentationManager;
import Viewpoints.FrontEnd.Shared.*;
import processing.core.*;
import processing.data.JSONObject;



public class ViewpointsFrontEnd extends PApplet{
	//TODO: add in another array for text field responses
	
	
	public static SegmentMethods SEGMENTATION_METHOD = SegmentMethods.ALL;
	private static String REHEARSAL_GESTURE_LIB = "gesture-segments" + File.separator;
//	private static String CUED_GESTURE_LIB = "recorded-input-sessions" + File.separator + "scott-post-hambidge" + File.separator;
	private static String GREETING_GESTURE_LIB = "greetings" + File.separator;
//	private static String RUNTIME_GESTURE_LIB = "runtime-gesture-segments" + File.separator;
	private static String GLYPH_LIB = "glyphs" + File.separator;
//	private ArrayList<Integer> experimentalModes = new ArrayList<Integer>();
//	private static boolean randomizedRoundStarted = false;
	public static String PROJECT_HOME;
	public static boolean isWindows;
	
	private static String playbackFilePrefix = "recorded-input-sessions/";
	private static String playbackVideoName = "Capture";
	private final static String playbackFile = playbackFilePrefix + playbackVideoName + ".jsg";
	private boolean isTracking = false;
	
	private static Slider debugSlider; 
	
	private Random random = new Random();
	private static final long serialVersionUID = 1L;
	private static ResponseLogic responseLogic;
	private static ResponseLogic backupSoarResponseLogic;
	private static ResponseLogic backupRandomResponseLogic;
	//TODO: sameResponse
	private Clock timer, sameResponseTimer, randomResponseTimer, newResponseTimer;
//	private static MusicControl audioController;
//	private ArrayList<String> listGUIText = new ArrayList<String>();
//	private ArrayList<Long> listGUIDuration = new ArrayList<Long>();
	private final float UPDATE_TIMER = 5; // in seconds
	private VAIState currentState = VAIState.CLOUD;
	private int userCount = 0;
	private int randomCounter = 0;
	private float maxZ = 25000;
	private float minZ = 500;
	
	private Embodiment vaiEmbodiment;
	private FireflyEffects vaiEffectsController = null;
	private PImage vaiEmbodimentFrame;
	private Embodiment userEmbodiment;
	private ShadowEffects userEffectsController;
	private PImage userEmbodimentFrame;
//	private Embodiment userStencilEmbodiment;
//	private StencilEffects userStencilEffectsController;
//	private PImage userStencilEmbodimentFrame;
	private static SegmentationManager segmentationManager;

//	public RemoteControlPreset currentPreset = RemoteControlPreset.NONE;
	
	//Glyphs
	public static PImage vaiGlyph;
	public static PImage userGlyph;
	private static ArrayList<String> glyphFiles;
	public static PVector vaiGlyphPos;
	public static PVector userGlyphPos;
	public static int vaiGlyphIndex;
	public static int userGlyphIndex;
	public static float radius = 150f;
	public static float angle = (float)(Math.PI / 6);

	//timers
	private final float randomDurationScaleTimeout = 3;
	private static float humanMovementTimeout;

	private float humanStillnessTimeout;
	private final float humanStillnessToRandom = 4;

	private float agentStillnessTimeout;
	private final float agentStillnessToRandom = 7;

	private float greetByeTimeout;
	private float agentMovementTimeout;

	private static Clock vaiGlyphTimer;
	private static Clock userGlyphTimer;
	
	//MotionInputs 
	private static MotionInput currentMotionInput;
	private static KinectMotionInput kinectInput;
	private JointSpaceGestureInterpolator response = null;
//	private static FileCueingMotionInput fileCueingMotionInput = null;
		
	//ControlP5 GUI variables
	
	int col = color(255);
	
	private ControlP5 cp5;
	private Debug debug;
	private DebugOverlay debugOverlay;
	private ControlsManager controlsManager;
	private GlyphController glyphController;
	
	private SyphonServer syphonServer;
	
	@SuppressWarnings("unused")
	private ViewpointsSocketManager socketManager;
	
	//Sending Joint Data
	OscP5 osc;
	NetAddress receiver;
	float[] jointPosition = new float[3];
	float[] jointOrientation = new float[16];
	float[][] jointData = new float[17][19];
	
	Server myServer;
	Client myClient;
	int val = 0;
	JSONObject injson;
	String data;
	Client thisClient = null;
	
	String stringSet = "";
	
	
//	@SuppressWarnings("unused")
//	private ViewpointsRemoteControlSocketManager remoteControlSocketManager;
	
	public void setup() {
//		size(displayWidth, displayHeight);
		size(displayWidth, displayHeight, P3D);
		try{
			textMode(MODEL);
			textSize(32);
			PROJECT_HOME = System.getProperty("user.dir");
			if(PROJECT_HOME.equalsIgnoreCase(null)) {
				try {
					PROJECT_HOME = new java.io.File( "." ).getCanonicalPath();
				}
				catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		
			if (System.getProperty("os.name").toLowerCase().indexOf("windows") > -1)  
			{
				isWindows = true;
			}
			else
			{
				isWindows = false;
			}

			noCursor();
			ClockManager.setMainClock(new Clock());
			vaiGlyphTimer = new Clock();
			userGlyphTimer = new Clock();
			setSegmentationManager(new SegmentationManager(SEGMENTATION_METHOD));

			controlsManager = new ControlsManager(this);

			//GUI setup
			cp5 = new ControlP5(this);
			debug = new Debug(controlsManager, this);
			controlsManager.setDebug(debug);
			debugOverlay = new DebugOverlay(debug, controlsManager, this);
			controlsManager.setDebugOverlay(debugOverlay);
			glyphController = new GlyphController(controlsManager, this);
			
			//Load file names into glyphFiles
			File glyphFolder = new File(GLYPH_LIB);
			File[] glyphFileList = glyphFolder.listFiles();
			glyphFiles = new ArrayList<String>();
			
			for(int i = 0; i < glyphFileList.length; i++) {
				if(glyphFileList[i].getName().endsWith(".png")) {
					glyphFiles.add(glyphFileList[i].getName());	
				}
			}
			
			System.out.println("Loaded " + glyphFiles.size() + " Glyph Files");
			
			//Initialize kinectInput if using Kinect and set it to currentMotionInput
			if (controlsManager.isUSE_UNITY()) {
				currentMotionInput = new UnityMotionInput();
			} else if (controlsManager.isUSE_KINECT()) {
				kinectInput = new KinectMotionInput(new SimpleOpenNI(this), getMinZ(), getMaxZ());
				kinectInput.getKinect().setMirror(true); //mirror
				currentMotionInput = kinectInput;
			} else {
				// Creating data for retraining the classifier.
				if(controlsManager.isRECORDING_TIMES()){
					currentMotionInput = new FileMotionInput(getPlaybackFile());
				}else{
					currentMotionInput = new FileMotionInput(getPlaybackFile());
				}
			}
			
//			fileCueingMotionInput = new FileCueingMotionInput(CUED_GESTURE_LIB, -1);
//			fileCueingMotionInput.init();
			
			vaiEffectsController = new FireflyEffects(controlsManager);

			if (currentMotionInput.init()) {
				
				if(currentMotionInput instanceof FileMotionInput &&
						(!controlsManager.isUSE_KINECT() && (controlsManager.isRECORDING_TIMES())) || controlsManager.isDEBUG()){
					((FileMotionInput)currentMotionInput).repeat(false);
				}
				
				//set response logic
				if(!controlsManager.isSOAR_RESPONSE_MODE()) {
					responseLogic = new SimilarityResponseLogic();
					backupSoarResponseLogic = new SoarResponseLogic();
					backupRandomResponseLogic = new RandomResponseLogic(getREHEARSAL_GESTURE_LIB());
				} else {
					backupSoarResponseLogic = new SoarResponseLogic();
					backupRandomResponseLogic = new RandomResponseLogic(getREHEARSAL_GESTURE_LIB());
					responseLogic = getBackupSoarResponseLogic();
				}
				getBackupSoarResponseLogic().start();
				backupRandomResponseLogic.start();
			} else {
				background(255, 0, 0);
			}

			//TODO: WHEN THIS SECTION IS COMMENTED, NO RUNTIME ERROR BUT STAYS RED FOREVER
//			if (!controlsManager.isUSE_KINECT()) {
//				controlsManager.isDEBUG()Slider = cp5.addSlider("sliderValue")
//					.setPosition(30, displayHeight*.8f)
//					.setSliderMode(Slider.FLEXIBLE)
//					.setSize(450, 20)
//					.setRange(0, ((FileMotionInput)currentMotionInput).getDuration());
//				if (!controlsManager.isDEBUG()) {
//					controlsManager.isDEBUG()Slider.hide()
//						.lock();
//				}
//			}
			
			ClockManager.getMainClock().start();
			timer = new Clock();
			timer.start();
			
//			audioController = new MusicControl(this);
			vaiEffectsController = new FireflyEffects(controlsManager);
			//TODO: Add way to specify which embodiment type we want.
			if(controlsManager.isBlendedGraphics())
			{
				
				if(controlsManager.isTwoStagesPresent())
				{
					vaiEmbodiment = new FireflyEmbodiment(createGraphics(this.displayWidth, this.displayHeight / 2, P3D), vaiEffectsController);
				}
				else
				{
					vaiEmbodiment = new FireflyEmbodiment(createGraphics(this.displayWidth, this.displayHeight, P3D), vaiEffectsController);
				}
			}
			else
			{
				vaiEmbodiment = new FireflyEmbodiment(this.g, vaiEffectsController);
			}
			vaiEmbodiment.initialize();
			
			userEffectsController = new ShadowEffects(controlsManager);
			if(controlsManager.isBlendedGraphics())
			{
				if(controlsManager.isTwoStagesPresent())
				{
					userEmbodiment = new ShadowEmbodiment(createGraphics(this.displayWidth, this.displayHeight / 2, P3D), userEffectsController);
				}
				else
				{
					userEmbodiment = new ShadowEmbodiment(createGraphics(this.displayWidth, this.displayHeight, P3D), userEffectsController);
				}
			}
			else
			{
				userEmbodiment = new ShadowEmbodiment(this.g, userEffectsController);
			}
			userEmbodiment.initialize();
			
//			userStencilEffectsController = new StencilEffects(controlsManager);
//			if(controlsManager.isBlendedGraphics())
//			{
//				if(controlsManager.isTwoStagesPresent())
//				{
//					userStencilEmbodiment = new StencilEmbodiment(createGraphics(this.displayWidth, this.displayHeight / 2, P3D), userStencilEffectsController);
//				}
//				else
//				{
//					userStencilEmbodiment = new StencilEmbodiment(createGraphics(this.displayWidth, this.displayHeight, P3D), userStencilEffectsController);
//				}
//			}
//			else
//			{
//				userStencilEmbodiment = new StencilEmbodiment(this.g, userStencilEffectsController);
//			}
//			userStencilEmbodiment.initialize();
			
			if(controlsManager.isVideoStreaming())
			{
				if(!isWindows)
				{
					syphonServer = new SyphonServer(this, "VAIFrontEnd#1");
					System.out.println("Syphon server loaded");
				}
			}
			
			background(0);
			
			if(controlsManager.isViewpointsAestheticsModelSocketStreaming())
			{
				socketManager = new ViewpointsSocketManager();
			}
			
//			if(controlsManager.isRemoteControlSocketReceiving())
//			{
//				remoteControlSocketManager = new ViewpointsRemoteControlSocketManager();
//			}
		}catch(Exception e){
			e.printStackTrace();
			restart();
		}
		
		//Sending Join Data
		//osc = new OscP5(this, 12000);
		//receiver = new NetAddress("127.0.0.1", 12000);
		//Socket
		myServer = new Server(this, 16003);
		myClient = new Client(this, "127.0.0.1", 16003);
		
		
	}

	public void draw() {
		
		while(thisClient == null){
			thisClient = myServer.available();
		}
		
		stringSet = thisClient.readString();
		/*
		try
		{
			int start = stringSet.indexOf("{\"0\"");
			int end = stringSet.indexOf("}}")+2;
			StringBuilder sb = new StringBuilder(stringSet);
			
			String jsonString = sb.substring(start,end);
			injson = JSONObject.parse(jsonString);
			stringSet = sb.substring(end);
		}
		catch(Exception e)
		{
			System.out.println("fail");
		}
		*/
		
		if (controlsManager.isUSE_UNITY()) {
			try {			
				injson = JSONObject.parse(stringSet);
				((UnityMotionInput)currentMotionInput).processJSONObject(injson);
			} catch(Exception e) {
				System.out.println("Error parsing JSON Object");
			}
		}
		
		try{
//			if(remoteControlSocketManager != null && remoteControlSocketManager.getCurrentRemoteControlData() != null && controlsManager.isRemoteControlSocketReceiving())
//			{
//				System.out.println("CURRENT PRESET: " + remoteControlSocketManager.getCurrentRemoteControlData().convertToString());
//				handlePresets(remoteControlSocketManager.getCurrentRemoteControlData().getPreset());
//				remoteControlSocketManager.setCurrentRemoteControlData(null);
//			}
			
			if(controlsManager.isMONITORING() && timer.iniTime() > UPDATE_TIMER){
		
				
				readInput();
				timer = new Clock();
				timer.start();
			}
			
//			lights();
			if(!controlsManager.isBlendedGraphics())
			{
				noStroke();
//				fill(0, 0, 0, vaiEffectsController.activeParameters().FADE);
				fill(0, 0, 0, userEffectsController.activeParameters().FADE);
				rect(0, 0, displayWidth, displayHeight);
			}
			if (!currentMotionInput.isInit()) {
				return;
			} else {
				if (debugSlider != null && currentMotionInput instanceof FileMotionInput && (!controlsManager.isDEBUG() || controlsManager.isPlaying())) {
					debugSlider.changeValue(((FileMotionInput)currentMotionInput).getTime());
				}

				ClockManager.getMainClock().check();
				noStroke();
				//TODO: (Lauren) Look here for the boxing
				
				Body userPose = null;
				if (controlsManager.isDEBUG() && !controlsManager.isPlaying() && currentMotionInput instanceof FileMotionInput) {
					userPose = ((FileMotionInput)currentMotionInput).next(debugSlider.getValue());
				} else {
//					if(controlsManager.isUseCueingFileMotionInput() && fileCueingMotionInput.isInit())
//					{
//						userPose = fileCueingMotionInput.next(ClockManager.getMainClock());
////						System.out.println("Cueing frame");
//					}
//					else
//					{
					userPose = currentMotionInput.next(ClockManager.getMainClock());
//					}
					if(userPose != null)
					{
						userPose.setTimestamp(ClockManager.getMainClock().time());
					}
				}
				Body vaiPose = null;
				if(controlsManager.isREHEARSAL_MODE()){
					drawing(userPose, vaiPose);
				}
				else if(controlsManager.isUSE_KINECT()){
					mainFSM(userPose, vaiPose);
				}else{
					drawing(userPose, vaiPose);
				}
				
				if(controlsManager.isBlendedGraphics())
				{
					blendFrames();
					debugOverlay.drawDebugGUI();
				}
				
				if(controlsManager.isDrawRGB())
				{
					if(currentMotionInput instanceof KinectMotionInput)
					{
						if(controlsManager.isTwoStagesPresent())
						{
							image(((KinectMotionInput)currentMotionInput).getRGBImage(), 0, displayHeight / 2, displayWidth, displayHeight / 2);
						}
						else
						{
							image(((KinectMotionInput)currentMotionInput).getRGBImage(), 0, 0, displayWidth, displayHeight);
						}
					}
				}
				
				if(controlsManager.isDrawDepth())
				{
					if(currentMotionInput instanceof KinectMotionInput)
					{
						if(controlsManager.isTwoStagesPresent())
						{
							image(((KinectMotionInput)currentMotionInput).getDepthImage(), 0, displayHeight / 2, displayWidth, displayHeight / 2);
//							image(((KinectMotionInput)currentMotionInput).getUserImage(), 0, displayHeight / 2, displayWidth, displayHeight / 2);
						}
						else
						{
							image(((KinectMotionInput)currentMotionInput).getDepthImage(), 0, 0, displayWidth, displayHeight);
//							image(((KinectMotionInput)currentMotionInput).getUserImage(), 0, 0, displayWidth, displayHeight);
						}
					}
				}
				
//				if(controlsManager.isDrawStencil())
//				{
//					if(currentMotionInput instanceof KinectMotionInput && userStencilEmbodimentFrame != null)
//					{
//						if(controlsManager.isTwoStagesPresent())
//						{
//							int oldStroke = g.strokeColor;
//							noStroke();
//							fill(0, 0, 0, vaiEffectsController.activeParameters().FADE);
////							fill(0, 0, 0, userEffectsController.activeParameters().FADE);
//							rect(0, displayHeight / 2, displayWidth, displayHeight);
//							g.stroke(oldStroke);
//							
//							image(userStencilEmbodimentFrame, 0, displayHeight / 2, displayWidth, displayHeight / 2);
//						}
//						else
//						{
//							image(userStencilEmbodimentFrame, 0, 0, displayWidth, displayHeight);
//						}
//					}
//				}
				
				if(vaiEmbodimentFrame != null && (controlsManager.isDrawDepth() || controlsManager.isDrawRGB() || controlsManager.isDrawStencil()))
				{
					if(controlsManager.isDRAW_EMBODIMENT())
					{
						if(controlsManager.isTwoStagesPresent() && controlsManager.isEmbodimentOnTopStage())
						{
							image(vaiEmbodimentFrame, 0, 0, displayWidth, displayHeight / 2);
						}
						else if(controlsManager.isTwoStagesPresent() && !controlsManager.isEmbodimentOnTopStage())
						{
							image(vaiEmbodimentFrame, 0, displayHeight / 2, displayWidth, displayHeight / 2);
						}
						else
						{
							image(vaiEmbodimentFrame, 0, 0, displayWidth, displayHeight);
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			restart();
		}
	}
	
	
//	public void handlePresets(RemoteControlPreset preset)
//	{
//		switch(preset)
//		{
//			case ONE:
//			{
//				//In blackness. Preshow sound. Stars fade in. Dancers entering unseen
//				System.out.println("In blackness. Preshow sound. Stars fade in. Dancers entering unseen");
//				controlsManager.setDRAW_EMBODIMENT(false);
//				controlsManager.setAI_OFF(true);
//				break;
//			}
//			case TWO:
//			{
//				//Fade in RGB. Second track starts. 30 count / seconds fade in
//				System.out.println("Fade in RGB. Second track starts. 30 count / seconds fade in");
//				//Fade in RGB
//				controlsManager.setDRAW_SHADOW(false);
//				controlsManager.setDrawRGB(true);
//				controlsManager.setDrawDepth(false);
//				controlsManager.setDrawStencil(false);
//				break;
//			}
//			case THREE:
//			{
//				//Fade out RGB. Fade in shadow with stars embodiment. 10 count / seconds fade in / out. single shadow with large trailing (5ish second trail)
//				System.out.println("Fade out RGB. Fade in shadow with stars embodiment. 10 count / seconds fade in / out. single shadow with large trailing (5ish second trail)");
//				controlsManager.setDRAW_SHADOW(true);
//				controlsManager.setDrawRGB(false);
//				controlsManager.setDrawDepth(false);
//				controlsManager.setDrawStencil(false);
//				//set trailing
//				
//				break;
//			}
//			case FOUR:
//			{
//				//Multishadow single line with no trail
//				System.out.println("Multishadow single line with no trail");
//				getUserEffectsController().activeParameters().isMultiShadow = true;
//				getUserEffectsController().activeParameters().multiShadowType = MultiShadowType.SINGLE_LINE_HORIZONTAL;
//				break;
//			}
//			case FIVE:
//			{
//				//Multishadow with diagonal double line
//				System.out.println("Multishadow with diagonal double line");
//				getUserEffectsController().activeParameters().isMultiShadow = true;
//				getUserEffectsController().activeParameters().multiShadowType = MultiShadowType.DOUBLE_LINE_DIAGONAL;
//				break;
//			}
//			case SIX:
//			{
//				//Stencil with hebru. Women dancers take over from men
//				System.out.println("Stencil with hebru. Women dancers take over from men");
//				controlsManager.setDRAW_SHADOW(false);
//				controlsManager.setDrawRGB(false);
//				controlsManager.setDrawDepth(false);
//				controlsManager.setDrawStencil(true);
////				getUserStencilEffectsController().activeParameters().drawMethod = StencilDrawMethod.HEBRU_IMAGE;
//				break;
//			}
//			case SEVEN:
//			{
//				//Shadow world fade out. RGB fade in. 10 count / seconds fade in / out
//				System.out.println("Shadow world fade out. RGB fade in. 10 count / seconds fade in / out");
//				controlsManager.setDRAW_SHADOW(false);
//				controlsManager.setDrawRGB(true);
//				controlsManager.setDrawDepth(false);
//				controlsManager.setDrawStencil(false);
//				break;
//			}
//			case EIGHT:
//			{
//				//AI particles fade in on top. Emma's solo. AI mirroring
//				System.out.println("AI particles fade in on top. Emma's solo. AI mirroring");
//				controlsManager.setDRAW_EMBODIMENT(true);
//				break;
//			}
//			case NINE:
//			{
//				//Emma stops in low arm. AI dances alone phrase 1
//				System.out.println("Emma stops in low arm. AI dances alone phrase 1");
//				controlsManager.setUseCueingFileMotionInput(true);
//				fileCueingMotionInput.cueNext();
//				break;
//			}
//			case ZERO:
//			{
//				//Emma stops in low arm. AI dances alone phrase 2
//				System.out.println("Emma stops in low arm. AI dances alone phrase 2");
//				fileCueingMotionInput.cueNext();
//				break;
//			}
//			case A:
//			{
//				//Emma stops in low arm. AI dances alone phrase 3
//				System.out.println("Emma stops in low arm. AI dances alone phrase 3");
//				fileCueingMotionInput.cueNext();
//				break;
//			}
//			case S:
//			{
//				//Emma continues. AI continues with her
//				System.out.println("Emma continues. AI continues with her");
//				controlsManager.setUseCueingFileMotionInput(false);
//				controlsManager.setAI_OFF(false);
//				break;
//			}
//			case D:
//			{
//				//Jakuelle joins in. AI stops. AI fades out
//				System.out.println("Jakuelle joins in. AI stops. AI fades out");
//				controlsManager.setDRAW_EMBODIMENT(false);
//				break;
//			}
//			case F:
//			{
//				//Hard stop and RGB cuts out immediately
//				System.out.println("Hard stop and RGB cuts out immediately");
//				controlsManager.setDRAW_SHADOW(false);
//				controlsManager.setDrawRGB(false);
//				controlsManager.setDrawDepth(false);
//				controlsManager.setDrawStencil(false);
//				break;
//			}
//			case G:
//			{
//				
//				break;
//			}
//			case H:
//			{
//				
//				break;
//			}
//			case J:
//			{
//				
//				break;
//			}
//			case K:
//			{
//				
//				break;
//			}
//			case L:
//			{
//				
//				break;
//			}
//			case NONE:
//			default:
//			{
//				
//			}
//		}
//	}
	
	private void blendFrames()
	{
		if(vaiEmbodimentFrame != null && userEmbodimentFrame != null)
		{
//			System.out.println("Both NOT NULL");
			
			int oldStroke = g.strokeColor;
			noStroke();
			fill(0, 0, 0, vaiEffectsController.activeParameters().FADE);
//			fill(0, 0, 0, userEffectsController.activeParameters().FADE);
			rect(0, 0, displayWidth, displayHeight);
			g.stroke(oldStroke);
			
//			if(controlsManager.isDRAW_EMBODIMENT())
			{
				if(controlsManager.isTwoStagesPresent() && controlsManager.isEmbodimentOnTopStage())
				{
					image(vaiEmbodimentFrame, 0, 0, displayWidth, displayHeight / 2);
//					blend(vaiEmbodimentFrame, 0, displayHeight / 2, displayWidth, displayHeight / 2, 0, displayHeight / 2, displayWidth, displayHeight / 2, PApplet.BLEND);
				}
				else if(controlsManager.isTwoStagesPresent() && !controlsManager.isEmbodimentOnTopStage())
				{
					image(vaiEmbodimentFrame, 0, displayHeight / 2, displayWidth, displayHeight / 2);
//					blend(vaiEmbodimentFrame, 0, displayHeight / 2, displayWidth, displayHeight / 2, 0, displayHeight / 2, displayWidth, displayHeight / 2, PApplet.BLEND);
				}
				else
				{
					image(vaiEmbodimentFrame, 0, 0, displayWidth, displayHeight);
//					blend(vaiEmbodimentFrame, 0, 0, displayWidth, displayHeight / 2, 0, 0, displayWidth, displayHeight / 2, PApplet.BLEND);
				}
			}
			if(controlsManager.isDRAW_SHADOW())
			{
				if(!userEffectsController.activeParameters().isMultiShadow)
				{
					if(controlsManager.isTwoStagesPresent())
					{
						image(userEmbodimentFrame, 0, displayHeight / 2, displayWidth, displayHeight / 2);
					}
					else
					{
						image(userEmbodimentFrame, 0, 0, displayWidth, displayHeight);
					}
				}
				else
				{
					switch(userEffectsController.activeParameters().multiShadowType)
					{
						case DOUBLE_LINE_DIAGONAL:
						{
							if(controlsManager.isTwoStagesPresent())
							{
								//Double line in windows
								//center image 1st
								image(userEmbodimentFrame, 0 - 50, displayHeight / 2 - 50, displayWidth, displayHeight / 2);
				
								//1st line
								image(userEmbodimentFrame, -1 * displayWidth / 2f - 50, displayHeight / 2 + -1 * displayHeight / 2 / 4f - 50, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, -1 * displayWidth / 4f - 50, displayHeight / 2 + -1 * displayHeight / 2 / 8f - 50, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, displayWidth / 4f - 50, displayHeight / 2 + displayHeight / 2 / 8f - 50, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, displayWidth / 2f - 50, displayHeight / 2 + displayHeight / 2 / 4f - 50, displayWidth, displayHeight / 2);
				
								//center image 2nd
								image(userEmbodimentFrame, 0 + 50, displayHeight / 2 + 50, displayWidth, displayHeight / 2);
				
								//2nd line
								image(userEmbodimentFrame, -1 * displayWidth / 2f + 50, displayHeight / 2 + -1 * displayHeight / 2 / 4f + 50, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, -1 * displayWidth / 4f + 50, displayHeight / 2 + -1 * displayHeight / 2 / 8f + 50, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, displayWidth / 4f + 50, displayHeight / 2 + displayHeight / 2 / 8f + 50, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, displayWidth / 2f + 50, displayHeight / 2 + displayHeight / 2 / 4f + 50, displayWidth, displayHeight / 2);
							}
							else
							{
								//Double line in windows
								//center image 1st
								image(userEmbodimentFrame, 0 - 50, 0 - 50, displayWidth, displayHeight);
				
								//1st line
								image(userEmbodimentFrame, -1 * displayWidth / 2f - 50, -1 * displayHeight / 4f - 50, displayWidth, displayHeight);
								image(userEmbodimentFrame, -1 * displayWidth / 4f - 50, -1 * displayHeight / 8f - 50, displayWidth, displayHeight);
								image(userEmbodimentFrame, displayWidth / 4f - 50, displayHeight / 8f - 50, displayWidth, displayHeight);
								image(userEmbodimentFrame, displayWidth / 2f - 50, displayHeight / 4f - 50, displayWidth, displayHeight);
				
								//center image 2nd
								image(userEmbodimentFrame, 0 + 50, 0 + 50, displayWidth, displayHeight);
				
								//2nd line
								image(userEmbodimentFrame, -1 * displayWidth / 2f + 50, -1 * displayHeight / 4f + 50, displayWidth, displayHeight);
								image(userEmbodimentFrame, -1 * displayWidth / 4f + 50, -1 * displayHeight / 8f + 50, displayWidth, displayHeight);
								image(userEmbodimentFrame, displayWidth / 4f + 50, displayHeight / 8f + 50, displayWidth, displayHeight);
								image(userEmbodimentFrame, displayWidth / 2f + 50, displayHeight / 4f + 50, displayWidth, displayHeight);
							}
							break;
						}
						case X_SHAPE:
						{
							if(controlsManager.isTwoStagesPresent())
							{
								//center image 1st
								image(userEmbodimentFrame, 0, displayHeight / 2, displayWidth, displayHeight / 2);
					
								//1st line
								image(userEmbodimentFrame, -1 * displayWidth / 2f - 50, displayHeight / 2 + -1 * displayHeight / 2 / 3f, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, -1 * displayWidth / 4f - 50, displayHeight / 2 + -1 * displayHeight / 2 / 6f, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, displayWidth / 4f - 50, displayHeight / 2 + displayHeight / 2 / 6f, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, displayWidth / 2f - 50, displayHeight / 2 + displayHeight / 2 / 3f, displayWidth, displayHeight / 2);
					
								//2nd line
								image(userEmbodimentFrame, -1 * displayWidth / 2f + 50, displayHeight / 2 + displayHeight / 2 / 3f, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, -1 * displayWidth / 4f + 50, displayHeight / 2 + displayHeight / 2 / 6f, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, displayWidth / 4f + 50, displayHeight / 2 + -1 * displayHeight / 2 / 6f, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, displayWidth / 2f + 50, displayHeight / 2 + -1 * displayHeight / 2 / 3f, displayWidth, displayHeight / 2);
							}
							else
							{
								//center image 1st
								image(userEmbodimentFrame, 0, 0, displayWidth, displayHeight);
					
								//1st line
								image(userEmbodimentFrame, -1 * displayWidth / 2f - 50, -1 * displayHeight / 3f, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, -1 * displayWidth / 4f - 50, -1 * displayHeight / 6f, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, displayWidth / 4f - 50, displayHeight / 6f, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, displayWidth / 2f - 50, displayHeight / 3f, displayWidth, displayHeight / 2);
					
								//2nd line
								image(userEmbodimentFrame, -1 * displayWidth / 2f + 50, displayHeight / 3f, displayWidth, displayHeight);
								image(userEmbodimentFrame, -1 * displayWidth / 4f + 50, displayHeight / 6f, displayWidth, displayHeight);
								image(userEmbodimentFrame, displayWidth / 4f + 50, -1 * displayHeight / 6f, displayWidth, displayHeight);
								image(userEmbodimentFrame, displayWidth / 2f + 50, -1 * displayHeight / 3f, displayWidth, displayHeight);
							}
							break;
						}
						case SINGLE_LINE_DIAGONAL:
						{
							if(controlsManager.isTwoStagesPresent())
							{
								//center image 1st
								image(userEmbodimentFrame, 0, displayHeight / 2, displayWidth, displayHeight / 2);
				
								//1st line
								image(userEmbodimentFrame, -1 * displayWidth / 2f, displayHeight / 2 + -1 * displayHeight / 2 / 4f, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, -1 * displayWidth / 4f, displayHeight / 2 + -1 * displayHeight / 2 / 8f, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, displayWidth / 4f, displayHeight / 2 + displayHeight / 2 / 8f, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, displayWidth / 2f, displayHeight / 2 + displayHeight / 2 / 4f, displayWidth, displayHeight / 2);
							}
							else
							{
								//center image 1st
								image(userEmbodimentFrame, 0, 0, displayWidth, displayHeight);
				
								//1st line
								image(userEmbodimentFrame, -1 * displayWidth / 2f, -1 * displayHeight / 4f, displayWidth, displayHeight);
								image(userEmbodimentFrame, -1 * displayWidth / 4f, -1 * displayHeight / 8f, displayWidth, displayHeight);
								image(userEmbodimentFrame, displayWidth / 4f, displayHeight / 8f, displayWidth, displayHeight);
								image(userEmbodimentFrame, displayWidth / 2f, displayHeight / 4f, displayWidth, displayHeight);
							}
						}
						case SINGLE_LINE_HORIZONTAL:
						default:
						{
							if(controlsManager.isTwoStagesPresent())
							{
								//center image 1st
								image(userEmbodimentFrame, 0, displayHeight / 2, displayWidth, displayHeight / 2);
				
								//1st line
								image(userEmbodimentFrame, -1 * displayWidth / 2f, displayHeight / 2, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, -1 * displayWidth / 4f, displayHeight / 2, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, displayWidth / 4f, displayHeight / 2, displayWidth, displayHeight / 2);
								image(userEmbodimentFrame, displayWidth / 2f, displayHeight / 2, displayWidth, displayHeight / 2);
							}
							else
							{
								//center image 1st
								image(userEmbodimentFrame, 0, 0, displayWidth, displayHeight);
				
								//1st line
								image(userEmbodimentFrame, -1 * displayWidth / 2f, 0, displayWidth, displayHeight);
								image(userEmbodimentFrame, -1 * displayWidth / 4f, 0, displayWidth, displayHeight);
								image(userEmbodimentFrame, displayWidth / 4f, 0, displayWidth, displayHeight);
								image(userEmbodimentFrame, displayWidth / 2f, 0, displayWidth, displayHeight);
							}
						}
					}
				}
			}
		}
		else if(vaiEmbodimentFrame != null && userEmbodimentFrame == null)
		{
//			System.out.println("VAI Not Null");
			int oldStroke = g.strokeColor;
			noStroke();
//			fill(0, 0, 0, vaiEffectsController.activeParameters().FADE);
			fill(0, 0, 0, userEffectsController.activeParameters().FADE);
			rect(0, 0, displayWidth, displayHeight);
			g.stroke(oldStroke);
//			if(controlsManager.isDRAW_EMBODIMENT())
			{
				if(controlsManager.isTwoStagesPresent() && controlsManager.isEmbodimentOnTopStage())
				{
					image(vaiEmbodimentFrame, 0, 0, displayWidth, displayHeight / 2);
				}
				else if(controlsManager.isTwoStagesPresent() && !controlsManager.isEmbodimentOnTopStage())
				{
					image(vaiEmbodimentFrame, 0, displayHeight / 2, displayWidth, displayHeight / 2);
				}
				else
				{
					image(vaiEmbodimentFrame, 0, 0, displayWidth, displayHeight);
				}
			}
		}
		else if(vaiEmbodimentFrame == null && userEmbodimentFrame != null)
		{
//			System.out.println("User Not Null");
			int oldStroke = g.strokeColor;
			noStroke();
			fill(0, 0, 0, vaiEffectsController.activeParameters().FADE);
//			fill(0, 0, 0, userEffectsController.activeParameters().FADE);
			rect(0, 0, displayWidth, displayHeight);
			g.stroke(oldStroke);
			if(controlsManager.isDRAW_SHADOW())
			{
				if(controlsManager.isTwoStagesPresent())
				{
//					background(userEmbodimentFrame);
					image(userEmbodimentFrame, 0, displayHeight / 2, displayWidth, displayHeight / 2);
				}
				else
				{
//					background(userEmbodimentFrame);
					image(userEmbodimentFrame, 0, 0, displayWidth, displayHeight);
				}
			}
		}
		else
		{
//			System.out.println("Both ARE NULL");
		}
	}
	
	@Override
	public boolean sketchFullScreen() {
		  return true;
	}
	
	public void mousePressed() {
		controlsManager.cListen();
	}
	
	public void keyPressed() {
		try{
			debugOverlay.updateFlags();
			controlsManager.listen();

			if(key == 'k' || key == 'K' || key == 'r' || key == 'R') {
				changeMotionInput();
			}
		}catch(Exception e){
			e.printStackTrace();
			restart();
		}
	}
	
	//TODO: CHANGE HARDCODED PATH
	private void readInput(){
		List<String> lines = null;
		try {
			lines = Files.readAllLines(Paths.get(PROJECT_HOME + File.separator + "file_communications" + File.separator + "commands.txt"),
					Charset.defaultCharset());
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}	    
		for (String line : lines) {
			if(line.contains("r")){
				try {
//			        Robot robot = new Robot();
//			        // Simulate a key press
//			        robot.keyPress(KeyEvent.VK_R);
//			        robot.keyRelease(KeyEvent.VK_R);
					Boolean biasRehearsal = true;
					char key = 'r';
					if(key == 'r' || key == 'R') {
//						controlsManager.setREHEARSAL_MODE(!controlsManager.isREHEARSAL_MODE());
						biasRehearsal = true;
					}
					if(!controlsManager.isUSE_KINECT() && !controlsManager.isREHEARSAL_MODE()) {
						currentMotionInput = new FileMotionInput(getPlaybackFile());
						debug.addGUIText("FILE PLAYBACK MODE: " + !controlsManager.isUSE_KINECT(), 5000);
					} else if(!controlsManager.isUSE_KINECT() && controlsManager.isREHEARSAL_MODE()) {
						currentMotionInput = new FileRehearsalMotionInput(getREHEARSAL_GESTURE_LIB(), -1);
						debug.addGUIText("REHEARSAL MODE: " + controlsManager.isREHEARSAL_MODE(), 5000);
					} else if(controlsManager.isUSE_KINECT() && !controlsManager.isREHEARSAL_MODE()) {
						if(getKinectInput() == null) {
							setKinectInput(new KinectMotionInput(new SimpleOpenNI(this), getMinZ(), getMaxZ()));
							kinectInput.getKinect().setMirror(true);
						}
						currentMotionInput = getKinectInput();
						debug.addGUIText("KINECT MODE: " + controlsManager.isUSE_KINECT(), 5000);
					} else if(controlsManager.isUSE_KINECT() && controlsManager.isREHEARSAL_MODE()) {
						if(biasRehearsal) {
							currentMotionInput = new FileRehearsalMotionInput(getREHEARSAL_GESTURE_LIB(), -1);
							debug.addGUIText("REHEARSAL MODE: " + controlsManager.isREHEARSAL_MODE(), 5000);
//							System.out.println("USE KINECT + REHEARSAL MODE: REMOTE REHEARSAL. KINECT MODE: " + USE_KINECT + ", REHEARSAL MODE: " + REHEARSAL_MODE + ", BIAS REHEARSAL MODE: " + biasRehearsal);
						}
						else {
							if(getKinectInput() == null) {
								setKinectInput(new KinectMotionInput(new SimpleOpenNI(this), getMinZ(), getMaxZ()));
								kinectInput.getKinect().setMirror(true);
							}
							currentMotionInput = kinectInput;
							debug.addGUIText("KINECT MODE: " + controlsManager.isUSE_KINECT(), 5000);
						}
					}
					changeMotionInput();
					
				} catch (Exception e) {
				        e.printStackTrace();
				} finally {
//					output("");
					FileUtilities.clearText(PROJECT_HOME + File.separator + "file_communications" + File.separator + "commands.txt");
				}
			}
        }
	}
	
	private void restart(){
		PrintWriter out = null;
		try {
			out = new PrintWriter(new FileWriter(PROJECT_HOME + File.separator + "file_communications" + File.separator + "restart_commands.txt", true));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		out.println("rf");
		out.close();
	}
	
	//TODO: CHANGE HARDCODED PATH
//	private void output(String str) {
//		PrintWriter out = null;
//		try {
//			out = new PrintWriter(new FileWriter(PROJECT_HOME + File.separator + "file_communications" + File.separator + "commands.txt", false));
//		} catch (IOException e1) {
//			e1.printStackTrace();
//		}
//		out.println(str);
//		out.close();
//	}
	
	public void changeBlendedGraphics()
	{
		if(controlsManager.isBlendedGraphics())
		{
			if(controlsManager.isTwoStagesPresent())
			{
				vaiEmbodiment = new FireflyEmbodiment(createGraphics(this.displayWidth, this.displayHeight / 2, P3D), vaiEffectsController);
			}
			else
			{
				vaiEmbodiment = new FireflyEmbodiment(createGraphics(this.displayWidth, this.displayHeight, P3D), vaiEffectsController);
			}
		}
		else
		{
			vaiEmbodiment = new FireflyEmbodiment(this.g, vaiEffectsController);
		}
		vaiEmbodiment.initialize();
		
		if(controlsManager.isBlendedGraphics())
		{
			if(controlsManager.isTwoStagesPresent())
			{
				userEmbodiment = new ShadowEmbodiment(createGraphics(this.displayWidth, this.displayHeight / 2, P3D), userEffectsController);
			}
			else
			{
				userEmbodiment = new ShadowEmbodiment(createGraphics(this.displayWidth, this.displayHeight, P3D), userEffectsController);
			}
		}
		else
		{
			userEmbodiment = new ShadowEmbodiment(this.g, userEffectsController);
		}
		userEmbodiment.initialize();
		
//		if(controlsManager.isBlendedGraphics())
//		{
//			if(controlsManager.isTwoStagesPresent())
//			{
//				userStencilEmbodiment = new StencilEmbodiment(createGraphics(this.displayWidth, this.displayHeight / 2, P3D), userStencilEffectsController);
//			}
//			else
//			{
//				userStencilEmbodiment = new StencilEmbodiment(createGraphics(this.displayWidth, this.displayHeight, P3D), userStencilEffectsController);
//			}
//		}
//		else
//		{
//			userStencilEmbodiment = new StencilEmbodiment(this.g, userStencilEffectsController);
//		}
//		userStencilEmbodiment.initialize();
	}
	
	private void changeMotionInput(){
		if (currentMotionInput.init()) {
			
//			vaiEffectsController = new FireflyEffects();
//			vaiEffectsController.setBlendedGraphics(controlsManager.isBlendedGraphics());
			
			// TODO: fix this
			if (!controlsManager.isUSE_KINECT() && getDebugSlider() == null && !controlsManager.isREHEARSAL_MODE()) {
				debugSlider = cp5.addSlider("sliderValue")
						.setPosition(30, displayHeight*.8f)
						.setSliderMode(Slider.FLEXIBLE)
						.setSize(450, 20)
						.setRange(0, ((FileMotionInput)currentMotionInput).getDuration());
				debugSlider.hide()
							.lock();
			}
			
//			if(controlsManager.isBlendedGraphics())
//			{
//				vaiEmbodiment = new FireflyEmbodiment(createGraphics(this.displayWidth, this.displayHeight / 2, P3D), vaiEffectsController);
//			}
//			else
//			{
//				vaiEmbodiment = new FireflyEmbodiment(this.g, vaiEffectsController);
//			}
//			vaiEmbodiment.initialize();

			if(responseLogic == null) {
				if(!controlsManager.isSOAR_RESPONSE_MODE()) {
					setResponseLogic(new SimilarityResponseLogic());
					setBackupSoarResponseLogic(new SoarResponseLogic());
					backupRandomResponseLogic = new RandomResponseLogic(getREHEARSAL_GESTURE_LIB());
				} else {
					setBackupSoarResponseLogic(new SoarResponseLogic());
					backupRandomResponseLogic = new RandomResponseLogic(getREHEARSAL_GESTURE_LIB());
					setResponseLogic(getBackupSoarResponseLogic());
				}
				getBackupSoarResponseLogic().start();
				backupRandomResponseLogic.start();
			}
		} else {
			background(255, 0, 0);
		}
	}
	
	public void onNewUser(SimpleOpenNI context, int userId)
	{
		if(controlsManager.isIGNORE_KINECT())
		{
			return;
		}
		System.out.println("onNewUser - userId: " + userId);
		context.startTrackingSkeleton(userId);
		kinectInput.getKinect().startTrackingSkeleton(userId);
		if(userCount == 0){
			if(controlsManager.isEnterGreetingModes())
			{
				File[] files = new File(GREETING_GESTURE_LIB).listFiles();
				currentMotionInput = new FileMotionInput(files[new Random().nextInt(files.length)].getAbsolutePath());
				changeMotionInput();
			}
			currentState = currentState.nextState(VAIState.GREETWELCOME);
		}
		userCount++;
	}
	
	public void onLostUser(SimpleOpenNI context, int userId)
	{
		if(controlsManager.isIGNORE_KINECT())
		{
			return;
		}
		System.out.println("onLostUser - userId: " + userId);
		userCount--;
		if(userCount < 1){
			if(controlsManager.isEnterGreetingModes())
			{
				File[] files = new File(GREETING_GESTURE_LIB).listFiles();
				currentMotionInput = new FileMotionInput(files[new Random().nextInt(files.length)].getAbsolutePath());
				changeMotionInput();
			}
			currentState = currentState.nextState(VAIState.GREETGOODBYE);
			getSegmentationManager().reset();

			newResponseTimer = null;
			randomResponseTimer = null;
		}
	//	context.stopTrackingSkeleton(userId);
	}

	private void mainFSM(Body userPose, Body vaiPose){
		switch(currentState){
			case CLOUD:
				vaiEffectsController.update(null, null, null, null, null);
				userEffectsController.update(null, null, null, null, null);
//				userStencilEffectsController.update(null, null, null, null, null);
				pushMatrix();
//				translate(width/2, height/2);
				debugOverlay.drawDebugGUI();
				vaiEmbodimentFrame = vaiEmbodiment.draw();
				debug.displayGUI();
				popMatrix();
			break;
			case GREETWELCOME:
				drawing(userPose, vaiPose);
				currentState = currentState.nextState(VAIState.INTERACTION);
				
				if(controlsManager.isEnterGreetingModes())
				{
					if(currentState == VAIState.INTERACTION)
					{
						currentMotionInput = kinectInput;
						changeMotionInput();
					}
				}
			break;
			case GREETGOODBYE:
				drawing(userPose, vaiPose);
				currentState = currentState.nextState(VAIState.CLOUD);

				if(controlsManager.isEnterGreetingModes())
				{
					if(currentState == VAIState.CLOUD)
					{
						currentMotionInput = kinectInput;
						changeMotionInput();
					}
				}
			break;
			case INTERACTION:
				drawing(userPose, vaiPose);
			break;
		}
	}
	
	/**
	 * Completes necessary steps for updating parameters before segmentation begins.
	 * @param userPose
	 */
	private void preSegmentationUpdate(Body userPose) {
		Body stillnessBody = null;
		if(controlsManager.isUSE_KINECT() && !(currentMotionInput instanceof KinectMotionInput) && !controlsManager.isREHEARSAL_MODE()){
			stillnessBody = kinectInput.next(ClockManager.getMainClock());
			if(stillnessBody != null)
			{
				stillnessBody.setTimestamp(ClockManager.getMainClock().time());
			}
		} else {
			stillnessBody = userPose;
		}

		ViewpointsAestheticsModel.preSegmentationUpdate(userPose, stillnessBody);
		getSegmentationManager().preSegmentationUpdate(userPose);
		isTracking = getSegmentationManager().isTracking();
	}
	
	/**
	 * Completes necessary steps for updating parameters before segmentation begins.
	 */
	private void postSegmentationUpdate() {
		//TODO: (Lauren) should stillness be added to postsegmentation update?
		if (ActivityGrapher.active()) {
			ActivityGrapher.update(((FileMotionInput)currentMotionInput).getTime(), segmentationManager.segmentStarted(), segmentationManager.segmentEnded());
		}
		getSegmentationManager().postSegmentationUpdate();
		ViewpointsAestheticsModel.postSegmentationUpdate();
	}
	
	private void drawing(Body userPose, Body vaiPose){

		if(debugSlider == null && !controlsManager.isUSE_KINECT()) {
			debugSlider = cp5.addSlider("sliderValue")
					.setPosition(30, displayHeight*.8f)
					.setSliderMode(Slider.FLEXIBLE)
					.setSize(450, 20)
					.setRange(0, ((FileMotionInput)currentMotionInput).getDuration());
			debugSlider.hide()
						.lock();
		}
		
		if(debugSlider != null)
			debugSlider.setVisible(controlsManager.isDEBUG());		
		
		if (null == userPose) {
			vaiEffectsController.update(null, null, null, null, null);
			userEffectsController.update(null, null, null, null, null);
//			userStencilEffectsController.update(null, null, null, null, null);
		} else {
			
			// Rehearsal mode
			if (controlsManager.isREHEARSAL_MODE()) {
				//transform rehearsal gestures to fit standard (arbitrarily selected) user size
				PVector[] targetBasis = FileRehearsalMotionInput.getIdealUserPose().localBasis();
				float characteristicSize = FileRehearsalMotionInput.getIdealUserPose().characteristicSize();
				for (PVector basisVector : targetBasis) {
					basisVector.mult(characteristicSize);
				}
				
				userPose = userPose.transformed(PVecUtilities.STANDARD_BASIS, targetBasis, FileRehearsalMotionInput.getIdealUserPose().center());
				//Print rehearsal percent if the motion input is of FileRehearsalMotionInput and not outputting random responses
				if(ViewpointsFrontEnd.currentMotionInput instanceof FileRehearsalMotionInput && randomResponseTimer == null) {
					String percentRehearsed = String.format("%.2f", ((FileRehearsalMotionInput)ViewpointsFrontEnd.currentMotionInput).getPercentRehearsed());
					debug.addGUIText("REHEARSAL: " + percentRehearsed + "%", 200);	
				}
			} 

			preSegmentationUpdate(userPose);

			//Logic for segmenting gestures
			if(ViewpointsAestheticsModel.getHumanStillness().isStill()){
				humanStillnessTimeout = ViewpointsAestheticsModel.getHumanStillness().getTime();
				humanMovementTimeout = -1;
			} else {
				humanStillnessTimeout = -1;
				humanMovementTimeout = ViewpointsAestheticsModel.getHumanStillness().getTime();
			}

			//LeaderRecord response = null;
			if(sameResponseTimer != null){
				sameResponseTimer.check();
			}
			if(newResponseTimer != null){
				newResponseTimer.check();
			}
			
			
			boolean segmentStarted = getSegmentationManager().segmentStarted();
			boolean segmentEnded = getSegmentationManager().segmentEnded();
			if (segmentEnded) {
				String endText = "END";
				if (!controlsManager.isUSE_KINECT() && !controlsManager.isREHEARSAL_MODE() && currentMotionInput instanceof FileMotionInput) {
					endText += ": " + ((FileMotionInput)currentMotionInput).getTime();
				}

				//System.out.println(endText);
				//debug.addGUIText(endText, 500);
				if (controlsManager.isDEBUG()) {
				}

				if (controlsManager.isDEBUG() && controlsManager.isPlaying()) {
					segmentationManager.annotate(playbackVideoName, ((FileMotionInput)currentMotionInput).getTime(), false);
				}
			}

			if (segmentStarted) {
				String startText = "START";
				if (!controlsManager.isUSE_KINECT() && !controlsManager.isREHEARSAL_MODE() && currentMotionInput instanceof FileMotionInput) {
					startText += ": " + ((FileMotionInput)currentMotionInput).getTime();
				}

				//System.out.println(startText);
				//debug.addGUIText(startText, 500);
				if (controlsManager.isDEBUG()) {
				}

				if (controlsManager.isDEBUG() && controlsManager.isPlaying()) {
					segmentationManager.annotate(playbackVideoName, ((FileMotionInput)currentMotionInput).getTime(), true);
				}
			}

			/*System.out.println("isTracking: " + isTracking + " - randomResponseTimer: " + (randomResponseTimer == null) +
					" - newReponseTimer: " + (newResponseTimer == null));*/
			//A gesture is being captured.
			if(isTracking && randomResponseTimer == null && newResponseTimer == null && 
					(!controlsManager.isAI_OFF() || controlsManager.isRANDOM_RESPONSE_MODE())) {
				//Checking for segmented gesture
				ArrayList<Body> segment = getSegmentationManager().getSegment();
				
				//Indicates the end of a gesture.
				if(segmentEnded && null != segment && segment.size() != 0){
					//TODO: (Lauren) this should only be happening once, not multiple times per segment
					
					responseLogic.requestReactionTo(segment);
					//debug.addGUIText("Requesting reaction", 1000);
					
//					System.out.println("MAYBE show a glyph!");
					
					//TODO: Glyph display here! - pick the glyph
					if(responseLogic.getShowGlyph()) {
						
//						System.out.println("DEFINITELY show a user Glyph!");
						
						userGlyphIndex = random.nextInt(glyphFiles.size());
						controlsManager.setSHOW_USER_GLYPH(true);
						userGlyphTimer.start();
						responseLogic.setShowGlyph(false);
					}
				}
				
				//Directed Response
				//response differs... once other things are done, this should theoretically not needed
				if(response != responseLogic.checkResponse()){
					sameResponseTimer = null;
					if(responseLogic.checkResponse() != null){
						newResponseTimer = new Clock();

						//debug.addGUIText("Response differs", 1000);
						newResponseTimer.start();
						//Updates response.
						response = responseLogic.checkResponse();
					}
				}else{
					sameResponseTimer = new Clock();
					sameResponseTimer.start();
				}
			}else if(response != null && newResponseTimer != null && newResponseTimer.time() > response.getBasicDuration() * randomDurationScaleTimeout){
				newResponseTimer.check();
				newResponseTimer = null;
			}
			if(response != null && randomResponseTimer != null && randomResponseTimer.time() >  response.getBasicDuration() * randomDurationScaleTimeout){
				randomResponseTimer.check();
				response = null;
				randomResponseTimer = null;
			}

			postSegmentationUpdate();
			
			//Random response. Occurs when the user has been doing same movement for a repeated amount of time or the user / agent stillness passes a certain threshold.
			//TODO: (Lauren) Change stillness to random names
			//TODO: (Lauren) Look at agent stillness -> agent cloning numbers
//			boolean activeSameResponse = sameResponse != null && sameResponse.time() > 8;

			boolean activeRandomResponse = response != null && randomResponseTimer != null &&
					randomResponseTimer.time() < response.getBasicDuration() * randomDurationScaleTimeout;
			boolean humanIsStill = ViewpointsAestheticsModel.getHumanStillness().isStill() && humanStillnessTimeout > humanStillnessToRandom;
			boolean agentIsStill = ViewpointsAestheticsModel.getAgentStillness().isStill() && agentStillnessTimeout > agentStillnessToRandom;

			if(currentState == VAIState.INTERACTION && (activeRandomResponse || (humanIsStill || agentIsStill))){				
				if(randomResponseTimer == null){
					//TODO: (Lauren) This is bad.
					PrintWriter out = null;
					try {
						out = new PrintWriter(new FileWriter(PROJECT_HOME + File.separator + "file_communications" + File.separator + "requestRandomResponse.txt", true));
					} catch (IOException e1) {
						e1.printStackTrace();
					}

					out.println("r");
					out.close();
					sameResponseTimer = null;

					//TODO: (Lauren) Make sure the instanceof check is here for all of the places
					if (responseLogic instanceof SoarResponseLogic) {
						response = ((SoarResponseLogic)responseLogic).requestRandomResponse();
					}

					if(response != null){
						if(randomCounter == 3){
							randomCounter = 0;
							debug.addGUIText("Try moving in rhythm! ;)", 3000);
						}
						randomCounter++;
						randomResponseTimer = new Clock();
						randomResponseTimer.check();
					}
				}
				if(randomResponseTimer != null)
					randomResponseTimer.check();

				//Human stillness needs to be reset to prevent continuously random responses with every frame.
				ViewpointsAestheticsModel.getHumanStillness().reset();
				ViewpointsAestheticsModel.getAgentStillness().reset();
			}

			// This handles if there is a directed response or if there is a random response.
			if (response != null && (newResponseTimer != null || randomResponseTimer != null)) { //directed response
				//debug.addGUIText("Directed Response", 1000);
				if(randomResponseTimer == null)
					randomCounter = 0;
				
				//TODO: Glyph display (above VAI figure) - take a design decision for following the person
				
				if(responseLogic.getClass().getSimpleName().equalsIgnoreCase("SoarResponseLogic"))
				{
					PredicateSpaceGesture psGesture =  ((SoarResponseLogic)responseLogic).checkPSG();
					
					if(psGesture != null) {
						for(int i = 0; i < psGesture.transforms.size(); i++) {
							if(psGesture.transforms.get(i) instanceof TransformPredicates.RESPONSE_MODE) {
								TransformPredicates.RESPONSE_MODE responseMode = (RESPONSE_MODE) psGesture.transforms.get(i);
								if(responseMode.name().equalsIgnoreCase(RESPONSE_MODE.NO_OP.name()))
								{
									//No Action
								} else if(responseMode.name().equalsIgnoreCase(RESPONSE_MODE.REPEAT.name()))
								{
									vaiGlyphIndex = userGlyphIndex;
								} else if(responseMode.name().equalsIgnoreCase(RESPONSE_MODE.VARY.name()))
								{
									//TODO: for the future, modify the glyph somehow to show a transformation
									vaiGlyphIndex = userGlyphIndex;
								} else if(responseMode.name().equalsIgnoreCase(RESPONSE_MODE.NEW.name()))
								{
									//Any index but the one currently assigned to userGlyph
									do
									{
										vaiGlyphIndex = random.nextInt(glyphFiles.size());
									}
									while(vaiGlyphIndex == userGlyphIndex);
								} else if(responseMode.name().equalsIgnoreCase(RESPONSE_MODE.PATTERN.name()))
								{
									//TODO: In the future change to some effect that shows a pattern response
									do
									{
										vaiGlyphIndex = random.nextInt(glyphFiles.size());
									}
									while(vaiGlyphIndex == userGlyphIndex);
								} else if(responseMode.name().equalsIgnoreCase(RESPONSE_MODE.EMOTION.name()))
								{
									//TODO: In future do some tint() on image to show emotion perhaps
								}
								
								controlsManager.setSHOW_VAI_GLYPH(true);
								vaiGlyphTimer.start();
								((SoarResponseLogic)responseLogic).setPSG(null);
							}
						}
					}
				}
				
				//TODO: (Lauren) I feel like this should maybe be moved outside of RhythmVAI, but I'm not sure.
				if(SEGMENTATION_METHOD == SegmentMethods.RHYTHM && isTracking)
				{
					response.setReplaySpeed((0.5f < (1.0f / ViewpointsAestheticsModel.getRhythmTracker().getPeriod())) ? (1.0f / ViewpointsAestheticsModel.getRhythmTracker().getPeriod()) : 0.5f);
				}
				else if (SEGMENTATION_METHOD == SegmentMethods.RHYTHM)
				{
					response.setReplaySpeed(response.getReplaySpeed() - (response.getReplaySpeed() - 1.3f) * 2f / 3f);
				}

				vaiPose = response.replayFrame(ClockManager.getMainClock().deltatime());				
				
				PVector[] targetBasis;
				float characteristicSize;
				//reflect userPose here
				PVector newCenter = new PVector(userPose.center().x * -1, userPose.center().y, userPose.center().z);
//				userPose = userPose.translated(newCenter);
				//proceed as usual
				targetBasis = userPose.localBasis();
				
				//ATTEMPT TO FIX USER SKELETON
				// SASI IS NOT SURE IF SHOULD BE REFLECTED IN THIS CASE
//				userPose = userPose.transformed(PVecUtilities.STANDARD_REFLECTED_BASIS, targetBasis, userPose.center());
				
				characteristicSize = userPose.characteristicSize();
				for (PVector basisVector : targetBasis) {
					basisVector.mult(characteristicSize);
				}
					vaiPose = vaiPose.transformed(PVecUtilities.STANDARD_BASIS, targetBasis, userPose.center());
			} else { //cloning
				
				PVector[] targetBasis = userPose.localBasis();
				//debug.addGUIText("Cloning", 1000);
				ViewpointsAestheticsModel.getAgentStillness().setStill();
				//reflect user pose here
				PVector newCenter = new PVector(userPose.center().x * -1, userPose.center().y, userPose.center().z);
//				TODO: VERIFY THAT NOTHING BROKE!!!!!
//				userPose = userPose.translated(newCenter);
				// ATTEMPT TO FIX USER SKELETON 
//				userPose = userPose.transformed(PVecUtilities.STANDARD_REFLECTED_BASIS, targetBasis, userPose.center());
				vaiPose = (Body) userPose.clone();
			}
			
//			// TRY TO DO ALL MOVING OF VAI WITHIN OFFSET
//			PVector newCenter = vaiPose.center();
//			if(controlsManager.isREFLECT_VAI_CENTER()) {
//				newCenter = new PVector(vaiPose.center().x * -1, vaiPose.center().y, vaiPose.center().z);
//				vaiPose = vaiPose.translated(newCenter);
////				addGUIText("REFLECT VAI CENTER: " + REFLECT_VAI_CENTER, 5000);
//			}
//			if(controlsManager.isREFLECT_VAI_MOVEMENTS()) {
//				vaiPose = vaiPose.transformed(PVecUtilities.STANDARD_REFLECTED_BASIS, vaiPose.localBasis(), newCenter);
////				addGUIText("REFLECT VAI MOVEMENTS: " + REFLECT_VAI_MOVEMENTS, 5000);
//			}
			
			//Update agent stillness.
			if(currentState == VAIState.INTERACTION){
				ViewpointsAestheticsModel.getAgentStillness().update(vaiPose);
				if(ViewpointsAestheticsModel.getAgentStillness().isStill()){
					agentStillnessTimeout = ViewpointsAestheticsModel.getAgentStillness().getTime();
					agentMovementTimeout = -1;
				}else{
					agentStillnessTimeout = -1;
					agentMovementTimeout = ViewpointsAestheticsModel.getAgentStillness().getTime();
				}
			}else{
				ViewpointsAestheticsModel.getAgentStillness().reset();
			}
			

			if(userPose != null)
			{
				userPose.setTimestamp(ClockManager.getMainClock().time());
			}
			vaiPose.setTimestamp(ClockManager.getMainClock().time());
			//update vaiPose to offset before updating this thing
//			vaiPose = GraphicsUtilities.offsetVai(controlsManager, userPose, vaiPose, this.g);
//			vaiEffectsController.update(userPose, vaiPose, null, null, null);
			if(currentMotionInput instanceof KinectMotionInput && currentMotionInput.isInit())
			{
				userEffectsController.update(null, null, kinectInput.getUserPixels(), null, null);
			}
//			if(currentMotionInput instanceof KinectMotionInput && currentMotionInput.isInit())
//			{
//				userStencilEffectsController.update(null, null, kinectInput.getUserPixels(), kinectInput.getDepthImage(), kinectInput.getRGBImage());
//			}
			
			/*
			 * @Sasi Do the following changes here or something!
			 * 
			 * 1. Look for Viewpoints Transforms
			 * 2. Map to physical transform methods you've created.
			 * 3. Transform the rendered stuff! Eg. Firefly parameters or other visual effects you create!
			 * */
		}
		pushMatrix();
//		fill(255);
//		textAlign(LEFT, TOP);
//		textSize(40);
		
		if(controlsManager.isDRAW_SHADOW() && (currentState == VAIState.INTERACTION || currentState == VAIState.GREETGOODBYE || currentState == VAIState.GREETWELCOME)) {
			userEmbodimentFrame = userEmbodiment.draw();
		}
		
//		if(controlsManager.isDrawStencil() && (currentState == VAIState.INTERACTION || currentState == VAIState.GREETGOODBYE || currentState == VAIState.GREETWELCOME)) {
//			userStencilEmbodimentFrame = userStencilEmbodiment.draw();
//		}

		
		//Sending Joint Data
		//for(JIDX jidx : JIDX.ALL_JIDX){
		
		/*
		JSONObject json = new JSONObject();
		//System.out.println(vaiOrientation.get(JIDX.LEFT_ELBOW).m00);
		
		if(vaiPose != null)
		{
			
			float scaleFactor = 1000;
			for(int i = 0 ; i < JIDX.ALL_JIDX.length; i++)
			{
				//Position
				jointPosition = new float[]{vaiPose.get(JIDX.ALL_JIDX[i]).x/scaleFactor, vaiPose.get(JIDX.ALL_JIDX[i]).y/scaleFactor, vaiPose.get(JIDX.ALL_JIDX[i]).z/scaleFactor};

				//Orientation
				vaiPose.getOrientation().get(JIDX.ALL_JIDX[i]).get().get(jointOrientation);
				
				//Copy position
				arrayCopy(jointPosition, 0, jointData[i], 0, 3);
				
				//Copy orientation
				arrayCopy(jointOrientation, 0, jointData[i], 3, 16);
				
				JSONObject pos = new JSONObject();
				pos.setDouble("X", jointPosition[0]);
				pos.setDouble("Y", jointPosition[1]);
				pos.setDouble("Z", jointPosition[2]);
				
				pos.setDouble("m00", jointOrientation[0]);
				pos.setDouble("m01", jointOrientation[1]);
				pos.setDouble("m02", jointOrientation[2]);
				pos.setDouble("m03", jointOrientation[3]);
				pos.setDouble("m10", jointOrientation[4]);
				pos.setDouble("m11", jointOrientation[5]);
				pos.setDouble("m12", jointOrientation[6]);
				pos.setDouble("m13", jointOrientation[7]);
				pos.setDouble("m20", jointOrientation[8]);
				pos.setDouble("m21", jointOrientation[9]);
				pos.setDouble("m22", jointOrientation[10]);
				pos.setDouble("m23", jointOrientation[11]);
				pos.setDouble("m30", jointOrientation[12]);
				pos.setDouble("m31", jointOrientation[13]);
				pos.setDouble("m32", jointOrientation[14]);
				pos.setDouble("m33", jointOrientation[15]);

				json.setJSONObject(JIDX.ALL_JIDX[i].toString(), pos);
			}
			
			//saveJSONObject(json, "data/new.json");
			//val = (val+1)%255;
			//System.out.println(val);
			myServer.write(json.toString());
			//osc.send(receiver, "JOINTS", (Object[])jointData);

		}
		else
		{
			System.out.println("vaiPose Null");
		}
		
*/
		
		if(injson != null) {
			myServer.write(injson.toString());
		}
		//Toggling drawing of the embodiment is done within the fireflies or other internal draw method.
		vaiEmbodimentFrame = vaiEmbodiment.draw();
		
		translate(width/2, height/2);

		//Draw GUI Text
		debugOverlay.drawDebugGUI();
		if(debugSlider != null)
			debugSlider.setVisible(controlsManager.isDEBUG());
		
		
		if(null != vaiPose && null != userPose) {
			vaiPose = GraphicsUtilities.offsetVai(controlsManager, userPose, vaiPose, this.g);
			// ALWAYS UPDATE EFFECTS CONTROLLER
			vaiEffectsController.update(userPose, vaiPose, null, null, null); 
		}
		
		if(controlsManager.isDRAW_USER()) {
//			addGUIText("DRAW USER: " + DRAW_USER, 5000);
			if (null != userPose) {
//				GraphicsUtilities.offsetVai(controlsManager, userPose, vaiPose, this.g);
				GraphicsUtilities.drawBody(userPose, this.g);
			}
		}
		
		if(controlsManager.isDRAW_VAI()) {
//			addGUIText("DRAW VAI: " + DRAW_VAI, 5000);
			if (null != vaiPose) {
//				vaiPose = GraphicsUtilities.offsetVai(controlsManager, userPose, vaiPose, this.g);
				GraphicsUtilities.drawBody(vaiPose, this.g);
			}
		}
		
		if (null != vaiPose && null != userPose) {
//			drawGlyph(userPose, vaiPose);
			glyphController.drawGlyph(userPose, vaiPose);
		}

		popMatrix();
		debug.displayGUI();
		
		if(controlsManager.isVideoStreaming())
		{
			if(!isWindows)
			{
				if(syphonServer == null)
				{
					syphonServer = new SyphonServer(this, "VAIFrontEnd#1");
					System.out.println("Syphon server loaded");
				}
				syphonServer.sendScreen();
			}
		}
	}
	
	
	/*public void restartTracker(){
		activityTracker = new ClassifierActivityTracker(GET_DATA);
	}*/
	

	private enum VAIState{
		CLOUD{
			@Override
			public VAIState nextState(VAIState input){
				stateTimer = new Clock();
				stateTimer.start();
				//always greetwelcome
				return input;
			}
		},
		GREETWELCOME{
			public VAIState nextState(VAIState input){
				//user left, to goodbye
				if(input == GREETGOODBYE){
					stateTimer = new Clock();
					stateTimer.start();
					return input;
				}
				//to interaction
				if(humanMovementTimeout > greetTimer && input != VAIState.CLOUD){
					return input;
				}else
					return this;
			}
		},
		INTERACTION{
			public VAIState nextState(VAIState input){
				stateTimer = new Clock();
				stateTimer.start();
				//always greetgoodbye
				return input;
			}
		},
		GREETGOODBYE{
			public VAIState nextState(VAIState input){
				if(input == GREETWELCOME){
					stateTimer = new Clock();
					stateTimer.start();
					return input;
				}
				stateTimer.check();
				if(stateTimer.time() > byeTimer){
					return input;
				}
				else
					return this;
			}
		};
		public abstract VAIState nextState(VAIState input);
		public static Clock stateTimer;
		public float greetTimer = 1f;
		public float byeTimer = 5f;
	}
	
	public static String getPlaybackName() {
		return playbackVideoName;
	}

	@Override
	public void exit() {
		segmentationManager.close();
		FileUtilities.flush(PROJECT_HOME);
		super.exit();
	}
	

	public static void main(String[] args) {
		PApplet.main("Viewpoints.FrontEnd.Application.ViewpointsFrontEnd", new String[]{"--full-screen", "--display=1"});
	}

	///// Getters and Setters /////

	public MotionInput getMotionInput() {
		return currentMotionInput;
	}

	public MotionInput getKinectInput() {
		return kinectInput;
	}

	public float getMinZ() {
		return minZ;
	}

	public float getMaxZ() {
		return maxZ;
	}

//	public MusicControl getAudioController() {
//		// TODO Auto-generated method stub
//		return audioController;
//	}

	public Slider getDebugSlider() {
		return debugSlider;
	}

	public static SegmentationManager getSegmentationManager() {
		return segmentationManager;
	}

	public void setSegmentationManager(SegmentationManager segmentationManager) {
		ViewpointsFrontEnd.segmentationManager = segmentationManager;
	}

	public static String getPlaybackFile() {
		return playbackFile;
	}

	public static String getREHEARSAL_GESTURE_LIB() {
		return REHEARSAL_GESTURE_LIB;
	}

	public static void setREHEARSAL_GESTURE_LIB(String rEHEARSAL_GESTURE_LIB) {
		REHEARSAL_GESTURE_LIB = rEHEARSAL_GESTURE_LIB;
	}

	public static void setKinectInput(KinectMotionInput kinectInput) {
		ViewpointsFrontEnd.kinectInput = kinectInput;
	}

	public ResponseLogic getResponseLogic() {
		return responseLogic;
	}

	public void setResponseLogic(ResponseLogic responseLogic) {
		ViewpointsFrontEnd.responseLogic = responseLogic;
	}

	public ResponseLogic getBackupSoarResponseLogic() {
		return backupSoarResponseLogic;
	}

	public void setBackupSoarResponseLogic(ResponseLogic backupSoarResponseLogic) {
		ViewpointsFrontEnd.backupSoarResponseLogic = backupSoarResponseLogic;
	}

	public void setMaxZ(float maxZ) {
		this.maxZ = maxZ;
	}

	public void setMinZ(float minZ) {
		this.minZ = minZ;
	}

	public void setMotionInput(MotionInput newMotionInput) {
		currentMotionInput = newMotionInput;
	}

	public static ArrayList<String> getGlyphFiles() {
		return glyphFiles;
	}

	public String getGLYPH_LIB() {
		return GLYPH_LIB;
	}

	public Clock getUserGlyphTimer() {
		return userGlyphTimer;
	}

	public FireflyEffects getVaiEffectsController()
	{
		return vaiEffectsController;
	}

	public ShadowEffects getUserEffectsController()
	{
		return userEffectsController;
	}

//	public StencilEffects getUserStencilEffectsController()
//	{
//		return userStencilEffectsController;
//	}

//	public static FileCueingMotionInput getFileCueingMotionInput()
//	{
//		return fileCueingMotionInput;
//	}
	public void stop(){
		myServer.stop();
	}
}
