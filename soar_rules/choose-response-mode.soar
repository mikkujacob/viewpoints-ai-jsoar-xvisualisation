#Rules for choosing the response mode for that response

sp { propose*choose*response*mode
	(state <s> ^name viewpoints-agent
			   ^phase choose-response-mode
			   ^response-mode null)
	-->
	(<s> ^operator <o> +)
	(<o> ^name choose-response-mode)
	(write (crlf) | REACHED propose*choose*response*mode |)
}

sp { propose*change*phase*choose-response-predicates-mode
	(state <s> ^name viewpoints-agent
			   ^phase choose-response-mode
			   ^response-mode <> null)
	-->
	(<s> ^operator <o> +)
	(<o> ^name change-phase-choose-response-predicates-mode)
	(write (crlf) | REACHED propose*change*phase*choose-response-predicates-mode |)
}

sp { propose*noop*mode
	(state <s> ^name choose-response-mode)
	-->
	(<s> ^operator <o> + =)
	(<o> ^name noop)
	(write (crlf) | REACHED propose*noop*mode |)
}

#DEV: COMMENT FOR TESTING EPMEM
sp { propose*emotion*response*mode
	(state <s> ^name choose-response-mode
		^superstate <ss>)
	(<ss> ^personality <persona>)	
	(<persona> ^input-emotion <> NONE)
	-->
	(<s> ^operator <o> + =)
	(<o> ^name emotion)
	(write (crlf) | REACHED PROPOSE EMOTION RESPONSE MODE|)
}

# Since NEW is used ONLY IN THIS DEMO'S CONTEXT to select a 
# PAST ACTION FROM HISTORY there needs to be MULTIPLE HISTORY
# It could also be used from scratch if the gesture library had previously added content
# AND/OR gesture persistance
sp { propose*new*mode
	(state <s> ^name choose-response-mode
			   ^history-mode multiple
			   ^epmem.present-id > 2)
	-->
	(<s> ^operator <o> + =)
	(<o> ^name new)
	(write (crlf) | REACHED PROPOSE NEW RESPONSE MODE|)
}

sp { propose*repeat*mode
	(state <s> ^name choose-response-mode
			   ^history-mode << one multiple >>)
	-->
	(<s> ^operator <o> + =)
	(<o> ^name repeat)
	(write (crlf) | REACHED PROPOSE REPEAT RESPONSE MODE|)
}

sp { propose*vary*mode
	(state <s> ^name choose-response-mode
			   ^history-mode << one multiple >>)
	-->
	(<s> ^operator <o> + =)
	(<o> ^name vary)
	(write (crlf) | REACHED PROPOSE VARY RESPONSE MODE|)
}

# Propose Pattern Mode
# NEW Version - Matches on current input gesture uuid with patterns.node.input.uuid
sp { propose*pattern*mode
	(state <s> ^name choose-response-mode
			   ^history-mode multiple
			   ^superstate <ss>)
	(<ss> ^history.node <node>
		  ^history-index <index>
		  ^patterns.node <pat-node>)
	(<node> ^index <index>
			^input.gesture.uuid <id>)
	(<pat-node> ^input.gesture.uuid <id>)
	-->
	(<s> ^operator <o> + =)
	(<o> ^name pattern)
	(write (crlf) | REACHED PROPOSE PATTERN RESPONSE MODE |)
}

 # sp { select*new*mode
 # 	(state <s> ^name choose-response-mode)
 # 	(<s> ^operator <o> +)
 # 	(<o> ^name new)
 # 	-->
 # 	(<s> ^operator <o> >)
 # }

# sp { select*not*repeat*mode
# 	(state <s> ^name choose-response-mode)
# 	(<s> ^operator <o> +)
# 	(<o> ^name repeat)
# 	-->
# 	(<s> ^operator <o> <)
# }

# sp { select*vary*mode
# 	(state <s> ^name choose-response-mode
# 			   ^history-mode one)
# 	(<s> ^operator <o> +)
# 	(<o> ^name vary)
# 	-->
# 	(<s> ^operator <o> >)
# 	(write (crlf) | REACHED SELECT VARY MODE IN ONE HISTORY | )
# }

# sp { select*repeat*mode
# 	(state <s> ^name choose-response-mode
# 			   ^history-mode one)
# 	(<s> ^operator <o> +)
# 	(<o> ^name repeat)
# 	-->
# 	(<s> ^operator <o> >)
# 	(write (crlf) | REACHED SELECT REPEAT MODE IN ONE HISTORY | )
# }

# sp { select*new*mode
# 	(state <s> ^name choose-response-mode
# 			   ^history-mode multiple)
# 	(<s> ^operator <o> +)
# 	(<o> ^name new)
# 	-->
# 	(<s> ^operator <o> >)
# 	(write (crlf) | REACHED SELECT NEW MODE IN MULTIPLE HISTORY | )
# }

# sp { select*new*mode*over*vary*mode
# 	(state <s> ^name choose-response-mode)
# 	(<s> ^operator <o1> +
# 		 ^operator <o2> +)
# 	(<o1> ^name vary)
# 	(<o2> ^name new)
# 	-->
# 	(<s> ^operator <o2> > <o1>)
# }

# sp { select*pattern*mode
# 	(state <s> ^name choose-response-mode)
# 	(<s> ^operator <o> +)
# 	(<o> ^name pattern)
# 	-->
# 	(<s> ^operator <o> >)
# 	(write (crlf) | REACHED SELECT PATTERN MODE | )
# }

sp { select*pattern*mode*over*repeat
	(state <s> ^name choose-response-mode)
	(<s> ^operator <o1> +
		 ^operator <o2> +)
	(<o1> ^name pattern)
	(<o2> ^name repeat)
	-->
	(<s> ^operator <o1> > <o2>)
	(write (crlf) | REACHED SELECT RESPONSE MODE PATTERN OVER REPEAT |)
}

sp { select*pattern*mode*over*vary
	(state <s> ^name choose-response-mode)
	(<s> ^operator <o1> +
		 ^operator <o2> +)
	(<o1> ^name pattern)
	(<o2> ^name vary)
	-->
	(<s> ^operator <o1> > <o2>)
	(write (crlf) | REACHED SELECT RESPONSE MODE PATTERN OVER VARY |)
}

# sp { select*pattern*mode*over*new
# 	(state <s> ^name choose-response-mode)
# 	(<s> ^operator <o1> +
# 		 ^operator <o2> +)
# 	(<o1> ^name pattern)
# 	(<o2> ^name new)
# 	-->
# 	(<s> ^operator <o1> > <o2>)
# 	(write (crlf) | REACHED SELECT RESPONSE MODE PATTERN OVER NEW |)
# }

sp { select*emotion*response*over*repeat
	(state <s> ^name choose-response-mode)
	(<s> ^operator <o1> +
		 ^operator <o2> +)
	(<o1> ^name emotion)
	(<o2> ^name repeat)
	-->
	(<s> ^operator <o1> > <o2>)
	(write (crlf) | REACHED SELECT RESPONSE MODE EMOTION OVER REPEAT |)
}

sp { select*emotion*response*over*vary*mode
	(state <s> ^name choose-response-mode)
	(<s> ^operator <o1> +
		 ^operator <o2> +)
	(<o1> ^name emotion)
	(<o2> ^name vary)
	-->
	(<s> ^operator <o1> > <o2>)
	(write (crlf) | REACHED SELECT RESPONSE MODE EMOTION OVER VARY |)
}

# sp { select*emotion*response*over*new*mode
# 	(state <s> ^name choose-response-mode)
# 	(<s> ^operator <o1> +
# 		 ^operator <o2> +)
# 	(<o1> ^name emotion)
# 	(<o2> ^name new)
# 	-->
# 	(<s> ^operator <o1> > <o2>)
# 	(write (crlf) | REACHED SELECT RESPONSE MODE EMOTION OVER NEW |)
# }

# sp { select*emotion*response*over*pattern*mode
# 	(state <s> ^name choose-response-mode)
# 	(<s> ^operator <o1> +
# 		 ^operator <o2> +)
# 	(<o1> ^name emotion)
# 	(<o2> ^name pattern)
# 	-->
# 	(<s> ^operator <o1> > <o2>)
# 	(write (crlf) | REACHED SELECT RESPONSE MODE EMOTION OVER PATTERN |)
# }


sp { reject*noop*mode
	(state <s> ^name choose-response-mode)
	(<s> ^operator <o> +)
	(<o> ^name noop)
	-->
	(<s> ^operator <o> -)
	(write (crlf) | REACHED reject*noop*mode |)
}

# sp { select*not*new*mode
# 	(state <s> ^name choose-response-mode)
# 	(<s> ^operator <o> +)
# 	(<o> ^name new)
# 	-->
# 	(<s> ^operator <o> -)
# }

sp { apply*noop*mode
	(state <s> ^name choose-response-mode
			   ^operator.name noop
			   ^io.output-link <olink>
			   ^superstate <ss>)
	(<ss> ^response-mode <rmode>)
	-->
	(<ss> ^response-mode <rmode> -)
	(<ss> ^response-mode noop)
	(<olink> ^outputs.response-mode NOOP)
	(write (crlf) | REACHED APPLY NOOP RESPONSE MODE | )
}

sp { apply*new*mode
	(state <s> ^name choose-response-mode
			   ^operator.name new
			   ^io.output-link <olink>
			   ^superstate <ss>)
	(<ss> ^response-mode <rmode>)
	-->
	(<ss> ^response-mode <rmode> -)
	(<ss> ^response-mode new)
	(<olink> ^outputs.response-mode NEW)
	(write (crlf) | REACHED APPLY NEW RESPONSE MODE | )
}

sp { apply*repeat*mode
	(state <s> ^name choose-response-mode
			   ^operator.name repeat
			   ^io.output-link <olink>
			   ^superstate <ss>)
	(<ss> ^response-mode <rmode>)
	-->
	(<ss> ^response-mode <rmode> -)
	(<ss> ^response-mode repeat)
	(<olink> ^outputs.response-mode REPEAT)
	(write (crlf) | REACHED APPLY REPEAT RESPONSE MODE | )
}

sp { apply*vary*mode
	(state <s> ^name choose-response-mode
			   ^operator.name vary
			   ^io.output-link <olink>
			   ^superstate <ss>)
	(<ss> ^response-mode <rmode>)
	-->
	(<ss> ^response-mode <rmode> -)
	(<ss> ^response-mode vary)
	(<olink> ^outputs.response-mode VARY)
	(write (crlf) | REACHED APPLY VARY RESPONSE MODE | )
}

sp { apply*pattern*mode
	(state <s> ^name choose-response-mode
			   ^operator.name pattern
			   ^io.output-link <olink>
			   ^superstate <ss>)
	(<ss> ^response-mode <rmode>)
	-->
	(<ss> ^response-mode <rmode> -)
	(<ss> ^response-mode pattern)
	(<olink> ^outputs.response-mode PATTERN)
	(write (crlf) | REACHED APPLY PATTERN RESPONSE MODE | )
}

sp { apply*emotion*response*mode
	(state <s> ^name choose-response-mode
			   ^operator.name emotion
			   ^io.output-link <olink>
			   ^superstate <ss>)
	(<ss> ^response-mode <rmode>)
	-->
	(<ss> ^response-mode <rmode> -)
	(<ss> ^response-mode emotion)
	(<olink> ^outputs.response-mode EMOTION)
	(write (crlf) | REACHED APPLY EMOTION RESPONSE MODE | )
}

sp { apply*change*phase*choose-response-predicates-mode
	(state <s> ^name viewpoints-agent
			   ^operator.name change-phase-choose-response-predicates-mode
			   ^phase <phase>
			   ^working-copy <wc>
			   ^history-index <hindex>
			   ^history.node <node>)
	(<node> ^index <hindex>
			^input.gesture <gesture>)
	-->
	(<s> ^phase <phase> -)
	(<s> ^phase choose-response-predicates-mode)
	(<s> ^working-copy <wc> -)
	# (<s> ^working-copy (deep-copy <gesture>)) # DEBUGGING TOO MANY UNIQUE GESTURES CREATED IN EPMEM
	(<s> ^working-copy <gesture>)
	(write (crlf) | REACHED apply*change*phase*choose-response-predicates-mode |)
}
